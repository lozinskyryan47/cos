brgc = ['00000','10000','11000','01000','01100','11100','10100','00100','00110','10110','11110','01110','01010','11010','10010','00010','00011','10011','11011','01011','01111','11111','10111','00111','00101','10101','11101','01101','01001','11001','10001','00001']
ints=[[0,5],[1,4],[2,3],[2,2]]
box = 12

# write svg of given bitstring x to file
def svg(file, x, pos):
   for i in range(len(x)):
      f.write("<rect class='r' x='" + str(pos[0]+box*i) + "' y='" + str(pos[1]) + "' width='" + str(box) + "' height='" + str(box) + "' style='fill:" + str("white" if x[i]=='0' else "darkgray") + "'/>")

def weight(x):
   return x.count('1')

if __name__ == "__main__":
   # reverse strings
   brgc = [x[::-1] for x in brgc]
   n = len(brgc[0])
   f = open("brgc.svg", "w")
   f.write("<?xml version='1.0' encoding='UTF-8'?>")
   f.write("<svg width='" + str((n+1)*box*len(ints)+5) + "' height='" + str(len(brgc)*box+25) + "' xmlns='http://www.w3.org/2000/svg'>")
   f.write("<style>.r{stroke:black;stroke-width:1}</style>")
   for i in range(len(ints)):
      f.write("<text x='" + str((n+1)*box*i+5) + "' y='15'>[" + str(ints[i][0]) + "," + str(ints[i][1]) + "]</text>")
      [k,l] = ints[i]
      for j in range(len(brgc)):
         if (weight(brgc[j]) >= k) and (weight(brgc[j]) <= l):
           pos = [((n+1)*box*i+5),box*j+25]
           svg(f, brgc[j], pos)
   f.write("</svg>")
   f.close()
