------------------
Directory overview
------------------
index.php         Welcome page
cos.php           Main PHP script for generating the object information pages.
                  It includes this information from the various object files in web/*.
old.php           PHP for old COS object information pages. It includes information from web/old/*
gen.php           PHP script wrapping the individual run.php scripts for each object by including them.
head.html         HTML header file with Google search terms, Google analytics script, Mathjax scripts.
title.html        Title bar with COS++ logo.
script.js         JavaScript for setting up the menu on the left side of the screen.
                  This file has to be modified when new menu items are added.
style.css         Style file for configuring font sizes, sizes/layout of various parts of the website.
.htaccess         Apache webserver symbolic redirects for OEIS sequence numbers.
web/*             HTML and PHP pages for generating the various kinds of combinatorial objects, grouped by object.
                  Each subfolder contains the description of the algorithms and objects, as well as references to the literature.
                  The page run.html calls the PHP script gen.php, which includes run.php, and the arguments are passed to this script via ? parameters.
web/graphics.php  Visualization layer for different objects.
web/output.php    Parse generation program output to hand it over to the visualization layer.
web/limits.php    Maximum bounds for number of objects to be generated (depending on output type).
refs/*            Latex sources for generating the references mentioned on the refs.html page.
                  Use initials+year style references.
code/*            C/C++-Code and Makefiles for generating all combinatorial objects.
code/fxt          Joerg Arndt's FXT library, with some small modifications/corrections for usage within COS.
                  The full unmodified library can be downloaded from Arndt's website.
code/nauty        Brendan McKay's graph generation and graph isomorphism toolkit nauty.
code/plantri      Brendan McKay's and Gunnar Brinkmann's planar graph and fullerene generation toolkits plantri and fullgen.

----------------------
FXT build instructions
----------------------
code/fxt/make clean
code/fxt/make install PREFIX=.
code/*/make clean
code/*/make

------------------------
Nauty build instructions
------------------------
code/nauty/.configure
code/nauty/make -f Makefile
Running only make will build the original nauty, and the above command will generate the COS version.

-----------------------
Adding a new object OBJ
-----------------------
1. Add OBJ to the list of keywords in head.html.
2. Add web/OBJ/{header.html,run.html,run.php,info.html,oeis.html,refs.html}.
3. Update script.js to add new menu item.
4. Add generation programs and makefiles to a folder code/OBJ/*.
5. For any OEIS sequence referenced on the page, include symbolic links in .htaccess file.
6. Update external links to our website on OEIS/Wikipedia as described below.
7. Add the symbolic link for OBJ in .htaccess  (eg. bits, brgc, perm)

---------------
Testing locally
---------------
From cos root directory:

       php -S localhost:8000

Then visit http://localhost:8000 or https://localhost:8000 in your browser.
When generating objects locally, the resolution of symbolic weblinks does not work, so object
sites have to be accessed directly via the browser's address field: http://localhost:8000/cos.php?obj=perm

--------------------------------
Pushing changes to the webserver
--------------------------------
1. Pull the COS repository to your local machine: 
       git clone https://gitlab.com/tmuetze81/cos.git 
   using your Gitlab username and password.

2. Create a new feature branch and switch to it (replace "new_feature" by a meaningful branch name):
       cd cos
       git branch new_feature
       git checkout new_feature

3. Do the desired modifications to the feature branch, commit them to the repository, and push the changes:
       git status      (to see what files have been changed) 
       git add <file.html>     (or "git add -A" to add all changes)
       git commit -m "added new feature"
       git push -u origin new_feature
   The generation programs must be compiled under Linux, as they will run on a Linux server.

4. Remotely login to Netcup webserver (Netcup is the company who provide the webspace):
       ssh hosting120729@a2faf.netcup.net
       Password: 

5. Go to the test directory on the webserver:  ***DO NOT USE THE LIVE DIRECTORY cos, BUT ONLY THE TEST DIRECTORY cos_test***
       cd cos_test

6. Pull the new branch of the repository into this test directory:
       git pull origin new_feature
   When updating a branch on cos_test, you may need to commit before re-pulling by: git commit -m "new_feature"

7. Try and test the new features online using the address http://test.combos.org in your browser.

8. Ask the other COS administrators (Joe, Aaron, Torsten) for their feedback and comments. Repeat steps 3-7 if necessary.

9. Once the changes are approved, merge the feature branch into the master branch **LOCALLY**, and delete the no longer needed feature branch:
       git checkout master
       git merge new_feature
       git branch -d new_feature
       git push
   You may need a "git pull" before the "git push" if there were outside changes to master since the branch.

10. Then pull the master branch of the repository into the live version of COS by changing to the live COS directory on the Netcup webserver, and pulling the repository there:
       cd cos
       git pull origin master
    Now the new version should be available via http://combos.org

---------------------------------
Undoing a change on the webserver
---------------------------------

1. Navigate to cos_test or cos directory where the change should be undone.
1. Run
       git log
   and note revision number of latest stable version.
2. Run
       git checkout <revision-number-of-latest-stable-version>       

--------------------------------
Links from other websites to COS
--------------------------------
IMPORTANT: Always place symbolic links (see .htaccess), but never internal links such as cos.php?obj=perm.

OEIS format
CombOS - Combinatorial Object Server, <a href="http://combos.org/perm">Generate permutations</a>

Wikipedia format (image file description)
Created using the [http://combos.org/perm permutation generation algorithms on the Combinatorial Object Server].

Wikipedia format (reference section)
<ref>{{cite web|url=http://combos.org/middle|title=Generate middle levels Gray codes|last1=M&uuml;tze|first1=Torsten|last2=Sawada|first2=Joe|last3=Williams|first3=Aaron|website=Combinatorial Object Server|access-date=May 23, 2019}}</ref>

Wikipedia format (external links section)
* [http://combos.org/perm Steinhaus-Johnson-Trotter algorithm] on the [http://combos.org Combinatorial Object Server]
* [http://combos.org/perm Permutation generation algorithms] on the [http://combos.org Combinatorial Object Server]