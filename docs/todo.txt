100% Let's do these
===================
- Torsten: integration of material from old COS to new COS

- Torsten: OEIS links to COS; progress is reported in oeis_cos_links.ods. Majority of links is fixed, some are still to be dealt with (red-black trees, rooted trees, B-trees, primitive and irreducible polynomials).

- redirect Frank's original COS website to combos.org using .htaccess file within ~cos and ~inf directory with content

Suggestions for discussion
==========================
- Joe: issue with downloading zip files on Mac; does not work for created objects, but for zipped code it does work.

- Joe: add "About" page, "History" page with foreword from Frank, list of contributors, list of open problems.

- Joe: enlarge "Generate" button.

- Joe: pad output numbering so that objects appear aligned.

- Aaron: Non-empty output console window in the beginning. Start automatic generation with default values? Torsten: text "Press generate" or something similar? Dennis Wong is also in favour of this. Frank prefers "generate" button.

- Torsten: Need long-term solution for consistent handling of references, when they appear on multiple pages. Python script automation? Joe suggested refs.php file with long list of strings that are included from the individual html files.

- Torsten: Need automatic generation/formatting of OEIS references/extraction for .htaccess redirection file.

- Torsten: Which output formats do we want: graphics/text/file? Joe does not like the file download, or at least wants plain text file download. Aaron and Torsten do not like the idea of copying large amounts of data from the browser. Torsten: Each of the three formats has its applications/merits:
* graphics: visually appealing representation of few objects (<=1000); long time to compute; lots of HTML/SVG formatting tags that blows up the transmitted file; slow to display in the browser if too many objects; SVG graphics can be copy-pasted by the user from the source code
* text: plain text representation (no formatting tags) that can still be looked at by a human (<=10.000 objects); fast to compute and display; still reasonable amount of data
* file: huge amounts of data (maybe 100.000 or 1.000.000 objects); compress to save traffic and speed; Joe: Is there any benefit to zipping the output? simple = just the text file.

- Torsten: We should clearly separate the generation and visualization stage. Generation parameters are provided in the highlighted box on the left, visualization parameters are provided above the output section on the right. In particular, toggling buttons like numbering/graphics should never rerun the generation program (efficiency/traffic), and numbering/graphics is only available for graphical output, not for textual and file output. These toggles are only visible when the last "Run" executed was with "graphics" output format.

- Joe: have a link (near the actual ouput) to download the output.  Keep the program input area as clean as possible. Torsten+Aaaron: Find better place for such a link.

- Joe: default output - prefer text with no numbers  (Torsten prefers graphics) 

- Joe: delivery of source code.  Joe prefer only source only when available (not zipped).  Torsten prefers uniform delivery with makefile.

- Joe: type of source code to provide.  Torsten prefers code custom made for cos with makefile.  Joe prefers code that allows for prompting, standalone with no makefile if not required.  This is especially important for the necklaces. Joe wants to maintain two separate versions of the code, one specifically for COS, the other one for the command line/download. Torsten: This will be a lot of work to maintain.

- Aaron:  co and reverse orders? Nice features, but will complicate the interface. Also, if we have more options, should all graphics be pre-generated in PHP, and then only one the many graphics shown at the time (blow-up of code)? What about the order of output/error messages that do not encode objects if the order is reversed?

- Aaron: cache all possible outputs
