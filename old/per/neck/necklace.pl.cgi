#!/usr/bin/perl

require "../common.pl";

$NecklaceBIN = "../../bin/neck/Necklace";
$ChordBIN = "../../bin/neck/fast";
$PolyBIN = "../../bin/neck/Poly";

MAIN: 
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

sub PrintTableHeaders {
   print "<P>\n";
   &TableSetup;  # from common.pl
   &ColumnLabel( "Output" );
}
 
sub ProcessForm {
      # make numericals entries from form numeric.
      &MakeNumeric( $input{'n'} ); 
      &MakeNumeric( $input{'k'} );
      &MakeNumeric( $input{'d'} );

      # chord diagrams
      if ($input{'program'} eq "Chord") {
	&OutputHeader( 'Chord Diagrams', 'neck/chords' );   
	if ($input{'n'} > 6) {
	  print "Maximum number of chords allowed is 6.<br />";
	  exit(1);
	}
	if ($input{'m'} || $input{'d'} || $input{'k'}) {
	  print "k, d, m and z do not apply to this object.<br /><br />";
        }    
	print "Chord Diagrams with " . $input{'n'} . " chords.<br /><br />";
	$parameters = "$input{'n'}";
        if ($input{'output1'}) {$outformat = 1;}
        if ($input{'output2'}) {$outformat = $outformat | 2;}
	$parameters = $parameters. " " . $outformat;
	$retval = system "$ChordBIN $parameters";
         &MoreInfo('neck/chords');


      # all other objects
      } else {


      if ($input{'m'} && $input{'d'}) {
        &OutputHeader( 'Necklace Output', 'neck/NecklaceInfo' );   
  	&ErrorMessage("You cannont specify both m and d.");
   	&MoreInfo('neck/NecklaceInfo');
 	exit(1);
      }

      # output1 = string representation, output 2 = coloured beads
      if ($input{'output1'}) {$outformat = 1;}
      if ($input{'output2'}) {$outformat = $outformat | 2;}

      if ($outformat == 0) {
        &OutputHeader( 'Necklace Output', 'neck/NecklaceInfo' );   
  	&ErrorMessage("You must specifiy an output");
   	&MoreInfo('neck/NecklaceInfo');
 	exit(1);
      }	
      # coloured bead output not available for all objects
      if (($input{'output2'}) && ($input{'program'} eq "LieBasis")) {
        &OutputHeader( 'Lie Basis Output', 'neck/NecklaceInfo' );   
  	&ErrorMessage("Colored bead output not available");
   	&MoreInfo('neck/NecklaceInfo');
 	exit(1);
      }	

      # format the header of web page
      if ($input{'program'} eq "LieBasis") {
      	&OutputHeader( 'Lie Basis Output', 'neck/NecklaceInfo' );   
      }
      else {
      	&OutputHeader( 'Necklace Output', 'neck/NecklaceInfo' );   
      }

      # restrictions on number of beads
      if (($input{'output2'}) && ($input{'k'}>7) ){ 
	  &ErrorMessage("For colored bead output k must be less than 8.");
  	  &MoreInfo('neck/NecklaceInfo');
	  exit(1);
      }

      # restrictions on n
      if (($input{'n'}) > 20) {
	  &TooBig(n, 20);
  	  &MoreInfo('neck/NecklaceInfo');
	  exit(1);
      }
      if (($input{'n'}) < 1) {
	  &ErrorMessage("You must enter a positive value for n");
  	  &MoreInfo('neck/NecklaceInfo');
	  exit(1);
      }
      
      # restrictions on k
      if (($input{'k'}) < 1) {
  	&ErrorMessage("You must enter a positive value for k");
   	&MoreInfo('neck/NecklaceInfo');
 	exit(1);
      }
      if ((($input{'k'}) > 2) && ($input{'program'} eq "unlabelled")) {
  	&ErrorMessage("k must be less than 3 for unlabelled necklaces");
   	&MoreInfo('neck/NecklaceInfo');
 	exit(1);
      }

  
      $DB = 0;
      $LIMIT = 1000;	# limit on number of objects to display

      $xx = "-n $input{'n'} -k $input{'k'} -l $LIMIT";  # parameters to pass to the c program
      
      # add additional parameters to $xx
      if (($input{'m'}) && ($input{'program'} ne "deBruijn"))  {
          $xx =  $xx. " -m $input{'m'}";
      }
      if ($input{'d'} ne "")  {
		if (($input{'program'} eq "bracelet") || 
			($input{'program'} eq "unlabelled") ||
			($input{'program'} eq "unlabelledL") ||
			($input{'program'} eq "forbid") ||
			($input{'program'} eq "forbidL") ||
			($input{'program'} eq "deBruijn")) {
  			&ErrorMessage("Fixed density not available for this option");
   			&MoreInfo('neck/NecklaceInfo');
 			exit(1);
		
		}
		else { 
          		$xx =  $xx. " -d $input{'d'}";
		}
      }

      if ($input{'program'} eq "Necklace") {
	  print "Necklaces";  $xx = $xx.' -N';
      }
      if ($input{'program'} eq "Lyndon") {
	  print "All Lyndon words";  $xx = $xx.' -L';
      }
      if ($input{'program'} eq "Prenecklace") {
	  print "Pre-necklaces";  $xx = $xx.' -P';
      }
      if ($input{'program'} eq "bracelet") {
	  print "Bracelets";  $xx = $xx.' -B';
      }
      if ($input{'program'} eq "unlabelled") {
	  print "Unlabelled necklaces";  $xx = $xx.' -U';
      }
      if ($input{'program'} eq "unlabelledL") {
	  print "Unlabelled Lyndon words";  $xx = $xx.' -UL';
      }
      if ($input{'program'} eq "LieBasis") {
	  print "Lie basis";  $xx = $xx.' -b';
      }
      if ($input{'program'} eq "forbid" || $input{'program'} eq "forbidL") {
	  $z = $input{'z'};
          if ($z < 1) {
  		&ErrorMessage("You must enter a positive value for z for forbidden necklaces.");
   		&MoreInfo('neck/NecklaceInfo');
 		exit(1);
	  }
     	  if ($input{'program'} eq "forbid") {
	  	print "Necklaces with forbidden ";  
	  	for ($i=1; $i<=$z; $i++){ print"0"; }
	  	print":";
	  	$xx = $xx.' -F -z '.$z;
	  }
	  else { 
	  	print "Lyndon words with forbidden ";  
	  	for ($i=1; $i<=$z; $i++){ print"0"; }
	  	print":";
	  	$xx = $xx.' -FL -z '.$z;
	  }
      }
      if ($input{'program'} eq "deBruijn") {
          if ($input{'m'}) { &ExtraParam('m', ($input{'m'})); }
	  print "The lexicographically smallest de Bruijn sequence";  
	  $xx = $xx.' -D';
	  $DB = 1;
      }
      print " for <I>n</I> = $input{'n'}";
      if ($input{'k'}) { print " and <I>k</I> = $input{'k'}"; }
      if (($input{'m'}) && ($input{'program'} ne "deBruijn")) { 
		print " and <I>m</I> = $input{'m'}.";
      }
      if (($input{'d'} ne "") && ($input{'program'} ne "deBruijn")) { 
		print " and <I>density</I> = $input{'d'}.";
      }
      else { print ".<P>"; }
      $retval = system "$NecklaceBIN $xx | /usr/bin/perl format.pl $outformat $LIMIT $DB";
      &LimitError if ($retval); 
      &MoreInfo('neck/NecklaceInfo');
} # else

      print "</BODY></HTML>\n";
}

