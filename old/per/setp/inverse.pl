#!/usr/bin/perl
#
# This program performs a transormation on the output of the genle
# program using the transformation rules in the transform.log file.
#

require "../common.pl";
# read the input parameters - first 5 are block size and following 4 are ouput
if ($#ARGV < 8) {exit(0);}
else { 
	for($i=1; $i<=5; $i++) {
		$block[$i] = $ARGV[$i-1];
	}
	$rg = $ARGV[5];
	$standard = $ARGV[6];
	$gray = $ARGV[7];
	$rook = $ARGV[8];
}
$#ARGV = -1;

open (RULES, "./transform.log");
open (STDIN);
$n = 1;
while (<RULES>) {
    @pair = split;
    $node[$n++] = $pair[0];  	#changed dec 6/96 
  # $node[$pair[1]] = $pair[0];
}
$n--;		# number of elements

print "<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
if ($rook == 1) {
	print '<TH COLSPAN=1><FONT SIZE="+1">Chessboard</FONT><BR></TH>';
	print "\n";
}
if ($rg == 1) {
	print '<TH COLSPAN=1><FONT SIZE="+1">RG function</FONT><BR></TH>';
	print "\n";
}
if ($standard == 1) {
	print '<TH COLSPAN=1><FONT SIZE="+1">Subsets</FONT><BR></TH>';
	print "\n";
}
if ($gray == 1) {
	print '<TH COLSPAN=1><FONT SIZE="+1">Gray Code</FONT><BR></TH>';
	print "\n";
}

$count = 0;
$returnval = 0;
$flag = 0;		#flags first iteration
while (<>) {
         
    @permutation = split;
    #compute inverse
    for ($i=0; $i<$n; $i++) {
	$inverse[$node[$permutation[$i]]] = $i+1;
    }
    # Do not perform on first time through
    if (($gray == 1) && ($flag == 1)) {
	$i = 0;
	for($i = 1; $i <= $n; $i++) {
		$diff[$i] = 0;
	}
        #compute inverse of previous
        for ($i=0; $i<$n; $i++) {
        	$pre_inverse[$node[$previous[$i]]] = $i+1;
        }
	#compute changes in inverse permutation
	for ($i=1; $i<=$n; $i++){ 
		if ($inverse[$i] != $pre_inverse[$i]) {
			$diff[$i] = 1; 
		}
	}
	#calculate and print trasposition
	print"<TD>";
	$color=0;
	$v1=0;
	$v2=0;
	for($i=1; $i<=$n; $i++) {
		if (($diff[$i] == 1) &&($v1 != $i) && ($v2 !=$i)) {
			#find where pre[i] is in inverse
			$pos = 0;
			for ($j=1; $j<=$n; $j++) {
				if ($inverse[$j] == $pre_inverse[$i]) {
					$pos = $j;
				}
			}
			if ($pos != 0) {	
				if ($inverse[$i] == $pre_inverse[$pos]) {
					print "($inverse[$i] $inverse[$pos])";
					if ($color == 1) {
						$diff[$i] = 2; $diff[$pos] =2;
						$v2 = $pos;
					}
					else { $v1 = $pos; }
				}	
				else {
					# find 3rd element in transposition
					$pos2=0;
					for ($j=1; $j<=$n; $j++) {
						if ($inverse[$j] == $pre_inverse[$pos]) {
							$pos2 = $j;
						}
					}
					$v1 = $pos;
					$v2 = $pos2;
					if ($pos2 != 0) {
						print "($inverse[$i] $inverse[$pos] $inverse[$pos2])";
					}
				}
			}
			$color = 1;
		}
	}
    }
    @previous = @permutation;
    print "<TR>";
    if ($rook == 1) {
	#initialize multi-dimensional array
	for ($i=1; $i<=$n; $i++) {
		for ($j=1; $j<=$n; $j++) {
			$table[$i][$j] = 0;
		}
	}
	#find position of rook
	$pos = 1;
      	for ($i=1; $i<=5; $i++) {
	   	for ($k=1; $k<=$block[$i]; $k++) {
			for ($j=1; $j<$i; $j++) {
				$x=$inverse[$pos++];
				$y=$inverse[$pos];
				if ($x<$y) {
					$table[$x][$y] = 1;
				}
				else {
					$table[$y][$x] = 1;
				}
			}
			$pos++;
		}
	}
	#print chessboard
	print "<TD ALIGN=CENTER>";
	print "<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n";
	for($i=1; $i<=$n;$i++) {
		for ($j=1; $j<=$i-1; $j++) {
			if ($table[$j][$i] == 1) {
		   		print "<TD><IMG SRC=\"ico/greenRook.gif\"></TD>\n";
			}
			else {
		   		print "<TD><IMG SRC=\"ico/green.gif\"></TD>\n";
			}
		}
		print "<TD><IMG SRC=\"ico/red.gif\"></TD></TR>\n";
	}
	print "</TABLE></TD>\n";

    }	

    if ($rg == 1) {
	for($i=1; $i<=$n; $i++) {
		$string[$i] = -1;
	}
	$next = 1; 
	$num_blocks = $block[1] + $block[2] + $block[3] + $block[4] + $block[5];
	for($iter=0; $iter<$num_blocks; $iter++) {
		#get position
		for($i=1; $i<=$n; $i++) {
			if ($inverse[$i] == $next) { $pos = $i;}
		}

		#determine block size that holds $next
		$temp_pos = $pos;
	  	$block_size = 1;
      	for ($i=1; $i<=5; $i++) {
			for ($k=1; $k<=$block[$i]; $k++) {
				$temp_pos = $temp_pos - $i;
			 }
			if ($temp_pos > 0) { $block_size++;}
		}

		#assign values to growth string
		$string[$next] = $iter;
		for($i=1; $i<$block_size; $i++) {
			$string[$inverse[$pos+$i]] = $iter;
		}

		#get next
		$i=2;
		$next=0;
	  	while ($i <= $n ) {
			if ($string[$i] == -1) {
				$next = $i;
				$i = $n;
			}
			$i++;
		}	
	}
	print "<TD>";
# print string     
	for($i=1; $i<$n; $i++) {
		if ( ($flag == 1) && ($string[$i] != $prev_string[$i]) ) {
			print "<FONT COLOR=#00FF00>";
		}
		print $string[$i];
		if ( ($flag == 1) && ($string[$i] != $prev_string[$i]) ) {
			print "</FONT>";
		}
		if ($num_blocks > 10) { print ",";}
	}
	if ( ($flag == 1) && ($string[$i] != $prev_string[$i]) ) {
		print "<FONT COLOR=#00FF00>";
	}
	print $string[$n];
	if ( ($flag == 1) && ($string[$i] != $prev_string[$i]) ) {
		print "</FONT>";
	}
	print "\n";
	@prev_string = @string;
    }
    if ($standard == 1) {
      $counter=1;
      print "<TD>";
      for ($i=1; $i<=5; $i++) {
	   for ($k=1; $k<=$block[$i]; $k++) {
		if ($counter != 1) {print",";}
		print "{";
		for ($j=1; $j<$i;$j++) {
	       	if ($diff[$counter] == 1) { 
					print "<FONT COLOR=#00FF00>";
			}
	       	if ($diff[$counter] == 2) { 
					print "<FONT COLOR=#0000FF>";
			}
			print "$inverse[$counter]";
	       	if ($diff[$counter++] >= 1) { 
				print "</FONT>";
			}
			print ",";
		}
	    if ($diff[$counter] == 1) { 
					print "<FONT COLOR=#00FF00>";
		}
	    if ($diff[$counter] == 2) { 
				print "<FONT COLOR=#0000FF>";
		}
		print "$inverse[$counter]";
	   	if ($diff[$counter++] >= 1) { 
			print "</FONT>";
		}
		print "}";
	   }
      }
      print "</TD>\n";
    }
   if ((++$count) ==  $LIMIT) {
       	     $returnval = $LIMITerror;  # WARNING must also change genle
	     print "</TABLE>";
       	     &LimitError;
      	     last;
    }
    $flag = 1;
}

print "</TABLE>";
print "<P>Total number of Partitions = $count";
close (RULES);
close (STDIN);
# print "$count  $returnval";
$returnval; # return value  

