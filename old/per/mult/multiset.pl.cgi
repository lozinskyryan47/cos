#!/usr/bin/perl

require "../common.pl";

$BinPath = "../../bin/mult";
$TempFile = "../../per/mult/mult.temp";

MAIN:
{
	# Read in all the variables set by the form
	if (&ReadParse(*input)) {
		&ProcessForm;
  	}
}

sub Initialize {

	$last = 0;
	$sum = 0;
	#if entry blank then set to 0.
	if ($input{'n1'}) {$n1 =  $input{'n1'}; $last=1;} else {$n1 = 0;} 
	if ($input{'n2'}) {$n2 =  $input{'n2'}; $last=2;} else {$n2 = 0;} 
	if ($input{'n3'}) {$n3 =  $input{'n3'}; $last=3;} else {$n3 = 0;} 
	if ($input{'n4'}) {$n4 =  $input{'n4'}; $last=4;} else {$n4 = 0;} 
	if ($input{'n5'}) {$n5 =  $input{'n5'}; $last=5;} else {$n5 = 0;} 
	if ($input{'n6'}) {$n6 =  $input{'n6'}; $last=6;} else {$n6 = 0;} 
	if ($input{'n7'}) {$n7 =  $input{'n7'}; $last=7;} else {$n7 = 0;} 
	if ($input{'n8'}) {$n8 =  $input{'n8'}; $last=8;} else {$n8 = 0;} 
	if ($input{'n9'}) {$n9 =  $input{'n9'}; $last=9;} else {$n9 = 0;} 
	if ($input{'n10'}) {$n10 =  $input{'n10'}; $last=10;} else {$n10 = 0;} 
	if ($input{'n11'}) {$n11 =  $input{'n11'}; $last=11;} else {$n11 = 0;} 
	if ($input{'n12'}) {$n12 =  $input{'n12'}; $last=12;} else {$n12 = 0;} 
	if ($input{'n13'}) {$n13 =  $input{'n13'}; $last=13;} else {$n13 = 0;} 
	if ($input{'n14'}) {$n14 =  $input{'n14'}; $last=14;} else {$n14 = 0;} 
	if ($input{'n15'}) {$n15 =  $input{'n15'}; $last=15;} else {$n15 = 0;} 
	if ($input{'n16'}) {$n16 =  $input{'n16'}; $last=16;} else {$n16 = 0;} 
	if ($input{'k'}) {$k =  $input{'k'};} else {$k = 0;} 

	if ($last == 0) {
		print "Input error:  No input entered.\n";
		&WorkingMoreInfo('mult/Multiset');
		exit;
	}

	if (($out eq "perm") || ($out eq "perm_gray") || ($out eq "perm_cool")) {
		if (($alt) && ($last > 7)) {
			print "Input Error:  For alternate output, only the first seven n<sub>i</sub>'s can be entered.\n";
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
	}
	if ($last > 9) { $space = 1; }
	else {$space = 0; }
	@size = ($n1,$n2,$n3,$n4,$n5,$n6,$n7,$n8,$n9,$n10,$n11,$n12,$n13,$n14,$n15,$n16);

	# Check that n(i) are less than 20
	for($i=0; $i<16; $i++) {
		if ($size[$i] > 19) {
			print "Input error:  All n<sub>i</sub> must be less than 20\n";
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
	}
	for ($i=0; $i < $last; $i++) {
		$sum += $size[$i];
	}

	# Create temporary file 
	open( FILE, ">$TempFile" ) || die "Can't open $TempFile:$!\n";

    # The program multicomb asks for "n k" where n is the last index in
	# 	[0..n] and k is number of elements in the combintation. 
	#	It then asks for the n[i]'s
  	if ($input{'program'} eq "comb_lex") {
		if ($k == 0) {
			print "Input error:  You must enter a value for k.\n";
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
		if ($k > $sum) {
			print "Input error:  The value for k may not exceed the sum of the n<sub>i</sub>.\n";
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
   		print FILE ($last-1) . " " . $k . "\n";
		for ($i=0; $i<$last; $i++) {
			print FILE "$size[$i] ";
		}
		print FILE "\n";
	}

	# The program GenMult asks for "k t" where k is the number of items in each permutation
	#	and t is the last index (relative to start=0) that has
	#	value > 0.  It then asks for the n[i]'s.
  	if ($input{'program'} eq "perm_lex") {
	    if ($k == 0) {
		print FILE $sum . " " . ($last-1). "\n"; 
	    }
	    else {
		print FILE $k . " " . ($last-1). "\n";
	    }
		for ($i=0; $i<$last; $i++) {
			print FILE "$size[$i] ";
		}
		print FILE "\n";
	}

	# The program BigGen asks for t where t is the last index and then
	#	for the n[i]'s
  	if ($input{'program'} eq "perm_gray") {
		print FILE ($last-1). "\n"; 
		for ($i=0; $i<$last; $i++) {
			print FILE "$size[$i] ";
		}
		print FILE "\n";
	}

	# The program multcool asks for "n e_1 e_2 ... e_n" where e_i is the ith smallest item in the permutation
	#	and n is the total number of items.  For example, multcool 6 1 1 2 3 3 3 for the multiset {1,1,2,3,3,3}.
	# An optional final argument limits the number of created permutations.
  	if ($input{'program'} eq "perm_cool") {
	    print FILE $sum	. " ";
        for ($i=0; $i<$n1; $i++)  { print FILE "1 "; } 
        for ($i=0; $i<$n2; $i++)  { print FILE "2 "; } 
        for ($i=0; $i<$n3; $i++)  { print FILE "3 "; } 
        for ($i=0; $i<$n4; $i++)  { print FILE "4 "; } 
        for ($i=0; $i<$n5; $i++)  { print FILE "5 "; } 
        for ($i=0; $i<$n6; $i++)  { print FILE "6 "; } 
        for ($i=0; $i<$n7; $i++)  { print FILE "7 "; } 
        for ($i=0; $i<$n8; $i++)  { print FILE "8 "; } 
        for ($i=0; $i<$n9; $i++)  { print FILE "9 "; } 
        for ($i=0; $i<$n10; $i++) { print FILE "10 "; } 
        for ($i=0; $i<$n11; $i++) { print FILE "11 "; } 
        for ($i=0; $i<$n12; $i++) { print FILE "12 "; } 
        for ($i=0; $i<$n13; $i++) { print FILE "13 "; } 
        for ($i=0; $i<$n14; $i++) { print FILE "14 "; } 
        for ($i=0; $i<$n15; $i++) { print FILE "15 "; } 
        for ($i=0; $i<$n16; $i++) { print FILE "16 "; } 
		print FILE "$LIMIT ";
		print FILE "\n";
	}

	# The program ContTable asks for the "r c" where the r+1 is the number
	#	of rows and c+1 is the number of columns.  The program then
	# 	asks for the row sums followed by the column sums.
  	if ($input{'program'} eq "cont_table") {
		# Preliminary error checking:
		if ($last < 9) {
			print "Input error:  You must enter the column sums.\n";
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
		$col_sum=0;
		$row_sum=0;
		$num_cols = $last - 8;
		$num_rows = 0;
		for ($i=0; $i<8; $i++) {
			if ($size[$i] > 0) {$num_rows = $i; }
		}
		$num_rows++;
			
		for ($i=0; $i<=7; $i++) {
			$col_sum += $size[$i+8];
			$row_sum += $size[$i];
		}
		if ($row_sum != $col_sum) {
			print "Input error:  The column sums must equal the row sums.\n";
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
		print FILE ($num_rows-1) . " " . ($num_cols-1) . "\n"; 
		for ($i=0; $i<$num_cols; $i++) {
			print FILE "$size[$i+8] ";
		}
		print FILE "\n";
		for ($i=0; $i<$num_rows; $i++) {
			print FILE "$size[$i] ";
		}
		print FILE "\n";
	}
		
   	close( FILE );
}
 
sub OutError {
  	&ErrorMessage('The output has not been specified or is not applicable');
}

sub ProcessForm {

  	&OutputHeader('Multiset Output','mult/Multiset');  # from common.pl

	$standard=0;
	$alt=0;
	$gray=0;
  	if ($input{'output1'}) { $standard = 1;}
  	if ($input{'output2'}) { $alt = 1;}
  	if ($input{'output3'}) { $gray = 1;}
	if ($standard == 0 && $alt == 0 && $gray==0) { 
		&OutError;
		&WorkingMoreInfo('mult/Multiset');
		exit;
	}

  	if ($input{'program'} eq "perm_lex") {
		$out = "perm";
		&Initialize;

		if (($k > 0) && ($k < $sum)) {
		    print "The k=";
		    print $k;
		    print " permutations of [";
	    }
		else {
		    print "The permutations of [";
		}

		$firstTimePrinting = 0;
		for ($i=0; $i<$last; $i++) {
		    for ($count=0; $count<$size[$i]; $count++) {
			    if ($firstTimePrinting == 0) {
			        $firstTimePrinting = 1;
			        print ($i + 1);
			    }
			    else {
			        print ", " . ($i + 1);
			    }
		    }
		}
		$firstTimePrinting = 0;

		print "] in lex order.<BR>";

		if (($standard == 0) && ($alt == 0)) { 
			&BadOutput; 
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
		if ($input{'output3'}) { &OutputIgnored('Gray Code')}
		$retval = system "$BinPath/GenMult < $TempFile | /usr/bin/perl format.pl $LIMIT $out $standard $alt $gray $space $sum";
		if ($retval) { &LimitError; }
 
 	} elsif ($input{'program'} eq "perm_gray") {
		$out = "perm_gray";
		&Initialize;
     		print "Permutations of a multiset in Gray Code order.<BR>";
		$retval = system "$BinPath/BigGen < $TempFile | /usr/bin/perl format.pl $LIMIT $out $standard $alt $gray $space $sum";
		if ($retval) { &LimitError; }

  	} elsif ($input{'program'} eq "perm_cool") {
		$out = "perm_cool";
		&Initialize;

		print "The permutations of [";
		$firstTimePrinting = 0;
		for ($i=0; $i<$last; $i++) {
		    for ($count=0; $count<$size[$i]; $count++) {
			    if ($firstTimePrinting == 0) {
			        $firstTimePrinting = 1;
			        print ($i + 1);
			    }
			    else {
			        print ", " . ($i + 1);
			    }
		    }
		}
		$firstTimePrinting = 0;
		print "] in <i>cool</i>-lex order.<BR>";
		
        if ($gray == 1) {
            print "The highlighted symbol is shifted into the first position to create the next permutation.<BR>";
        }		

		if ($input{'k'}) { &OutputIgnored('k')}
		$retval = system "$BinPath/multicool < $TempFile | /usr/bin/perl format.pl $LIMIT $out $standard $alt $gray $space $sum";
		if ($retval) { &LimitError; }
  
 	} elsif ($input{'program'} eq "comb_lex") {
		$out = "comb";
		&Initialize;
     		print "Combinations of a multiset in co-lex order.<BR>";
		if (($standard == 0) && ($alt == 0)) { 
			&BadOutput; 
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
		if ($input{'output3'}) { &OutputIgnored('Gray Code')}
     		$retval = system "$BinPath/multicomb < $TempFile | /usr/bin/perl format.pl $LIMIT $out $standard $alt $gray $space $last $k"; 
		if ($retval) { &LimitError; }

 	} elsif ($input{'program'} eq "comb_gray") {

 	} elsif ($input{'program'} eq "cont_table") {
		$out = "cont_table";
		&Initialize;
     		print "Contigency tables with ". $num_rows  . " rows and ";
		print $num_cols . " columns.<br>\n";
		if (!($input{'output1'})) { 
			&BadOutput; 
			&WorkingMoreInfo('mult/Multiset');
			exit;
		}
		if ($input{'output2'}) { &OutputIgnored('Alternate')}
		if ($input{'output3'}) { &OutputIgnored('Gray Code')}
		$retval = system "$BinPath/cont_table < $TempFile | /usr/bin/perl format.pl $LIMIT $out $standard $alt $gray $space $num_cols $num_rows";
		if ($retval) { &LimitError; }
	}
  
	&WorkingMoreInfo('mult/Multiset');
  	print "</body></html>\n";
}


