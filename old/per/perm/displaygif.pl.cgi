#!/usr/bin/perl

# Utility perl CGI that returns the image specified
# by it's arguments to the web-browser
#
# Hacked by Udi Taylor, April 1996

require "../cgi-lib.pl";

MAIN: 
{
# Read in all the variables set by the form
   if (&ReadParse(*input)) { &initialize; &sendGif; &eraseGif; }
}


# This little routine builds the GIF name from the parameters
sub initialize {
	$pid = $input{'pid'};
	$num = $input{'num'};
	$gifName = sprintf("/tmp/test_%s_%s.gif",$pid,$num);
}

# This here routine dumps the .GIF to the client
sub sendGif {
 
 
 open(STDIN, $gifName);
 $len = read(STDIN, $data, 16384);  # yes yes, only the first 16K of the gif is sent.  
                                    # This is sufficient for our purposes since most
                                    # of the gis are < 1K in size.
 printf "Content-type: image/gif\n";
 printf "Content-length: $len\n\n";
 print $data;

}

# This routine erases the GIf once it has been retransmitted

sub eraseGif { 
    unlink $gifName;
}
