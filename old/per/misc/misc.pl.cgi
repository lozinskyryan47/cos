#!/usr/bin/perl

require "../common.pl";
$BinDir = "../../bin/misc";

$QueensBIN = "$BinDir/Queen";
$PentominoBIN = "$BinDir/BigPentomino";
$ShowPentominoBIN = "$BinDir/PentShowSolution";
$BigPentBIN = "$BinDir/BigPentomino";
$PolyominoBIN = "$BinDir/Polyomino";
$FormatBIN = "$BinDir/format";
$FibonacciBIN = "$BinDir/fibo";
$MaxSolutions = 10;

MAIN:
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

# for testing
sub my_sigint_catcher {
  $saw_sigint = 1;
}


sub SmallPent {
    if ($input{'dimensions'} eq "6by10") { $n = 6;  $m = 10; }
    if ($input{'dimensions'} eq "5by12") { $n = 5;  $m = 12; }
    if ($input{'dimensions'} eq "4by15") { $n = 4;  $m = 15; }
    if ($input{'dimensions'} eq "3by20") { $n = 3;  $m = 20; }
    print "Solutions to the pentomino problem with $n";
    print " rows and $m columns.<BR>";
    # if (int($input{'n'})*int($input{'m'}) != 60) { 
    #    &ErrorMessage( "Product of <I>n</I> and <I>m</I> must be 60" ); 
    #    exit(0); 
    #}
    if ($input{'n'}) { &ExtraParam('n',$input{'n'}); }
    if ($input{'k'}) { &ExtraParam('k',$input{'k'}); }
    if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
    $file = "./BigPent.temp";
    open( FILE, ">$file" ) || die "Can't open $file:$!\n";
    print FILE "9 21 $MaxSolutions \n";  # 10 solutions is the limit
    for ($i = 1; $i <= 9; $i++) {
    	for ($z = 1; $z <= 21; $z++){
    	     if ((( 9 - $i - $n ) >= 0) || ( (21 - $z - $m ) >= 0 ) ||( 
$i eq 1 )) { print FILE "-1 ";}
    	     else { print FILE " 0 "; }
    	}
        print FILE "\n";
    }
    close( FILE );
    print "<P>Only first $MaxSolutions solutions output.<p>";
    $retval = system "$BigPentBIN | $ShowPentominoBIN $outformat";
}

sub BigPent {
    print "Solutions to the pentomino problem with custom shape.<BR>";
    $file = "./BigPent.temp";
    open( FILE, ">$file" ) || die "Can't open $file:$!\n";
    $count = 0;
    print FILE "9 17 $MaxSolutions \n";  # 10 solutions is the limit
    for ($i = 1; $i <= 9; $i++) {
        print FILE "-1 ";
	for ($j = 1; $j <= 16; $j++) {
	    if ($input{"pent$i.$j"}) { ++$count; print FILE 
" 0 "; }
	    else { print FILE "-1 "; }
	}
	print FILE "\n";
    }
    close( FILE );
    if ($count > 60) { &TooBig('Square_Count',60); exit(0); }
    if ($count < 60) { &TooSmall('Square_Count',60); exit(0); }
    print "<P> Only first 10 solutions output.<p>";
    $retval = system "nice -10 $BigPentBIN | $ShowPentominoBIN $outformat";
}

sub ProcessForm {


  if ($input{'program'} eq "Queens") {
     
     &OutputHeader( 'Miscellaneous Output', 'misc/Queen' );  
     print "Solutions to the queens problem for <I>n</I> = $input{'n'}.<BR>";
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if ($input{'k'}) { &ExtraParam('k',$input{'k'}); }
     if (int($input{'n'}) > 20) { &TooBig('n',20); exit(0); }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
     &TableSetup;
     $xx = ''; # output parameter string
     if ($input{'output1'}) 
        { $xx = $xx." -P"; &ColumnLabel( "Permutation" ); }
     if ($input{'output2'}) 
        { $xx = $xx." -B"; &ColumnLabel( "Chessboard" ); }
     $retval = system "$QueensBIN -n $input{'n'} $xx -l $LIMIT ";
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &MoreInfo('misc/Queen');

  } elsif ($input{'program'} eq "Pentomino") {

     &OutputHeader( 'Miscellaneous Output', 'misc/PentInfo');  
     $outformat = 0;
     if ($input{'output1'}) { $outformat = 1; }
     if ($input{'output2'}) { $outformat = $outformat | 2; }
     if ($outformat eq 0) { $outformat = 1; }
     if ($input{'dimensions'} eq "custom") {
         &BigPent;
     } else {
         &SmallPent;
     }
     &WorkingMoreInfo('misc/PentInfo');

#  } elsif ($input{'program'} eq "CustomPentomino") {
#
#     &OutputHeader( 'Miscellaneous Output', 'misc/PentInfo' );  
#     $outformat = 0;
#     if ($input{'output1'}) { $outformat = 1; }
#     if ($input{'output2'}) { $outformat = $outformat | 2; }
#     if ($outformat eq 0) { $outformat = 1; }
#     &BigPent;
#     &WorkingMoreInfo('misc/PentInfo');

  } elsif ($input{'program'} eq "Polyomino") {
     
     &OutputHeader( 'Polyomino Output', 'misc/PolyominoInfo' );  
     if (!($input{'n'})) { $input{'n'} = 0 }
     if (int($input{'n'}) > 20) { &TooBig('n',20); exit(0); }
     if (($input{'n'}) < 1) { 
     	&ErrorMessage( "Enter a value for n > 0\n" );
      	&WorkingMoreInfo( 'misc/PolyominoInfo' );
	exit(1);
     }
     print "Non-isomorphic (free) polyominoes for <I>n</I> = $input{'n'}.<BR><BR>";
     $outformat = 0;
     if ($input{'output1'}) { $outformat = 1; }
     if ($input{'output2'}) { $outformat = $outformat | 2; }
     if ($outformat eq 0) { $outformat = 1; }
     $retval = system "$PolyominoBIN -n $input{'n'} -t $LIMIT | $FormatBIN $input{'n'} $LIMIT | $ShowPentominoBIN $outformat";
     &WorkingMoreInfo( 'misc/PolyominoInfo' );
 
  } elsif ($input{'program'} eq "Knights") {
     print "Solutions to the knight's tour problems for <I>n</I> = $input{'n'}";
     if ($input{'k'} && $input{'m'}) { 
        print ", starting at (<I>k,m</I>) = ($input{'k'},$input{'m'}).\n"; 
     }
     print ".<P>\n"; 
     if (int($input{'n'}) > 7) { &TooBig('n',7); exit(0); }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
     &TableSetup;
     $xx = ''; # output parameter string
     if ($input{'output1'}) { $xx = $xx.' -a ';  &ColumnLabel( "One-line" ); }
     if ($input{'output2'}) { $xx = $xx.' -u ';  &ColumnLabel( "Matrix" ); }
     print "</TR>";
     if (!$input{'m'}) { $input{'m'} = '0'; }
     if (!$input{'k'}) { $input{'k'} = '0'; }
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &WorkingMoreInfo('misc/Knight');

  } elsif ($input{'program'} eq "Fibonacci") {
     &OutputHeader( 'Miscellaneous Output', 'misc/Fibo' );  
     print "Fibonacci sequences for <math>F<sub>n</sub> = </math>$input{'n'}";
     print ".<P>\n"; 
     if (int($input{'n'}) > 11) { &TooBig('n',11); exit(0); }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
     &TableSetup;
     $outformat = 0;
     if ($input{'output1'}) { $outformat = $outformat | 1; &ColumnLabel("Bitstrings" ); }
     if ($input{'output3'}) { $outformat = $outformat | 4; &ColumnLabel("Stair Steps"); }
     if ($input{'output2'}) { $outformat = $outformat | 2; &ColumnLabel("Pane Reflections" ); }
     print "</TR>";
     if (!$input{'k'}) { $input{'k'} = '0'; }
     if ($outformat eq 0) { $outformat = 1; }
     $retval = system "$FibonacciBIN -n $input{'n'} -k $input{'k'} -L $LIMIT -o $outformat";
     if ($retval) { &LimitError; }
     &WorkingMoreInfo('misc/Fibonacci');
  
  } elsif ($input{'program'} eq "Testing") {

     if ($pid = fork) {
        # parent here
        print "program=$input{'program'}<BR>";  
        print "start=$input{'start'}<BR>";  
        print "process id = $input{'process'}<BR>";  
        $value = $input{'start'}+10; 
        # if (defined $input{'start'}) { $value = $input{'start'}+10; 
        # } else {$value = 0};
        print "Ten more numbers ($value)?<BR>";
        print "<form method=get action=per/misc/misc.pl.cgi>\n";
        print "<input type=submit value=Yes>\n";
        print "<input type=hidden name=program value=Testing>\n";
        print "<input type=hidden name=process value=$pid>\n"; 
        print "<input type=hidden name=start value=$value></form>\n";
        print "<form action=gen/misc.html><input type=submit value=No></form>\n";
        print "Parent pid = $pid<BR>";
     } else {
        # child here
        print "Child pid = $pid<BR>";
        for ($i=1;$i<=1000;++$i) { print "$i " };
        for ($i=1;$i<=1000;++$i) { print "$i " };
        print "<br>\n";
        exit(0);
     }
 
  } else {

     &ErrorMessage( "Please choose a type of object to generate" );
  
  }

  &CloseDocument;
}









