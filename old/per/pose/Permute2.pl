#!/usr/bin/perl
#
# This program performs a transormation on the output of the genle
# program using the transformation rules in the transform.log file.
#

require "../common.pl";
if ($#ARGV == -1) {exit(0);}
else { $output = $ARGV[0];}
$#ARGV = -1;

open (RULES, "./transform.log");
open (STDIN);
$n = 1;
while (<RULES>) {
    @pair = split;
    $node[$n++] = $pair[0];  	#changed dec 6/96 
  # $node[$pair[1]] = $pair[0];
}
$n--;		# number of elements

print "<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
if ($output eq "standard") {
	print '<TH COLSPAN=1><FONT SIZE="+1">Extension</FONT><BR></TH>';
	print "\n";
}
elsif ($output eq "gray") {
	print '<TH COLSPAN=1><FONT SIZE="+1">Transposition</FONT><BR></TH>';
	print "\n";
}
else {
	print '<TH COLSPAN=1><FONT SIZE="+1">Extension<TH COLSPAN=1>Transposition</FONT><BR></TH>';
	print "\n";
}

$count = 0;
$returnval = 0;
$flag = 0;
while (<>) {
    @permutation = split;
    # Do not perform on first time through
    if ((($output eq "gray") || ($output eq "standardgray")) && ($flag == 1)) {
	$i = 0;
	for($i = 0; $i < $n+2; $i++) {
		$diff[$i] = 0;
	}
	for ($i=0; $i<$n; $i++){ 
		if ($permutation[$i] != $previous[$i]) {
			$diff[$i] = 1; 
		}
	}
	#calculate and print trasposition
	print"<TD>";
	for($i=0; $i<$n; $i++) {
		if ($diff[$i] == 1) {
			# (i,i+1)
			if ( ($previous[$i] == $permutation[$i+1]) &&
				($previous[$i+1] == $permutation[$i]) ){
				print "(";
				print ($i+1);
				print " ";
				print ($i+2);
				print ")";
				$i++;
			}
			# (i,i+1,i+2)
			elsif ($previous[$i] == $permutation[$i+1]) {
				print "(";
				print ($i+1);
				print " ";
				print ($i+2);
				print " ";
				print ($i+3);
				print ")";
				$i=$i+2;
			}
			# (i,i+2,i+1)
			else {
				print "(";
				print ($i+1);
				print " ";
				print ($i+3);
				print " ";
				print ($i+2);
				print ")";
				$i=$i+2;
			}
		}
			
	}
    }
    @previous = @permutation;
    print "<TR>";
    if (($output eq "standard") || ($output eq "standardgray")) {
   	 print "<TD>";
	 $i = 0;
  	 foreach $item (@permutation) {
	       if ($diff[$i] == 1) { print "<FONT COLOR=#00FF00>";}
 	       print "$node[$item] ";
	       if ($diff[$i++] == 1) { print "</FONT>";}
	 }
   	 print "<BR></TD>\n";
  	 if ((++$count) ==  $LIMIT) {
 	      $returnval = $LIMITerror;  # WARNING must also change genle
	       print "</TABLE>";
       	 	&LimitError;
      		 last;
   	 }
    }
    $flag = 1;
}



print "</TABLE>";
print "<P>Extensions output = $count";
close (RULES);
close (STDIN);
# print "$count  $returnval";
$returnval; # return value  

