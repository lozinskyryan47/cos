/*********************************************************************
* Java program to generate length n permutations with k inversions.  *
*                                                                    *
* This program was obtained from the (Combinatorial) Object Server   * 
* at http://www.theory.csc.uvic.ca/    .                             *
*                                                                    *
* The inputs are n, the length of the string, and k, the number of   *
* inversions.                                                        *
*                                                                    *
* The program can be modified, translated to other languages, etc.,  *
* so long as proper acknowledgement is given (author and source).    *
* The algorithm is CAT and was developed by Scott Effler and Frank   *
* Ruskey.                                                            * 
* Programmer: Frank Ruskey.          				     *
**********************************************************************/

class Node {
   int value;
   Node[] L;  // L[0] = next,  L[1] = prev.
   Node ( int vv, Node nn, Node pp ) {
      value = vv;
      L = new Node[2];  L[0] = nn;  L[1] = pp;
   }
   void delete() {
     this.L[0].L[1] = this.L[1];
     this.L[1].L[0] = this.L[0];
   }
   void undelete() {
     this.L[0].L[1] = this;
     this.L[1].L[0] = this;
   }
}

class Inversion {

static int perm[] = new int[256];
static Node head; // header node of doubly linked list
static int Tri[] = new int[256]; // triangular numbers
static int n, k, count = 0, inc[] = {+1,-1};

static void printPerm () {
  ++count;
  for ( int i=1; i<=n; i++ )
    System.out.print( perm[i]+" " );
  System.out.println();
}

static void initializeTri ( int n ) {
  Tri[0] = Tri[1] = 0;
  for( int i=1; i<=n; i++ ) Tri[i] = i-1 + Tri[i-1];
}

static void permSwap ( int i, int j ) {
  int temp = perm[i];
  perm[i] = perm[j];
  perm[j] = temp;
}

static void PET( int n, int dir ) {
   Node p = head.L[dir];
   for( int i=1; i<=n; i++, p = p.L[dir] ) perm[i] = p.value;
   for( int i=1; i<=n-1; i++ ) {
      permSwap( i, i+1 );  printPerm();  permSwap( i, i+1 );
   }
}

static void gen( int n, int k ) {
  int rank, dir;
  if ( n == 0 )        { printPerm();  return; }
  if ( k == 1 )        { PET( n, 0 );  return; }
  if ( k == Tri[n]-1 ) { PET( n, 1 );  return; }
  if ( k > Tri[n-1] )  { rank = 1;  dir = 0;   } // increasing
  else                 { rank = n;  dir = 1;   } // decreasing
  for ( Node p = head.L[dir];  p != head;  p = p.L[dir] ) {
     if ( k >= n-rank && k <= Tri[n-1]+n-rank ) {
        perm[n] = p.value;
        p.delete();
        gen( n-1, k-n+rank );
        p.undelete();
     } else return;
     rank = rank + inc[dir];
  }
}

public static void main( String[] args ) {
  if( args.length != 2 ) {
    System.out.println( "java Inversions <n> <k>\n" );
    return;
  }
  n = Integer.parseInt( args[0] );
  k = Integer.parseInt( args[1] );

  head = new Node ( 0, null, null );
  head.L[0] = head.L[1] = head;
  for( int i=n; i>=1; i-- ) {
    Node p = new Node( i, head.L[0], head );
    p.undelete();
  }

  initializeTri( n );
  gen( n, k );
  System.out.println( "count = "+count );
}

}
