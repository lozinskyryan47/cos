(*=================================================================*)
(* Pascal program for distribution.                                *)
(* Generates subsets in colex order.                               *)
(* No input error checking.  Assumes 0 <= NN <= MAX.               *)
(* Algorithm is CAT (Constant Amortized Time).                     *)
(* Written by Frank Ruskey.                                        *)
(*=================================================================*)

program Subsets ( input, output );

const MAX = 50;  {maximum value of NN}

var NN : integer;                    {generate all subsets of [NN]}
    b  : array [1..MAX] of integer;  {bitstring representation of subset}
    
procedure PrintIt;
var i : integer;  first : boolean;
begin
  for i := 1 to NN do write( b[i]:2 );  {bitstring}
  write( '   {' );   first := true;     {list of elements}
  for i := 1 to NN do    
     if b[i] = 1 then begin
        if not first then write( ',' );  first := false;
        write( i:0 );  
     end;
  write( '}' ); 
  writeln;
end {of PrintIt};

procedure SubLex ( n : integer );
begin
  if n = 0 then PrintIt
  else begin
     b[n] := 0;  SubLex( n-1 );  
     b[n] := 1;  SubLex( n-1 );  
  end;
end {of SubLex};

{-------------------------- MAIN ----------------------------}
begin
  write( 'Enter n: ' );  readln( NN );
  SubLex( NN );
end.


