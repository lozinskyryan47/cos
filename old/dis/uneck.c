/*********************************************************************
* C program to generate binary UNLABELED necklaces, prenecklaces     *
* and Lyndon words.                                                  *
* This program, was obtained from the (Combinatorial) Object Server  * 
* at http://theory.cs.uvic.ca  The inputs are n, the length of    *
* the string and d, the maximum number of non-0's in the string.     *
* The program can be modified, translated to other languages, etc.,  *
* so long as proper acknowledgement is given (author and source).    *
* The algorithm is CAT and was developed by Joe Sawada, by modifying *
* Frank Ruskey's recursive necklace algorithm.                       * 
* Programmer: Joe Sawada           				     *
**********************************************************************/
#include <stdio.h>

int a[101];
int n,option,d;

void Print(int p) {

	int j;

	if ( ( option == 0) || ((n%p == 0)&&(option == 1)) ||
		((option == 2) && (p == n)) ) {
		for(j=1; j<=n; j++) 
			printf("%d ",a[j]);
		printf("\n");
	}
	else if ((option == 3) && (n%p == 0)) {
		for(j=1; j<=p; j++) 
			printf("%d ",a[j]);
		printf("\n");
		
	}
}

void Gen(int t, int p, int c, int ones) {
	
	if (ones <= d) {
		if(t>n) Print(p);
		else {
 			if (a[t-c] == 0) {
				if (a[t-p] == 0) {
					a[t] = 0;
					Gen(t+1, p,t,ones);
				}
				a[t] = 1;
				if (a[t-p] == 1) Gen(t+1,p,c,ones+1);
				else Gen(t+1,t,c,ones+1);
			}
			else {
				a[t] = 0;
				Gen(t+1, p,c, ones);
			}
		}
	}
}
void main() {

	printf("Enter n d:");
	scanf("%d %d",&n,&d);
	printf("pre-necklace = 0, necklace = 1, lyndon = 2:");
	scanf("%d",&option);
	a[0] = a[1] = 0;
	Gen(2,1,1,0); 
}

