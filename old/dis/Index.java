/*********************************************************************
* Java program to generate length n permutations with index k.       *
*                                                                    *
* This program was obtained from the (Combinatorial) Object Server   * 
* at http://www.theory.csc.uvic.ca/.                             *
*                                                                    *
* The inputs are n, the length of the string, and k, the index.      *
*                                                                    *
* The program can be modified, translated to other languages, etc.,  *
* so long as proper acknowledgement is given (author and source).    *
* The algorithm has been experimentally observed to be CAT and was   *
* developed by Scott Effler and Frank Ruskey.                        * 
* Programmers: Scott Effler and Frank Ruskey.                        *
**********************************************************************/

class Node {
   int value;
   Node[] L;  // L[0] = next,  L[1] = prev.
   Node ( int vv, Node nn, Node pp ) {
      value = vv;  
      L = new Node[2];  L[0] = nn;  L[1] = pp;
   }
   void delete() {
     this.L[0].L[1] = this.L[1];
     this.L[1].L[0] = this.L[0];
   }
   void undelete() {
     this.L[0].L[1] = this;  
     this.L[1].L[0] = this;
   }
}

class Index {

static int perm[] = new int[256];
static Node head; // header node of doubly linked list
static int Tri[] = new int[256]; // triangular numbers
static int n, k, count = 0, inc[] = {+1,-1};
static Node q;  // "middle" of list
static int rankQ=1;  // rank of the "middle" of the list

static void printPerm () {
  ++count;
  for ( int i=1; i<=n; i++ )
    System.out.print( perm[i]+" " );
  System.out.println();
}

static void initializeTri ( int n ) {
  Tri[0] = Tri[1] = 0;
  for( int i=1; i<=n; i++ ) Tri[i] = i-1 + Tri[i-1];
}

static void permSwap ( int i, int j ) {
  int temp = perm[i];
  perm[i] = perm[j];
  perm[j] = temp;
}

static void PET( int n, int dir ) {   
   Node p = head.L[dir]; 

   for( int i=1; i<=n; i++, p = p.L[dir] ) perm[i] = p.value;
   for( int i=1; i<=n-1; i++ ) { 
      permSwap( 1, i+1 );  printPerm();
   }     
}

static void gen( int n, int k ) {
  int rank, dir, K;
  int a,b;

  if ( n == 0 )                                       { printPerm(); return; }
  if ( k == 1        && head.L[1].value < perm[n+1] ) { PET( n, 0 ); return; }
  if ( k == Tri[n]-1 && head.L[1].value < perm[n+1] ) { PET( n, 1 ); return; }

  dir=0; rank=1; a=0;  // forwards from head
  for ( Node p = head.L[dir]; (p!=head)&&(a==0);  p = p.L[dir] )
  {
    perm[n] = p.value;
    if ( p.value > perm[n+1] ) K = k - n;  else K = k;
    if ( K >= n-rank && K <= Tri[n]+1-rank ) {
      q = p.L[0];
      rankQ = rank;
      p.delete();
      gen( n-1, K );
      p.undelete();
    }
    else a = rank;
    rank = rank + inc[dir];
  }
  if( a==0 ) return;

  dir=1; rank=n; b=0;  // backwards from tail
  for ( Node p = head.L[dir]; (p!=head)&&(b==0);  p = p.L[dir] )
  {
    perm[n] = p.value;
    if ( p.value > perm[n+1] ) K = k - n;  else K = k;
    if ( K >= n-rank && K <= Tri[n]+1-rank ) {
      q = p.L[0];
      rankQ = rank;
      p.delete();
      gen( n-1, K );
      p.undelete();
    }
    else b = rank;
    rank = rank + inc[dir];
  }
  if( (a>rankQ)||(b<rankQ) ) return;

  dir=1; rank=rankQ-1;  // backwards from middle
  for ( Node p = q.L[dir]; (p!=head)&&(rank>a);  p = p.L[dir] )
  {
    perm[n] = p.value;
    if ( p.value > perm[n+1] ) K = k - n;  else K = k;
    if ( K >= n-rank && K <= Tri[n]+1-rank ) {
      q = p.L[0];
      rankQ = rank;
      p.delete();
      gen( n-1, K );
      p.undelete();
    }
    else a = rank + inc[dir];
    rank = rank + inc[dir];
  }

  dir=0; rank=rankQ;  // forwards from middle
  for ( Node p = q; (p!=head)&&(rank<b);  p = p.L[dir] )
  {
    perm[n] = p.value;
    if ( p.value > perm[n+1] ) K = k - n;  else K = k;
    if ( K >= n-rank && K <= Tri[n]+1-rank ) {
      q = p.L[0];
      rankQ = rank;
      p.delete();
      gen( n-1, K );
      p.undelete();
    }
    else b = rank + inc[dir];
    rank = rank + inc[dir];
  }
}

public static void main( String[] args ) {
  if( args.length != 2 ) {
    System.out.println( "java Index <n> <k>\n" );
    return;
  }
  n = Integer.parseInt( args[0] );
  k = Integer.parseInt( args[1] );

  head = new Node ( 0, null, null );
  q = head.L[0] = head.L[1] = head;
  for( int i=n; i>=1; i-- ) {
    Node p = new Node( i, head.L[0], head );
    p.undelete();
  }

  initializeTri( n );
  perm[n+1] = n+1;
  gen( n, k );
  System.out.println( "count = "+count );
}
}
