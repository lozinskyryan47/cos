(*===================================================================*)
(* Pascal program for distribution.                                  *)
(* Generates product spaces in colex order.                          *)
(* INPUT:  n (on a separate line, followed by num[1],...,num[n]).    *)
(* OUTPUT: all sequences (b[1],b[2],...,b[n])                        *)
(*         such that 0 <= b[i] < num[i], for i = 1,2,...,n.          *)
(* No input error checking.  Assumes 0 <= n <= MAX.                  *)
(* Algorithm is CAT (Constant Amortized Time).                       *)
(* The program can be modified, translated to other languages, etc., *)
(* so long as proper acknowledgement is given (author and source).   *)
(* Programmer: Frank Ruskey, 1994.                                   *)
(* The latest version of this program may be found at the site       *)
(* http://theory.cs.uvic.ca/inf/perm/SubsetInfo.html                  *)
(*===================================================================*)

program Subsets ( input, output );

const MAX = 50;  {maximum value of NN}

var NN  : integer;                     {dimensions are num[1..NN]}
    b   : array [1..MAX] of integer;   {the string} 
    num : array [1..MAX] of integer;   {0..num[i]-1 can go in position i} 
    i   : integer;

procedure PrintIt;
var i : integer;  
begin
  for i := 1 to NN do write( b[i]:2 );  {string}
  writeln;
end {of PrintIt};

procedure ProdLex ( n : integer );
var i : integer;
begin
  if n = 0 then PrintIt else 
  for i := 0 to num[n]-1 do begin
     b[n] := i;  ProdLex( n-1 );  
  end;
end {of ProdLex};

{-------------------------- MAIN ----------------------------}
begin
  write( 'Enter n: ' );    readln( NN );
  write( 'Enter num[1..NN]: ' );
  for i := 1 to NN do read( num[i] );   readln;
  ProdLex( NN );
end.


