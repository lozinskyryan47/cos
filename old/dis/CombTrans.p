(*=================================================================*)
(* Pascal program for distribution.                                *)
(* Generates the k-combinations of [n] by transpositions via a     *)
(* direct algorithm (see the book for what "direct" means).        *)
(* No input error checking.  Assumes 0 <= k <= n <= MAX.           *)
(* Outputs both the bitstring and the transposition (x,y) (meaning *)
(* that x leaves the subset and y enters).                         *)
(* Algorithm is CAT (Constant Amortized Time).                     *)
(* Written by Frank Ruskey.                                        *)
(*=================================================================*)

program CombTrans ( input, output );

const MAX = 100;

var a : array [1..MAX] of integer;
    n,k,i,cnt : integer;

procedure PrintIt ( x, y : integer );
begin
  cnt := cnt + 1;
  write( cnt:8,': ' );
  for i := 1 to n do write( a[i]:2 );  
  if x <> 0 then write( '     (',x:0,',',y:0,')' ); 
  writeln;
end {of PrintIt};

procedure swap ( x, y : integer );
var t : integer;
begin
  a[x] := 1;  a[y] := 0;
  {t := a[x];  a[x] := a[y];  a[y] := t;}
  PrintIt( x, y );
end {of swap};

procedure gen ( n,k : integer );  forward;
procedure neg ( n,k : integer );  forward;

procedure gen (* n,k : integer *);
begin
if (k > 0) and (k < n) then begin
  gen( n-1, k );
  if k = 1 then swap( n, n-1 ) else swap( n, k-1 );
  neg( n-1, k-1 );
end;
end {of gen};

procedure neg (* n,k : integer *);
begin
if (k > 0) and (k < n) then begin
  gen( n-1, k-1 );
  if k = 1 then swap( n-1, n ) else swap( k-1, n );
  neg( n-1, k );
end;
end {of neg};

begin
  write('Enter n,k: ');  readln(n,k);
  for i := 1 to k do a[i] := 1;
  for i := k+1 to n do a[i] := 0;
  cnt := 0;
  PrintIt( 0, 0 );
  gen(n,k);
end.

