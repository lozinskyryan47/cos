/*========================================================================*/
/* C program for distribution from the Combinatorial Object Server.       */
/* Generates all numerical partitions of n whose largest part is k.       */
/* No input error checking.  Assumes 0 <= k <= n <= MAX.                  */
/* A simple modification will generate all partitions of n (see           */
/* comment at end of program.)                                            */
/* Algorithm is CAT (Constant Amortized Time).                            */
/* The program can be modified, translated to other languages, etc.,      */
/* so long as proper acknowledgement is given (author and source).        */
/* Programmer: Joe Sawada, 1997.                                          */
/* The latest version of this program may be found at the site            */
/* http://theory.cs.uvic.ca/inf/nump/NumPartition.html    or               */
/* http://theory.cs.uvic.ca/dis/programs.html                              */
/*========================================================================*/
                                                                            
#define MAX 100;

int  n,k;
int  p[100];

int min( int x, int y) {

	if (x<y) return x;
	return y;
}

void PrintIt(int t) {
	
	int i;

	for(i=1; i<=t; i++) printf("%d ", p[i]);
	printf("\n");
}

void P (int n, int k, int t) {

	int j;

	p[t] = k;
	if (n==k) PrintIt(t);
	for (j=min(k,n-k); j>=1; j--) P(n-k,j,t+1);
}

                                                   
void main() {
  
	printf("Enter n,k: ");  
	scanf("%d %d", &n ,&k );
  	P( n,k,1 );
  	/* NOTE: The call P( 2*n, n, 0 ) will produce all partitions of n. */ 
}


