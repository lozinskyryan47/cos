/*======================================================================*/
/* C program for distribution from the Combinatorial Object             */
/* Server. Generate combinations of a multiset in co-lex order.         */
/* This is the same version used in the book "Combinatorial Generation."*/
/* The program can be modified, translated to other languages, etc.,    */
/* so long as proper acknowledgement is given (author and source).      */  
/* Programmer: Joe Sawada, 1997.                                        */
/* The latest version of this program may be found at the site          */
/* http://theory.cs.uvic.ca/inf/mult/Multiset.html                       */
/*======================================================================*/

#include <stdio.h>

int a[100], num[100];
int T;

void PrintIt() {

	int i;
	for (i=0; i<=T; i++) printf("%d", a[i]);
	printf("\n");
}

int max(int i,int j) {

	if (i > j) return(i);
	return(j);
}

int min(int i,int j) {

	if (i < j) return(i);
	return(j);
}

void gen1( int k, int t, int n) {

	int i;
	
	if (k == 0) PrintIt();
	else {
		for(i=max(0,k-n+num[t]); i<= min(num[t], k); i++) {
			a[t] = i;
        		gen1(k-i, t-1, n-num[t]);
			a[t] = 0;
		}
	}
}

void gen2( int k, int t, int n) {

	int i;
	
	if (k == n) PrintIt();
	else {
		for (i=max(0,k-n+num[t]); i<= min(num[t], k); i++) {
			a[t] = i;
        		gen2(k-i, t-1, n-num[t]);
			a[t] = num[t];
		}
	}
}

void main() {

	int i,k,n;

	n = 0;
	printf("Enter t: "); scanf("%d", &T);
	printf("Enter k: "); scanf("%d", &k);
	for (i=0; i<= T; i++) {
		printf("Enter n(%d): ",i); scanf("%d", &num[i]);
		n += num[i];
	}
	printf("\n");
	
  
  	if (k <= n/2) {
     		for (i=0; i<=T; i++)  a[i]=0;
     		gen1( k, T, n ); 
  	}
  	else { 
     		for (i=0; i<=T; i++) a[i]=num[i];
     		gen2( k, T, n ); 
  	}
	printf("\n");
}


