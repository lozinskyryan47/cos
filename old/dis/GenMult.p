(*======================================================================*)
(* Pascal program for distribution from the Combinatorial Object        *)
(* Server. Generate permutations of a multiset in lexicographic order.  *)
(* This is the same version used in the book "Combinatorial Generation."*)
(* The program can be modified, translated to other languages, etc.,    *)
(* so long as proper acknowledgement is given (author and source).      *)  
(* Programmer: Joe Sawada, 1997.                                        *)
(* The latest version of this program may be found at the site          *)
(* http://theory.cs.uvic.ca/inf/mult/Multiset.html                       *)
(*======================================================================*)

(* This program:  Takes as input: n t n(0) n(1) ... n(t)
		and outputs all permutations in lex order *)

program GenMult(input, output);

var 

	P: array [0..100] of integer;	(* max size of perm = 100 *)
	a: array [0..20] of integer; 	(* represent n(i); *)
	N,t,i : integer;

procedure  printP;
var 
	j: integer;
begin
	for j:=N-1 downto 0 do write(P[j]+1:1);
	writeln;
end;

procedure Gen(n: integer); 
var
	j: integer;
begin
	if a[0] = n  then printP
	else begin 
		for j:=0 to t do begin 
			if a[j] > 0 then begin 
				P[n-1] := j;
				a[j] := a[j] - 1;
				Gen(n-1);
				a[j] := a[j] + 1;
				P[n-1] := 0;
			end;	
		end;	
 	end;	
end;

begin
	
	write('Enter n: ');
	readln(N);	
	write('Enter t: ');
	readln(t);	
	for i:=0 to t do begin
		write('Enter n(', i:1 , '): ');
		readln(a[i]);
	end;
	writeln;
	Gen(N);
	writeln;
end.

			
			
