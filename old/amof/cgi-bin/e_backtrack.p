{ File name: e_backtrack.p }
{ This is an include file for e_BigPent.p }
{ Copyright Frank Ruskey, 1995-1998.      }

function conv( nn : integer ) : char;
begin
    case nn of
     -1: conv := '.';
      0: conv := '.';
      1: conv := 'X';
      2: conv := 'I';
      3: conv := 'F';
      4: conv := 'Z';
      5: conv := 'W';
      6: conv := 'T';
      7: conv := 'U';
      8: conv := 'N';
      9: conv := 'Y';
     10: conv := 'V';
     11: conv := 'L';
     12: conv := 'P';
     13: conv := '.';
    end {case};
end {of conv};
  
procedure PrintSolution;
  label 9999;
  var 
    ch : char;
    bd : BoardNumber;
    pc : PieceNumber;
    i,j : integer;
    (* A : array [1..MAX_RC,1..MAX_RC] of integer; *)
  begin
    count := count + 1;
    if count > LIMIT then goto 9999;
    (* writeln( '<P>Solutions found:', count:5, '<BR>' ); *)
    (* writeln( '<TR><TD><PRE>' ); *)
    for pc := 1 to NUM_PIECES do
       for bd := 0 to RxC do
          if bd in Solution[pc]^.Position then
              A[bd mod ROW + 1, bd div ROW + 1] := pc;
    for i := 1 to ROW do begin
       for j := 1 to COL do begin
          if (A[i,j] < -1) or (A[i,j] > 13) then
             writeln( 'Oops ',i,j )
          else write( conv(A[i,j]) );  
       end;
       writeln;
    end; 
    (* write( '</PRE></TD></TR>' ); *)
    (*
    if sol_prompt then begin
      write( 'Do you want another solution (y/n)? ' );
      readln( ch );
      if ch in NO then exit( );
    end;
    *)
    writeln;
9999:
end {of PrintTheSolution};
    
procedure BackTrack ( k : BoardNumber );
label 9999;
var  pc : PieceNumber;
begin
  (* writeln( 'BT k = ',k ); *)
  if count > LIMIT then goto 9999;
  while k in TheBoard do k := k + 1;
  for pc := 1 to 12 do
     if pc in PieceAvail then begin
        (* writeln( '  pc = ',pc ); *)
        Solution[pc] := List[pc,k];
        PieceAvail := PieceAvail - [pc];
        while Solution[pc] <> nil do
        with Solution[pc]^ do begin
           if TheBoard * Position = [] then begin
              TheBoard := TheBoard + Position;
              if PieceAvail = []
                 then PrintSolution
                 else BackTrack( k + 1 );
              TheBoard := TheBoard - Position;
           end;
           Solution[pc] := Link;
        end {while};
        PieceAvail := PieceAvail + [pc];
    end;
9999:
end {of BackTrack};

procedure FindSolutions;
begin
  PieceAvail := [1..12];
  count := 0; 
  writeln(ROW:5, COL:5 ); {NEEDED FOR PentShowSolutions !!!}
  BackTrack( Min_Square );
end {of FindSolutions};

