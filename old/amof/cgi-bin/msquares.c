/* This file is called "msquares.c" and was written by Scott Lausch.  
Copyright Frank Ruskey and Scott Lausch, March 1998.
"Ownership": Frank Ruskey */

#include<stdio.h>

struct Node
	{
	int valu;
	struct Node *link;
	} Node;

typedef struct Node *NodePtr;

int LIMIT;

int n = 1, NN, nsq, count;
int used[101];
int X[11][11];
int colSum[11];
int rowSum[11];
struct Node *avail;
struct Node *NodeList[101];
int csum[11];
int nextR[11][11];
int nextC[11][11];
int checkRow[11][11];
int checkCol[11][11];

int pi[2][11];
int tempn;
int done = 0;
int pass = 1;   /* pass number */
int diag3[6] = { 2, 5, 8, 4, 5, 6 };
int diag4[][8] = { { 1, 9, 10, 14, 4, 8, 7, 15 },
{ 1, 9, 13, 11, 16, 8, 4, 6 }, { 1, 11, 13, 9, 16, 6, 4, 8 },
{ 1, 13, 9, 11, 16, 4, 8, 6 }, { 1, 13, 11, 9, 16, 4, 6, 8 },
{ 1, 14, 10, 9, 16, 3, 7, 8 }, { 1, 14, 9, 10, 15, 4, 7, 8 },
{ 1, 15, 7, 11, 14, 2, 10, 8 }, { 1, 15, 11, 7, 14, 2, 6, 12 },
{ 2, 6, 14, 12, 13, 11, 3, 7 }, { 2, 6, 16, 10, 15, 9, 3, 7 },
{ 2, 9, 13, 10, 15, 8, 4, 7 }, { 2, 10, 14, 8, 13, 7, 3, 11 },
{ 2, 10, 16, 6, 15, 5, 3, 11 }, { 2, 13, 9, 10, 15, 4, 8, 7 },
{ 2, 13, 10, 9, 16, 3, 8, 7 }, { 3, 15, 5, 11, 14, 4, 10, 6 },
{ 3, 15, 9, 7, 14, 4, 6, 10 }, { 5, 11, 3, 15, 10, 6, 14, 4 },
{ 6, 2, 10, 16, 9, 15, 7, 3 }, { 6, 2, 12, 14, 11, 13, 7, 3 },
{ 6, 10, 16, 2, 11, 5, 3, 15 }, { 7, 9, 12, 6, 11, 5, 8, 10 },
{ 7, 9, 13, 5, 12, 4, 8, 10 }, { 7, 11, 1, 15, 10, 8, 14, 2 },
{ 7, 12, 9, 6, 11, 8, 5, 10 }, { 7, 13, 9, 5, 12, 8, 4, 10 },
{ 7, 15, 9, 3, 10, 4, 6, 14 }, { 7, 15, 11, 1, 12, 2, 6, 14 },
{ 8, 10, 12, 4, 9, 7, 5, 13 }, { 8, 10, 14, 2, 11, 7, 3, 13 },
{ 9, 7, 3, 15, 6, 10, 14, 4 }, { 9, 10, 1, 14, 7, 8, 15, 4 },
{ 9, 10, 2, 13, 8, 7, 15, 4 }, { 9, 11, 1, 13, 8, 6, 16, 4 },
{ 9, 11, 13, 1, 8, 6, 4, 16 }, { 9, 13, 10, 2, 7, 3, 8, 16 },
{ 9, 13, 11, 1, 8, 4, 6, 16 }, { 9, 14, 1, 10, 8, 15, 4, 7 },
{ 9, 14, 10, 1, 8, 3, 7, 16 }, { 10, 2, 6, 16, 5, 15, 11, 3 },
{ 10, 2, 8, 14, 7, 13, 11, 3 }, { 10, 6, 16, 2, 7, 9, 3, 15 },
{ 10, 9, 1, 14, 7, 8, 16, 3 }, { 10, 9, 2, 13, 8, 7, 16, 3 },
{ 10, 9, 13, 2, 7, 8, 4, 15 }, { 10, 13, 9, 2, 7, 4, 8, 15 },
{ 10, 14, 1, 9, 7, 15, 4, 8 }, { 10, 14, 9, 1, 8, 4, 7, 15 },
{ 11, 13, 9, 1, 6, 4, 8, 16 }, { 11, 7, 1, 15, 6, 12, 14, 2 },
{ 11, 9, 13, 1, 6, 8, 4, 16 }, { 11, 9, 1, 13, 6, 8, 16, 4 },
{ 11, 15, 5, 3, 6, 4, 10, 14 }, { 11, 15, 7, 1, 8, 2, 10, 14 },
{ 12, 7, 6, 9, 8, 11, 10, 5 }, { 12, 6, 7, 9, 8, 10, 11, 5 },
{ 12, 6, 14, 2, 7, 11, 3, 13 }, { 13, 7, 5, 9, 8, 12, 10, 4 },
{ 13, 9, 1, 11, 4, 8, 16, 6 }, { 13, 9, 2, 10, 3, 7, 16, 8 },
{ 13, 10, 2, 9, 4, 7, 15, 8 }, { 13, 11, 1, 9, 4, 6, 16, 8 },
{ 14, 2, 8, 10, 3, 13, 11, 7 }, { 14, 2, 12, 6, 3, 13, 7, 11 },
{ 14, 9, 1, 10, 3, 8, 16, 7 }, { 14, 9, 10, 1, 15, 8, 7, 4 },
{ 14, 10, 1, 9, 4, 8, 15, 7 }, { 15, 7, 1, 11, 2, 12, 14, 6 },
{ 15, 7, 3, 9, 4, 10, 14, 6 }, { 15, 11, 1, 7, 2, 8, 14, 10 },
{ 15, 11, 3, 5, 4, 6, 14, 10 }, { 16, 2, 6, 10, 3, 15, 11, 5 },
{ 16, 2, 10, 6, 3, 15, 7, 9 } };
int diag5[2][10] = { { 20, 16, 13, 10, 6, 4, 14, 13, 12, 22},
		     { 19, 17, 13, 9, 7, 6, 12, 13, 14, 20} };
int bord6[] = { 1, 35, 34, 5, 30, 6, 4, 29, 9, 27, 36, 7, 32, 3, 2, 31, 10,
		28, 8, 33 };
int bord7[] = { 46, 1, 2, 3, 41, 42, 40, 5, 6, 43, 38, 39, 4, 8, 9, 47, 48,
		49, 10, 11, 12, 7, 44, 45 };
int bord8[] = { 1, 63, 62, 4, 5, 59, 58, 8, 9, 10, 54, 12, 52, 51, 64, 7,
		6, 60, 61, 3, 2, 57, 14, 13, 53, 11, 55, 56 };
int bord9[] = { 77, 1, 2, 3, 4, 72, 71, 70, 69, 6, 7, 8, 73, 66, 67, 68, 5,
		12, 11, 10, 78, 79, 80, 81, 13, 14, 15, 16, 9, 74, 75, 76 };
int bord10[] = { 2, 100, 98, 4, 5, 6, 94, 93, 92, 11, 10, 12, 88, 14, 87, 86,
		 17, 83, 99, 9, 8, 7, 95, 96, 97, 3, 1, 90, 18, 84, 16, 15,
		 87, 13, 89, 91 };
char junk[80];

int odd(int n)
	{
	return (n % 2 == 1);
	}

void PrintList()
	{
	struct Node *p;

	printf("avail list: ");
	p = avail;
	while (p != 0)
		{
		printf("%3d", p->valu);
		p = p->link;
		}
	printf("\n");
	}

void InitNext()
	{
	int r, c;

	for (r = 1; r <= tempn; r++)
		{
		for (c = 1; c <= tempn; c++)
			{
			nextR[r][c] = r;
			nextC[r][c] = c + 1;
			checkRow[r][c] = 0;  /* false */
			checkCol[r][c] = 0;  /* false */
			}
		}
	nextR[1][tempn - 1] = 2;            nextC[1][tempn - 1] = 1;
	nextR[tempn][tempn - 1] = n + 1;    nextC[tempn][tempn - 1] = 1;
	checkRow[1][tempn - 1] = 1;         checkRow[tempn][tempn - 1] = 1;
	checkCol[tempn - 1][1] = 1;         checkCol[tempn - 1][tempn] = 1;
	for (r = 2; r <= tempn - 2; r++)
		{
		nextR[r][tempn] = r + 1;    nextC[r][tempn] = 1;
		}
	nextR[tempn - 1][tempn] = tempn;    nextC[tempn - 1][tempn] = 2;
	for (r = 2; r <= tempn - 1; r++)
		{
		nextR[r][r-1] = r;      nextC[r][r-1] = r+1;
		checkRow[r][tempn] = 1; checkCol[tempn][r] = 1;
		}
	for (r = 2; r <= tempn-1; r++)
		{
		nextR[r][tempn-r] = r;      nextC[r][tempn-r] = tempn-r+2;
		}
	if (!odd(tempn))
		{
		r = tempn/2;
		nextR[r][r-1] = r;      nextC[r][r-1] = r+2;
		r++;
		nextR[r][r-2] = r;      nextC[r][r-2] = r+1;
		}
	}

void insert (struct Node *q, int x)
	{
	if (q == 0)
		{
		NodeList[x]->link = avail;
		avail = NodeList[x];
		}
	else
		{
		NodeList[x]->link = q->link;
		q->link = NodeList[x];
		}
	}

void delete(struct Node *q, struct Node *p)
	{
	if (p == 0)
		{
		return;
		}
	if (q == 0)
		avail = p->link;
	else
		q->link = p->link;
	}

void PrintIt(int rr, int cc)
	{
	int r, c, sum1, sum2;

	count++;
	printf("\n<TR><TD><CENTER><TABLE BORDER = 1");
    printf(" CELLSPACING=2 CELLPADDING=2>");
	for (r = 1; r <= n; r++)
		{
		printf("\n<TR>");
		for (c = 1; c <= n; c++)
			{
			if (X[r][c] > 0)
				{
				printf("<TD ALIGN=center>%d</TD>", X[r][c]);
				}
			else
				printf("<TD>  .</TD>");
			}
		}
	printf("\n</TABLE></CENTER></TD></TR>");
	if (count >= LIMIT) { done = 1; }
	}

int SeemsOk(int r, int c, int rsum, int rowCheck, int colCheck)
	{
	int ok = 1;

	if (checkRow[r][c])
		ok = (rowCheck == 0);
	else
		ok = (rowCheck > 0);
	if (ok && checkCol[r][c])
		ok = ok && (colCheck == 0);
	else
		ok = ok && (colCheck > 0);
	return ok;
	}

void Swap(int a, int b, int dim)
	{
	int temp, i;

	if (dim == 0)
		{
		temp = X[1][a+1];
		X[1][a+1] = X[1][b+1];
		X[1][b+1] = temp;

		temp = X[n][a+1];
		X[n][a+1] = X[n][b+1];
		X[n][b+1] = temp;
		}
	else
		{
		temp = X[a+1][1];
		X[a+1][1] = X[b+1][1];
		X[b+1][1] = temp;

		temp = X[a+1][n];
		X[a+1][n] = X[b+1][n];
		X[b+1][n] = temp;
		}

	temp = pi[dim][a];
	pi[dim][a] = pi[dim][b];
	pi[dim][b] = temp;

	}

int factorial(int a)
	{
	int i, result;

	result = 1;
	for (i = 1; i <= a; i++)
		{
		result *= i;
		}
	return result;
	}

void Border(void)
	{
	int add, displace, r, c, startsize, pass;
	int border_add, index, numperms, j, k, s, dim;

	/* move inside and adjust values up */
	if (n < 8)
		add = 2*n - 2;
	else if (n == 8)
		add = 24;
	else if (n == 9)
		add = 28;
	else if (n == 10)
		add = 42;

	if (n%2 == 1)
		startsize = 5;
	else
		startsize = 4;
	displace = (n - startsize)/2;

	for (r = startsize; r >= 1; r--)
		{
		for (c = startsize; c >= 1; c--)
			{
			X[r+displace][c+displace] = add + X[r][c];
			}
		}

	/* place adjusted borders */
	pass = 1;
	for (j = displace; j >= 1 ; j--)
		{
		border_add = 0;
		if (n == 6 || n == 7) border_add = 0;
		if (n == 8)
			{
			if (displace == 2)
				border_add = 14;
			else
				border_add = 0;
			}
		if (n == 9)
			if (displace == 2)
				border_add = 16;
			else
				border_add = 0;
		if (n == 10)
			if (displace == 3)
				border_add = 32;
			else if (displace == 2)
				border_add = 18;
			else
				border_add = 0;
		displace--;
		index = 0;
		for (c = displace + 1; c <= n - displace; c++)
			{
			if (startsize == 4)
				{
				if (pass == 1)
					{
					X[displace+1][c] = bord6[index] + border_add;
					}
				else if (pass == 2)
					X[displace+1][c] = bord8[index] + border_add;
				else if (pass == 3)
					{
					X[displace+1][c] = bord10[index] + border_add;
					}
				}
			else
				{
				if (pass == 1)
					X[displace+1][c] = bord7[index] + border_add;
				else if (pass == 2)
					X[displace+1][c] = bord9[index] + border_add;
				}
			index++;
			}
		for (r = displace+2; r <= n - displace - 1; r++)
			{
			if (startsize == 4)
				{
				if (pass == 1)
					{
					X[r][n-displace] = bord6[index] + border_add;
					}
				else if (pass == 2)
					X[r][n-displace] = bord8[index] + border_add;
				else if (pass == 3)
					X[r][n-displace] = bord10[index] + border_add;
				}
			else
				{
				if (pass == 1)
					X[r][n-displace] = bord7[index] + border_add;
				else if (pass == 2)
					X[r][n-displace] = bord9[index] + border_add;
				}
			index++;
			}
		for (c = n - displace; c >= displace + 1; c--)
			{
			if (startsize == 4)
				{
				if (pass == 1)
					{
					X[n-displace][c] = bord6[index] + border_add;
					}
				else if (pass == 2)
					X[n-displace][c] = bord8[index] + border_add;
				else if (pass == 3)
					X[n-displace][c] = bord10[index] + border_add;
				}
			else
				{
				if (pass == 1)
					X[n-displace][c] = bord7[index] + border_add;
				else if (pass == 2)
					X[n-displace][c] = bord9[index] + border_add;
				}
			index++;
			}
		for (r = n - displace - 1; r >= displace + 2; r--)
			{
			if (startsize == 4)
				{
				if (pass == 1)
					{
					X[r][displace+1] = bord6[index] + border_add;
					}
				else if (pass == 2)
					X[r][displace+1] = bord8[index] + border_add;
				else if (pass == 3)
					X[r][displace+1] = bord10[index] + border_add;
				}
			else
				{
				if (pass == 1)
					X[r][displace+1] = bord7[index] + border_add;
				else if (pass == 2)
					X[r][displace+1] = bord9[index] + border_add;
				}
			index++;
			}
		pass++;
		}
	PrintIt(n, n+1);

	/* permute pairs in the outer border */
	pi[0][0] = pi[1][0] = 0;
	for (index = 1; index <= n-2; index++)
		{
		pi[0][index] = pi[1][index] = index;
		}
	numperms = factorial(n-2);
	dim = 0; index = 2;
	while (count < LIMIT)
		{
		if (index >= numperms)
			{
			for (index = 1; index <= n-2; index++)
				pi[0][index] = index;
			index = 0;
			dim = 1;
			}
		/* 'next permutation' algorithm */
		k = n-3;
		while (pi[dim][k] > pi[dim][k+1]) k--;
		if (k == 0)
			index=numperms+1;
		else
			{
			j = n-2;
			while (pi[dim][k] > pi[dim][j]) j--;
			Swap(k, j, dim);
			r = n-2; s = k + 1;
			while (r > s)
				{
				Swap(r, s, dim);
				r--; s++;
				}
			}
		PrintIt(n, n+1);
		index++; dim = 0;
		if (done) return;
		}
	}

void back(int r, int c, int rsum)
	{
	int rowCheck, colCheck, pval;
	struct Node *p; struct Node *q;

	if (avail == 0)
		{
		if (n > 5)
			{
			Border();
			done = 1;
			return;
			}
		PrintIt(n, n+1);
		if (done) return;
		}
	else
		{
		p = avail; q = 0;
		while (p != 0)
			{
			pval = p->valu;
			if (!used[pval])
				{
				rowCheck = NN-rsum-pval;
				colCheck = NN-csum[c]-pval;
				if (SeemsOk(r, c, rsum, rowCheck, colCheck))
					{
					used[pval] = 1;
					X[r][c] = pval;
					csum[c] = csum[c]+pval;
					delete(q, p);
					if (nextR[r][c] == r) /* stay in row? */
						{
						back(nextR[r][c], nextC[r][c], rsum+pval);
						}
					else
						{
						back(nextR[r][c], nextC[r][c], rowSum[r+1]);
						}
					if (done) return;
					insert(q, pval);
					csum[c] = csum[c]-pval;
					X[r][c] = 0;
					used[pval] = 0;
					}
				}
			q = p; p = p->link;
			}
		}
	}

void Initialize(void)
	{
	int i;

	if (n <= 5) tempn = n;
	else if (n%2 == 0) tempn = 4;
	else if (n%2 == 1) tempn = 5;

	NN = tempn*(tempn*tempn+1)/2; nsq = tempn*tempn;
	for (i = 1; i <= nsq; i++) used[i] = 0;
	for (i = 1; i <= tempn; i++)
		{
		if (tempn == 3)
			X[i][i] = diag3[i - 1];
		else if (tempn == 1)
			X[i][i] = 1;
		else if (tempn == 4 || tempn%2 == 0)
			X[i][i] = diag4[pass - 1][i - 1];
		else if (tempn == 5 || tempn%2 == 1)
			X[i][i] = diag5[pass - 1][i - 1];
		used[X[i][i]] = 1;
		colSum[i] = X[i][i];
		rowSum[i] = X[i][i];
		}
	for (i = 1; i <= tempn; i++)
		{
		if (tempn == 3)
			X[i][tempn-i+1] = diag3[i + 2];
		else if (tempn == 1)
			X[i][tempn-i+1] = 1;
		else if (tempn == 4)
			X[i][tempn-i+1] = diag4[pass - 1][i + 3];
		else if (tempn == 5)
			X[i][tempn-i+1] = diag5[pass - 1][i + 4];
		used[X[i][tempn-i+1]] = 1;
		colSum[tempn-i+1] += X[i][tempn-i+1];
		rowSum[i] += X[i][tempn-i+1];
		}
	avail = 0;
	for (i = nsq; i >= 1; i--)
		{
		NodeList[i] = (struct Node *)malloc(sizeof(struct Node));
		NodeList[i]->valu = i; NodeList[i]->link = 0;
		if (!used[i]) insert(avail, i);
		}
	/* fixes the double-count of the middle element */
	if (odd(tempn))
		{
		colSum[(int)tempn/2+1] = colSum[(int)tempn/2+1]/2;
		rowSum[(int)tempn/2+1] = rowSum[(int)tempn/2+1]/2;
		}
	for (i = 1; i <= tempn; i++) csum[i] = colSum[i];
	}

int main(int argc, char *argv[])
	{
	int i;

	if (argc != 3) return(0);
	n = atoi(argv[1]);
	LIMIT = atoi(argv[2]);
	if (n < 1 || n == 2 || n > 10) return(0);
	count = 0;
	do
		{
		Initialize();
		InitNext();
 		back(1, 2, rowSum[1]);

		pass++;
		for (i = 1; i <= 100; i++)
			{
			if (NodeList[i] != 0)
				free(NodeList[i]);
			}
		}
	while (!done && n != 3 && n != 1);
	if (count == LIMIT)
		{
		return(1);
		}
	else
		return(0);
	}
