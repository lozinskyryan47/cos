#!/usr/bin/perl
use Benchmark;

# This file called "e_pent.pl.cgi"
# Written and copyrighted by Frank Ruskey, June 1996.
# Some code taken from COS, but most everything re-written for ECOS/AMOF.
# Not to be distributed without permission.
# "Ownership": Frank Ruskey 

require "e_common.pl";

$ShowPentominoBIN = "./PentShow.exe";
$PentominoBIN = "./e_BigPentApollo.exe";

$MAXrow =  8;
$MAXcol = 10;

MAIN:
{
# Read in all the variables set by the form
   if (&ReadParse(*input)) {
      &ProcessForm;
   }
   &CloseDocument( 'pent' );
}

sub SmallPent {
   if ($input{'dimensions'} eq "6by10") { $n = 6;  $m = 10; }
   if ($input{'dimensions'} eq "5by12") { $n = 5;  $m = 12; }
   if ($input{'dimensions'} eq "4by15") { $n = 4;  $m = 15; }
   if ($input{'dimensions'} eq "3by20") { $n = 3;  $m = 20; }
   # print "SmallPent<BR>";
   print "Solutions to the pentomino problem with <I>n</I> = $n";
   print " rows and <I>m</I> = $m columns.<BR>";
   $file = "./e_BigPent.temp";
    open( FILE, ">$file" ) || die print "Can't open $file:$!\n";
    print FILE "9 21 10 \n";  # 10 solutions is the limit
    for ($i = 1; $i <= 9; $i++) {
    	for ($z = 1; $z <= 21; $z++){
    	     if ((( 9 - $i - $n ) >= 0) || 
				( (21 - $z - $m ) >= 0 ) ||
				( $i eq 1 )) { print FILE "-1 ";}
    	     else { print FILE " 0 "; }
    	}
        print FILE "\n";
    }
    close( FILE );
   print "<P>Only first 10 solutions output.<p>";
   $retval = system "nice -10 $PentominoBIN | $ShowPentominoBIN $outformat";
}

sub BigPent {
   print "Solutions to the pentomino problem with your custom shape.<BR>";
   $file = "./e_BigPent.temp";
    open( FILE, ">$file" ) || die "Can't open $file:$!\n";
    $count = 0;
    print FILE "9 17 10 \n";  # 10 solutions is the limit
    for ($i = 1; $i <= 9; $i++) {
        print FILE "-1 ";
		for ($j = 1; $j <= 16; $j++) {
	    	if ($input{"pent$i.$j"}) { 
				++$count; print FILE " 0 "; }
		    else { print FILE "-1 "; }
		}
		print FILE "\n";
    }
    close( FILE );
	if ($count > 60) {  $diff = $count-60;
      &ErrorMessage("Too many squares!  You pressed $count; please take away $diff."); 
      return; }
   if ($count < 60) {  $diff = 60-$count;
      &ErrorMessage("Too few squares!  You pressed $count; please press $diff more."); 
      return; }
   print "<P>Only first 10 solutions output.<p>";
   $retval = system "nice -10 $PentominoBIN | $ShowPentominoBIN $outformat";
}


sub ProcessForm {

   &OutputHeader('Pentominoes Output','pent');  # from common.pl
   $outformat = 0;
   if ($input{'outText'}) { $outformat = 1; }
   if ($input{'outGifs'}) { $outformat = $outformat | 2; }
   if ($outformat eq 0) { &OutError;  return; }

   if ($input{'dimensions'} eq "custom") {
      &BigPent;
   } else {
      &SmallPent;
   } 

}

