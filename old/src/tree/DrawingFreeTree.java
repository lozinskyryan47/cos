/*Code by Michael Miller*/

/*Takes in two arguements, output filename and a string of the level order representation of the three
 *ex. java Buffer Test outputFileName.png "0 1 2 3 3 4 4 5 5"
 *File type needs to be .png*/

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;

public class DrawingFreeTree {

	public static void main(String[] args) {

/*The first arguement is the file name, the second argument is the string representing the tree*/
		String levelOrder = args[1];
		String filename = args[0];

/*Send the level order for processing*/
		int[][] testSet = TreeMatrix.treeArray(levelOrder);

/*Extract the max depth in */
		int xfactor = testSet[3][0];
		int yfactor = testSet[2][0];
		int max = testSet[1][0];
		int parent = 0;
		int xspace = 25;
		int yspace = 40;
		int xdim = xfactor*xspace+30;
		int ydim = yfactor*yspace+5;

		testSet[1][0] = 0;
		testSet[2][0] = 0;
		testSet[3][0] = 0;

		BufferedImage img = new BufferedImage(xdim, ydim, BufferedImage.TYPE_INT_RGB);

		Graphics2D g = img.createGraphics();
/*set the back ground colour and fill in the background*/
		g.setColor(Color.WHITE);
		g.fillRect(0,0, img.getWidth(), img.getHeight());

/*set the colour for the tree and draw the tree iterativly using the processed matrix*/
		g.setColor(Color.BLACK);
/*we know the x and y limits so we can use those instead of the general maximum to gain some improvement in running the algorithm*/
		for(int z = 0; z<=yfactor; z++)
		{
			for(int x = 0; x<=xfactor; x++)
			{
				if(testSet[x][z] != 0)
				{
					g.fillRect(x*xspace+10, z*yspace, 5, 5);
				}
				if(z > 0)
				{
					int w =xfactor; 
					int done = 0;
					while(w >= 0 && done == 0 )
					{
						if(testSet[w][z-1] < testSet[x][z] && testSet[w][z-1] != 0)
						{
							parent = w;
							g.drawLine(x*xspace+12, z*yspace, parent*xspace+12, (z-1)*yspace);
							done = 1;
						}
						w--;
					}
				}
			}
		}
		g.dispose();

/*Save the image to the specified output file*/
		try{
			File destFile = new File(filename);
			String format = "PNG";
			ImageIO.write(img, format, destFile);
		}catch (IOException e){
		     System.out.println("Error with file");
		     System.exit(1);
        }
	}
}
