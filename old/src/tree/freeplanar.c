/*===============================================*/
/* Program: Generating free plane trees          */
/* Input: n                                      */
/* Output: Level sequences with n nodes          */
/* Programmer: Lara Wear                         */
/* Algorithm:  Joe Sawada, University of Victoria*/
/*	       March 22, 2002			             */
/*===============================================*/

#ifndef FREEPLANAR_C 
#define FREEPLANAR_C


#include <stdlib.h>
#include <math.h>
#include "ProcessInput.c"
#include "OutputTrees.c"
#include "LevelToParent.c"

#define FALSE 0
#define TRUE 1

static int a[MAX];
static int counter = 0;
int N; /* # nodes */

typedef char boolean;

void unicentroid (int t, int p, int s);
void bicentroid (int t, boolean flag);

int main(int argc, char *argv[])
{
      /* variables */
      int i;

      // Process command line args
      ProcessInput(argc,argv);
      if (out_format & 1) outputP = 1; /* main representation is parent array */
      if (out_format & 2) outputL = 1; /* alternate representation is level sequence */
	  if (out_format & 8) outputPNG = 1; /* draw the tree */
    // initialize array a to 0111... (a[0] not used)
      a[0] = 0; a[1] = 0;
      for (i = 2; i <= N; i++)
          a[i] = 1;

      /* for all values of N, generate unicentroidal trees */
      if (N == 1) {
	//	  PrintIt(N, &a[0], levelToParent(&a[0], N));
		  PrintIt(N, levelToParent(&a[0], N), &a[0]);
		  counter = 1;
	  }
      else unicentroid(2,1,1);

      /* for N even, also generate bicentroidal trees */
      if (!(N%2)) bicentroid(1,0);

      printf("</table><P>The total number of trees is: %4d<BR>\n",counter);

      return 0;
} /* main */

/* unicentroid - Generates unicentroidal trees with N nodes
 * Algorithm: Joe Sawada, University of Victoria
 * Inputs: t - # nodes, p - # trees for t nodes, s - size of current subtree from root
 * Outputs: none
*/
void unicentroid(int t, int p, int s) {
     int max;
     int j;
     if (t == N ) {
         if ((N-1)%p == 0) {
         //   PrintIt(N, &a[0], levelToParent(&a[0], N));
             PrintIt(N, levelToParent(&a[0], N), &a[0]);
			 counter++;
         }
     }
     else {
        if (s == (N-1)/2) {
          max = 1;
        }
        else
           max = a[t]+1;
        for (j = a[t-p+1]; j <= max; j++) {
            a[t+1] = j;
            if (j == a[t-p+1] && j == 1)
               unicentroid(t+1, p, 1);
            else if (j == a[t-p+1])
               unicentroid(t+1, p, s+1);
            else
               unicentroid(t+1, t, s+1);
        } /* for */
     } /*else */

} /* unicentroid */


/* bicentroid - Generates bicentroidal trees with N nodes (N even)
 * Algorithm: Joe Sawada, University of Victoria
 * Inputs: t - # nodes, flag - boolean flag
 * Outputs: none
*/

void bicentroid(int t, boolean flag) {
     int j;
     int min;
     if (t == N) {
     //   PrintIt(N, &a[0], levelToParent(&a[0], N));
         PrintIt(N, levelToParent(&a[0], N), &a[0]);
		 counter++;
     }
     else if (t == N/2) {
          a[t+1] = 1;
          bicentroid(t+1, 1);
     }
     else if (!flag) {
          if (t > N/2) min = 2;
          else min = 1;
          for (j = min; j <= a[t]+1; j++) {
              a[t+1] = j;
              bicentroid(t+1, 0);
          } /* for */
     }
     else {
          for (j = a[(t-N/2)+1]+1; j <= a[t]+1; j++) {
              a[t+1] = j;
              if (j == a[(t-N/2)+1] + 1) bicentroid (t+1,1);
              else bicentroid(t+1, 0);
          } /* for */
     } /* else */
} /* bicentroidal */


#endif
