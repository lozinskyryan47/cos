#define MAXNODES 50


int * levelToParent(int * levelSequence, int N)
{
      /* parent array representation of a labelled tree in preorder */
      int * parentArray = malloc(MAXNODES*sizeof(int));

      /* lastVisited[i] = label of last node visited a ith level */
      int lastVisitedThisLevel[MAXNODES] = {0};

      int currentLabel = 1;              /* label of current node */
      int numNodes = N;                  /* number of nodes */
      int currentLevel = 0;              /* level of current node */

      /* initialize root stored at index 1 (index 0 ignored) */
      levelSequence[1] = 0;  /* level sequence of root is zero */
      parentArray[1] = 0; /* parent of root is zero */
      lastVisitedThisLevel[0] = 1; /* label of root is 1 */

      /* for each non-root node in level sequence, find parent array representation */
      for (currentLabel = 2; currentLabel <= numNodes; currentLabel++) {

           /* get current level */
           currentLevel = levelSequence[currentLabel];

           /* set label of lastVisted node at this level */
           lastVisitedThisLevel[currentLevel] = currentLabel;

           /* set parent array representation for current node */
           parentArray[currentLabel] = lastVisitedThisLevel[currentLevel - 1];
      } // for

      /* retrun the parent array */
      return parentArray;
} // leveltoParent
