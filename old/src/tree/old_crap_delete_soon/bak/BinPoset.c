/* C program to produce a 2 by n grid poset.  The linear extensions */
/* of this poset correspond to binary trees and are counted by the  */
/* Catalan numbers */
/* Programmer: Frank Ruskey */

#include <stdio.h>

int n;

void main ( int argv, char **argc ) {
	int i;
	n = atoi( argc[1] );
	printf( "%d\n", 2*n );
	for (i=1; i<=n; ++i) printf( "%d   %d\n", 2*i-1, 2*i );
	for (i=1; i<n; ++i) printf( "%d   %d\n", 2*i-1, 2*i+1 );
	for (i=1; i<n; ++i) printf( "%d   %d\n", 2*i, 2*i+2 );
	printf( "%d\n", 0 );
}

