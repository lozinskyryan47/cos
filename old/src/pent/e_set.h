#ifndef __SET_H__
#define __SET_H__

#include <stdlib.h>
#include <string.h>

#define MAX_SET_CARD  256
#define eos (MAX_SET_CARD)   /* end of set marker */
#define INTERVAL_MARKER  (MAX_SET_CARD+1) /* interval marker */

typedef unsigned long SetElemType;
typedef long SetArrayElemType;

#define BITS_PER_WORD (sizeof(SetArrayElemType)*8)

#define ALL_BITS_SET  ((SetArrayElemType)-1)
#define SET_LENGTH (MAX_SET_CARD/BITS_PER_WORD)
typedef struct {
     SetArrayElemType setarray[SET_LENGTH];
} set;

set e_add_to_set(set s, SetElemType elem) {
    s.setarray[(size_t)elem / BITS_PER_WORD] |= 
		(SetArrayElemType)1 << ((size_t)elem % BITS_PER_WORD); 
	return s;
}

set e_union(set a, set b) { 
    set c;
    size_t i;

    for(i = 0; i < SET_LENGTH; i++)
        c.setarray[i] = a.setarray[i] | b.setarray[i];
    return c;
}

set e_diff(set a, set b) {
    set c;
    size_t i;

    for (i = 0; i < SET_LENGTH; i++)
        c.setarray[i] = a.setarray[i] & ~b.setarray[i];
    return c;      
}

set e_intersect(set a, set b) { 
    set c;
    size_t i;

    for (i = 0; i < SET_LENGTH; i++)
        c.setarray[i] = a.setarray[i] & b.setarray[i];
    return c;
}

int e_equivalent(set a, set b) { 
    size_t i;

    for (i = 0; i < SET_LENGTH; i++)
         if (a.setarray[i] != b.setarray[i]) return 0;
    return 1;
}

int e_inset(SetElemType elem, set s) { 
    return (s.setarray[(size_t)elem / BITS_PER_WORD] & 
	    ((SetArrayElemType)1 << ((size_t)elem % BITS_PER_WORD))) != 0;
}

set e_init() {
	set s = {{0}};
	size_t i;
    for (i = 0; i < SET_LENGTH; i++) { 
		s.setarray[i] = 0;
    }
	return s;
}

void e_add5(SetElemType elem1, SetElemType elem2, SetElemType elem3,
			SetElemType elem4, SetElemType elem5, set *st) {  
    set s;
	s = e_init();
	s = e_add_to_set(s, elem1);
	s = e_add_to_set(s, elem2);
	s = e_add_to_set(s, elem3);
	s = e_add_to_set(s, elem4);
	s = e_add_to_set(s, elem5);
	*st = e_union(*st, s);
}

int e_isempty(set s) {
	set st = e_init();
	return e_equivalent(s, st);
}

set e_copy( set a) {
	size_t i;
	set b = e_init();

    for (i = 0; i < SET_LENGTH; i++)
         b.setarray[i] = a.setarray[i];
	return b;
}

#endif 