program MultiComb( input, output );

const MAX_T = 100;

var
  num, a : array [0..MAX_T] of integer;
  n, i, k, t : integer;

procedure PrintIt;
var i : integer;
begin
  for i := 0 to t do write( a[i]:3 );  writeln;
end;

procedure gen1 ( k, t, n : integer );
var i : integer;
begin
  if k = 0 then PrintIt
  else
     for i := max(0,k-n+num[t]) to min(num[t],k) do begin
        a[t] := i;
        gen1( k-i, t-1, n-num[t] );
        a[t] := 0;
     end;
end {of gen1};

procedure gen2 ( k, t, n : integer );
var i : integer;
begin
  if k = n then PrintIt
  else
     for i := max(0,k-n+num[t]) to min(num[t],k) do begin
        a[t] := i;
        gen2( k-i, t-1, n-num[t] );
        a[t] := num[t];
     end;
end {of gen2};

begin
  n := 0;
  readln(t,k);
  for i := 0 to t do begin
     read( num[i] );  n := n + num[i];
  end;
  readln;
  
  if k <= n/2 then begin
     for i := 0 to t do a[i] := 0;
     gen1( k, t, n ); 
  end else begin
     for i := 0 to t do a[i] := num[i];
     gen2( k, t, n ); 
  end;  
end.


