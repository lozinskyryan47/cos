/* Author: Aaron Williams 

This program outputs the permutations of any multiset in cool-lex order.
The program stores a single permutation in a singly-linked list, and then
each successive permutation in O(1)-time while using O(1) additional pointers.

Input is read from stdin as follows: n e_1 e_2 ... e_n max where e_i <= e_{i+1} for all i. 

For example, if input.txt is a file containing the following single line
6 1 1 2 3 3 3 
then multicool < input.txt will output the cool-lex order for the permutations of {1,1,2,3,3,3}:
333211
133321
313321
etc 

The final value, max, limits the number of permutations that are output.
This value can be defaulted to INT_MAX by using 0, or by piping in a file that does not contain the value,
or by entering CTRL-D (Windows) on the command line.  Roll-over ensures that the program will visit
at most INT_MAX permutations.

*/
  
#include<stdlib.h>
#include<stdio.h>
#include<limits.h>

/* each node in the singly-linked list has a value v, and a next pointer n */
struct list_el {
   int v;
   struct list_el * n;
};
typedef struct list_el item;

void visit(item *b);
void coolmultilist(item *h, item *i);
void usage_error();
int numvisited, maxvisited;

/* reads the multiset into a singly-linked list with head h */
int main() {
   item *h;
   item *t;
   item *i;
   int c, n;
   
   i = NULL;
   h = NULL;
   scanf("%d",&n);
   for (c=1; c<=n; c++) {
      t = (item *)malloc(sizeof(item));
	  t->v = INT_MAX;
      scanf("%d",&(t->v));
      t->n = h;
      h = t;
	  if(h->n && h->n->v > h->v) {
	     usage_error();
	  }
	  if(c==2) {
         i = h;
	  }	  
   }   
   
   maxvisited = INT_MAX;
   scanf("%d",&maxvisited);
   
   numvisited = 0;
   if(i) {
      coolmultilist(h,i);
   } else {
      visit(h);
   }
   
   /* You need to free the memory in the singly-linked list (or terminate the program) */
   return 1;
}

/* visits the permutation with head b; by default the permutation is simply printed to stdout */
void visit(item *b)
{
   item *y;
   y = b;
   while(y) {
      printf("%d ", y->v);
      y = y->n ;
   }
   printf("\n");
   if (++numvisited == maxvisited) exit(0);
}

/* looplessly generate all of the possible permutations */
void coolmultilist(item *h, item *i)
{
   item *j;   
   item *t;   
   item *s;   
   
   visit(h);   
   j = i->n;   
   while(j->n || j->v < h->v) { 
      if (j->n && i->v >= j->n->v) {
         s = j; }
	  else {
	     s = i;
	  }
	  t = s->n;
	  s->n = t->n;
	  t->n = h;
      if(t->v < h->v) {
         i = t;
	  }
      j = i->n;
	  h = t;
      visit(h);
   }
}

/* see description at the start of this file for usage information */
void usage_error()
{
   printf("usage: 'coolmulti' and then 'n e_1 e_2 ... e_n' with e_i <= e_{i+1} using stdin\n");
   exit(0);
}
