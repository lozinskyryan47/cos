/* Modified Knuth Gray code for set partitions.         */
/* Each RG function differs in exactly one position.    */
/* Also includes an option for generation in lex order. */
/* -n num    the number of elements [n]       */
/* -k num    the number of blocks (optional)  */
/* -g        Gray code order (otherwise lex)  */
/* -R        generate RG functions            */
/* -C        generate rooks on chessboard     */
/* -S        generate subset lists            */
/* -G        generate Gray code changes       */
/* -l num    max number of objects generated  */
/* programmer: Frank Ruskey, Sept. 15, 1995   */
 
#include <stdio.h>
#include <string.h>
 
#define MAX    100
#define TRUE     1
#define FALSE    0
#define max(x,y) ( x<y ? y : x )
#define min(x,y) ( x<y ? x : y )
#define LIMIT_ERROR  -1
#define ICON     printf("ico/"); 

int     a[MAX];
int     N, K, M, i, count, LIMIT;
int     rg_out, chess_out, sub_out, gray_out, first, gray_order;
 
void PrintIt (bit, newvalue)
int bit, newvalue;

{ int i, j, pos;
  static int gray;
  ++count;
  if (count > LIMIT) { exit(LIMIT_ERROR); }
  if (rg_out) {
     printf( "<TD ALIGN=CENTER>" );
     for (i=1; i<=N; ++i) 
       if (i==gray) printf("<FONT COLOR=00FF00>%d</FONT>",a[i]);
       else printf( "%d", a[i] );  
     printf( "<BR></TD>\n" );
  }
  if (sub_out) { /* this is Theta(n^2), could be faster. */
     printf( "<TD ALIGN=CENTER>{" );
     for (j=0; j<N; ++j) {
       first = TRUE;
       for (i=1; i<=N; ++i)
         if (a[i] == j) { 
            if (first && j>0) printf( "},{" );  first = FALSE;
            printf( "%d",i ); 
         }
     }
     printf( "}<BR></TD>\n" );
  }
  if (gray_out) {
    if(bit!=-1) {
      printf("<TD ALIGN=CENTER>(%d,%d)<BR></TD>\n",bit,newvalue);
    } 
    else {
      printf("<TD ALIGN=CENTER><BR></TD>\n");
    }
  }
  if (chess_out) {
     printf( "<TD ALIGN=CENTER>" );
     printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
     for (j=1; j<=N; ++j) {
        printf("<TR>");
        /* first = TRUE; */
        pos = 0;
        for (i=j-1; i>0 && pos==0; --i) if (a[i] == a[j]) pos = i;
        for (i=1; i<j; ++i) { 
           printf("<TD><IMG SRC="); ICON 
           if (i == pos) printf("greenRook.gif></TD>\n"); 
           else printf("green.gif></TD>\n");
        }
        printf("<TD><IMG SRC="); ICON  printf("red.gif></TD></TR>\n");
     }
     printf("</TABLE></TD>\n" );
  }
  printf("</TR>\n");
  gray = bit;
}

void ProcessInput (int argc, char *argv[])
{  int i;
   rg_out = chess_out = sub_out = gray_order = FALSE;
   N = 0;  M = 0;  K = 0;
   for (i=1; i<argc; i++) {
       if (strcmp(argv[i], "-n") == 0) {
          if (i+1 < argc) { N = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-k") == 0) {
          if (i+1 < argc) { K = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-m") == 0) {
          if (i+1 < argc) { M = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-g") == 0) gray_order = TRUE;
       else if (strcmp(argv[i], "-R") == 0) rg_out = TRUE;
       else if (strcmp(argv[i], "-C") == 0) chess_out = TRUE;
       else if (strcmp(argv[i], "-S") == 0) sub_out = TRUE;
       else if (strcmp(argv[i], "-G") == 0) gray_out = TRUE;
       else if (strcmp(argv[i], "-l") == 0) {
          if (i+1 < argc) { LIMIT = atoi(argv[++i]); }
       }
       else printf("Spurious input data: %s\n", argv[i]);
   }
}

void genLex( int l, int m )
{ int i;
   if (l > N) PrintIt(-1);
   else {
       for (i=0; i<=m; ++i) {
          a[l] = i;
          genLex( l+1, m );
       }
       if (m < K-1) {
          a[l] = m+1;
          genLex( l+1, m+1 );
       }
   }
}

void genKnuth ( int l, int m ) /* global N */
{ int i;
  if (l <= N) {
   if (a[l]==0) {
      genKnuth( l+1, m);
      for (i=1; i<=m; ++i) { PrintIt(l,i);  a[l] = i; genKnuth( l+1, m ); }
      PrintIt(l,m+1);  a[l] = m+1;  genKnuth( l+1, m+1 );
   } else {
      if (a[l] == m) { 
         genKnuth( l+1, m );
         PrintIt(l,m+1);  a[l] = m+1;  genKnuth( l+1, m+1 );
         for (i=m-1; i>=0; --i) { PrintIt(l,i);  a[l] = i;  genKnuth( l+1, m ); }
      } else { 
	 a[l] = m+1;    
         genKnuth( l+1, m+1 );
         for (i=m; i>=0; --i) { PrintIt(l,i);  a[l] = i;  genKnuth( l+1, m ); }
   }  } 
  }
}

void main ( int argc, char *argv[] )
{
  a[0] = 0;  a[1] = 0;
  ProcessInput( argc, argv );
  count = 0; 
  if (gray_order) {
    genKnuth( 2,0 );  PrintIt(-1); 
  }else genLex( 2,0 );
  printf("</table><br>Total number of Partitions = %d<BR>\n",count);
  exit(0);
}
