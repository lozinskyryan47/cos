#include <stdio.h>

#define NUMOFDISKS 8
#define MAXLAYERS 9
#define IMGDIR "ico/"

int pos[NUMOFDISKS];


/* right now, the function can only handle the maximum of 8 disks
   i.e. n < 9
*/
void Arrange (int (*pos)[], int n)
{
    int i, p0, p1, p2;
    int peg0[MAXLAYERS], peg1[MAXLAYERS], peg2[MAXLAYERS];

    for (i = 0; i <= n; i++) {
	peg0[i] = peg1[i] = peg2[i] = -1;
    }

    p0 = p1 = p2 = 0;
    for (i = (n-1); i >= 0; i--) {
	switch ((*pos)[i]) {
	case 0 : peg0[p0] = i;
	    p0++;
	    break;
	case 1 : peg1[p1] = i;
	    p1++;
	    break;
	case 2 : peg2[p2] = i;
	    p2++;
	    break;
	default : break;
	}
    }

    printf("<table border=0 cellspacing=0 cellpadding=0>\n");
    for (i = n; i >= 0; i--) {
	printf("<tr>\n");
	printf("<td><img src=\"%sdisk%1d.gif\"></td>",IMGDIR,peg0[i]+1);
	printf("<td><img src=\"%sdisk%1d.gif\"></td>",IMGDIR,peg1[i]+1);
	printf("<td><img src=\"%sdisk%1d.gif\"></td>",IMGDIR,peg2[i]+1);
	printf("</tr>\n");
    }
    printf("</table>\n");
}

