/****given a value for the base of each digit in a string, and output
mode specified by ProcessInput, this program prints out the product
space in lex order *****/

#include <stdio.h>
#include "ProcessInput.c"

/*********globals*********/
int bitstring = 0;
int list = 0;
int A[MAX];

/************local functions**********/

/*prints out the generated number according to the output format*/
/*which can only be in bitstring representation*/
void printit()
{
  int i;
  int j;
  int beg = 1;

  printf("<TR>");
  if(bitstring) {
    printf("<TD ALIGN=CENTER>");
    for(i=0;i<N;i++)
      printf("%d",A[i]);
    printf("<BR></TD>\n");
  }
  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for(i=N-1; i>=0; i--) {
      if(A[i]) {
	for(j=1; j<=A[i]; j++) {
	  printf("%s%d",beg ? "" : ",",N-i);
	  beg = 0;
        }
      }
    }
    printf("}<BR></TD>\n");
  }
  printf("</TR>");
  LIMIT--; /*count down the number of printable objects*/
}

/*recursively generates the array A*/
void prod(k)
int k;
{
  int i;

  if(!LIMIT)
    return;
  if(k>=N)
     printit();
  else {
     for(i=0;i<lambda[k+1];i++) {
	A[k] = i;
	prod(k+1);
     }
  }
}

/***********main program********/

int main(int argc, char **argv)
{

  int count;

  ProcessInput(argc, argv);
  count = LIMIT;

  if(out_format & 1)
    bitstring = 1;
  if(out_format & 2)
    list = 1;

  prod(0);
  printf("</TABLE><BR>Total number of products = %d<BR>\n",count-LIMIT);
  if(LIMIT)
    return(0);
  else
    return(-1);
}
