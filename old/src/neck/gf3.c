/* Program to enumerate all irreducible and/or primitive polynomials 
   over GF(3). 
   Gilbert Lee
*/

#include <stdio.h>
#include <stdlib.h>

typedef unsigned long long ULL;  
/* Change MAX to size of unsigned long long */
#define MAX 64
#define MAX_ITERATIONS 10000

ULL ONE = (ULL)1;
ULL TWO = (ULL)2;
ULL counts[3][3][3];
ULL iter_limit = 0;

typedef struct{
  ULL top;
  ULL bot;
} Poly_GF3;
/* Each polynomial is expressed in two words.  00 = 0, 01 = 1, 11 = 2 */

/* A list of primitive polynomials 
   poly_table[i] contains what x^i is equivalent to.
   E.g. given primitive polynomial x^3 + 2x + 1:
        x^3 ~= x + 2 --> {01,11} = {1,3}
*/

Poly_GF3 poly_table[MAX+1] = {
  /*  0 */ { 0 , 1 },    
  /*  1 */ { 0 , 1 },  
  /*  2 */ { 0 , 3 },  
  /*  3 */ { 1 , 3 },  
  /*  4 */ { 0 , 3 },  
  /*  5 */ { 1 , 3 },  
  /*  6 */ { 0 , 3 },  
  /*  7 */ { 1 , 5 },  
  /*  8 */ { 0 , 9 },  
  /*  9 */ { 1 ,15 },  
  /* 10 */ { 0 ,11 },  
  /* 11 */ { 1 , 5 },  
  /* 12 */ { 2 ,31 },  
  /* 13 */ { 1 , 3 },  
  /* 14 */ { 0 , 3 },  
  /* 15 */ { 1 , 5 },  
  /* 16 */ {24 ,27 }, 
  /* 17 */ { 1,  3 },
  /* 18 */ {32, 39 }, 
  /* 19 */ { 1,  5 },
  /* 20 */ {34, 35 },
  /* 21 */ { 1, 15 },
  /* 22 */ { 2, 15 },
  /* 23 */ { 1,  9 },
  /* 24 */ { 8, 27 },
  /* 25 */ { 1,  9 },
  /* 26 */ { 0, 11 },
  /* 27 */ {49, 59 },
  /* 28 */ {48, 51 },
  /* 29 */ {21, 21 },
  /* 30 */ { 0,  3 },
  /* 31 */ { 3, 15 },
  /* 32 */ {24, 27 }
#if (MAX > 32)
  ,
  /* 33 */ {33, 37 },
  /* 34 */ { 0, 11 },
  /* 35 */ { 1,  5 },
  /* 36 */ {48, 57 },
  /* 37 */ { 9, 15 },
  /* 38 */ { 6, 15 },
  /* 39 */ {33, 45 },
  /* 40 */ { 0,  3 },
  /* 41 */ { 1,  3 },
  /* 42 */ {62, 63 },
  /* 43 */ {11, 11 },
  /* 44 */ { 0,  9 },
  /* 45 */ { 5, 13 },
  /* 46 */ {18, 23 },
  /* 47 */ {29, 29 },
  /* 48 */ {32, 61 },
  /* 49 */ { 5, 13 },
  /* 50 */ { 8, 31 },
  /* 51 */ { 1,  3 },
  /* 52 */ {32, 51 },
  /* 53 */ { 1, 29 },
  /* 54 */ { 2,  3 },
  /* 55 */ { 1, 15 },
  /* 56 */ { 8,  9 },
  /* 57 */ {35, 47 },
  /* 58 */ {18, 23 },
  /* 59 */ { 9, 15 },
  /* 60 */ {34, 35 },
  /* 61 */ { 9, 15 },
  /* 62 */ {18, 23 },
  /* 63 */ { 1, 67108865 },
  /* 64 */ { 8,  9 }
#endif
};

/* Global Variables */
int N,D;
int outformat, type;
ULL maxNumber;
ULL ir_count = 0, pr_count = 0;
int printed = 0;
Poly_GF3 threeN_1;
int a[MAX+1] = {0};

/* Printing routines */
void PrintString(Poly_GF3 a){
  int i;
  printf("1 ");
  for(i = N-1; i >= 0; i--)
    if(a.top&(ONE<<i)) printf(" 2");
    else if(a.bot&(ONE<<i)) printf(" 1");
    else printf(" 0");
  printf("\n");
}

void PrintCoeff(Poly_GF3 a){
  int i;
  printf("%d<sub>1</sub>", N);
  for(i = N-1; i >= 0; i--){
    if(a.bot & (ONE << i)) printf(" %d<sub>%d</sub>", i, (a.top & (ONE << i)) ? 2 : 1);
  }
  printf("\n");
}

void PrintPoly(Poly_GF3 a){
  int i;
  printf("x<sup>%d</sup> + ", N);
  for(i = N-1; i > 0; i--){
    if(a.top & (ONE << i)){
      if(i != 1) printf("2x<sup>%d</sup> + ", i);
      else printf("2x + ");
    } else if(a.bot & (ONE << i)){
      if(i != 1) printf("x<sup>%d</sup> + ", i);
      else printf("x + ");
    } 
  }
  if(a.top & ONE){
    printf("2");
  } else if(a.bot & ONE){
    printf("1");
  } 
  printf("\n");
}

void PrintBit(Poly_GF3 a){
  int i;
  for(i = N-1; i >= 0; i--)
    printf("%d", a.top&(ONE<<i) ? 1 : 0);
  printf("\n");
  for(i = N-1; i >= 0; i--)
    printf("%d", a.bot&(ONE<<i) ? 1 : 0);
  printf("\n");
}

/* Counting bits in O(number of bits) */
int bitcount(ULL x){
  int c = 0;
  while(x){
    c++;
    x &= x-1;
  }
  return c;
}

/* Density - (By our encoding, we count number of bits in the bottom word) */
int density(Poly_GF3 a){
  return bitcount(a.bot);
}

/* Returns a + b */
Poly_GF3 add(Poly_GF3 a, Poly_GF3 b){
  Poly_GF3 res;
  ULL t1 = a.bot^b.top;
  ULL t2 = a.top^b.bot;
  res.top = t1&t2;
  res.bot = (a.top^t1)|(b.top^t2);
  return res;
}

/* Returns -1 if a < b, 0 if a == b, +1 if a > b */
int cmp(Poly_GF3 a, Poly_GF3 b){
  int i, x, y;
  for(i = N-1; i >= 0; i--){
    x = (a.top & (ONE<<i)) ? 2 : (a.bot & (ONE<<i)) ? 1 : 0;
    y = (b.top & (ONE<<i)) ? 2 : (b.bot & (ONE<<i)) ? 1 : 0;
    if(x < y) return -1;
    if(x > y) return 1;
  }
  return 0;
}

/* Returns a - b, (assuming a > b and a,b are ternary numbers */
Poly_GF3 subtract(Poly_GF3 a, Poly_GF3 b){
  int i, borrow = 0, x, y;
  Poly_GF3 r = {0,0};
  for(i = 0; i < N; i++){
    x = ((a.top & (ONE<<i)) ? 2 : (a.bot & (ONE<<i)) ? 1 : 0) - borrow;
    y =  (b.top & (ONE<<i)) ? 2 : (b.bot & (ONE<<i)) ? 1 : 0;
    if(x < y){
      borrow = 1;
      x += 3;
    } else {
      borrow = 0;
    }
    switch(x-y){
    case 1: r.bot |= (ONE<<i); break;
    case 2: r.bot |= (ONE<<i); r.top |= (ONE<<i); break;
    }
  }
  return r;
}

/* Returns a % b (a and b treated as ternary numbers) */
Poly_GF3 mod(Poly_GF3 a, Poly_GF3 b){
  Poly_GF3 r = {0,0};
  int i;
  for(i = N-1; i >= 0; i--){
    r.top <<= 1; r.bot <<= 1;
    if(a.top & (ONE<<i)) r.top++;
    if(a.bot & (ONE<<i)) r.bot++;
    while(cmp(b,r) <= 0){
      r = subtract(r, b);
    }
  }
  return r;
}

/* Returns the gcd of a and b (a and b treated as ternary numbers */
Poly_GF3 gcd(Poly_GF3 a, Poly_GF3 b){
  if(!b.top && !b.bot) return a;
  return gcd(b, mod(a, b));
}

/* Returns (a * b) mod p (where p = poly_table[N]) */
Poly_GF3 multmod(Poly_GF3 a, Poly_GF3 b){
  Poly_GF3 t = a, result = {0,0};
  ULL top_bit = ONE << (N-1);
  int i;

  for(i = 0; i < N; i++){
    if(b.bot & ONE) result = add(result, t);
    if(b.top & ONE) result = add(result, t);
    if(t.top & top_bit){
      t.top = (t.top & ~top_bit) << 1;
      t.bot = (t.bot & ~top_bit) << 1;
      t = add(t, poly_table[N]);
      t = add(t, poly_table[N]);
    } else if(t.bot & top_bit){
      t.top <<= 1;
      t.bot = (t.bot & ~top_bit) << 1;
      t = add(t, poly_table[N]);      
    } else {
      t.top <<= 1;
      t.bot <<= 1;
    }
    b.top >>= 1;
    b.bot >>= 1;
  }
  return result;
}

/* Returns (a^power) mod p (where p = poly_table[N]) */
Poly_GF3 powmod(Poly_GF3 a, Poly_GF3 power){
  Poly_GF3 t = a, result = {0,ONE};
  while(power.top || power.bot){
    if(power.bot & ONE) result = multmod(result, t);
    if(power.top & ONE) result = multmod(result, t);
    t = multmod(multmod(t, t), t);
    power.top >>= 1;
    power.bot >>= 1;
  }
  return result;
}

/* Calculates the minimum polynomial, given a necklace */
Poly_GF3 minpoly(Poly_GF3 necklace){
  Poly_GF3 root = {0,TWO}, result = {0,0};
  Poly_GF3 f[MAX]; /* f[i] contains the polynomials listing the
		      coefficient of x^i in terms of the primitive root */
  int i, j;

  f[0].top = 0;
  f[0].bot = ONE;
  for(i = 1; i < N; i++)
    f[i].top = f[i].bot = 0;
  /*
  printf("Necklace:");
  PrintString(necklace);
  */
  root = powmod(root, necklace); 
  /*
  printf("Root:");
  PrintString(root);
  */
  for(i = 0; i < N; i++){
    if(i) root = multmod(multmod(root, root), root); 
    for(j = N-1; j >= 1; j--)
      f[j] = add(f[j-1], multmod(f[j], root));
    f[0] = multmod(f[0], root);
  }

  /* Because we compute (x+a)(x+a^2)...(x+a^n) instead of (x-a)(x-a^2)...(x-a^n) 
     we now have to inverse all odd positions */
  for(i = N-1; i >= 0; i--){
    if(f[i].top == ONE){
      if(i % 2 == N % 2) result.top |= ONE<<i;
      result.bot |= ONE<<i;	
    } else if(f[i].bot == ONE){
      if(i % 2 != N % 2) result.top |= ONE<<i;
      result.bot |= ONE<<i;
    } else if(f[i].bot) fprintf(stderr, "TROUBLE\n");
  }
  return result;
}

void OutputPoly(Poly_GF3 poly){
  switch(outformat){
  case 0: PrintBit(poly); break;
  case 1: PrintString(poly); break;
  case 2: PrintPoly(poly); break;
  }
  printed++;
}

void PrintIt(){
  int i;
  Poly_GF3 necklace = {0,0};
  Poly_GF3 gcdResult;
  Poly_GF3 mpoly;
  
  if(++iter_limit > MAX_ITERATIONS){
    printf("</TABLE>\n<BR>");
    if (type == 0) {
      printf( "The first %llu irreducible polynomials<BR>\n", ir_count);
      printf("<FONT COLOR=0000ff>");
    }
    printf( "The first %llu primitive polynomials\n<br><br>", pr_count );
    printf("<br><b><blink><font color=ff0000>Iteration limit exceeded !!</B><br></blink></font>\n");
    exit(0);     
  }
  
  for(i = 1; i <= N; i++){
    necklace.top <<= 1;
    necklace.bot <<= 1;
    if(a[i]) necklace.bot++;
    if(a[i] == 2) necklace.top++;
  }

  mpoly = minpoly(necklace);
  if(density(mpoly)+1 > D) return;
  
  ir_count++;  
  gcdResult = gcd(necklace, threeN_1);
  if(gcdResult.top == 0 && gcdResult.bot == ONE){
    printf("<TR>");
    ++pr_count;
    ++printed;
    if(outformat & 1){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintString(mpoly);
      if(type == 0) printf("</FONT>");
    }
    if(outformat & 2){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintCoeff(mpoly);
      if(type == 0) printf("</FONT>");
    } 
    if(outformat & 4){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintPoly(mpoly);
      if(type == 0) printf("</FONT>");
    }
  } else if(type == 0){
    ++printed;
    printf("<TR>");
    if(outformat & 1){
      printf("<TD>");
      PrintString(mpoly);
    } 
    if(outformat & 2){
      printf("<TD>"); 
      PrintCoeff(mpoly);
    }
    if(outformat & 4){
      printf("<TD>");
      PrintPoly(mpoly);
    }
  }
  if(maxNumber && printed >= maxNumber){
    printf("</TABLE>"); 
    exit(1);
  }
}

void Gen(int t, int p){
  int j;
  if(t > N){
    if(p == N) PrintIt();
  } else {
    a[t] = a[t-p]; 
    Gen(t+1,p);
    for(j=a[t-p]+1; j<=2; j++) {
      a[t] = j; 
      Gen(t+1,t);
    }
  }
}

void ProcessInput(int argc, char *argv[]){
  int i;
  outformat = 0;
  type = 0;
  maxNumber = 0;

  if (argc > 1) N = atoi(argv[1]);
  if (argc > 2) D = atoi(argv[2]); else D = N+1;
  if (argc > 3) outformat = atoi(argv[3]); else outformat = 1;
  if (argc > 4) type = atoi(argv[4]); 
  if (argc > 5) maxNumber = atoi(argv[5]);
  if (argc > 6 || N < 2 || N > MAX){
    fprintf(stderr,"Usage: gf3 n d format type number\n");
    fprintf(stderr,"         All params except 'n' are optional\n");
    fprintf(stderr,"         n = degree (2-%d)\n", MAX);
    fprintf(stderr,"         d = density (2-%d)\n", MAX+1);
    fprintf(stderr,"         format = 0,1,2 = string,ceoffs,poly\n");
    fprintf(stderr,"         type = 0/1 = all/primitive only\n");
    fprintf(stderr,"         num = max to find (0 for all)\n");
    exit( 1 );
  }
  /*
  printf( "degree      = %d\n", N );
  printf( "format      = %d\n", outformat );
  printf( "type        = %s\n", type ? "primitive" : "all" );
  printf( "num         = %lld %s\n", maxNumber, maxNumber ? "" : "(none)" );
  */
  for(i = 0; i < N; i++)
    threeN_1.top |= (ONE<<i);
  threeN_1.bot = threeN_1.top;
}

int main(int argc, char **argv){

  ProcessInput(argc, argv);

  printf("N = %d.  Maximum density = %d\n", N, D);
  printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
  if (outformat & 1) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>String</FONT><BR></TH>\n");
  }
  if (outformat & 2) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>Coefficients</FONT><BR></TH>\n");
  }
  if (outformat & 4) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>Polynomials</FONT><BR></TH>\n");
  }

  Gen(1,1);
  printf("</TABLE>\n<BR>");

  if(type == 0){
    printf("The number of irreducible polynomials = %llu<BR>\n", ir_count);
    printf("<FONT COLOR = 0000ff>");
  } 
  printf("The number of primitive polynomials   = %llu\n", pr_count);
  return 0;
}
