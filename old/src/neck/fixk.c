
/*---------------------------------------------------*/
/* This program generates all k-ary fixed density    */ 
/*	necklaces, Lyndon words or prencklaces       */
/*	with length n and density d in lex order     */ 
/*---------------------------------------------------*/

#include <stdio.h>
#include <math.h>

int a[100];
int b[100];
int n,k,d,total;
char type;

void Print(int p) {

	int i,j,next,end,min;

	/* Determine minimum position for next bit */
	next =  (d/p)*a[p] + a[d%p];	
	if (next < n) return;

	/* Determine last bit */
	min = 1;	
	if (type == 'p') min = k-1;
	else if ((next == n) && (d%p != 0)) {
		min =  b[d%p]+1;
		p = d;
	}
	else if ((next == n) && (d%p == 0)) {
		min = b[p];
	}

	/* Determine length of String */
	end = n;
	if (type == 'p') end = n-1; 

	for( b[d]=min; b[d]<k; b[d]++ ) {	
		i = 1;
		/* Test for lyndon words */
		if ( (type == 'l') && (n%a[p] == 0) && (a[p] != n)) {}
		else {	
			for(j=1; j<=end; j++) {
				if (a[i] == j) {
						printf("%d ",b[i]);
						i++;
					}
					else printf("0 ");  
				}
				printf("\n"); 
				total++;
		}
		p = d;
	}     
}

void Gen(int t,int p) {

	int i,j,max,tail;

	if (t >= d-1) Print(p);
	else {
		tail = n - (d - t) + 1;
		max = ((t+1)/p)*a[p] + a[(t+1)%p];
		if (max <=tail) {
			a[t+1] = max;
			if ((t+1)%p == 0) b[t+1] = b[p];
			else b[t+1] = b[(t+1)%p];

			Gen(t+1,p);
			for (i=b[t+1] +1; i<k; i++) {
				b[t+1] = i;
				Gen(t+1,t+1);
			}
			tail = max-1;
		}
		for(j=tail; j>=a[t]+1; j--) {
			a[t+1] =  j;
			for (i=1; i<k; i++) {
				b[t+1] =  i;
				Gen(t+1,t+1);
			}
		} 
	}
} 

void Fixed() {

	int i,j;

	/* initialize string */
	for(j=0; j<=d; j++) a[j] = 0;

	if (d == 0) {
		if (type == 'n') {
			for (j=1; j<=n; j++) printf("0 ");
			printf("\n");
			total = 1; 
		}
	} 
	else {
		a[0] = 0;
		a[d] = n; 	
		for(j=n-d+1; j>=(n-1)/d + 1; j--) {
			a[1] = j;
			for (i=1; i<k; i++) {
				b[1] = i;
				Gen(1,1);		
			}
		} 
	}
}

void main() {

	int i,j;

	/* type: 'n' necklace or 'l' Lyndon word or 'p' prenecklace */
	printf("Enter n k d type: ");
	scanf("%d %d %d %c", &n, &k, &d, &type);  

	/* For prenecklaces we generate necklaces with density n+1 and
		density d+1 and then ignore the last bit */		
	if (type == 'p') { n++; d++; } 

	total = 0;
	Fixed();		
	printf("Total = %d\n",total);  
}

