/*----------------------------------------------------------------------
This program generates a basis for the nth homogeneous component
of the free Lie algebra (or the Lyndon brackets).

Developed by: Joe Sawada 2000.
----------------------------------------------------------------------*/

#include <stdio.h>

int a[50];	/* Holds the Lyndon word */
int split[50][50]; /* If a[i]..a[j] is a Lyndon word then the value
			of split[i][j] is the starting position of the
			longest proper right factor of a[i]..a[j] */
int p[50];	/* Stores the p value for each right factor */
int n;		/* Size of homogeneous component: length of Lyndon words */
int k;		/* Size of alphabet */

/*----------------------------------------------------------------------*/
/* A recursive function which prints out the Lie bracket for the 
	generated Lyndon word */
/*----------------------------------------------------------------------*/
void PrintBracket(int start, int end) {

	if (start == end)  printf("%d", a[start]); 
	else {
		printf("[ ");
		PrintBracket(start,split[start][end]-1);
		printf(" , ");
		PrintBracket(split[start][end],end);
		printf(" ]");
	}
}

/*----------------------------------------------------------------------*/
void GenLie(int t) {
	
	int i,j;
	int q[50];

	if(t>n) {
		if (n == p[1]) { PrintBracket(1,n); printf("\n");  }
	}
	else {
		for(i=1; i<=n; i++) q[i] = p[i];
        	for(j=a[t-p[1]]; j<=k-1; j++) {
			a[t] = j;
			for (i=1; i<=t-1; i++) {
				if (a[t] < a[ t-p[i] ]) p[i] = 0;
				if (a[t] > a[ t-p[i] ]) p[i] = t-i+1; 
			}
			for (i=t-1; i >=1; i--) {
				if (p[i+1] == t-i) split[i][t] = i+1;
				else split[i][t] = split[i+1][t];
			}
			GenLie(t+1);
			for(i=1; i<=n; i++) p[i] = q[i];
		}
	}
}

/*----------------------------------------------------------------------*/
int main() {

	int i;

	printf("enter n,k: ");
	scanf("%d %d", &n, &k); 
	p[0] = 0;
	for (i=1; i<=n; i++)  p[i] = 1; 
	GenLie(1);
}

