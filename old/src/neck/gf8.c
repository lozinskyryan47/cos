/* Program to enumerate all irreducible and/or primitive polynomials over GF(8) 
   Gilbert Lee
   Sept 11, 2006
*/

#include <stdio.h>
#include <stdlib.h>

/* Type definitions */
typedef unsigned long long ULL;

typedef struct{
  /* Each polynomial is expressed in three words. */
  ULL x1;    /* 0 0 0 1 0 1 1  1  */
  ULL x2;    /* 0 1 0 0 1 1 1  t  */
  ULL x3;    /* 0 0 1 1 1 0 1 t^2 */
} Poly_GF8; 

/*========================================================================*/
/* Global Constants                                                       */
/*========================================================================*/
ULL ONE = (ULL)1;
ULL TWO = (ULL)2;
ULL iter_limit = 0;

#define MAX 64
/* MAX is the size of unsigned long long */
#define MAX_ITERATIONS 10000
#define HTML_OUTPUT 1

char index[9] = "01abcdef";

#define value(a,i) (((a).x3&(ONE<<(i)))?4:0) + (((a).x2&(ONE<<(i)))?2:0) + (((a).x1&(ONE<<(i)))?1:0)
#define val(a,i)   ((((a).x3&(ONE<<(i)))?4:0) + (((a).x2&(ONE<<(i)))?2:0) + (((a).x1&(ONE<<(i)))?1:0))

Poly_GF8 poly_table[MAX+1] = { 
/* A list of primitive polynomials                  */        
/* poly_table[i] contains what x^i is equivalent to */
/* E.g. given primitive polynomial x^3 + (a+1)x + 1:*/
/* x^3 ~= (a+1)x + 1 --> {110,100} = {3,2,0}        */
 /*   x1:   1      1   -->  3                       */ 
 /*   x2:   1      0   -->  2                       */
 /*   x3:   0      0   -->  0                       */

/* 00  */ {(ULL)0ULL, (ULL)0ULL, (ULL)1ULL}, 
/* 01  */ {(ULL)0ULL, (ULL)0ULL, (ULL)1ULL},
/* 02  */ {(ULL)              1ULL, (ULL)              1ULL, (ULL)              2ULL},
/* 03  */ {(ULL)              2ULL, (ULL)              7ULL, (ULL)              2ULL},
/* 04  */ {(ULL)              6ULL, (ULL)             13ULL, (ULL)              0ULL},
/* 05  */ {(ULL)              2ULL, (ULL)             13ULL, (ULL)              1ULL},
/* 06  */ {(ULL)             27ULL, (ULL)             39ULL, (ULL)              1ULL},
/* 07  */ {(ULL)             51ULL, (ULL)            105ULL, (ULL)              3ULL},
/* 08  */ {(ULL)             31ULL, (ULL)             64ULL, (ULL)            119ULL},
/* 09  */ {(ULL)            223ULL, (ULL)             24ULL, (ULL)            283ULL},
/* 10  */ {(ULL)              5ULL, (ULL)             92ULL, (ULL)             19ULL},
/* 11  */ {(ULL)            862ULL, (ULL)           1374ULL, (ULL)            251ULL},
/* 12  */ {(ULL)           1275ULL, (ULL)           1879ULL, (ULL)           3516ULL},
/* 13  */ {(ULL)             53ULL, (ULL)            418ULL, (ULL)            877ULL},
/* 14  */ {(ULL)           6673ULL, (ULL)          11151ULL, (ULL)          12037ULL},
/* 15  */ {(ULL)          10156ULL, (ULL)          32359ULL, (ULL)           9771ULL},
/* 16  */ {(ULL)          28463ULL, (ULL)           1007ULL, (ULL)          40001ULL},
/* 17  */ {(ULL)          51147ULL, (ULL)          92851ULL, (ULL)          90913ULL},
/* 18  */ {(ULL)          75056ULL, (ULL)         217527ULL, (ULL)          77697ULL},
/* 19  */ {(ULL)         247990ULL, (ULL)         130239ULL, (ULL)         313618ULL},
/* 20  */ {(ULL)         495915ULL, (ULL)         187113ULL, (ULL)         679394ULL},
/* 21  */ {(ULL)          10681ULL, (ULL)          36034ULL, (ULL)         150059ULL},
/* 22  */ {(ULL)        1433096ULL, (ULL)        3584133ULL, (ULL)        1787612ULL},
/* 23  */ {(ULL)        2370192ULL, (ULL)        5675453ULL, (ULL)        7647333ULL},
/* 24  */ {(ULL)         504720ULL, (ULL)        2418806ULL, (ULL)        3510817ULL},
/* 25  */ {(ULL)        9623805ULL, (ULL)       21351018ULL, (ULL)       15986939ULL},
/* 26  */ {(ULL)       27898229ULL, (ULL)        6898306ULL, (ULL)       49898223ULL},
/* 27  */ {(ULL)       51405635ULL, (ULL)      112974076ULL, (ULL)        5766885ULL},
/* 28  */ {(ULL)       29990013ULL, (ULL)      109778747ULL, (ULL)       50626344ULL},
/* 29  */ {(ULL)      247629147ULL, (ULL)      131851615ULL, (ULL)      517849951ULL},
/* 30  */ {(ULL)      529746862ULL, (ULL)      573803871ULL, (ULL)      142799126ULL},
/* 31  */ {(ULL)      784230605ULL, (ULL)     1514402837ULL, (ULL)     1664029269ULL},
/* 32  */ {(ULL)     2138147652ULL, (ULL)     3812091704ULL, (ULL)      970141953ULL}
#if (MAX > 32)
,
/* 33  */ {(ULL)     3936298134ULL, (ULL)     7981477320ULL, (ULL)     1194409139ULL},
/* 34  */ {(ULL)     7761133195ULL, (ULL)    14021524791ULL, (ULL)     4178520922ULL},
/* 35  */ {(ULL)     3515100116ULL, (ULL)    16619923135ULL, (ULL)     5873458528ULL},
/* 36  */ {(ULL)     6499750044ULL, (ULL)     9049256419ULL, (ULL)    28509604978ULL},
/* 37  */ {(ULL)    50937147576ULL, (ULL)    61348984779ULL, (ULL)    88757261073ULL},
/* 38  */ {(ULL)   117923155733ULL, (ULL)   176466116509ULL, (ULL)   204711302893ULL},
/* 39  */ {(ULL)    47917974615ULL, (ULL)   218282751560ULL, (ULL)   269248661339ULL},
/* 40  */ {(ULL)   455150097365ULL, (ULL)   688915274458ULL, (ULL)   774386428831ULL},
/* 41  */ {(ULL)   921003731345ULL, (ULL)  2069102331613ULL, (ULL)  2073865140280ULL},
/* 42  */ {(ULL)  1165144547255ULL, (ULL)  1801850891194ULL, (ULL)  3362142714065ULL},
/* 43  */ {(ULL)  3751890080251ULL, (ULL)  5228751218780ULL, (ULL)   171787793621ULL},
/* 44  */ {(ULL)  1949847206641ULL, (ULL)  6117907463709ULL, (ULL)  3984410799769ULL},
/* 45  */ {(ULL) 14640517700057ULL, (ULL)  1089879695325ULL, (ULL) 18667669673951ULL},
/* 46  */ {(ULL) 25072899325453ULL, (ULL) 53087873007364ULL, (ULL) 33862127131117ULL},
/* 47  */ {(ULL) 37350186264645ULL, (ULL)129686835304275ULL, (ULL) 49371679604523ULL},
/* 48  */ {(ULL)112215915843942ULL, (ULL)219920095496339ULL, (ULL) 13449818701671ULL},
/* 49  */ {(ULL) 42742561014449ULL, (ULL)263390981097063ULL, (ULL)258420882678615ULL},
/* 50  */ {(ULL)444820784508519ULL, (ULL)1096341124269791ULL, (ULL) 93732489648117ULL},
/* 51  */ {(ULL)692868336752152ULL, (ULL)1503883503279761ULL, (ULL)2019765034886019ULL},
/* 52  */ {(ULL)1990038359102655ULL, (ULL)3906800805494742ULL, (ULL)4415264892938477ULL},
/* 53  */ {(ULL)2874162448354058ULL, (ULL)6033970660558084ULL, (ULL)7066840970479109ULL},
/* 54  */ {(ULL) 76369220898684ULL, (ULL)1659502225615637ULL, (ULL) 72990460622173ULL},
/* 55  */ {(ULL)3665667177237913ULL, (ULL)14234398177066675ULL, (ULL)11177496835220224ULL},
/* 56  */ {(ULL)22945946355587181ULL, (ULL)28181868466825701ULL, (ULL)42662455068421952ULL},
/* 57  */ {(ULL)40807351735698741ULL, (ULL)83897012396528567ULL, (ULL)70739027526357919ULL},
/* 58  */ {(ULL)97241006970782504ULL, (ULL)117474757051816553ULL, (ULL)187375537542904266ULL},
/* 59  */ {(ULL)52403853004007320ULL, (ULL)221546192246313383ULL, (ULL)280888612305900073ULL},
/* 60  */ {(ULL)446556534533058159ULL, (ULL)723525321580874747ULL, (ULL)68835342931178914ULL},
/* 61  */ {(ULL)1004541418205048833ULL, (ULL)90440908304916992ULL, (ULL)1200380170301256081ULL},
/* 62  */ {(ULL)91748205991460572ULL, (ULL)963173763418783772ULL, (ULL)104963561539044509ULL},
/* 63  */ {(ULL)3639919274124392035ULL, (ULL)103856177855202033ULL, (ULL)7540586054602874888ULL},
/* 64  */ {(ULL)7054681620300997099ULL, (ULL)10211838174816942303ULL, (ULL)10793246732180434694ULL},
#endif
};

/*========================================================================*/
/* Global Variables                                                       */
/*========================================================================*/
int N;                 /* Degree of the desired polynomials               */
int D;                 /* Density of the polynomial                       */
int outformat = 1;     /* Output format of the polynomials                
                          0 = string, 1 = coefficients, 2 = expanded      */
int type = 0;          /* Type of polynomials to find: 0=all, 1=primitive */
ULL maxNumber = 0;   /* Maximum # of polynomials to output, 0 = all       */
ULL ir_count = 0;    /* Count # of irreducible polynomials found          */
ULL pr_count = 0;    /* Count # of primitive polynomials found            */
ULL printed = 0;     /* Current number of polynomials found               */
int doneFlag = 0;      /* Flag to mark when maxNumber of polys found      */
Poly_GF8 eightN_1;     /* Represents 8^N-1                                */
int a[MAX+1] = {0};    /* Used to store the unlabelled necklace           */

/*========================================================================*/
/* Printing routines                                                      */
/*========================================================================*/
void PrintPoly(Poly_GF8 a){
  int i, x;
  if(HTML_OUTPUT){
    printf("x<sup>%d</sup> + ", N);
  } else {
    printf("x^%d + ", N);
  }
  for(i = N-1; i > 0; i--){
    x  = value(a,i);
    if(x){
      if(x > 1) printf("%c", index[x]);
      if(i == 1) printf("x + ");
      else printf("x^%d + ", i);
    }
  }
  x = value(a,0);
  printf("%c\n", index[x]);
}

void PrintBit(Poly_GF8 a){
  int i;
  for(i = N-1; i >= 0; i--) printf("%d", a.x3&(ONE<<i) ? 1 : 0); printf("\n");
  for(i = N-1; i >= 0; i--) printf("%d", a.x2&(ONE<<i) ? 1 : 0); printf("\n");
  for(i = N-1; i >= 0; i--) printf("%d", a.x1&(ONE<<i) ? 1 : 0); printf("\n");
}

void PrintCoeff(Poly_GF8 a){
  int i;
  if(HTML_OUTPUT){
    printf("%d<SUB>1</SUB>", N);
  } else {
    printf("1");
  }
  for(i = N-1; i >= 0; i--){
    if(value(a,i)){
      if(HTML_OUTPUT){
	printf(" %d<SUB>%c</SUB>", i, index[value(a,i)]);
      } else {
	printf("%d", index[value(a,i)]);
      }
    }
  }
  printf("\n");
}

void PrintString(Poly_GF8 a){
  int i;
  printf("1 ");
  for(i = N-1; i >= 0; i--){
    switch(value(a,i)){
    case 0: printf("0 "); break;
    case 1: printf("1 "); break;
    case 2: printf("A "); break;
    case 3: printf("A+1 "); break;
    case 4: printf("A<sup>2</sup> "); break;
    case 5: printf("A<sup>2</sup>+1 "); break;
    case 6: printf("A<sup>2</sup>+A "); break;
    case 7: printf("A<sup>2</sup>+A+1 "); break;
    }
  }
  printf("\n");
}

void PrintBase(Poly_GF8 a){
  ULL r = 0;
  int i;
  for(i = N-1; i >= 0; i--){
    r = r*8 + value(a,i);
  }
  printf("%lld\n", r);
}

int bitcount(ULL x){
  int c = 0;
  while(x){
    c++;
    x &= x-1;
  }
  return c;
}

int density(Poly_GF8 a){
  return bitcount(a.x1|a.x2|a.x3);
}

/*========================================================================*/
/* Number Operations                                              */
/*========================================================================*/

/* Returns -1 if a < b, 0 if a == b, +1 if a > b */
int cmp(Poly_GF8 a, Poly_GF8 b){
  int i, x, y;
  for(i = N-1; i >= 0; i--){
    x = value(a,i);
    y = value(b,i);
    if(x < y) return -1;
    if(x > y) return 1;
  }
  
  return 0;
}

/* Returns a - b, (assuming a > b and a,b are ternary numbers) */
Poly_GF8 subtract(Poly_GF8 a, Poly_GF8 b){
  int i, borrow = 0, x, y;
  Poly_GF8 r = {0,0,0};
  for(i = 0; i < N; i++){
    x = value(a,i) - borrow;
    y = value(b,i);
    if(x < y){
      borrow = 1;
      x += 8;
    } else {
      borrow = 0;
    }
    switch(x-y){
    case 1:                                     r.x1 |= (ONE<<i); break;
    case 2:                   r.x2 |= (ONE<<i);                   break;
    case 3:                   r.x2 |= (ONE<<i); r.x1 |= (ONE<<i); break;
    case 4: r.x3 |= (ONE<<i);                                     break;
    case 5: r.x3 |= (ONE<<i);                   r.x1 |= (ONE<<i); break;
    case 6: r.x3 |= (ONE<<i); r.x2 |= (ONE<<i);                   break;
    case 7: r.x3 |= (ONE<<i); r.x2 |= (ONE<<i); r.x1 |= (ONE<<i); break;
    }
  }
  return r;
}

/* Returns a % b (a and b treated as ternary numbers) */
Poly_GF8 mod(Poly_GF8 a, Poly_GF8 b){
  Poly_GF8 r = {0,0,0};
  int i;

  for(i = N-1; i >= 0; i--){
    r.x1 <<= 1; r.x2 <<= 1; r.x3 <<= 1;
    if(a.x1 & (ONE<<i)) r.x1++;
    if(a.x2 & (ONE<<i)) r.x2++;
    if(a.x3 & (ONE<<i)) r.x3++;
    while(cmp(b,r) <= 0){
      r = subtract(r, b);
    }
  }
  return r;
}

/* Returns the gcd of a and b (a and b treated as ternary numbers */
Poly_GF8 gcd(Poly_GF8 a, Poly_GF8 b){
  if(!b.x1 && !b.x2 && !b.x3) return a;
  return gcd(b, mod(a, b));
}

/*========================================================================*/
/* Polynomial Operations                                                  */
/*========================================================================*/

/* Returns a + b */
Poly_GF8 add(Poly_GF8 a, Poly_GF8 b){
  Poly_GF8 res;

  res.x1 = a.x1 ^ b.x1;
  res.x2 = a.x2 ^ b.x2;
  res.x3 = a.x3 ^ b.x3;

  return res;
}

Poly_GF8 mult_constant(Poly_GF8 a, int c){
  Poly_GF8 res = {0,0,0};
  
  switch(c){
  case 0: return res;
  case 1: return a;
  case 2: res.x1 = a.x3;      res.x2 = a.x1^a.x3;   res.x3 = a.x2;          return res;
  case 3: res.x1 = a.x1^a.x3; res.x2 = res.x1^a.x2; res.x3 = a.x2^a.x3;   return res;
  case 4: res.x1 = a.x2;      res.x2 = a.x2^a.x3;   res.x3 = a.x1^a.x3;   return res;
  case 5: res.x1 = a.x1^a.x2; res.x2 = a.x3;        res.x3 = a.x1;        return res;
  case 6: res.x1 = a.x2^a.x3; res.x2 = a.x1^a.x2;   res.x3 = res.x1^a.x1; return res;
  case 7: res.x3 = a.x1^a.x2; res.x2 = a.x1;        res.x1 = res.x3^a.x3; return res;
  }
  return res;
}

/* Returns (a * b) mod p (where p = poly_table[N]) */
Poly_GF8 multmod(Poly_GF8 a, Poly_GF8 b){
  Poly_GF8 t = a, result = {0,0,0};
  ULL top_bit = ONE << (N-1);
  int x;
  int i;

  for(i = 0; i < N; i++){
    result = add(result,mult_constant(t,value(b,0)));
    x = value(t,N-1);
    t.x1 = (t.x1 & ~top_bit) << 1;
    t.x2 = (t.x2 & ~top_bit) << 1;
    t.x3 = (t.x3 & ~top_bit) << 1;
    t = add(t,mult_constant(poly_table[N],x));
    b.x1 >>= 1;
    b.x2 >>= 1;
    b.x3 >>= 1;
  }
  return result;
}

/* Returns (a^power) mod p (where p = poly_table[N]) */
Poly_GF8 powmod(Poly_GF8 a, Poly_GF8 power){
  Poly_GF8 t = a, result = {ONE,0,0}, t2, t4, t6;
  int x;

  while(power.x1 || power.x2 || power.x3){
    x = value(power,0);
    t2 = multmod(t,t);
    t4 = multmod(t2,t2);
    t6 = multmod(t4,t2);
    switch(x){
    case 1: result = multmod(result, t); break;
    case 2: result = multmod(result, t2); break;
    case 3: result = multmod(result, multmod(t2,t));break;
    case 4: result = multmod(result, t4); break;
    case 5: result = multmod(result, multmod(t4,t)); break;
    case 6: result = multmod(result, t6); break;
    case 7: result = multmod(result, multmod(t6,t)); break;
    }
    t = multmod(t2,t6);
    power.x1 >>= 1;
    power.x2 >>= 1;
    power.x3 >>= 1;
  }
  return result;
}

/* Calculates the minimum polynomial, given a necklace */
Poly_GF8 minpoly(Poly_GF8 necklace){
  Poly_GF8 root = {TWO,0,0}, result = {0,0,0}, root2, root4;
  Poly_GF8 f[MAX]; /* f[i] contains the polynomials listing the
		      coefficient of x^i in terms of the primitive root */
  int i, j;

  f[0].x1 = ONE;
  f[0].x2 = 0;
  f[0].x3 = 0;
  for(i = 1; i < N; i++) f[i].x1 = f[i].x2 = f[i].x3 = 0;
  
  root = powmod(root, necklace); 

  for(i = 0; i < N; i++){
    if(i){
      root2 = multmod(root,root);
      root4 = multmod(root2,root2);
      root  = multmod(root4,root4);
    }
    for(j = N-1; j >= 1; j--)
      f[j] = add(f[j-1], multmod(f[j], root));
    f[0] = multmod(f[0], root);
  }

  for(i = N-1; i >= 0; i--){
    j = value(f[i],0);
    switch(j){
    case 1:                                               result.x1 |= (ONE<<i); break;
    case 2:                        result.x2 |= (ONE<<i);                        break;
    case 3:                        result.x2 |= (ONE<<i); result.x1 |= (ONE<<i); break;
    case 4: result.x3 |= (ONE<<i);                                               break;
    case 5: result.x3 |= (ONE<<i);                        result.x1 |= (ONE<<i); break;
    case 6: result.x3 |= (ONE<<i); result.x2 |= (ONE<<i);                        break;
    case 7: result.x3 |= (ONE<<i); result.x2 |= (ONE<<i); result.x1 |= (ONE<<i); break;
    }
  }
  return result;
}

void OutputPoly(Poly_GF8 poly, int isPrimitive){
  printed++;
  
  if(HTML_OUTPUT){
    if(isPrimitive){
      printf("<TR>");
      if(outformat & 1){
	printf("<TD>");
	if(type == 0) printf("<FONT COLOR=0000ff>");
	PrintString(poly);
	if(type == 0) printf("</FONT>");
      }
      if(outformat & 2){
	printf("<TD>");
	if(type == 0) printf("<FONT COLOR=0000ff>");
	PrintCoeff(poly);
	if(type == 0) printf("</FONT>");
      }
      if(outformat & 4){
	printf("<TD>");
	if(type == 0) printf("<FONT COLOR=0000ff>");
	PrintPoly(poly);
	if(type == 0) printf("</FONT>");
      }
    } else {
      printf("<TR>");
      if(outformat & 1){
	printf("<TD>");
	PrintString(poly);
      }
      if(outformat & 2){
	printf("<TD>");
	PrintCoeff(poly);
      }
      if(outformat & 4){
	printf("<TD>");
	PrintPoly(poly);
      }
    }
    if(maxNumber && printed >= maxNumber){
      printf("</TABLE>");
      exit(1);
    }
  } else {
    if(isPrimitive){
      printf("P ");
    } else {
      printf("I ");
    }
    switch(outformat){
    case 0: PrintBit(poly); break;
    case 1: PrintCoeff(poly); break;
    case 2: PrintPoly(poly); break;
    }
  }
}

void PrintIt(){
  int i;
  Poly_GF8 necklace = {0,0,0};
  Poly_GF8 gcdResult;
  Poly_GF8 mpoly;

  if(++iter_limit > MAX_ITERATIONS){
    printf("</TABLE>\n<BR>");
    if(type == 0){
      printf("The first %llu irreducible polynomials<BR>\n", ir_count);
      printf("<FONT COLOR=0000ff");
    }
    printf("The first %llu primitive polynomials\n<BR><BR>", pr_count);
    printf("<BR><B><BLINK><FONT COLOR=FF0000>Iteration limit exceeded !!</B><BR></BLINK></FONT>\n");
    exit(0);
  }

  for(i = 1; i <= N; i++){
    necklace.x1 <<= 1;
    necklace.x2 <<= 1;
    necklace.x3 <<= 1;
    switch(a[i]){
    case 1:                               necklace.x1++; break;
    case 2:                necklace.x2++;                break;
    case 3:                necklace.x2++; necklace.x1++; break;
    case 4: necklace.x3++;                               break;
    case 5: necklace.x3++;                necklace.x1++; break;
    case 6: necklace.x3++; necklace.x2++;                break;
    case 7: necklace.x3++; necklace.x2++; necklace.x1++; break;
    }
  }
  mpoly = minpoly(necklace);
  if(density(mpoly)+1 > D) return;

  ir_count++;  
  gcdResult = gcd(necklace, eightN_1);

  if(gcdResult.x3 == 0 && gcdResult.x2 == 0 && gcdResult.x1 == ONE){
    ++pr_count;
    OutputPoly(mpoly,1); 
  } else if(type == 0){
    OutputPoly(mpoly,0); 
  }

  if(maxNumber && printed >= maxNumber){
    if(HTML_OUTPUT){
      printf("</TABLE>");
      exit(1);
    } else {
      printf("Print limit exceeded\n");
      doneFlag = 1;
    }
  }
}

void Gen(int t, int p){
  int j;
  if(doneFlag) return;
  if(t > N){
    if(p == N) PrintIt();
  } else {
    a[t] = a[t-p]; 
    Gen(t+1,p);
    for(j=a[t-p]+1; j<=7; j++) {
      a[t] = j; 
      Gen(t+1,t);
    }
  }
}

void ProcessInput(int argc, char *argv[])
{
  int i;
  if (argc > 1) N = atoi(argv[1]);
  if (argc > 2) D = atoi(argv[2]); else D = N+1;
  if (argc > 3) outformat = atoi(argv[3]);
  if (argc > 4) type = atoi(argv[4]); 
  if (argc > 5) maxNumber = atoi(argv[5]);
  if (argc > 6 || N < 2 || N > MAX){
    fprintf(stderr,"Usage: gf8 n d format type number\n");
    fprintf(stderr,"         All params except 'n' are optional\n");
    fprintf(stderr,"         n = degree (2-%d)\n", MAX);
    fprintf(stderr,"         d = density (2-%d)\n", MAX);
    fprintf(stderr,"         format = 0,1,2 = string,coeffs,poly\n");
    fprintf(stderr,"         type = 0/1 = all/primitive only\n");
    fprintf(stderr,"         num = max to find (0 for all)\n");
    exit( 1 );
  }
  for(i = 0; i < N; i++)
    eightN_1.x1 |= (ONE<<i);
  eightN_1.x3 = eightN_1.x2 = eightN_1.x1;
}

int main(int argc, char **argv)
{

  ProcessInput(argc, argv);
  
  if(HTML_OUTPUT){
    printf("N = %d. Maximum density = %d\n<BR>", N, D);
    printf("A = Root of y<sup>3</sup>+y+1 a = A, b = A+1, c = A<sup>2</sup>, d = A<sup>2</sup>+1, e = A<sup>2</sup>+A, f = A<sup>2</sup>+A+1<BR>\n");
    printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
    if (outformat & 1) {
      printf("<TH COLSPAN=1><FONT SIZE=+1>String</FONT><BR></TH>\n");
    }
    if (outformat & 2) {
      printf("<TH COLSPAN=1><FONT SIZE=+1>Coefficients</FONT><BR></TH>\n");
    }
    if (outformat & 4) {
      printf("<TH COLSPAN=1><FONT SIZE=+1>Polynomials</FONT><BR></TH>\n");
    }
  }

  Gen(1,1);
  if(HTML_OUTPUT){
    printf("</TABLE>\n<BR>");
  }

  /* Display final counts */
  if(type == 0){
    printf("The number of irreducible polynomials = %llu\n", ir_count);
    if(HTML_OUTPUT) printf("<BR><FONT COLOR=0000ff>");
  }
  printf("The number of primitive polynomials   = %llu\n", pr_count);
  return 0;
}
