#include "./p2c.h"
#include <stdio.h>
#include "../ProcessInput.c"
#include "NumPart.h"

#define ERR -1

/*Author:  Carla D. Savage*/
/*Modified by: Ryan Sutherland on Aug 24/95 */
/*  Input:  n   k   (positive integers, n even) */
/*  Output:  a list of the graphical partitions of n whose largest part*/
/*           is at most k*/
/*  To get ALL graphical partitions of n, let k=n.*/

/*June 15, 1995: modified to count*/
/*June 21, 1995: modified to match pseudocode in paper*/
/*July 10, 1995:  modified to initialize t to 0 in the call rather*/
/*than to 1*/
/*August 24, 1995: translated into C and command line switches added */

typedef int partition[100];
typedef int vector[100];

Static int n, i, k, l;
Static partition a;
Static vector r;

int     NATURAL;
int     MULTIPLICITY;

Static Void print(a, l, n)
    int *a;
    int l, n;
{
    int i,j;
    
    i = 1;
    for (j = 0; j < l; j++) 
	p[i++]=a[j];
    for (j = 1; j <= n; j++)
	p[i++]=1;
    PrintIt(n+l);
}


Static Void g(a_, l, n, k, t, d, r_)
int *a_;
int l, n, k, t, d;
int *r_;
{
	  partition a;
	  vector r;
	  int done;
	  int i, c, m;

	  memcpy(a, a_, sizeof(partition));
	  memcpy(r, r_, sizeof(vector));
	  if (k == 1 || n == 0) {
	    print(a, l, n);
	    return;
	  }
	  done = false;
	  i = 1;
	  c = 0;
	  m = 0;
	  while (!done && i <= d && i <= k && i <= n) {
	    r[i - 1]++;
	    c += 1 - r[i - 1];
	    if (c > m)
	      m = c;
	    if (m >= t)
	      t = m;
	    else
	      t--;
	    done = (t > n - i);
	    if (!done) {
	      a[l] = i;
	      g(a, l + 1, n - i, i, t, d, r);
	    }
	    i++;
	  }

	  if (done || i > k || i > n)
	    return;
	  d++;
	  r[d - 1] = 1;
	  while (!done && i <= k) {
	    r[d - 1]--;
	    c++;
	    if (c > m)
	      m = c;
	    if (m > t)
	      t = m;
	    done = (t > n - i);
	    if (!done) {
	      a[l] = i;
	      g(a, l + 1, n - i, i, t, d, r);
	    }
	    i++;
	  }

}


int main(int argc, char *argv[])
{
    int	FORLIM;

    ProcessInput(argc,argv);

    /* variable translation */
    M = 1;
    n = N;
    k = K;
    
    count = 0;
    if (n == 0 && k == 0) {
	count++;     /* printf("graphical partition:   empty partition\n"); */
	exit(ERR);
    }
    if (n <= 0 || k <= 0) {
                     /* printf("emptyset\n"); */
	exit(ERR);
    }
    if (k > n)
	k = n;
    l = 0;
    FORLIM = n;
    for (i = 1; i <= FORLIM; i++) {
	a[i - 1] = 0;
	r[i - 1] = 0;
    }
    g(a, l, n, k, 0, 0, r);
    printf("</TABLE><P>count = %d",count);
    exit(0);
}



