/* Generating Numerical Partitions into distinct parts */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../ProcessInput.c"  /* input routines, globals below */

#define LOG2 log(2)	/* constant lg2 */
extern int N;  /* partitions of integer N into odd parts */
extern int M;  /*   with largest part M   */

#include "NumPart.h"    /* output routines, globals below */

int temp[100];
extern int p[100];      /* stores the partition */
int a[100];      /* store the binary partition */
long int binary[30];	/* storing 2^x */
int flag;


int decompose(int x)
{
  int index, pos, tmp;
  pos = floor(log(x)/LOG2); 
  index = 1;
  while(x > 0 && pos >= 0)
    {
      tmp = x - binary[pos];
     
      if(tmp >= 0)
	{ 
	  x = tmp;
	  a[index] = binary[pos];
	  index++;
	}
      pos--;
    }
  return (index-1);
}


void Convert(int limit)
{
  int i, j, k, counter, index, big, tmp;
  
  if(M == 0){
    if(N%2)
      M = N;
    else
      M = N-1;
   
  }
  for(i = 1, j = 1; i <= M; i++)
    {
      counter = 0;
     
      for(k = (flag)?2:1; k <= limit; k++)
	if(temp[k] == i)
	  counter ++;
      if(counter > 0)
	{
	index = decompose(counter);

        for(k = 1; k <= index; k++)
	  {
	    p[j] = a[k]*i;
	    j++;
	  }
	}
    }
  for(i = 1; i < j; i++)
    {
      big = i;
      for(k = i+1; k < j; k++)
	if(p[big] < p[k])
	    big = k;
      tmp = p[big];
      p[big] = p[i];
      p[i] = tmp;
      if(!flag && p[i] > M)
	return;
    }
  PrintIt(j-1);
}
  

/* Partitions of n into odd parts with largest part k 
 * and then convert it to partition into distinct parts */
void PartO ( int n, int k, int t )
{ int j;
  /* if (k==1 && n==M)	return;
  if (k==1) printf("%d,%d,%d   ",n,k,t); */
  temp[t] = k;
  if (n == k || k == 1) Convert( t+n-k );
  else for (j=1; j<=min((k+1)/2,(n-k+1)/2); j++) PartO( n-k, 2*j-1, t+1 );
  temp[t] = 1;
}

int main (int argc, char *argv[])
{ int i;
  binary[0] = 1;
  for(i = 1; i < 10; i++)
    binary[i] = binary[i-1]*2; 
  ProcessInput( argc, argv );
  flag = (M == 0)?1:0;
  if (N != 0) {	
     for (i=1; i<=N+1; i++) temp[i] = 1;
     count = 0;  
     if (M == 0) {
        if (N%2) PartO( N+N, N, 1 );
        else PartO( N+N-1, N-1, 1 );
     } else PartO( N, M, 1 );
     printf("</TABLE><P>Partitions = %d",count);
  } else printf("Sorry.  N equals zero.  Goodbye\n");
  exit( 0 );
}
