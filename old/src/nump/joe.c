/* Generating Numerical Partitions */

#include <stdio.h>
#include <string.h>

#define min(a,b)  (a<b) ? a : b
#define max(a,b)  (a>b) ? a : b

int N;  /* partitions of integer N */
int K;  /*   into K parts          */
int M;  /*   with largest part M   */
int p[100]; /* stores the partition */
int count;


void PrintIt( K ) {
int i;
	for (i=1; i<K; i++) printf("%d, ", p[i]);
        printf("%d\n", p[K]);	
	count++;
}



/* Partitions of n into k parts with largest part s */
void Part ( int n, int k, int s, int t )
{ int j, lower;
  p[t] = s;
  if (k == 1 || n == k) PrintIt( K );
  else {
     if ((n-s)%(k-1) > 0) lower = max( 1, 1+(n-s)/(k-1));
     else lower = max( 1, (n-s)/(k-1) );
     for (j=lower; j<=min(s,n-s-k+2); j++) Part( n-s, k-1, j, t+1 );
  }
  p[t] = 1;
}

int main () { 
int i;

  printf("Enter N: "); 			scanf("%d", &N); 
  printf("Enter how many parts K: "); 	scanf("%d", &K); 
  printf("Enter larges part M: "); 	scanf("%d", &M); 

  if ((N != 0) && (K != 0)) {	
     for (i=1; i<=N+1; i++) p[i] = 1;
     count = 0;  
     if (M == 0) Part( 2*N, ++K, N, 1 );
     else if ( N-M >= K -1) Part( N, K, M, 1 );
  } 
	printf("count = %d\n", count);
}

