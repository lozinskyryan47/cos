/* Anton An
 * June 1999
 * 
 * This program generates ideals in lexicographic order for a poset, 
 * using Steiner's Variant. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_POINTS 50	// max set size
#define NUMBER 0	// display number of ideals only
#define SUBSET 1	// display all ideals

unsigned int table[MAX_POINTS+1];
unsigned int up[MAX_POINTS+1];

int N;			// size of the set
int counter = 0;	// number of ideals
int input_mode;		

unsigned getbit(unsigned long word, int n)
{
  return (word >> n) & 01;
}

unsigned int setbit(unsigned int word, int n, unsigned v)
{
  if (v != 0)
    return word | (01 << n);
  else
    return word & ~(01 << n);
}

/* process the relation table and generate upper/lower 
 * for each element in the poset.
 */
void Process(int* steiner)
{
  int i, j, k, count, bit; 

  // transitive closure
  for(k = 1; k <= N; k++){
    for(i = 1; i <= N; i++){
      for(j= 1; j <= N; j++){
	bit = getbit(table[i], j) | (getbit(table[i], k) 
				     & getbit(table[k], j));
	table[i] = setbit(table[i], j, bit);
      }
    }
  }

  for(i = 1; i <= N; i++){
    up[i] = 0;
    count = 0;
    for(j = 1; j <= N; j++){
      if(getbit(table[i], j)){
	up[i] = setbit(up[i], j, 1);
      }
      if(getbit(table[j], i)){
	count ++;
      }
    }
    steiner[i] = count - 1;
  }
}



// find the minimal element in the poset
int findMinimal(unsigned long poset, int *steiner)
{
  int i, min = 0;
  for(i = 1; i <= N; i++)
    if((getbit(poset, i)) && (!steiner[i])){
      min = i;
      break;
    }
  if(min == 0)
    {
      printf("A cycle has been found. Please check.");
      printf("</TABLE>");
      exit(1);
    }
  return min;
}

// print out the ideals
void printResult(unsigned long set)
{
  int i, flag = 0;
  if(input_mode == SUBSET){
    printf("<TR>\n<TD ALIGN=CENTER>");
    printf("{");
    for(i = 1; i <= N; i++)
      if(getbit(set, i)){
	if(flag)
	  printf(",%d", i);
	else
	  printf("%d", i);
	if(!flag)
	  flag = 1;
      }
    printf("}");
    printf("<BR></TD>\n");
  }
  counter ++;
}

// update the list for finding minimal element
void Update(int *steiner, int poset, int min)
{
  int i;
  for(i = 1; i <= N; i++)
    if(getbit(poset, i) && getbit(up[min], i))
      steiner[i]--;
}

void Recover(int *steiner, int poset, int min)
{
  int i;
  for(i = 1; i <= N; i++)
    if(getbit(poset, i) && getbit(up[min], i))
      steiner[i]++;
}

// generate ideals recursively
void Ideal(unsigned long poset, unsigned long ideal, int *steiner)
{
  int i, j, min, count;
  int poset1, ideal1;
  int  steiner1[N+1];
  if(poset == 0)
    printResult(ideal);
  else{
    min = findMinimal(poset, steiner);
    for(i = 1; i<=N; i++)
      steiner1[i] = steiner[i];

    poset1 = setbit(poset, min, 0);
    ideal1 = setbit(ideal, min, 1);
    poset = (~up[min]) & poset;
    Ideal(poset, ideal, steiner);
    Update(steiner1, poset1, min); 
    Ideal(poset1, ideal1, steiner1);
  }    
}

void main(int argc, char* argv[])
{
  FILE *input;
  int i, j, left, right;
  unsigned long poset, ideal;
  int steiner[MAX_POINTS+1];
  char *fname;
  char *mode;

  if(argc != 3){
    fprintf(stderr, "Usage: genIdeal -flag in_file_name\n");
    exit(1);
  }
  
  mode = argv[1];
  if(!strcmp(mode, "-c"))
    input_mode = NUMBER;
  else if(!strcmp(mode, "-g"))
    input_mode = SUBSET;

  if(input_mode == SUBSET){
    printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
    printf("<TH COLSPAN=1><FONT SIZE=+1>Ideals");
    printf("</FONT><BR></TH>\n");
  }

  fname = argv[2];
  if((input = fopen(fname, "r")) == NULL) {
    fprintf(stderr, "Error: could not read file %s\n", fname);
    exit(1);
  }

  // read number of elements in the set
  fscanf(input, "%d", &N);
  
  for(i = 1; i <= N; i++){
    table[i] = 0;
    table[i] = setbit(table[i], i, 1);
  }

  // store partial orderings
  do {
    fscanf(input,"%d %d", &left, &right);
    table[left] = setbit(table[left], right, 1);
  }while(left != 0);

  // initialize ideal and poset
  ideal = 0;
  poset = 0;
  for(i = 1; i <= N; i++)
    poset = setbit(poset, i, 1);

  Process(steiner);
  Ideal(poset, ideal, steiner); 
    
  printf("</TABLE><P>%d ideals in total\n", counter);
  fclose(input);
}

       
