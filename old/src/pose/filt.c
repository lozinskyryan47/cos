#include <stdio.h>
#include <stdlib.h>

#define MAX 101
#define BOOLEAN int
#define TRUE 1
#define FALSE 0

struct link {
    int tag;
    struct link *next;
};
int numadj,head,tail,front, rear;
int queue[MAX], new_data[MAX];
struct link *node[MAX];
int visited[MAX];		/* holds current number of dependencies */
int bad_poset = FALSE;

void build_graph(void);
void bfs(void);
void init(void);
void get_first_nodes(void);
void enqueue(int);
int dequeue(void);
void transform(void);
void print_out(void);
 
main(){
        
    build_graph();
    init();
    bfs();
    transform();
    print_out();
}

/* Build the directed graph */
void build_graph(void) {
    
    int n;
    struct link *tmp[MAX], *tmpnode;
    FILE *fp;

/* Open data file for reading */           
    if (!(fp=fopen("poset.temp", "r"))){
	printf("\t\t\a:::ERROR:::Can't open \"poset.temp\"\n");
	exit(1);
    }

/* Read number of nodes and set up array of pointers 
   to linked lists of adjacent nodes */
    fscanf(fp,"%d", &numadj);
    for (n=1; n<=numadj; n++){
	node[n] = (struct link *) calloc(1, sizeof(node[n]));
        node[n]->tag = n;
        tmp[n] = node[n];
	tmp[n]->next = (struct link *) calloc(1, sizeof(tmp)); 
    }
/* Read data pairs and convert to directional graph */	
     /* printf("There are %d nodes\n", numadj);  */
    fscanf(fp,"%d %d", &tail, &head);
    while( tail != 0 ) {
        tmp[tail]=tmp[tail]->next;
        tmp[tail]->tag=head;
        tmp[tail]->next = (struct link *) calloc(1, sizeof(tmp)); 
	fscanf(fp,"%d %d", &tail, &head);
    }

 /*Testing Section   
    for (n=1; n<=numadj;n++){  
	for (tmpnode=node[n];tmpnode != NULL; tmpnode=tmpnode->next)
	    printf("%d ", tmpnode->tag);
	printf("\n");
    } 
    End of testing block */ 
  
    fclose (fp);
}

/* BFS algorithm for organizing graph for mapping */
void bfs(void) {

    int current_node, n,i;
    struct link *tmpnode;

/* Get top layer of nodes not adjacent to others */
    get_first_nodes();

/* Actual Breadth First Search Algorithm */
    while(rear > front){
	current_node = dequeue();
	tmpnode = node[current_node];
	for(tmpnode=tmpnode->next; tmpnode != NULL; tmpnode=tmpnode->next){
	    visited[tmpnode->tag]--;
	    if ((visited[tmpnode->tag]) == 0){
		enqueue(tmpnode->tag);
            }
	}
    }
    /* test for invalid poset  */

    for(i=1; i<=numadj; i++) {
	if (visited[i] >0) { bad_poset = TRUE;}
    }

}

/* Initialize queue for BFS */
void init(void) {

    int n;

    front = 1;
    rear = 1;
    for(n=1; n<=numadj; n++)
	visited[n] = 0;
}

/* Get the first layer of nodes for BFS */
void get_first_nodes(void){

    int temp_visited[MAX];
    int i,n,m;
    struct link *tmpnode;

/* Check each linked list for node */         
	for(n=1; n<=numadj; n++) {
	    tmpnode=node[n];
	    for(tmpnode=tmpnode->next; tmpnode != NULL; tmpnode=tmpnode->next){
		visited[tmpnode->tag]++;
	    }
	} 
	for(i=1; i<=numadj; i++) {
		temp_visited[i] = 0;
		if (visited[i] == 0)
			enqueue(i);
	}
	for(i=1; i<=numadj; i++) {
        	if (visited[i] == 0) {
	    		tmpnode = node[i];
    	    		for(tmpnode=tmpnode->next; tmpnode != NULL; tmpnode=tmpnode->next){
	    			temp_visited[tmpnode->tag]++;
	    		}	
	  	}
	}
/* visited[i] represents the number of dependencies left for a node */
	for(i=1; i<=numadj; i++) {
/*		visited[i] = visited[i] - temp_visited[i]; */
	}
}

/* Add new nodes to the queue */
void enqueue(int nodelabel) {

    if (nodelabel > 0){
	queue[rear]=nodelabel;
	rear++;
    }
}

/* Pull a node from the queue */
int dequeue(void){
    front++;
    return queue[front-1];
}

/* Transform to new node labeling */
void transform(void){

    int n;

    for(n=1;n<=numadj;n++)
	new_data[queue[n]] = n;
}

/* Write out data */
/* I am reopening the data file for reading so that I don't
   have to save all of the pairs from the data file for the
   length of the program.  There could be n^2 pairs and so
   what I lose in time efficiency, I will make up in memory
   usage [njb.]  */
void print_out(void){

    int n;
    FILE *fp_in, *fp_out, *fp_log;

    if (!(fp_in=fopen("poset.temp", "r"))){
	printf("\t\t\a:::ERROR::: Can't open \"poset.temp\"\n");
	exit(1);
    }
    if (!(fp_out=fopen("data.new", "w"))){
	printf("\t\t\a:::ERROR::: Can't create \"data.new\"\n");
	exit(1);
    }
    if (!(fp_log=fopen("transform.log", "w"))){
	printf("\t\t\a:::ERROR::: Can't create \"transform.log\"\n");
	exit(1);
    }
    if (bad_poset == TRUE) {
	fprintf(fp_out,"BAD\n");
    }
    else {
    	fscanf(fp_in, "%d", &numadj);
    	fprintf(fp_out, "%d\n", numadj);
    	for (n=1; n<=numadj; n++)
		fprintf(fp_log,"%d %d\n",queue[n], new_data[n]);  
    	fscanf(fp_in, "%d %d", &tail, &head);
    	while (tail != 0) {
		fprintf(fp_out, "%d %d\n", new_data[tail], new_data[head]); 
		fscanf(fp_in, "%d %d", &tail, &head);
    	}
    	fprintf(fp_out,"%d %d", 0, 0);
    }

    fclose (fp_in);
    fclose (fp_out);
    fclose (fp_log);
}



