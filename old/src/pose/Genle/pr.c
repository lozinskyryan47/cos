/* pr.c - Pruesse and Ruskey Algorithm */

/* Copyright 1991, Kenny Wong and Frank Ruskey. All Rights Reserved.
 *
 * This program is free software. You may use this software, and copy,
 * redistribute, and/or modify it under the terms of version 2 of the 
 * GNU General Public License. This and all modified works may not be
 * sold for profit. Please do not remove this notice or the original 
 * copyright notices.
 *
 * This software is provided "as is" and without any express or implied
 * warranties, including, without limitation, any implied warranties
 * of merchantability or fitness for a particular purpose. You bear all
 * risk as to the quality and performance of this software.
 *
 * You should have received a copy of the GNU General Public License
 * with this software (see the file COPYING); if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Please report bugs, fixes, applications, modifications, and requests to:
 *
 * Kenny Wong or Frank Ruskey
 * Department of Computer Science, University of Victoria
 * PO Box 3055, Victoria, BC, Canada, V8W 3P6
 * Internet: kenw@csr.UVic.CA or fruskey@csr.UVic.CA
 *
 * All correspondence should include your name, address, phone number,
 * and preferably an electronic mail address.
 */

/* BTW, be careful with the multistatement macros ... */

#include <stdio.h>
#include "genle.h"		/* global declarations */
#include "timer.h"		/* timing features */
#include "hooks.h"		/* client actions */

#include "pr.h"

static char sInc[kN+2][kN+2];	/* poset "incomparability" matrix */
static int sM;			/* maximum number of pairs */

/* optimizations */
static int *sLocA[kN+2];	/* starting locations of A elements */
static int *sLocB[kN+2];	/* starting locations of B elements */
static int *sLocA1;		/* sLocA[1] */
static int *sLocB1;		/* sLocB[1] */

/* fast counting stuff */
static int sCount;		/* running count */
static int sMR[kN+2][kN+2];	/* number of extensions in the basis case */
static int sOldmra;		/* previous "mra" number */
static int sOldmrb;		/* previous "mrb" number */
static int sOldA1;		/* previous contents of *sLocA1 */

/* P table stuff */
static int sTally;		/* running count */
static int sAccum[kN+2][kN+2];	/* running totals */
static int sLast[kN+2][kN+2];	/* sTally when last moved */

/* called when the program is started */
void PruesseRuskeyInit()
{
	register int mrb, mra;

	/* initialize poset independent variables ... */

	/* diagonal and upper "triangle" */
	for (mrb = 0; mrb <= kN-2; mrb++) {
	    sMR[mrb][mrb+1] = (mrb+1) * (mrb+1) - ((mrb+1)*mrb)/2;
	    for (mra = mrb+1; mra <= kN-2; mra++) {
		sMR[mrb][mra+1] = sMR[mrb][mrb+1];
	    }
	    /* upper entries are the same as the "diagonal" entries */
	}

	/* lower "triangle" */
	for (mrb = 1; mrb <= kN-2; mrb++) {
	    for (mra = 1; mra <= mrb; mra++) {
		sMR[mrb][mra] = sMR[mrb-1][mra] + mra;
	    }
	}

	/* note that sMR is actually shifted right by one column */
}

/* set up for processing the new poset ... */
void PruesseRuskeyBegin()
{
	int i, j;

	StartTimer();

	/* initialize "incomparability" matrix ... */
	for (i = 1; i <= gN; i++) {
	    for (j = 1; j <= gN; j++) {
		sInc[i][j] = gCov[i][j] == 0;
	    }
	}

	/* define a sentinel element ... */
	for (i = 1; i <= gN+1; i++) {
	    sInc[i][gN+1] = 0;
	}

	/* locate A/B pairs ... */
	for (i = 1, sM = 0; i < gN; i++) {
	    if (sInc[i][i+1]) {
		sM++;
		sLocA[sM] = gExt + i;
		i++;
		sLocB[sM] = gExt + i;
	    }
	}
	sLocA1 = sLocA[1];
	sLocB1 = sLocB[1];
}

#define moveAleft(a)	\
	tt = t--; *tt = *t; *t = (a);

#define moveAright(a)	\
	*t = *tt; *tt = (a); t = tt++;

#define moveBleft(b)	\
	ss = s--; *ss = *s; *s = (b);

#define moveBright(b)	\
	*s = *ss; *ss = (b); s = ss++;

/* Following are two sets of three macros which implement the Pruesse/
 * Ruskey algorithm. The two sets are essentially the same except that
 * the first is slightly more efficient because rotations of three
 * elements are sometimes used (instead of two transpositions). The
 * second is slightly more flexible because it supports a hook which
 * is "passed" the addresses of the elements swapped to form the next 
 * linear extension.
 *
 * The two sets may eventually be merged into one in some future version.
 */

/* base case of the recursion */
#define PruesseRuskeyBasis(hookproc) \
	/* hookproc is "called" for each extension */			\
									\
	register int a1, b1;						\
									\
	register int *s, *ss;						\
	register int *t, *tt;						\
									\
	register int mb, ma;						\
	register int j, k;						\
									\
	ss = (s = sLocB1) + 1;						\
	b1 = *s;							\
	/* s points to b1, ss points to right neighbor */		\
									\
	if (sInc[b1][*ss]) {						\
	    /* can move b1 right ... */					\
	    *s = *ss; s = ss++; /* b1=>, partly! */			\
									\
	    tt = (t = sLocA1) + 1;					\
	    a1 = *t;							\
	    /* t points to a1, tt points to right neighbor */		\
									\
	    sInc[a1][b1] = 0;						\
									\
	    if (sInc[b1][*ss]) {					\
		/* can move b1 another right ... */			\
		moveBright( b1 );					\
		mb = 2;							\
		hookproc;						\
									\
		if (sInc[a1][*tt]) {					\
		    /* can move a1 right, typical ... */		\
		    while (sInc[b1][*ss]) {				\
			/* can move b1 right */				\
			moveBright( b1 );				\
			mb++;						\
			hookproc;					\
		    }							\
									\
		    ma = 0;						\
		    /* because typical ... */				\
		    do {						\
			/* can move a1 right */				\
			moveAright( a1 );				\
			ma++;						\
			hookproc;					\
		    } while (sInc[a1][*tt]);				\
									\
		    /* now retreat ... */				\
		    if (ma == 1) {					\
			for (j = mb-1; j; j--) {			\
			    moveBleft( b1 );				\
			    hookproc;					\
			}						\
									\
			moveAleft( a1 );				\
			hookproc;					\
									\
			/* restore gExt */				\
			moveBleft( b1 );				\
		    } else {						\
			j = mb - ma;					\
			ma--; /* ma >= 1 */				\
									\
			while (j) {					\
			    moveBleft( b1 );				\
			    hookproc;					\
			    for (k = ma; k; k--) {			\
				moveAleft( a1 );			\
				hookproc;				\
			    }						\
									\
			    if (j == 1) {				\
				moveBleft( b1 );			\
				hookproc;				\
				ma--;					\
				for (k = ma; k; k--) {			\
				    moveAright( a1 );			\
				    hookproc;				\
				}					\
				break;					\
			    }						\
									\
			    moveBleft( b1 );				\
			    hookproc;					\
			    for (k = ma; k; k--) {			\
				moveAright( a1 );			\
				hookproc;				\
			    }						\
									\
			    j -= 2;					\
			}						\
									\
			if (ma) {					\
			    for ( ;; ) {				\
				/* move a1 first! */			\
				moveAleft( a1 );			\
				moveBleft( b1 );			\
				hookproc;				\
									\
				ma--;					\
				if (ma == 0) break;			\
									\
				for (k = ma; k; k--) {			\
				    moveAleft( a1 );			\
				    hookproc;				\
				}					\
									\
				moveBleft( b1 );			\
				hookproc;				\
									\
				ma--;					\
				if (ma == 0) break;			\
									\
				for (k = ma; k; k--) {			\
				    moveAright( a1 );			\
				    hookproc;				\
				}					\
			    }						\
			}						\
									\
			moveAleft( a1 );				\
			hookproc;					\
									\
			/* restore gExt */				\
			moveBleft( b1 );				\
		    }							\
		} else {						\
		    /* can't move a1 right, atypical ... */		\
		    while (sInc[b1][*ss]) {				\
			*s = *ss; s = ss++; /* b1=>, partly! */		\
									\
			if (sInc[b1][*ss]) {				\
			    moveBright( b1 );				\
			    mb += 2;					\
			    hookproc;					\
			} else {					\
			    *s = b1; /* finish b1=> */			\
			    mb++;					\
			    hookproc;					\
			    goto mbOdd; /* boo .. hiss */		\
			}						\
		    } 							\
									\
		    /* mb is even (2, 4, ...) */			\
		    moveBleft( b1 );					\
		    mb--;						\
		    hookproc;						\
mbOdd:									\
		    while (mb != 1) {					\
			ss = s--; *ss = *s; /* <=b1, partly! */		\
			moveBleft( b1 );				\
			mb -= 2;					\
			hookproc;					\
		    }							\
									\
		    /* restore gExt */					\
		    moveBleft( b1 );					\
		}							\
	    } else {							\
		/* can't move b1 another right */			\
		*s = b1; /* finish b1=> */				\
									\
		if (sInc[a1][*tt]) {					\
		    /* can move a1 right */				\
		    *t = *tt; *tt = a1; /* a1=> */			\
		    hookproc;						\
									\
		    *tt = *t; *t = a1; /* <=a1 */			\
		}							\
		hookproc;						\
									\
		/* restore gExt */					\
		moveBleft( b1 );					\
	    }								\
									\
	    sInc[a1][b1] = 1;						\
	}								\
/* PruesseRuskeyBasis */

#define swapAB(k) do { \
	int tmp;							\
	tmp = *sLocA[(k)];						\
	*sLocA[(k)] = *sLocB[(k)];					\
	*sLocB[(k)] = tmp;						\
} while (0)

/* inductive step of the recursion */
#define PruesseRuskeyStep(hookproc,recursivestep) \
	/* hookproc is "called" for each extension			\
	 * recursivestep makes the appropriate recursive call		\
	 */								\
	int ai, bi;							\
									\
	int *s, *ss;							\
	int *t, *tt;							\
									\
	int mrb, mra;							\
	int j, k;							\
									\
	int i1;								\
									\
	recursivestep;							\
	i1 = i - 1;							\
									\
	ss = (s = sLocB[i]) + 1;					\
	bi = *s;							\
									\
	if (sInc[bi][*ss]) {						\
	    mrb = 0;							\
	    do {							\
		moveBright( bi );					\
		mrb++;							\
		hookproc;						\
		recursivestep;						\
	    } while (sInc[bi][*ss]);					\
									\
	    tt = (t = sLocA[i]) + 1;					\
	    ai = *t;							\
									\
	    sInc[ai][bi] = 0;						\
									\
	    if (sInc[ai][*tt]) {					\
		/* can move ai right, typical */			\
		tt++;							\
		if (sInc[ai][*tt]) {					\
		    /* mra >= 2 */					\
		    tt--;						\
		    if (mrb & 0x1) {					\
			/* mrb odd */					\
			moveAright( ai );				\
			hookproc;					\
			recursivestep;					\
									\
			moveAright( ai );				\
			hookproc;					\
			recursivestep;					\
									\
			mra = 2;					\
			while (sInc[ai][*tt]) {				\
			    /* can move ai right */			\
			    moveAright( ai );				\
			    mra++;					\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			j = mrb-mra;					\
			while (j) {					\
			    swapAB( i1 );				\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra; k; k--) {			\
				moveAleft( ai );			\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    moveBleft( bi );				\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra; k; k--) {			\
				moveAright( ai );			\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    if (j >= 2) {				\
				swapAB( i1 );				\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra-1; k; k--) {		\
				    moveAleft( ai );			\
				    hookproc;				\
				    recursivestep;			\
				}					\
			    						\
				moveBleft( bi );			\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra-1; k; k--) {		\
				    moveAright( ai );			\
				    hookproc;				\
				    recursivestep;			\
				}					\
			     						\
				j -= 2;					\
			    } else {					\
				break;					\
			    }						\
			}						\
		    } else {						\
			/* mrb even */					\
			swapAB( i1 );					\
			hookproc;					\
			recursivestep;					\
									\
			moveAright( ai );				\
			hookproc;					\
			recursivestep;					\
									\
			moveAright( ai );				\
			hookproc;					\
			recursivestep;					\
									\
			mra = 2;					\
			while (sInc[ai][*tt]) {				\
			    /* can move ai right */			\
			    moveAright( ai );				\
			    mra++;					\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			j = mrb-mra;					\
			while (j) {					\
			    swapAB( i1 );				\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra-1; k; k--) {			\
				moveAleft( ai );			\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    moveBleft( bi );				\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra-1; k; k--) {			\
				moveAright( ai );			\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    if (j >= 2) {				\
				swapAB( i1 );				\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra; k; k--) {			\
				    moveAleft( ai );			\
				    hookproc;				\
				    recursivestep;			\
				}					\
			    						\
				moveBleft( bi );			\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra; k; k--) {			\
				    moveAright( ai );			\
				    hookproc;				\
				    recursivestep;			\
				}					\
									\
				j -= 2;					\
			    } else {					\
				break;					\
			    }						\
			}						\
		    }							\
								    	\
		    if ((mra & 0x1) == 0) {				\
			/* mra even */					\
			mra--;						\
			swapAB( i1 );					\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra; k; k--) {				\
			    moveAleft( ai );				\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			moveBleft( bi );				\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra-1; k; k--) {			\
			    moveAright( ai );				\
			    hookproc;					\
			    recursivestep;				\
			}						\
		    }							\
									\
		    j = mra-1;						\
		    /* j is even */					\
		    while (j) {						\
			swapAB( i1 );					\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra; k; k--) {				\
			    moveAleft( ai );				\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			moveBleft( bi );				\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra-1; k; k--) {			\
			    moveAright( ai );				\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			swapAB( i1 );					\
			hookproc;					\
			recursivestep;					\
									\
			mra -= 2;					\
			for (k = mra; k; k--) {				\
			    moveAleft( ai );				\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			moveBleft( bi );				\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra-1; k; k--) {			\
			    moveAright( ai );				\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			j -= 2;						\
		    }							\
								    	\
		    swapAB( i1 );					\
		    hookproc;						\
		    recursivestep;					\
									\
		    moveAleft( ai );					\
		    hookproc;						\
		    recursivestep;					\
									\
		    moveBleft( bi );					\
		    hookproc;						\
		    recursivestep;					\
		} else {						\
		    /* mra == 1 */					\
		    tt--;						\
		    *t = *tt; *tt = ai; /* ai=> */			\
		    hookproc;						\
		    recursivestep;					\
									\
		    for (k = mrb-1; k; k--) {				\
			moveBleft( bi );				\
			hookproc;					\
			recursivestep;					\
		    }							\
									\
		    swapAB( i1 );					\
		    hookproc;						\
		    recursivestep;					\
									\
		    for (k = mrb-1; k; k--) {				\
			moveBright( bi );				\
			hookproc;					\
			recursivestep;					\
		    }							\
									\
		    *tt = *t; *t = ai; /* <=ai */			\
		    hookproc;						\
		    recursivestep;					\
									\
		    for (k = mrb; k; k--) {				\
			moveBleft( bi );				\
			hookproc;					\
			recursivestep;					\
		    }							\
		}							\
	    } else {							\
		/* atypical */						\
		swapAB( i1 );						\
		hookproc;						\
		recursivestep;						\
									\
		for (k = mrb; k; k--) {					\
		    moveBleft( bi );					\
		    hookproc;						\
		    recursivestep;					\
		}							\
	    }								\
									\
	    sInc[ai][bi] = 1;						\
	} else {							\
	    swapAB( i1 );						\
	    hookproc;							\
	    recursivestep;						\
	}								\
/* PruesseRuskeyStep */

/* the algorithm */
#define PruesseRuskey(hookbegin,hookproc,hookend,call) \
	/* hookbegin is "called" just before the main processing	\
	 * hookproc is "called" for each linear extension		\
	 * hookend is "called" just after the main processing		\
	 * call makes the appropriate first recursive call		\
	 */								\
	hookbegin;							\
									\
	hookproc;							\
	if (sM > 0) {							\
	    call;							\
									\
	    swapAB( sM );						\
									\
	    hookproc;							\
	    call;							\
	}								\
									\
	hookend;							\
/* PruesseRuskey */

/* base case of the recursion */
#define PruesseRuskeyBasisDetailed(hookproc,changedproc) \
	/* hookproc is "called" for each linear extension		\
	 * changeproc is "called" for each transposition used in	\
	 *     producing this next linear extension			\
	 */								\
	register int a1, b1;						\
									\
	register int *s, *ss;						\
	register int *t, *tt;						\
									\
	register int mb, ma;						\
	register int j, k;						\
									\
	ss = (s = sLocB1) + 1;						\
	b1 = *s;							\
	/* s points to b1, ss points to right neighbor */		\
									\
	if (sInc[b1][*ss]) {						\
	    /* can move b1 right ... */					\
	    *s = *ss; *ss = b1;						\
	    changedproc( s, ss ); s = ss++;				\
									\
	    tt = (t = sLocA1) + 1;					\
	    a1 = *t;							\
	    /* t points to a1, tt points to right neighbor */		\
									\
	    sInc[a1][b1] = 0;						\
									\
	    if (sInc[b1][*ss]) {					\
		/* can move b1 another right ... */			\
		*s = *ss; *ss = b1;					\
		changedproc( s, ss ); s = ss++;				\
		hookproc;						\
		mb = 2;							\
									\
		if (sInc[a1][*tt]) {					\
		    /* can move a1 right, typical ... */		\
		    while (sInc[b1][*ss]) {				\
			/* can move b1 right */				\
			*s = *ss; *ss = b1;				\
			changedproc( s, ss ); s = ss++;			\
			hookproc;					\
			mb++;						\
		    }							\
									\
		    ma = 0;						\
		    /* because typical ... */				\
		    do {						\
			/* can move a1 right */				\
			*t = *tt; *tt = a1; 				\
			changedproc( t, tt); t = tt++;			\
			hookproc;					\
			ma++;						\
		    } while (sInc[a1][*tt]);				\
									\
		    /* now retreat ... */				\
		    if (ma == 1) {					\
			for (j = mb-1; j; j--) {			\
			    ss = s--; *ss = *s; *s = b1;		\
			    changedproc( s, ss );			\
			    hookproc;					\
			}						\
									\
			tt = t--; *tt = *t; *t = a1;			\
			changedproc( t, tt );				\
			hookproc;					\
									\
			/* restore gExt */				\
			ss = s--; *ss = *s; *s = b1;			\
			changedproc( s, ss );				\
		    } else {						\
			j = mb - ma;					\
			ma--; /* ma >= 1 */				\
									\
			while (j) {					\
			    ss = s--; *ss = *s; *s = b1;		\
			    changedproc( s, ss );			\
			    hookproc;					\
			    for (k = ma; k; k--) {			\
				tt = t--; *tt = *t; *t = a1;		\
				changedproc( t, tt );			\
				hookproc;				\
			    }						\
									\
			    if (j == 1) {				\
				ss = s--; *ss = *s; *s = b1;		\
				changedproc( s, ss );			\
				hookproc;				\
				ma--;					\
				for (k = ma; k; k--) {			\
				    *t = *tt; *tt = a1;			\
				    changedproc( t, tt ); t = tt++;	\
				    hookproc;				\
				}					\
				break;					\
			    }						\
									\
			    ss = s--; *ss = *s; *s = b1;		\
			    changedproc( s, ss );			\
			    hookproc;					\
			    for (k = ma; k; k--) {			\
				*t = *tt; *tt = a1;			\
				changedproc( t, tt ); t = tt++;		\
				hookproc;				\
			    }						\
									\
			    j -= 2;					\
			}						\
									\
			if (ma) {					\
			    for ( ;; ) {				\
				/* move a1 first! */			\
				tt = t--; *tt = *t; *t = a1;		\
				changedproc( t, tt );			\
				ss = s--; *ss = *s; *s = b1;		\
				changedproc( s, ss );			\
				hookproc;				\
									\
				ma--;					\
				if (ma == 0) break;			\
									\
				for (k = ma; k; k--) {			\
				    tt = t--; *tt = *t; *t = a1;	\
				    changedproc( t, tt );		\
				    hookproc;				\
				}					\
									\
				ss = s--; *ss = *s; *s = b1;		\
				changedproc( s, ss );			\
				hookproc;				\
									\
				ma--;					\
				if (ma == 0) break;			\
									\
				for (k = ma; k; k--) {			\
				    *t = *tt; *tt = a1;			\
				    changedproc( t, tt ); t = tt++;	\
				    hookproc;				\
				}					\
			    }						\
			}						\
									\
			tt = t--; *tt = *t; *t = a1;			\
			changedproc( t, tt );				\
			hookproc;					\
									\
			/* restore gExt */				\
			ss = s--; *ss = *s; *s = b1;			\
			changedproc( s, ss );				\
		    }							\
		} else {						\
		    /* can't move a1 right, atypical ... */		\
		    while (sInc[b1][*ss]) {				\
			*s = *ss; *ss = b1;				\
			changedproc( s, ss ); s = ss++;			\
									\
			if (sInc[b1][*ss]) {				\
			    *s = *ss; *ss = b1;				\
			    changedproc( s, ss ); s = ss++;		\
			    mb += 2;					\
			    hookproc;					\
			} else {					\
			    mb++;					\
			    hookproc;					\
			    goto mbOdd; /* boo .. hiss */		\
			}						\
		    } 							\
									\
		    /* mb is even (2, 4, ...) */			\
		    ss = s--; *ss = *s; *s = b1;			\
		    changedproc( s, ss );				\
		    mb--;						\
		    hookproc;						\
mbOdd:									\
		    while (mb != 1) {					\
			ss = s--; *ss = *s; *s = b1;			\
			changedproc( s, ss );				\
			ss = s--; *ss = *s; *s = b1;			\
			changedproc( s, ss );				\
			mb -= 2;					\
			hookproc;					\
		    }							\
									\
		    /* restore gExt */					\
		    ss = s--; *ss = *s; *s = b1;			\
		    changedproc( s, ss );				\
		}							\
	    } else {							\
		/* can't move b1 another right */			\
									\
		if (sInc[a1][*tt]) {					\
		    /* can move a1 right */				\
		    *t = *tt; *tt = a1; /* a1=> */			\
		    changedproc( t, tt );				\
		    hookproc;						\
									\
		    *tt = *t; *t = a1; /* <=a1 */			\
		    changedproc( t, tt );				\
		}							\
		hookproc;						\
									\
		/* restore gExt */					\
		ss = s--; *ss = *s; *s = b1;				\
		changedproc( s, ss );					\
	    }								\
									\
	    sInc[a1][b1] = 1;						\
	}								\
/* PruesseRuskeyBasisDetailed */

/* inductive step of the recursion */
#define PruesseRuskeyStepDetailed(hookproc,changedproc,recursivestep) \
	/* hookproc is "called" for each linear extension		\
	 * changeproc is "called" for each transposition used in	\
	 *     producing this next linear extension			\
	 * recursivestep makes the appropriate recursive call		\
	 */								\
	int ai, bi;							\
									\
	int *s, *ss;							\
	int *t, *tt;							\
									\
	int mrb, mra;							\
	int j, k;							\
									\
	int i1;								\
	int tmp;							\
									\
	recursivestep;							\
	i1 = i - 1;							\
									\
	ss = (s = sLocB[i]) + 1;					\
	bi = *s;							\
									\
	if (sInc[bi][*ss]) {						\
	    mrb = 0;							\
	    do {							\
		*s = *ss; *ss = bi;					\
		changedproc( s, ss ); s = ss++;				\
		mrb++;							\
		hookproc;						\
		recursivestep;						\
	    } while (sInc[bi][*ss]);					\
									\
	    tt = (t = sLocA[i]) + 1;					\
	    ai = *t;							\
									\
	    sInc[ai][bi] = 0;						\
									\
	    if (sInc[ai][*tt]) {					\
		/* can move ai right, typical */			\
		tt++;							\
		if (sInc[ai][*tt]) {					\
		    /* mra >= 2 */					\
		    tt--;						\
		    if (mrb & 0x1) {					\
			/* mrb odd */					\
			*t = *tt; *tt = ai;				\
			changedproc( t, tt ); t = tt++;			\
			hookproc;					\
			recursivestep;					\
									\
			*t = *tt; *tt = ai;				\
			changedproc( t, tt ); t = tt++;			\
			hookproc;					\
			recursivestep;					\
									\
			mra = 2;					\
			while (sInc[ai][*tt]) {				\
			    /* can move ai right */			\
			    *t = *tt; *tt = ai;				\
			    changedproc( t, tt ); t = tt++;		\
			    mra++;					\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			j = mrb-mra;					\
			while (j) {					\
			    tmp = *sLocA[i1];				\
			    *sLocA[i1] = *sLocB[i1];			\
			    *sLocB[i1] = tmp;				\
									\
			    changedproc( sLocA[i1], sLocB[i1] );	\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra; k; k--) {			\
				tt = t--; *tt = *t; *t = ai;		\
				changedproc( t, tt );			\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    ss = s--; *ss = *s; *s = bi;		\
			    changedproc( s, ss );			\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra; k; k--) {			\
				*t = *tt; *tt = ai; 			\
				changedproc( t, tt ); t = tt++;		\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    if (j >= 2) {				\
				tmp = *sLocA[i1];			\
				*sLocA[i1] = *sLocB[i1];		\
				*sLocB[i1] = tmp;			\
									\
				changedproc( sLocA[i1], sLocB[i1] );	\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra-1; k; k--) {		\
				    tt = t--; *tt = *t; *t = ai;	\
				    changedproc( t, tt );		\
				    hookproc;				\
				    recursivestep;			\
				}					\
			    						\
				ss = s--; *ss = *s; *s = bi;		\
				changedproc( s, ss );			\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra-1; k; k--) {		\
				    *t = *tt; *tt = ai; 		\
				    changedproc( t, tt ); t = tt++;	\
				    hookproc;				\
				    recursivestep;			\
				}					\
			     						\
				j -= 2;					\
			    } else {					\
				break;					\
			    }						\
			}						\
		    } else {						\
			/* mrb even */					\
			tmp = *sLocA[i1];				\
			*sLocA[i1] = *sLocB[i1];			\
			*sLocB[i1] = tmp;				\
									\
			changedproc( sLocA[i1], sLocB[i1] );		\
			hookproc;					\
			recursivestep;					\
									\
			*t = *tt; *tt = ai; 				\
			changedproc( t, tt ); t = tt++;			\
			hookproc;					\
			recursivestep;					\
									\
			*t = *tt; *tt = ai; 				\
			changedproc( t, tt ); t = tt++;			\
			hookproc;					\
			recursivestep;					\
									\
			mra = 2;					\
			while (sInc[ai][*tt]) {				\
			    /* can move ai right */			\
			    *t = *tt; *tt = ai; 			\
			    changedproc( t, tt ); t = tt++;		\
			    mra++;					\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			j = mrb-mra;					\
			while (j) {					\
			    tmp = *sLocA[i1];				\
			    *sLocA[i1] = *sLocB[i1];			\
			    *sLocB[i1] = tmp;				\
									\
			    changedproc( sLocA[i1], sLocB[i1] );	\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra-1; k; k--) {			\
				tt = t--; *tt = *t; *t = ai;		\
				changedproc( t, tt );			\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    ss = s--; *ss = *s; *s = bi;		\
			    changedproc( s, ss );			\
			    hookproc;					\
			    recursivestep;				\
									\
			    for (k = mra-1; k; k--) {			\
				*t = *tt; *tt = ai; 			\
				changedproc( t, tt ); t = tt++;		\
				hookproc;				\
				recursivestep;				\
			    }						\
									\
			    if (j >= 2) {				\
				tmp = *sLocA[i1];			\
				*sLocA[i1] = *sLocB[i1];		\
				*sLocB[i1] = tmp;			\
									\
				changedproc( sLocA[i1], sLocB[i1] );	\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra; k; k--) {			\
				    tt = t--; *tt = *t; *t = ai;	\
				    changedproc( t, tt );		\
				    hookproc;				\
				    recursivestep;			\
				}					\
			    						\
				ss = s--; *ss = *s; *s = bi;		\
				changedproc( s, ss );			\
				hookproc;				\
				recursivestep;				\
									\
				for (k = mra; k; k--) {			\
				    *t = *tt; *tt = ai; 		\
				    changedproc( t, tt ); t = tt++;	\
				    hookproc;				\
				    recursivestep;			\
				}					\
									\
				j -= 2;					\
			    } else {					\
				break;					\
			    }						\
			}						\
		    }							\
								    	\
		    if ((mra & 0x1) == 0) {				\
			/* mra even */					\
			mra--;						\
			tmp = *sLocA[i1];				\
			*sLocA[i1] = *sLocB[i1];			\
			*sLocB[i1] = tmp;				\
									\
			changedproc( sLocA[i1], sLocB[i1] );		\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra; k; k--) {				\
			    tt = t--; *tt = *t; *t = ai;		\
			    changedproc( t, tt );			\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			ss = s--; *ss = *s; *s = bi;			\
			changedproc( s, ss );				\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra-1; k; k--) {			\
			    *t = *tt; *tt = ai; 			\
			    changedproc( t, tt ); t = tt++;		\
			    hookproc;					\
			    recursivestep;				\
			}						\
		    }							\
									\
		    j = mra-1;						\
		    /* j is even */					\
		    while (j) {						\
			tmp = *sLocA[i1];				\
			*sLocA[i1] = *sLocB[i1];			\
			*sLocB[i1] = tmp;				\
									\
			changedproc( sLocA[i1], sLocB[i1] );		\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra; k; k--) {				\
			    tt = t--; *tt = *t; *t = ai;		\
			    changedproc( t, tt );			\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			ss = s--; *ss = *s; *s = bi;			\
			changedproc( s, ss );				\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra-1; k; k--) {			\
			    *t = *tt; *tt = ai; 			\
			    changedproc( t, tt ); t = tt++;		\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			tmp = *sLocA[i1];				\
			*sLocA[i1] = *sLocB[i1];			\
			*sLocB[i1] = tmp;				\
									\
			changedproc( sLocA[i1], sLocB[i1] );		\
			hookproc;					\
			recursivestep;					\
									\
			mra -= 2;					\
			for (k = mra; k; k--) {				\
			    tt = t--; *tt = *t; *t = ai;		\
			    changedproc( t, tt );			\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			ss = s--; *ss = *s; *s = bi;			\
			changedproc( s, ss );				\
			hookproc;					\
			recursivestep;					\
									\
			for (k = mra-1; k; k--) {			\
			    *t = *tt; *tt = ai; 			\
			    changedproc( t, tt ); t = tt++;		\
			    hookproc;					\
			    recursivestep;				\
			}						\
									\
			j -= 2;						\
		    }							\
								    	\
		    tmp = *sLocA[i1];					\
		    *sLocA[i1] = *sLocB[i1];				\
		    *sLocB[i1] = tmp;					\
									\
		    changedproc( sLocA[i1], sLocB[i1] );		\
		    hookproc;						\
		    recursivestep;					\
									\
		    tt = t--; *tt = *t; *t = ai;			\
		    changedproc( t, tt );				\
		    hookproc;						\
		    recursivestep;					\
									\
		    ss = s--; *ss = *s; *s = bi;			\
		    changedproc( s, ss );				\
		    hookproc;						\
		    recursivestep;					\
		} else {						\
		    /* mra == 1 */					\
		    tt--;						\
		    *t = *tt; *tt = ai; /* ai=> */			\
		    changedproc( t, tt );				\
		    hookproc;						\
		    recursivestep;					\
									\
		    for (k = mrb-1; k; k--) {				\
			ss = s--; *ss = *s; *s = bi;			\
			changedproc( s, ss );				\
			hookproc;					\
			recursivestep;					\
		    }							\
									\
		    tmp = *sLocA[i1];					\
		    *sLocA[i1] = *sLocB[i1];				\
		    *sLocB[i1] = tmp;					\
									\
		    changedproc( sLocA[i1], sLocB[i1] );		\
		    hookproc;						\
		    recursivestep;					\
									\
		    for (k = mrb-1; k; k--) {				\
			*s = *ss; *ss = bi; 				\
			changedproc( s, ss ); s = ss++;			\
			hookproc;					\
			recursivestep;					\
		    }							\
									\
		    *tt = *t; *t = ai; /* <=ai */			\
		    changedproc( t, tt );				\
		    hookproc;						\
		    recursivestep;					\
									\
		    for (k = mrb; k; k--) {				\
			ss = s--; *ss = *s; *s = bi;			\
			changedproc( s, ss );				\
			hookproc;					\
			recursivestep;					\
		    }							\
		}							\
	    } else {							\
		/* atypical */						\
		tmp = *sLocA[i1];					\
		*sLocA[i1] = *sLocB[i1];				\
		*sLocB[i1] = tmp;					\
									\
		changedproc( sLocA[i1], sLocB[i1] );			\
		hookproc;						\
		recursivestep;						\
									\
		for (k = mrb; k; k--) {					\
		    ss = s--; *ss = *s; *s = bi;			\
		    changedproc( s, ss );				\
		    hookproc;						\
		    recursivestep;					\
		}							\
	    }								\
									\
	    sInc[ai][bi] = 1;						\
	} else {							\
	    tmp = *sLocA[i1];						\
	    *sLocA[i1] = *sLocB[i1];					\
	    *sLocB[i1] = tmp;						\
									\
	    changedproc( sLocA[i1], sLocB[i1] );			\
	    hookproc;							\
	    recursivestep;						\
	}								\
/* PruesseRuskeyStepDetailed */

/* the algorithm */
#define PruesseRuskeyDetailed(hookbegin,hookproc,hookend,changedproc,call) \
	/* hookbegin is "called" just before the main processing	\
	 * hookproc is "called" for each linear extension		\
	 * hookend is "called" just after the main processing		\
	 * changeproc is "called" for each transposition used in	\
	 *     producing the next linear extension			\
	 * call makes the appropriate first recursive call		\
	 */								\
	hookbegin;							\
									\
	hookproc;							\
	if (sM > 0) {							\
	    int tmp;							\
									\
	    call;							\
									\
	    tmp = *sLocA[sM];						\
	    *sLocA[sM] = *sLocB[sM];					\
	    *sLocB[sM] = tmp;						\
									\
	    changedproc( sLocA[sM], sLocB[sM] );			\
	    hookproc;							\
	    call;							\
	}								\
									\
	hookend;							\
/* PruesseRuskeyDetailed */

/* generating ... */

static void PruesseRuskeyGenBasis()
{
	PruesseRuskeyBasis( 
	    uGenProc() 
	)
}

static void PruesseRuskeyGenStep( i )
int i;
{
	PruesseRuskeyStep( 
	    uGenProc(), 
	    if (i == 2) {
		PruesseRuskeyGenBasis();
	    } else {
		PruesseRuskeyGenStep( i-1 );
	    }
	)
}

void PruesseRuskeyGen()
{
	PruesseRuskey(
	    uGenBegin(), 
	    uGenProc(), 
	    uGenEnd(),
	    if (sM == 1) {
		PruesseRuskeyGenBasis();
	    } else {
		PruesseRuskeyGenStep( sM );
	    }
	)
}

/* printing ... */

static void PruesseRuskeyPrintBasis()
{
	PruesseRuskeyBasis( 
	    uPrintProc() 
	)
}

static void PruesseRuskeyPrintStep( i )
int i;
{
	PruesseRuskeyStep(
	    uPrintProc(),
	    if (i == 2) {
		PruesseRuskeyPrintBasis();
	    } else {
		PruesseRuskeyPrintStep( i-1 );
	    }
	)
}

void PruesseRuskeyPrint()
{
	PruesseRuskey(
	    uPrintBegin(),
	    uPrintProc(),
	    uPrintEnd(),
	    if (sM == 1) {
		PruesseRuskeyPrintBasis();
	    } else {
		PruesseRuskeyPrintStep( sM );
	    }
	)
}

/* counting ... */

static void PruesseRuskeyCountBasis()
{
	register int a1, b1;

	register int *ss;
	register int *tt;

	register int mrb, mra;

	a1 = *sLocA1;

	if (sOldA1 != a1) { /* swap occurred */
	    sOldA1 = a1;
	    mrb = sOldmra-1;
	    mra = sOldmrb+1;
	} else {
	    mrb = sOldmrb-1;
	    mra = sOldmra-1;

	    b1 = *sLocB1;

	    ss = sLocB1 + sOldmrb;
	    if (sInc[b1][*ss++]) {
		if (sInc[b1][*ss]) {
		    mrb += 2;
		} else {
		    mrb++;
		}
	    }

	    tt = sLocA1 + sOldmra;
	    if (sInc[a1][*tt++]) {
		if (sInc[a1][*tt]) {
		    mra += 2;
		} else {
		    mra++;
		}
	    }
	}
	sOldmrb = mrb;
	sOldmra = mra;

	sCount += sMR[mrb][mra];
}

static void PruesseRuskeyCountStep( i )
int i;
{
	PruesseRuskeyStep(
	    ;,
	    if (i == 2) {
		PruesseRuskeyCountBasis();
	    } else {
		PruesseRuskeyCountStep( i-1 );
	    }
	)
}

void PruesseRuskeyCount()
{
	PruesseRuskey(
	    {
		/* setup counting hook ... */
		int a1;
		int b1;

		int *ss;
		int *tt;

		if (sM > 0) {
		    b1 = *sLocB1;
		    ss = sLocB1+1;
		    sOldmrb = 0;
		    while (sInc[b1][*ss++]) sOldmrb++;

		    a1 = *sLocA1;
		    tt = sLocA1+1;
		    sOldmra = 0;
		    while (sInc[a1][*tt++]) sOldmra++;

		    sOldA1 = a1;
		    sCount = 0;
		} else {
		    sCount = 1;
		}
	    },
	    ;,
	    (void)fprintf( stdout, "Count: %d\n", sCount );,
	    if (sM == 1) {
		PruesseRuskeyCountBasis();
	    } else {
		PruesseRuskeyCountStep( sM );
	    }
	)
}

/* adjust P table depending on what was transposed */
#define TableChangeProc(p,pp) do { \
	int e, ee;							\
									\
	e = *(p);							\
	ee = *(pp);							\
	sAccum[ee][e] += (sTally - sLast[ee][e]);			\
	sLast[e][ee] = sTally;						\
} while (0)
	
void PruesseRuskeyTableBasis()
{
	PruesseRuskeyBasisDetailed( 
	    sTally++, 
	    TableChangeProc 
	)
}

void PruesseRuskeyTableStep( i )
int i;
{
	PruesseRuskeyStepDetailed(
	    sTally++,
	    TableChangeProc,
	    if (i == 2) {
		PruesseRuskeyTableBasis();
	    } else {
		PruesseRuskeyTableStep( i-1 );
	    }
	)
}

void PruesseRuskeyTable()
{
	PruesseRuskeyDetailed(
	    {
		int i;
		int j;

		for (i = 1; i <= gN; i++) {
		    for (j = 1; j <= gN; j++) {
			sAccum[i][j] = 0;
			sLast[i][j] = 0;
		    }
		}
		sTally = 0;
	    },
	    sTally++,
	    {
		int i;
		int j;

		/* tidy up */
		for (i = 1; i < gN; i++) {
		    for (j = i+1; j <= gN; j++) {
			sAccum[gExt[i]][gExt[j]] += 
			    (sTally - sLast[gExt[i]][gExt[j]]);
		    }
		}

		/* print table */
		for (i = 1; i <= gN; i++) {
		    for (j = 1; j <= gN; j++) {
			(void)fprintf( stdout, "%d\t", sAccum[i][j] );
		    }
		    (void)fputc( '\n', stdout );
		}

		/* find best pair */
		{
		    int halfTally;
		    int bi;
		    int bj;
		    int bAccum;

		    halfTally = sTally / 2;
		    bAccum = 0;
		    bi = 0;
		    bj = 0;
		    for (i = 1; i <= gN; i++) {
			for (j = 1; j <= gN; j++) {
			    if (sAccum[i][j] <= halfTally &&
				sAccum[i][j] > bAccum) {
				bi = i;
				bj = j;
				bAccum = sAccum[i][j];
			    }
			}
		    }

		    (void)fprintf( stdout, "%d\t%d\t%d\t%d\t%.10f\n",
			bi, bj, bAccum, sTally, (double)bAccum/sTally );
		}    
	    },
	    TableChangeProc,
	    if (sM == 1) {
		PruesseRuskeyTableBasis();
	    } else {
		PruesseRuskeyTableStep( sM );
	    }
	)
}

/* clean up after processing the poset */
void PruesseRuskeyEnd()
{
	StopTimer();
}

/* called when the program quits */
void PruesseRuskeyQuit()
{
}
