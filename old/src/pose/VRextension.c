#include <stdio.h>
#include <stdlib.h>

const int MAX_POINTS = 100;

/*-------------------------------------------------------------------*/
/* rotateL:	rotates the integer array poset to the left from the right
/*			index to the left index.  The value at the left becomes
/*			the value at the right.
/*-------------------------------------------------------------------*/
void rotateL(int *poset, int left, int right) {

	int temp,i;
	
	temp = poset[left];
	for(i=left; i<right; i++) {
		poset[i] = poset[i+1];
	}
	poset[right] = temp;

}

/*-------------------------------------------------------------------*/
void print_poset(int *poset, int N) {
	int i;
	
	for (i=1; i<= N; i++) {
		printf("%d ",poset[i]);
	}
	printf("\n");
}

/*-------------------------------------------------------------------*/
/* VR:	Recursive Varol Rotem algorithm for generating linear 
/*		extensions.
/*-------------------------------------------------------------------*/

void VR(int table[MAX_POINTS+1][MAX_POINTS+1], int *poset, int N, int k) {

	int i;
	int temp;

	if (k > N) {
		print_poset(poset,N);
	}
	else {
		VR(table,poset,N,k+1);
		i=k;
		while ( (i>1) && ( table[poset[i-1]][poset[i]] != 1 ) )  {

	/*		printf("switch %d %d\n",poset[i], poset[i-1]); */
			/*switch array values*/
			temp = poset[i];			
			poset[i] = poset[i-1];
			poset[i-1] = temp;	
			VR(table,poset,N,k+1);
			i--;
		}
		rotateL(poset,i,k);
	}
}

/*-------------------------------------------------------------------*/
void main(int argc, char **argv) {

	FILE *input;
	int i,j,N;
	int left, right;
	int table[MAX_POINTS+1][MAX_POINTS+1];
	int poset[MAX_POINTS+1]; 
	int flag;
	char *fname;

	if (argc != 2) {
		fprintf(stderr, "Usage:  VRextension in_file\n");
		exit(1);
	}
	else fname = argv[1];
	if ( (input = fopen(fname, "r")) == NULL) {
		fprintf(stderr, "Error:  Could not read %s\n", fname);
		exit(1);
	}

	/* Read number of elements */
	fscanf(input,"%d",&N); 

	/* Read partial orderings */
        left = -1;
	while (left != 0) {
		fscanf(input,"%d %d", &left, &right);
		table[left][right] = 1;
		table[right][left] = 1;
	}

	/* Initialize the permutation to the identity */
	for (i=1; i<=N; i++) {
		poset[i] = i;
	}

	/* Call VR to calculate all partial orderings */
	if (N > 0) {
		VR(table,poset,N,1);
	}
	
	fclose(input);
}
