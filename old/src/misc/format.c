/* This program converts the output of the polyominos program
	written by John Boyer into a format readable by 
	the program 'PentShowSolution'.  The total number of
	polyominoes generated is dumped to the file poly.temp  */

/* Written by Joe Sawada April 97 */


#include <stdio.h>
#include <stdlib.h>

#define MAX 50

int main(int argc, char* argv[]) {

	char TOKEN[80];
	int count = 0 ;
	char ch;
	int a[MAX][MAX];
	int i,j,col,total,num,limit,row;

	if (argc != 3) { fprintf(stderr, "Usage: format n limit outformat\n"); exit(1); }

	num = atoi(argv[1]);
	limit = atoi(argv[2]);
	
	for(row=0; row<num; row++)
		for(col=0; col<num; col++) a[row][col] = 0;
	total = 0;
	row = 0; col = 0;
	printf("%d %d ", num,num);
	gets(TOKEN);
	gets(TOKEN);
	gets(TOKEN);
	ch = getchar();
	while ((ch != 'N') && (total < limit)) {
		while(1) {
			if (ch == 'o') {
				a[row][col] = 1;
				count++;
			}
			col++;
			if (ch == '\n') break;
			ch = getchar();
		}
		row++;
		col = 0;
		if (count == num) {
			for(i=0; i<num; i++) {
				for(j=0; j<num; j++) {
					if (a[i][j]==1) printf("X");
					else printf(".");
				}
				printf(" ");
			}

			for(i=0; i<num; i++)
				for(j=0; j<num; j++) a[i][j] = 0;
			total++;
			count = 0;
			row = 0;
			/* read blank line */
			gets(TOKEN);
		}
		ch = getchar();
	}
	if (total < limit) {
		gets(TOKEN);
		printf("*<BR>N%s\n",TOKEN);
	}
	else {
		printf("*<BR><BLINK><B><FONT COLOR=ff0000>Oops: </BLINK></B><FONT> You asked for more objects than the current limit of %d.<BR>\n", limit);

	}
 	return 0;	
}
