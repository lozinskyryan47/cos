//========================================================================
// By : John Boyer
// On : 17 NOV 1995
// For: Dr. Ruskey, CSC 520
//
/* This program is an implementation of Redelmeier's algorithm for
   generating the set of fixed polyominoes.  A description of the
   algorithm appeared in Discrete Mathematics 36 (1981) 191-203.
   I added a canonicity check to reject isomorphic configurations.

   The CSet class is included to represent sets.  I originally developed
   the CSet class to assist with the first homework assignment of this
   class, which included a backtracking problem. Although it seemed like
   more work than necessary at the time, the CSet class has proven quite
   useful in this project, although I had to fix a minor bug in the copy
   constructor (not used in the first assignment) and also add two new
   functions to it.

   This program is submitted to fulfill the requirement of CSC 520 that
   a graduate student must read a journal article and demonstrate an
   understanding of it.  I've actually read two articles; the first was
   by Ronald Read in Annals of Discrete Mathematics 2 (1978) 107-120.
   The article describes how to eliminate isomorphism in constant time
   per node during combinatorial generation... subject to a few
   conditions.  The spirit of the algorithm is that it will also prune
   the search tree very effectively by only expanding those subtrees
   whose roots contain the canonical form of a combinatorial object,
   i.e. the particular object that is chosen by some method to represent
   its equivalence class of isomorphic objects.

   Unfortunately, there did not seem to be an straightforward way to
   apply this algorithm to polyominoes because the canonical forms of
   larger polyominoes cannot be generated from only the canonical forms
   of their ancestors.  We looked to Redelmeier hoping to find a coding
   system that would eliminate this problem.  However, Redelmeier was
   seemingly more interested in counting polyominoes than in cataloging
   the actual shapes.  He first presented a technique for generating
   (and counting) fixed polyominoes.  Once this is done, the number of
   free (non-isomorphic) polyominoes can be deduced in a very small
   amount of time.  The reason, as Redelmeier discovered, is that the
   number of polyominoes that contain one or more symmetries is rather
   small.  He therefore presented algorithms that would generate all of
   the polyominoes that had various types of symmetries.  All S if these
   shapes correspond to a number of fixed polyominoes F, but most of
   the fixed polyominoes correspond to polyominoes that have no
   symmetries.  With N the total number of fixed polyominoes, (N-F) is
   the number of these fixed polyominoes that have no symmetries, and
   S + (N-F)/8 is the number of free polyominoes, since each non-symmetric
   free polyomino generates 8 non-symmetric fixed polyominoes.  Thus, one
   can know how many free polyominoes there are without actually
   generating them.

   It is actually possible to apply part of Read's idea to the polyomino
   problem.  As we generate each fixed polyomino, we can apply all of the
   rotations and flips to it and if any result has a lesser coding, then
   we know (in constant time) to reject the current object since we've
   already accepted an isomorphic object having the least code.
   This is a great advantage, since we aren't having to search the
   previously generated objects to determine isomorphism.  However, we are
   losing the most important benefit of Read's algorithm:  the pruning.
   We are still be expanding the subtrees whose roots contain
   non-canonical objects.

   Copyright 1995 by John M. Boyer
   Permission to use and distribute this program, its output and
   and its source is granted provided that the author is acknowledged
   in works that use this program, its output or any portion of its
   source code.
*/
//========================================================================

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "cset.h"

#include <fstream.h>

#define SHAPE_FILLEDCELL 'o'
#define SHAPE_EMPTYCELL  ' '

// These globals help to stop the recursion before the exhaustive search
// is completed (an unnatural termination).

long NumPolyominoes, MaxNumPolyominoes;
int  OutputWasTruncated;

// Flag that specifies whether or not the objects generated will be
// sent to output.

int CountOnly;

//========================================================================
// CLASS POLYOMINOSET
//========================================================================

class PolyominoSet : public CSet
{
int  NumCells, MaxNumCells;

     int  CloserToCanonical(PolyominoSet&);
     void HorizFlip();
     void DescendingDiagonalFlip();

char *Matrix;
int  Left, Width, Height;

     void GetMatrix();
     void HFlip();
     void DFlip();
     void SetMatrix();
     void ClearMatrix(int Constructing=0);

public:

     PolyominoSet(int);
     PolyominoSet();
     PolyominoSet(PolyominoSet& P);
     ~PolyominoSet() { ClearMatrix(); };

     void Initialize(int);

     int  GetNumCells() { return NumCells; };
     void IncNumCells() { NumCells ++; };
     void DecNumCells() { NumCells --; };

     void GetAdjacentCells(PolyominoSet&);
     void WriteMatrix(ostream&);
     int  CanonicalForm();
};


//========================================================================
// Constructor
//
// M is the number of cells in the polyominoes that we want to generate.
//========================================================================

PolyominoSet::PolyominoSet(int M)
	    : CSet(2*M*M)
{
     NumCells = 0;
     MaxNumCells = M;
     ClearMatrix(1);
}

//========================================================================
//========================================================================

PolyominoSet::PolyominoSet()
            : CSet()
{
     NumCells = 0;
     MaxNumCells = 0;
     ClearMatrix(1);
}

//========================================================================
// Copy constructor for value parameters
//
// P is the polyomino that we want to duplicate on our stack
//========================================================================

PolyominoSet::PolyominoSet(PolyominoSet& P)
	    : CSet(P)
{
     NumCells = P.NumCells;
     MaxNumCells = P.MaxNumCells;
     ClearMatrix(1);
}

//========================================================================
// Initializer allows one to set the properties of the set at some point
// after its creation, or to reset the properties to something other than
// the properties given at creation.
//========================================================================

void PolyominoSet::Initialize(int M) {
     CSet::Initialize(2*M*M, 0);
     MaxNumCells = M;
     NumCells = 0;

     ClearMatrix();
}

//========================================================================
// PRIVATE:  ClearMatrix()
//
// Cleans up the internal matrix.
//========================================================================

void PolyominoSet::ClearMatrix(int Constructing) {
     if (Matrix != NULL)
	 if (!Constructing) delete Matrix;

     Left = Width = Height = 0;
     Matrix = NULL;
}

//========================================================================
// PRIVATE:  GetMatrix()
//
// Creates a small matrix containing only the space necessary to represent
// the actual polyomino.  Extracts the essence of the polyomino from the
// rather large set that contains it. 
//========================================================================

void PolyominoSet::GetMatrix() {
int  _2P, I, J, Element, Right, Temp;

// Compute left border and height of the embedded polyomino's bounding box

     _2P = MaxNumCells<<1;
     Left = _2P;
     Height = MaxNumCells;

     for (I=0; I < MaxNumCells; I++)
     {
	  Element = GetLeastElement(I*_2P);
	  if (Element < 0)
	  {
	      Height = I;
              break;
	  }
	  else if (Left > Element - I*_2P)
		   Left = Element - I*_2P;
     }

// Compute width of polyomino bounding box

     Right = 0; 
     for (I = 0; I < Height; I++)
     {
	  Temp = GetGreatestElement(I*_2P + _2P-1);
	  if (Right < Temp - I*_2P)
	      Right = Temp - I*_2P;
     }

     Width = (Right - Left) + 1;

// The commented line below is where the bug was.  It was
// replaced by the loop above.  The line generated the width
// of the largest possible bounding box, rather than generating
// precisely the width required to bound the particular polyomino
// embedded within 'this' set.

//     Width = MaxNumCells - Height + 1;

// Create Matrix to store embedded polyomino

     Matrix = new char[Width*Height + 1];
     memset(Matrix, SHAPE_EMPTYCELL, Width*Height);

// Extract the embedded polyomino

     for (I=0; I < Height; I++)
	  for (J=0; J < Width; J++)
	  {
	      Element = I*_2P + Left + J;
	      if (Element << *this)
		  Matrix[I*Width + J] = SHAPE_FILLEDCELL;
	  }
}

//========================================================================
// PRIVATE:  SetMatrix()
//
// Uses the Matrix, previously created by GetMatrix() to set the contents
// of the polyomino set. This is done after the matrix has been flipped
// in some way, so that the result of the flip can be compared with the
// original for canonicity.
//========================================================================

void PolyominoSet::SetMatrix() {
int  _2P, I, J;

// Compute new offset for leftmost edge of polyomino

     for (Left=MaxNumCells, I = 0; I < Width; I++)
	  if (Matrix[I] == SHAPE_FILLEDCELL) break;
	  else Left --;

// Clear the polyomino set

     FillSet(0, 1);
     -- (*this);

// Embed the polyomino into the polyomino set

     _2P = MaxNumCells<<1;

     for (I=0; I < Height; I++)
	  for (J=0; J < Width; J++)
	      if (Matrix[I*Width + J] == SHAPE_FILLEDCELL)
		  *this <<= (I*_2P + Left + J);

// Get rid of the matrix representation of the polyomino

     ClearMatrix();
}

//========================================================================
// WriteMatrix()
//
// Dumps the essence of the polyomino to the given output stream.  This is
// useful for displaying the polyomino shape instead of the internal set
// of numbers used to generate it.
//========================================================================

void PolyominoSet::WriteMatrix(ostream& theOutput) {
int  I, J;
char *Line;

     GetMatrix();

     Line = new char[Width+2];

     for (I=Height-1; I >= 0; I--)
     {
	  for (J=0; J < Width; J++)
	       Line[J] = Matrix[I*Width + J];
	  Line[Width] = '\n';
	  Line[Width+1] = '\0';
	  theOutput << Line;
     }

     delete Line;

     ClearMatrix();
}

//========================================================================
// PRIVATE:  HFlip()
//
// Performs a horizontal flip on the internal matrix created to represent
// the polyomino.
//========================================================================

void PolyominoSet::HFlip() {
char *Line;

     Line = new char[Width+1];

     for (int I=0; I < Height/2; I++)
     {
	  memcpy(Line, Matrix + I*Width, Width);
	  memcpy(Matrix + I*Width, Matrix + (Height-1-I)*Width, Width);
	  memcpy(Matrix + (Height-1-I)*Width, Line, Width);
     }

     delete Line;
}

//========================================================================
// PRIVATE:  DFlip()
//
// Performs a flip along the descending diagonal axis on the internal
// matrix created to represent the polyomino.
//========================================================================

void PolyominoSet::DFlip() {
char *NewMatrix;
int  NewWidth, NewHeight, I, J;

     NewWidth = Height;
     NewHeight = Width;
     NewMatrix = new char[NewWidth*NewHeight];

     for (I=0; I < Height; I++)
	  for (J=0; J < Width; J++)
	       NewMatrix[(NewHeight-1-J)*NewWidth + (NewWidth-1-I)]
		       = Matrix[I*Width + J];

     ClearMatrix();

     Matrix = NewMatrix;
     Height = NewHeight;
     Width = NewWidth;
}

//========================================================================
//========================================================================

void PolyominoSet::HorizFlip() {
     GetMatrix();
     HFlip();
     SetMatrix();
}

//========================================================================
//========================================================================

void PolyominoSet::DescendingDiagonalFlip() {
     GetMatrix();
     DFlip();
     SetMatrix();
}

//========================================================================
// CloserToCanonical()
//
// Tests whether the smallest element of P that does not equal an element
// in this set smaller than the corresponding element in this set.
// The canonical polyomino is the one with the smallest numbers in it.
//
// NOTE: We presume that the two objects being compared here have the same
// cardinality (the polyominoes are the same size), so if the two sets
// are equal, then P is not closer to canonical so return 0.  The reason
// is that the fixed polyomino generate will only generate identical
// objects once, so if we flip a given object and get something
// that is identical, we must continue to assume that this is the first
// time we've seen it (until we finish all flips or find a contradiction).
//========================================================================

int  PolyominoSet::CloserToCanonical(PolyominoSet& P) {
int  LeastP, LeastThis;

     LeastP = LeastThis = MaxNumCells;

     while ((LeastThis = GetLeastElement(LeastThis+1)) > 0)
     {
         LeastP = P.GetLeastElement(LeastP+1);
	 if (LeastThis != LeastP)
             return LeastP < LeastThis ? 1 : 0;
     }

     return 0;
}

//========================================================================
// This function was written to help debug what turned out to be an
// error in GetMatrix() that caused me to generate slightly more
// non-isomorphic polyominoes than exist for N>=7.
//========================================================================

/*
int  NumFixedPolyominoes = 0;

void ReportError(char *ErrorMsg, int Counter=-1) {
char Ch;

     cout << ErrorMsg << Counter << "-- Hit a letter key then <Enter>";
     cin >> Ch;
}

int  PolyominoSet::_CanonicalForm_FullCheck() {
PolyominoSet *List;
int CanonicalFlag, EqualityCount, I;

     List = new PolyominoSet[7];

     for (I = 0; I < 7; I++)
	  List[I].Initialize(MaxNumCells);

     if (List == NULL)
     {
	 ReportError("Out of memory");
         return _CanonicalForm();
     }

     List[0] = *this;
     if (List[0].GetStatus() != OK)
     {
	 ReportError("Set Assignment Failed");
         return _CanonicalForm();
     }

     List[0].HorizFlip();

     List[1] = List[0];
     List[1].DescendingDiagonalFlip();
     List[2] = List[1];
     List[2].HorizFlip();
     List[3] = List[2];
     List[3].DescendingDiagonalFlip();
     List[4] = List[3];
     List[4].HorizFlip();
     List[5] = List[4];
     List[5].DescendingDiagonalFlip();
     List[6] = List[5];
     List[6].HorizFlip();

     CanonicalFlag = 1;
     EqualityCount = 0;

     for (I = 0; I < 7; I++)
     {
	  if (List[I].GetStatus() != OK)
	  {
	      ReportError("Set Assignment Failed");
	      return _CanonicalForm();
	  }

	  if (List[I] == *this)
	      EqualityCount++;

	  if (CloserToCanonical(List[I]))
              CanonicalFlag = 0;
     }

     if (CanonicalFlag)
	 switch (EqualityCount)
         {
	    case 0  : NumFixedPolyominoes+=8; break;
	    case 1  : NumFixedPolyominoes+=4; break;
	    case 3  : NumFixedPolyominoes+=2; break;
	    case 7  : NumFixedPolyominoes+=1; break;
	    default : WriteMatrix(cout);
		      ReportError("Wrong number of equalities", EqualityCount);
		      for (I = 0; I < 7; I++)
                      {
			   List[I].WriteMatrix(cout);
                      }
		      ReportError("Wrong number of equalities", EqualityCount);
		      break;
         }

     delete List;

     return CanonicalFlag;
}
*/

//========================================================================
// Notice that all possible alternate configurations can be generated using
// only a sequence of horizontal and descending diagonal flips.  Other
// possibilities exist (e.g. vertical plus rotate pi/2) but these two
// seemed easiest to code and fastest.
//========================================================================

int  PolyominoSet::CanonicalForm()
{
PolyominoSet Temp(*this);

// Check if flip about horizontal axis produces an object
// that is closer to canonical (possibly THE canonical form)
 
     Temp.HorizFlip();
     if (CloserToCanonical(Temp)) return 0;

// Check if a rotate by pi/2 radians produces a more canonical form
 
     Temp.DescendingDiagonalFlip();
     if (CloserToCanonical(Temp)) return 0;

// Check if a flip about the ascending diagonal axis produces
// a more canonical form
 
     Temp.HorizFlip();
     if (CloserToCanonical(Temp)) return 0;

// Check if a rotate by pi radians produces a more canonical form

     Temp.DescendingDiagonalFlip();
     if (CloserToCanonical(Temp)) return 0;

// Check if a flip about the vertical axis produces a more canonical form

     Temp.HorizFlip();
     if (CloserToCanonical(Temp)) return 0;

// Check if a rotate by 3pi/2 radians produces a more canonical form

     Temp.DescendingDiagonalFlip();
     if (CloserToCanonical(Temp)) return 0;

// Check if a flip about the descending diagonal axis produces a more
// canonical form

     Temp.HorizFlip();
     if (CloserToCanonical(Temp)) return 0;

     return 1;
}

//========================================================================
// GetAdjacentCells()
//
// P represents the current polyomino, whose adjacent cells can be used
//    to create a polyomino that is one cell larger than P.
// Least starts at P since 0 to P-1 are the forbidden cells that border
//    the negative x-axis.
//========================================================================

void PolyominoSet::GetAdjacentCells(PolyominoSet& thePolyomino) {
int P = thePolyomino.MaxNumCells;
int Least = P-1, _2P = P<<1;
int Adjacent;

     while ((Least = thePolyomino.GetLeastElement(Least+1)) > 0)
     {
// Test whether cell on left is valid "reachable" cell

	 Adjacent = Least-1;
	 if (Adjacent > P && !(Adjacent << thePolyomino))
             *this <<= Adjacent;

// Test whether cell on right is valid "reachable cell
// The use of modulus protects the +1 from wrap-around error

	 Adjacent = Least+1;
	 if (!(Adjacent << thePolyomino) && Adjacent % _2P)
	     *this <<= Adjacent;

// Test whether cell below is valid "reachable" cell

	 Adjacent = Least - _2P;
	 if (Adjacent > P && !(Adjacent << thePolyomino))
             *this <<= Adjacent;

// Test whether cell above is valid "reachable" cell

	 Adjacent = Least + _2P;
	 if (Adjacent < thePolyomino.GetSize() && !(Adjacent << thePolyomino))
	     *this <<= Adjacent;
     }
}

//========================================================================
// According to Redelmeier's algorithm, the fixed polyominoes can be
// generated exactly once if the "untried" set, which is really the
// subtree set Sk, contains only those adjacent cells that are
//
// 1) Above the x-axis
// 2) Not on the lowest row of the negative x-axis
// 3) Not used by any older brother
// 4) Not used by any older brother of any ancestor in the search tree
//
// Condition 1 is met by construction.  The elements 0 to 2P-1 represent
//      the first row above the x-axis.
// Condition 2 is met by GetAdjacentCells(), which removes all cells with
//      a value < P from the untried set.
// Condition 3 is met by the standard backtrack procedure.  Each time we
//      finish exploring a subtree, the last line of the loop removes
//      the cell from the polyomino.
// Condition 4 is met by the AncestorRestrictors set, which keeps track
//      of those cells that have already been tried by ancestors and
//      their older brothers.  The loop always adds the current cell to
//      the set because a copy is passed downtried so far on a level.  This information is passed down
//      to the next level of recursion so that the cells used by the
//      "ancestor's older brothers" can be removed from the next level's
//      untried set (see the line UntriedSet -= AncestorRestrictors;).
//========================================================================

void PolyominoBacktrack(ostream& theOutput, int RejectIsomorphs,
			int MaxDepth, PolyominoSet& CurPolyomino,
				      PolyominoSet AncestorRestrictors)
{
PolyominoSet UntriedSet(MaxDepth);
int  Least;

// Here we obey the output size restriction by cutting off the recursion

     if (MaxNumPolyominoes > 0 && NumPolyominoes >= MaxNumPolyominoes)
     {
	 OutputWasTruncated = 1;
	 return;
     }

// Here is the P(x) test.  Do we have a solution?
// If so, then "print" the polyomino

     if (CurPolyomino.GetNumCells() == MaxDepth)
     {
	 if (!RejectIsomorphs || CurPolyomino.CanonicalForm())
	 {
	      NumPolyominoes ++;
	      if (!CountOnly)
              {
	          CurPolyomino.WriteMatrix(theOutput);
//	          theOutput << CurPolyomino << '\n';
		  theOutput << '\n';
              }
	 }

	 return;
     }

// The set of all subtrees requiring exploration, usu.
// denoted Sk, is here given as the "Untried" set:
// the set of all cells adjacent to cells in the
// current polyomino, except those used by the ancestor's
// older brothers.  

     UntriedSet.GetAdjacentCells(CurPolyomino);
     UntriedSet -= AncestorRestrictors;

// Exhaust all possibilities in the untried set

     while ((Least = UntriedSet.GetLeastElement()) >= 0)
     {
           UntriedSet >>= Least;
	   AncestorRestrictors <<= Least;

	   CurPolyomino <<= Least; CurPolyomino.IncNumCells();

	   PolyominoBacktrack(theOutput, RejectIsomorphs, MaxDepth,
			      CurPolyomino, AncestorRestrictors);

	   CurPolyomino >>= Least; CurPolyomino.DecNumCells();
     }
} 

//========================================================================
//========================================================================

void PrintHeader(ostream& theOutput)
{
     theOutput << "THE POLYOMINO GENERATOR\n";
     theOutput << "by John M. Boyer\n\n";
}

//========================================================================
// The Start polyomino contains the single-cell polyomino (P=1), and there
// no starting restrictions on growth.  NumPolyominoes is cleared to 0 to
// indicate that we haven't yet generated any polyominoes of size MaxDepth
//========================================================================

void GeneratePolyominoes(char *Filename, int RejectIsomorphs, int MaxDepth)
{
PolyominoSet StartPolyomino(MaxDepth), StartRestrictors(MaxDepth);
fstream Outfile;
ostream *theOutput;

     NumPolyominoes = 0;
     OutputWasTruncated = 0;
     StartPolyomino <<= MaxDepth;
     StartPolyomino.IncNumCells();

     if (Filename != NULL)
     {
	  Outfile.open(Filename, ios::out);
	  theOutput = &Outfile;
     }
     else theOutput = &cout;

     PrintHeader(*theOutput);
     PolyominoBacktrack(*theOutput, RejectIsomorphs, MaxDepth,
	                StartPolyomino, StartRestrictors);

     if (!OutputWasTruncated)
	  *theOutput << "Number of " << (RejectIsomorphs?"free":"fixed")
		     << " polyominoes of size " << MaxDepth << " is "
		     << NumPolyominoes << "\n\n";

     if (Filename != NULL)
	 Outfile.close();
}

//========================================================================
// Handles interactive mode
//========================================================================

void InteractiveMode()
{
int  MaxDepth, RejectIsomorphs;
char Reject, JustCount, Filename[80], *theFilename;

     cout << "At the cursor, enter the filename for output (or the word 'screen')\n";
     cout << "then a y or n for whether or not to reject isomorphs, then a y or n\n";
     cout << "for whether or not to just count the objects, then enter the\n";
     cout << "number of cells per polyomino, then max number of polyominoes (or\n";
     cout << "use -1 for no truncation).\n\n";
     cout << "The Cursor->";
     cin >> Filename >> Reject >> JustCount >> MaxDepth >> MaxNumPolyominoes;

     theFilename = NULL;
     if (strcmp(Filename, "screen") != 0)
         theFilename = Filename;

     RejectIsomorphs = tolower(Reject)=='y' ? 1 : 0;
     CountOnly = tolower(JustCount)=='y' ? 1 : 0;

     GeneratePolyominoes(theFilename, RejectIsomorphs, MaxDepth);

}

//========================================================================
// Handles command-line parameters
// Apologies to UNIX buffs; this was developed on my home PC, and
// getopt() was unavailable.
// The default of size 6 is used because this size is what inspired
// my investigation into polyomino generation.
//========================================================================

void CommandLineMode(int argc, char **argv)
{
int  MaxDepth=6, RejectIsomorphs=1;
char *theFilename=NULL;
int I;

     MaxNumPolyominoes = -1; // No truncation
     CountOnly = 0;

     for (I = 0; I < argc; I++)
     {
	  if (strcmp(argv[I], "-h") == 0)
          {
	      cout << "-i for include isomorphs (i.e. generate the fixed polyominoes)\n";
	      cout << "-c for count only (objects generated but not sent to output)\n";
	      cout << "-f filename to send output to a file\n";
	      cout << "-n ## to specify polyomino size\n";
	      cout << "-t ## to truncate (limit number of objects generated)\n";
	  }

	  else if (strcmp(argv[I], "-i") == 0)
	      RejectIsomorphs = 0;

	  else if (strcmp(argv[I], "-c") == 0)
	      CountOnly = 1;

	  else if (strcmp(argv[I], "-f") == 0)
	  {
	      if (argc > ++I)
                  theFilename = argv[I];
	  }

	  else if (strcmp(argv[I], "-n") == 0)
	  {
	      if (argc > ++I)
              {
		  MaxDepth = atoi(argv[I]);
                  if (MaxDepth <= 0) MaxDepth = 6;
	      }
	  }

	  else if (strcmp(argv[I], "-t") == 0)
	  {
	      if (argc > ++I)
		  MaxNumPolyominoes = atol(argv[I]);
	  }
     }

     GeneratePolyominoes(theFilename, RejectIsomorphs, MaxDepth);
}

//========================================================================
//========================================================================

int main (int argc, char *argv[] ) {
     if (argc < 2) InteractiveMode();
     else CommandLineMode(argc, argv);
	 return( 0 );
}

