/* This file called "fibo.c" */
/* Written and copyrighted by Scott Lausch, March 1998 */
/* Ownership: Scott Lausch */
#include <stdio.h>
#include <stdlib.h>
#include "ProcessInput.c"
#define MAXINPUT 12  /* maximum input size */

int p[MAXINPUT + 1];
int count = 0;
int retval = 0;
extern int N;
extern int K;
extern int LIMIT;
int LIMITerror = 1;

void Fib(int i);
void Bif(int i);


void Reflections(int KK)
	{
	int i, location;  /* $location is 3, 2, 1 or 0. */

	printf ("<TABLE BORDER='0' CELLPADDING='0' CELLSPACING='0'><TR><TD>\n");
	location = 0;
	for (i = 1; i <= KK; i++)
		{
		if (p[i] == 0)
			{
			switch (location)
				{
			 case 0:
				{
				printf ("<IMG SRC=ico/rdown.gif width=18 height=30>");
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 3;
				break;
				}
			 case 3:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				printf ("<IMG SRC=ico/rup.gif width=18 height=30>");
				location = 0;
				break;
				}
			 case 2:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 3;
				break;
				}
			 case 1:
				{
				printf ("<IMG SRC=ico/rup.gif width=18 height=30>");
				location = 0;
				break;
				}
			 default:
				{;}
				}
			}
		else
			{
			switch (location)
				{
			 case 0:
				{
				printf ("<IMG SRC=ico/rdown.gif width=18 height=30>");
				location = 1;
				break;
				}
			 case 3:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 2;
				break;
				}
			 case 2:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 3;
				break;
				}
			 case 1:
				{
				printf ("<IMG SRC=ico/rup.gif width=18 height=30>");
				location = 0;
				break;
				}
			 default:
				{;}
				}
			}
		}
	switch (location)
		{
	 case 0:
		{
		printf ("<IMG SRC=ico/rdown.gif><IMG SRC=ico/rblank.gif width=18 height=30>");
		break;
		}
	 case 1:
		{
		printf ("<IMG SRC=ico/rup.gif width=18 height=30>");
		break;
		}
	 case 2:
		{
		printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
		break;
		}
	 case 3:
		{
		printf ("<IMG SRC=ico/rblank.gif width=18 height=30><IMG SRC=ico/rup.gif width=18 height=30>");
		break;
		}
		}
	printf ("</TR><TR><TD>");
	location = 0;
	for (i = 1; i <= KK; i++)
		{
		if (p[i] == 0)
			{
			switch (location)
				{
			 case 0:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				printf ("<IMG SRC=ico/rdown.gif width=18 height=30>");
				location = 3;
				break;
				}
			 case 3:
				{
				printf ("<IMG SRC=ico/rup.gif width=18 height=30>");
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 0;
				break;
				}
			 case 2:
				{
				printf ("<IMG SRC=ico/rdown.gif width=18 height=30>");
				location = 3;
				break;
				}
			 case 1:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 0;
				break;
				}
				}
			}
		else
			{
			switch (location)
				{
			 case 0:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 1;
				break;
				}
			 case 3:
				{
				printf ("<IMG SRC=ico/rup.gif width=18 height=30>");
				location = 2;
				break;
				}
			 case 2:
				{
				printf ("<IMG SRC=ico/rdown.gif width=18 height=30>");
				location = 3;
				break;
				}
			 case 1:
				{
				printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
				location = 0;
				break;
				}
				}
			}
		}
	switch (location)
		{
	 case 0:
		{
		printf ("<IMG SRC=ico/rblank.gif width=18 height=30><IMG SRC=ico/rdown.gif width=18 height=30>");
		break;
		}
	 case 1:
		{
		printf ("<IMG SRC=ico/rblank.gif width=18 height=30>");
		break;
		}
	 case 2:
		{
		printf ("<IMG SRC=ico/rdown.gif width=18 height=30>");
		break;
		}
	 case 3:
		{
		printf ("<IMG SRC=ico/rup.gif width=18 height=30><IMG SRC=ico/rblank.gif width=18 height=30>");
		break;
		}
		}

	printf ("</TR></TABLE>\n");
	}

void PrintIt(int KK)
	{
	int i, j;

	if (count == LIMIT) { retval = LIMITerror; return; }
	++count;
	printf ("<TR>\n");
	if (out_format & 1)
		{
		printf ("<TD ALIGN=CENTER>");
		for (i = 1; i <= KK; i++)
			{
			printf (" %2d ", p[i]);
			}
		printf ("<BR></TD>\n");
		}
	if (out_format & 4)
		{
		printf ("<TD ALIGN=CENTER>");
		j = 0;
		for (i = 1; i <= KK; i++)
			{
			if (p[i] == 0)
				{
				printf (" 1 "); j++;
				}
			else
				{
				printf (" 2 "); j += 2;
				if (i < KK)
					{
					i++;                   
					}
				}
			}
		if (j < KK + 1)
			{
			printf (" 1 ");
			}
		printf ("<BR></TD>\n");
		}
	if (out_format & 2)
		{
		printf ("<TD>");
		Reflections(KK);
		printf ("</TD>");
		}
	}

void Fibonacci(int n, int val, int i)
	{
	p[i] = val;
	if (n == 1) { PrintIt(i); return; }

	if (val == 0)
		{
		Fibonacci(n - 1, 0, i + 1);
		Fibonacci(n - 1, 1, i + 1);          
		}
	else
		{
		Fibonacci(n - 1, 0, i + 1);
		}
	}


void Fib(int i)
        {
	/* base case 1 */
        if (i == N)
                {
                p[i] = 0;
                PrintIt(i);
                p[i] = 1;
                PrintIt(i);
                return;
                }
	/* base case 2 */
        if (i == N - 1)
                {
                p[i] = 1; p[i + 1] = 0;
                PrintIt(i + 1);
                p[i] = 0; p[i + 1] = 0;
                PrintIt(i + 1);
                p[i] = 0; p[i + 1] = 1;
                PrintIt(i + 1);
                return;
                }
	/* base case 3 */
	if (i == N - 2)
		{
                p[i] = 1; p[i + 1] = 0; p[i + 2] = 0;
                PrintIt(i + 2);
                p[i] = 1; p[i + 1] = 0; p[i + 2] = 1;
                PrintIt(i + 2);
                p[i] = 0; p[i + 1] = 0; p[i + 2] = 1;
                PrintIt(i + 2);
                p[i] = 0; p[i + 1] = 0; p[i + 2] = 0;
                PrintIt(i + 2);
                p[i] = 0; p[i + 1] = 1; p[i + 2] = 0;
                PrintIt(i + 2);
                return;
		}
        p[i] = 1; p[i + 1] = 0;
        Bif( i + 2 );
        p[i] = 0;
        Bif( i + 1 );
        }

void Bif(int i)
        {
	/* base case 1 */
        if (i == N)
                {
                p[i] = 1;
                PrintIt(i);
                p[i] = 0;
                PrintIt(i);
                return;
                }
	/* base case 2 */
        if (i == N - 1)
                {
                p[i] = 0; p[i + 1] = 1;
                PrintIt(i + 1);
                p[i] = 0; p[i + 1] = 0;
                PrintIt(i + 1);
                p[i] = 1; p[i + 1] = 0;
                PrintIt(i + 1);
                return;
                }
	/* base case 3 */
	if (i == N - 2)
		{
                p[i] = 0; p[i + 1] = 1; p[i + 2] = 0;
                PrintIt(i + 2);
                p[i] = 0; p[i + 1] = 0; p[i + 2] = 0;
                PrintIt(i + 2);
                p[i] = 0; p[i + 1] = 0; p[i + 2] = 1;
                PrintIt(i + 2);
                p[i] = 1; p[i + 1] = 0; p[i + 2] = 1;
                PrintIt(i + 2);
                p[i] = 1; p[i + 1] = 0; p[i + 2] = 0;
                PrintIt(i + 2);
                return;
		}
        p[i] = 0;
        Fib( i + 1 );
        p[i] = 1; p[i + 1] = 0;
        Fib( i + 2 );
        }



void GenFib(int n)
	{
	if (n != 0)
		{
		if (K)
			{
			p[0] = 0;
			Fib(1);			
			}
		else
			{
			p[0] = 0;
			Fibonacci(n, 0, 1);
			Fibonacci(n, 1, 1);
			}
		}
	else
		{
		printf ("Sorry.  N equals zero.  Goodbye");
		}
	}



int main(int argc, char *argv[])
	{
	/* get input from Perl call */
	ProcessInput(argc, argv);

	GenFib(N);
	
	printf("</TABLE><P>%d items output\n", count);
	exit(retval);
	}

