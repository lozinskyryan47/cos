#include <stdio.h>
#include <stdlib.h>
#include "ProcessInput.c"
#define TRANSP
#include "commonio.h"

#define TRUE 1
#define FALSE 0
#define odd(x) ( (x)%2==1 ? TRUE : FALSE )
#define LIMIT_ERROR -1
#define MAX_SIZE 100

void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );
int NN;

int count = 0;

int perm[MAX_SIZE];
int taken[MAX_SIZE + 1];


void genocchi (int value, int position); /* last value placed and position
					    in array where it was placed */

void Display(void);

 
int main(int argc, char *argv[])
	{
	int i;

	ProcessInput(argc, argv);

	NN = N;

	/* indicate no numbers have been used */
	for (i = 1; i <= N; i++)
		{
		taken[i] = 0;
		}

	genocchi (0, -1);

	printf("\n</TABLE><P> Genocchi sequences = %d", count);

	exit(0);
	}

void genocchi (int value, int position)
	{
	int i;		/* loop control */

	if (position == -1)	/* starting case */
		{
		for (i = 2; i <= N; i++)
			{
			perm[0] = i;
			taken[i] = 1;
			genocchi (i, 0);
			taken[i] = 0;
			}
		return;
		}

	if (value == 2)
		{
		perm[position + 1] = 1;
		taken[1] = 1;
		genocchi (1, position + 1);
		taken[1] = 0;
		perm[position + 1] = 0;
		return;
		}

	if (position == N - 1)
		{
		if (value % 2 == 0)	/* cannot end with an even */
			return;
		Display();
		return;
		}

	if (value == N - 1)
		{
		if (position == N - 2)	/* implies N must be last - N is even */
			return;
		if (taken[N])		/* N is already used */
			return;
		perm[position + 1] = N;
		taken[N] = 1;
		genocchi (N, position + 1);
		taken[N] = 0;
		perm[position + 1] = 0;
		return;
		}

	if (value % 2 == 0)
		{
		/* search for a smaller value not taken yet */
		for (i = 2; i < value; i++)
			{
			if (taken[i])
				continue;
			perm[position + 1] = i;
			taken[i] = 1;
			genocchi (i, position + 1);
			taken[i] = 0;
			perm[position + 1] = 0;
			}
		return;
		}

	if (value % 2 == 1)
		{
		/* search for a larger value not taken yet */
		for (i = value + 1; i <= N; i++)
			{
			if (taken[i])
				continue;
			perm[position + 1] = i;
			taken[i] = 1;
			genocchi (i, position + 1);
			taken[i] = 0;
			perm[position + 1] = 0;
			}
		return;
		}

	}

void Display(void)
	{
	int i;

	Pi[0] = N + 1;
	iP[0] = N + 1;
	printf("\n");
	for (i = 0; i < N; i++)
		{
		Pi[i + 1] = perm[i];
		iP[perm[i]] = i + 1;
		}

	PrintIt(0,0);
	}






/*  from  perm.c
	   Provide routines for I/O of permutations

           Added KoDeZ to add commas if N > 10 
*/


void PrintIt(int x, int y) {
    int i;
    
    ++count;
    if(count > LIMIT) exit (-1); // Do this if we exceed max allowable

    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<NN; i++) printf("%d, ",Pi[i]); // one line
	    printf("%d",Pi[NN]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=NN; i++) iP[Pi[i]] = i;
	    for(i=1; i<NN; i++) printf("%d, ",iP[i]); // one line
	    printf("%d",iP[NN]);
	    printf("<BR></TD>");
	}

    if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(Pi, NN);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(NN);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(NN);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }
    printf("</TR>");

}

void PrintCycle(void) {
    int j,k;
    int a[NN];
    for (k=0; k<=NN;k++) a[k] = 1;

    k=1;

    while( k <= NN) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if (Pi[j] != k) {
	       printf("%d, ",j);
	    } else {
               printf("%d",j);
            }
	    j = Pi[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=NN)) k++;
    }
}
	
    

/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define even(x) !odd(x)

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}
	
	    
		


#define INFINITY 999

int Tab[MAX][MAX];


void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;

}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=NN; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[MAX];

   x[1] = ex;  

   for (j = 1; j< MAX; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}

