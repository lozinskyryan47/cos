#include <stdio.h>
#include "ProcessInput.c"
#define TRANSP
#include "commonio.h"

#define TRUE	1
#define FALSE	0
#define odd(x) ( (x)%2==1 ? TRUE : FALSE ) 
#define LIMIT_ERROR  -1

void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );
int NN;


#define ARRAY_MAX 50

int stamp[ARRAY_MAX + 1][ARRAY_MAX];
int next_list[ARRAY_MAX + 1][ARRAY_MAX];
int space_list[ARRAY_MAX + 1][ARRAY_MAX];
int count = 0;

void GenStamp(int); 	/* Recursively generates stamps of size N
				   using a stamp of the specified size */
void DisplayStamp(int);
int min (int, int);
int max (int, int);

int main(int argc, char *argv[])
	{
	int i;
	int j;

	ProcessInput(argc, argv);

	NN = N;

	/* initialize the data structures */
	stamp[0][0] = 1;
	stamp[1][0] = 1;
	next_list[1][0] = 1;
	for (i = 0; i <= N; i++)
		{
		for (j = 0; j < N; j++)
			{
			space_list[i][j] = 1;	/* every place is usable */
			}
		}

	GenStamp(1);

	printf("\n</TABLE><P> Stamps = %d", count);
	exit(0);
	}

void GenStamp(int size)
	{
	int i, j, k;

	if (size == N)
		{
		DisplayStamp(size - 1);
		return;
		}

	/* preprocessing */
	/* we will use i to keep track of our search */
	/* stamp[0][size - 1] contains the location of the highest elem */

	i = next_list[1][size - 1];
	while (stamp[i][size - 1] != size )
		{
		if (stamp[i][size - 1] % 2 == size % 2)
			{
			j = next_list[stamp[i][size - 1] + 1][size - 1];
			/* is the highest elem between this element and the
			   next? */
			if ( ( stamp[0][size - 1] < max(i, j) ) &&
			     ( stamp[0][size - 1] > min(i, j) )    )
				{
				for (k = 0; k < min(i, j); k++)
					{
					space_list[k][size - 1] = 0;
					}
				for (k = max(i, j); k <= N; k++)
					{
					space_list[k][size - 1] = 0;
					}
				}
			else
				{
				for (k = min(i, j); k < max(i, j); k++)
					{
					space_list[k][size - 1] = 0;
					}
				}
			}

		i = next_list[stamp[i][size - 1] + 1][size - 1];	/* advance to the next item */
		}

	/* now that we know all the valid positions, place stamps there */
	for (i = 0; i <= size; i++)
		{
		if (space_list[i][size - 1])
			{

			/* place the new stamp in space i
			   space i is the space before the ith stamp */

			stamp[0][size] = i + 1;
			for (j = 1; j <= i; j++)
				{
				stamp[j][size] = stamp[j][size - 1];
				next_list[stamp[j][size]][size] = j;
				}

			/* place the new stamp */
			stamp[i + 1][size] = size + 1;
			next_list[size + 1][size] = i + 1;

			for (j = i + 2; j <= size + 1; j++)
				{
				stamp[j][size] = stamp[j - 1][size - 1];
				next_list[stamp[j][size]][size] = j;
				}

			/* recursively call the procedure */
			GenStamp (size + 1);
			}
		}
	/* set the space_list for this level back to all 1's */
	for (i = 0; i <= N; i++)
		{
		space_list[i][size - 1] = 1;
		}
	}

void DisplayStamp(int level)
	{
	int i;

	Pi[0] = N + 1;
	iP[0] = N + 1;
	for (i = 0; i <= N; i++)
		{
		Pi[i] = stamp[i][level];
		iP[stamp[i][level]] = i;
		}
	PrintIt(0,0);
	}

int min(int i, int j)
	{
	if (i < j)
		return i;
	else
		return j;
	}

int max(int i, int j)
	{
	if (i > j)
		return i;
	else
		return j;
	}




/*  from  perm.c
	   Provide routines for I/O of permutations

           Added KoDeZ to add commas if N > 10 
*/


void PrintIt(int x, int y) {
    int i;
    
    ++count;
    if(count > LIMIT) exit (-1); // Do this if we exceed max allowable
     
    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<NN; i++) printf("%d, ",Pi[i]); // one line
	    printf("%d",Pi[NN]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=NN; i++) iP[Pi[i]] = i;
	    for(i=1; i<NN; i++) printf("%d, ",iP[i]); // one line
	    printf("%d",iP[NN]);
	    printf("<BR></TD>");
	}

    if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(Pi, NN);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(NN);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(NN);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }
    printf("</TR>");

}

void PrintCycle(void) {
    int j,k;
    int a[NN];
    for (k=0; k<=NN;k++) a[k] = 1;

    k=1;

    while( k <= NN) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if (Pi[j] != k) {
	       printf("%d, ",j);
	    } else {
               printf("%d",j);
            }
	    j = Pi[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=NN)) k++;
    }
}
	
    

/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define even(x) !odd(x)

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}
	
	    
		


#define INFINITY 999

int Tab[MAX][MAX];


void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;

}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=NN; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[MAX];

   x[1] = ex;  

   for (j = 1; j< MAX; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}

