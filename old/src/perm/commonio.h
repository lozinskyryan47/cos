/*
// commonio.h
//
// Header files for all the stuff common to the permuation-style
// routines.
*/

int Pi[MAX];
int iP[MAX]; 
int calls, count;

#ifdef TRANSP
  void PrintIt(int, int);
#else
  void PrintIt(void);
#endif
void PrintCycle(void);
void PrintBoard(int Perm[], int n);
void tableaux( int );
void tableaux2( int );
void insert_tab( int );
void init_tab( int );
