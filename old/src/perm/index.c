/****************************************************
 * Constant amortized time algorithm for generating *
 * length n permutations with index k               *
 *   index <n> <k>                                  *
 *                                                  *
 * Written by: Scott Effler, seffler@csc.uvic.ca    *
 *             March 5, 2002                        *
 ****************************************************/

#include <stdio.h>
#include <stdlib.h>

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

#define odd(x) ( (x)%2==1 ? TRUE : FALSE )
#define even(x) !odd(x)
#define N 50
#define TRUE 1
#define FALSE 0
#define INFINITY 999

struct list
{
  int value;
  struct list *next;
  struct list *prev;
};

typedef struct list item;

void PrintCycle( void );
void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );

int perm[N];
item *head;
item *tail;
int n;
int NN;
int Tri[N];
int out_format;
int iP[N];
int Tab[N][N];
int count = 0;
int limit;

void printPerm( void )
{
    int i;
    count++;

    if( count > limit )
      return;

    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<n; i++) printf("%d, ",perm[i]); /* one line */
	    printf("%d",perm[n]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=n; i++) iP[perm[i]] = i;
	    for(i=1; i<n; i++) printf("%d, ",iP[i]); /* one line */
	    printf("%d",iP[n]);
	    printf("<BR></TD>");
	}

   if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(perm, n);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(n);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(n);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }
    printf("</TR>");
}

void initializeTri( int n )
{
  int i;

  Tri[0] = Tri[1] = 0;
  for( i=1; i<=n; i++ )
    Tri[i] = i-1 + Tri[i-1];
}

void permSwap( int i, int j )
{
  int temp;

  temp = perm[i];
  perm[i] = perm[j];
  perm[j] = temp;
}

void gen( int n, int k )
{
  item *currentPt;
  int rank, i;

  if( count > limit )
  {
    return;
  }
  else if( n == 0 )
  {
    printPerm();
  }
  else if( (k==0) && ((n==NN) || ((n<NN) && ((tail->value)<perm[n+1]))) )
  {
    currentPt = head;
    for( i=1; i<=n; i++,currentPt=currentPt->next )
      perm[i] = currentPt->value;
    printPerm();
  }
  else if( (k==1) && ((n==NN) || ((n<NN) && ((tail->value)<perm[n+1]))) )
  {
    currentPt = head;
    for( i=1; i<=n; i++,currentPt=currentPt->next )
      perm[i] = currentPt->value;
    for( i=1; i<=n-1; i++ )
    {
      permSwap( 1, i+1 );
      printPerm();
    }     
  }
  else if( (k==Tri[n]) && ((n==NN) || ((n<NN) && ((tail->value)<perm[n+1]))) )
  {
    currentPt = tail;
    for( i=1; i<=n; i++,currentPt=currentPt->prev )
      perm[i] = currentPt->value;
    printPerm();
  }
  else if( (k==Tri[n]-1) && ((n==NN) || ((n<NN) && ((tail->value)<perm[n+1]))))
  {
    currentPt = tail;
    for( i=1; i<=n; i++,currentPt=currentPt->prev )
      perm[i] = currentPt->value;
    for( i=1; i<=n-1; i++ )
    {
      permSwap( 1, i+1 );
      printPerm();
    }
  }
  else
  {
    rank = 1;
    currentPt = head;
    while( currentPt != NULL )
    {
      if( (n==NN && (n-rank)<=k && k<=(Tri[n]-rank+1))
       || (n<NN && (currentPt->value)<perm[n+1] && (n-rank)<=k &&
		k<=(Tri[n]-rank+1))
       || ( n<NN && (currentPt->value)>perm[n+1] && (n-rank)<=(k-n) &&
		(k-n)<=(Tri[n]-rank+1)))
      {
        perm[n] = currentPt->value;
        if( rank == 1 )  /* delete first item in list */
        {
          head = currentPt->next;
          if( currentPt->next != NULL )
            currentPt->next->prev = NULL;
          else
            tail = currentPt;
        }
        else
        {
          currentPt->prev->next = currentPt->next;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt->prev;
          else
            tail = currentPt->prev;
        }
        if( ( n==NN ) || ( perm[n] < perm[n+1] ) )
          gen( n-1, k );
	else
          gen( n-1, k-n );
        if( rank == 1 )
        {
          head = currentPt;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt;
          else
            tail = currentPt;
        }
        else
        {
          currentPt->prev->next = currentPt;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt;
          else
            tail = currentPt;
        }
      }
      currentPt = currentPt->next;
      rank++;
    }
  }
}

int main( int argc, char *argv[] )
{
  int i;
  item *current;

  if( argc != 5 )
  {
    printf( "inversions <n> <k> <outformat> <limit>\n" );
    exit(1);
  }

  out_format = atoi( argv[3] );
  limit = atoi( argv[4] );

  n = NN = atoi( argv[1] );

  /* set up doubly linked list from n..1 */

  current = (item *)malloc(sizeof(item));
  current->value = NN;
  current->next = NULL;
  current->prev = NULL;
  tail = current;
  head = current;
  
  for( i=NN-1; i>=1; i-- )
  {
    current = (item *)malloc(sizeof(item));
    current->value = i;
    current->next = head;
    current->next->prev = current;
    current->prev = NULL;
    head = current;
  }

  initializeTri( NN );
  gen( NN, atoi( argv[2] ) );
  if( count > limit )
  {
    return 1;
  }
  else
  {
    printf( "</TABLE><b><p>Permutations = %d\n", count );
    return 0;
  }
}

void PrintCycle(void) {
    int j,k;
    int a[n];
    for (k=0; k<=n;k++) a[k] = 1;

    k=1;

    while( k <= n) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if (perm[j] != k) {
	       printf("%d, ",j);
	    } else {
               printf("%d",j);
            }
	    j = perm[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=n)) k++;
    }
}    

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}

void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;
}

void tableaux( int n ) {
int i,j;

init_tab(N);
for (i=1; i<=n; i++) insert_tab(perm[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}
printf("</TABLE>");
}

void tableaux2( int n ) {
int i,j;

init_tab(N);
for(i=1; i<=n; i++) iP[perm[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}
printf("</TABLE>");
}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[N];

   x[1] = ex;  

   for (j = 1; j< N; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}
