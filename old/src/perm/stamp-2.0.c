/* stamp-2.0.c

   A program to calculate stamp foldings, with possibly restricted
   output.

   It can output all foldings of size n, those where 1 appears before n,
   the lexicographically least of equivalent foldings (reverse, complement,
   or both). It also can output meanders of size n, and closed meanders of 
   size 2n.

   This file replaces files named with variations of stamps, stampsym,
   stampsym2, meander, and cmeander, with or without "new", "newer", or
   "newest" prepended, and files concerning gdstamp.

   sjc - January 2006
*/

#include <stdio.h>
#include <stdlib.h>
#include "./gd1.2/gd.h"
#include "ProcessInput.c"

#define LIMIT_ERROR -1
#define MAX_SIZE 200

struct stamp
{
	struct stamp *next;     /* Points to next stamp */
	struct stamp *prev;     /* Points to previous stamp */
	int label;
	struct perforation *perf[2];
};

struct perforation
{
	struct stamp *left_link;
	struct stamp *right_link;
};

struct tree
{
	struct perforation *perfor;
	int level;              /* level in the drawing */
	int right_order;        /* these are the order in which the right */
	int left_order;         /* and left labels of the perf appear in */
                           /* the permutation */
	struct tree *left_child;
	struct tree *right_child;
	struct tree *parent;
} tree;

void init();
void myfree();
void GenStamp(struct stamp *latest);
void AddStamp(struct stamp *current, struct stamp *new);
void RemoveStamp(struct stamp *new);
void DisplayStamp();
void relabel();
int isCanonical();
void reverse();
int N_free();
void closeMeander();
void openMeander();

void PrintIt();
void PrintCycle(void);
void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );

void gdStampFolding(struct stamp *stamp_zero, int N, int option);
void draw_perfs(gdImagePtr im, struct stamp *stamp_zero, int N, int black, int option);
struct tree * add_son(struct tree *current, struct tree *temp);
struct tree * find_parent(struct tree *current);
int compute_depth(struct tree *node);
void gif_perf(gdImagePtr im, struct tree *node, int parity, int N, int black);
void remove_tree(struct tree *node);

struct stamp *stamp_zero;
struct stamp *close_stamp;

struct stamp *access[MAX_SIZE];
int stamp_array[MAX_SIZE];
int second_array[MAX_SIZE];

/* Used by PrintIt() */
int count = 0;
int Pi[MAX];
int iP[MAX];
int calls;


int main(int argc, char *argv[])
{
	ProcessInput(argc, argv);
	init();
	GenStamp(stamp_zero->prev);
	myfree();

	printf("\n</TABLE><P>\nThere are %d stamp foldings meeting the selected criteria\n", count);

	exit(0);
}

/* initialize the data structures */
void init(void)
{
	int i = 0;
	stamp_zero = (struct stamp *) malloc(sizeof(struct stamp));
	stamp_zero->label = 0;

	stamp_zero->perf[0] = (struct perforation *) malloc(sizeof(struct perforation));
	stamp_zero->perf[1] = stamp_zero->perf[0];
	stamp_zero->perf[0]->left_link  = stamp_zero;
	stamp_zero->perf[0]->right_link = stamp_zero;

	if (stamp_restrict == STAMP_CMEANDER) N = 2 * N - 1;
	
	for (i = 1; i <= N; i++) {
		access[i] = (struct stamp *) malloc(sizeof(struct stamp));
		access[i]->label = i;
		access[i]->perf[i & 1] = (struct perforation *) malloc(sizeof(struct perforation));
		access[i]->perf[i & 1]->left_link = 0;
		access[i]->perf[i & 1]->right_link = access[i];
	}
	
	stamp_zero->prev = access[1];
	stamp_zero->next = access[1];
	
	access[1]->prev = stamp_zero;
	access[1]->next = stamp_zero;
	
}

void myfree()
{
	free (stamp_zero->perf[0]);
	free (stamp_zero->prev->perf[1]);
	free (stamp_zero->prev);
	free (stamp_zero);
}


/* Recursive function */
void GenStamp(struct stamp *latest)
{
	struct stamp *new_stamp = 0;         /* points to new stamp */
	struct stamp *current = 0;      /* points to a current stamp */

	struct stamp *temp_stamp = 0;
	struct perforation *temp_perf = 0;

	int parity = latest->label & 1;
	
	
	if (latest->label == N)
	{
		DisplayStamp();
		return;
	}
	
	new_stamp = access[latest->label + 1];
	/* connect next stamp to latest perforation */
	new_stamp->perf[parity] = latest->perf[parity];
	latest->perf[parity]->left_link = new_stamp;
	latest->perf[parity]->right_link = latest;
	/* leave new perf other side unconnected */
	new_stamp->perf[parity ^ 1]->left_link = 0;
	new_stamp->perf[parity ^ 1]->right_link = new_stamp;

	/* go left in linked list of stamps */
	/* and repeatedly insert new stamp into the list */
	current = latest;
	for(;;)
	{
		AddStamp(current, new_stamp);
		GenStamp(new_stamp);
		RemoveStamp(new_stamp);
		current = current->prev;

		if (current == latest) break;

		if (current == stamp_zero) {
			/* new_stamp was on left end --
			   swing around to right end */
			temp_stamp = latest->perf[parity]->left_link;
			latest->perf[parity]->left_link = latest->perf[parity]->right_link;
			latest->perf[parity]->right_link = temp_stamp;
			if (((stamp_restrict == STAMP_MEANDER) || (stamp_restrict == STAMP_CMEANDER))
					&& (parity == 0)) {
				/* don't go under stamp 1 */
				current = access[1];
			}
		}
		else if ((current->label == 1) && (parity == 0)) {
			/* new_stamp was to right of stamp 1, points up, and can
			   be tucked under stamp 1 */
			if ((stamp_restrict == STAMP_MEANDER) || (stamp_restrict == STAMP_CMEANDER)) {
				/* don't tuck -- send to front instead */
				current = stamp_zero;
				temp_stamp = latest->perf[parity]->left_link;
				latest->perf[parity]->left_link = latest->perf[parity]->right_link;
				latest->perf[parity]->right_link = temp_stamp;
			}
			else /* tuck */
				continue;
		}
		else if (current->perf[parity]->right_link == current) {
			/* can swing around perf to left */
			current = current->perf[parity]->left_link;
		}
		else {
			/* swing inside perf to right */
			current = current->perf[parity]->right_link;
			temp_stamp = latest->perf[parity]->left_link;
			latest->perf[parity]->left_link = latest->perf[parity]->right_link;
			latest->perf[parity]->right_link = temp_stamp;
		}
	}

	/* remove the extra stamp and extra perforation */
	temp_perf = new_stamp->perf[parity];
	if (temp_perf->left_link->label == new_stamp->label)
	{
		temp_perf->left_link = 0;
	}
	else
	{
		temp_perf->right_link = temp_perf->left_link;
		temp_perf->left_link = 0;
	}
}


/* Add a new stamp before current stamp */
void AddStamp(struct stamp *current, struct stamp *new)
{
	new->next = current;
	new->prev = current->prev;
	current->prev->next = new;
	current->prev = new;
}

/* Remove a stamp from the list */
void RemoveStamp(struct stamp *togo)
{
	togo->prev->next = togo->next;
	togo->next->prev = togo->prev;
	togo->next = 0;
	togo->prev = 0;
}

void DisplayStamp()
{
	/* Only display the requested foldings */
	/* Return before displaying if not a requested folding */
	switch (stamp_restrict) {
	case STAMP_ALL:
		/* do nothing -- accept all*/
		break;
	case STAMP_INCREASING:
		if(stamp_zero->next->label > stamp_zero->prev->label) {return;}
		break;
	case STAMP_UNLABELLED:
		if(!isCanonical()) {return;}
		break;
	case STAMP_MEANDER:
		if (!N_free()) {return;}
		break;
	case STAMP_CMEANDER:
		if (!N_free()) {return;}
		closeMeander();
		PrintIt();
		openMeander();
		return;
	}

	PrintIt();
}

int isCanonical()
{
	struct stamp *left = stamp_zero->next;
	struct stamp *right = stamp_zero->prev;
	int a, z, l1 = 0, l2 = 0, l3 = 0;

	/* We accept if perm is less than its reverse, its complement (subtracting each
	   number from N+1), and its complements reverse */

	while (left != stamp_zero) {
		a = left->label; z = right->label;
		
		if (!l1 && (a > z)) return 0;
		if (!l2 && (a + a > N + 1)) return 0;
		if (!l3 && (a + z > N + 1)) return 0;

		if (a < z) l1 = 1;
		if (a + a < N + 1) l2 = 1;
		if (a + z < N + 1) l3 = 1;
		
		left = left->next;
		right = right->prev;
	}
	
	return 1;
}

void closeMeander() {
	if (!close_stamp) {
		close_stamp = (struct stamp*)malloc(sizeof(struct stamp));
		close_stamp->perf[0] = (struct perforation *) malloc(sizeof(struct perforation));
		close_stamp->perf[1] = access[N]->perf[1];

		close_stamp->label = N + 1;
		access[N+1] = close_stamp;
		
		close_stamp->perf[0]->right_link = close_stamp;
		close_stamp->perf[0]->left_link = access[1];
	}

	AddStamp(stamp_zero, close_stamp);
	
	access[1]->perf[0] = close_stamp->perf[0];
	
	close_stamp->perf[1]->right_link = close_stamp;
	close_stamp->perf[1]->left_link = access[N];
}


void openMeander() {
	RemoveStamp(close_stamp);
	access[1]->perf[0] = 0;
	access[N]->perf[1]->right_link = 0;
}

void relabel()
{
	int i;

	for (i = 0; i < N; i++)
	{
		second_array[i] = N + 1 - second_array[i];
	}
}


void reverse()
		  {
		  int i;
		  int temp;

		  for (i = 0; i < N/2; i++)
			      {
			      temp = second_array[i];
			      second_array[i] = second_array[N - 1 - i];
			      second_array[N - 1 - i] = temp;
			      }
		  }

int N_free()
{
	/* Check if the N stamp is free to above
		follow trail of stamps to the right
		and make sure none fold back left.
	*/
	struct stamp *temp;
	
	temp = access[N];

	if (N & 1) { 
		/* N is odd */
		if (temp == stamp_zero->next) return 1;
	
		while (temp != stamp_zero->prev)
		{
			temp = temp->next;
			if (temp->perf[1]->right_link == temp) 
				/* perf folds back over N */
				return 0;
			else
				temp = temp->perf[1]->right_link;	
		}
	}
	else {
		/*  even N */
		while (temp != stamp_zero->prev)
		{
			temp = temp->next;
			if (temp == access[1]) return 0; /* left of 1 */ 
			
			if (temp->perf[0]->right_link == temp) 
				/* perf folds back under N */
				return 0;
			else
				temp = temp->perf[0]->right_link;
		}
	}

	return 1;
}




/* *******************************************
	   from  perm.c
			 Provide routines for I/O of permutations

			 Added KoDeZ to add commas if N > 10
*************************************************/

void PrintIt() {
	int i;
	struct stamp *stamp;
	
	if(++count > LIMIT) exit (LIMIT_ERROR); /* Do this if we exceed max allowable */

	stamp = stamp_zero->next;
	for (i = 1; stamp != stamp_zero; i++)
	{
		Pi[i] = stamp->label;
		iP[stamp->label] = i;
		stamp = stamp->next;
	}
	
	Pi[0] = iP[0] = i - 1;

	printf("<TR>\n");
	if (out_format & OUT_ONELINE)
	{   
		printf("<TD ALIGN=CENTER>");
		for(i=1; i<Pi[0]; i++) printf("%d, ",Pi[i]); /* one line */
		printf("%d",Pi[Pi[0]]);
		printf("<BR></TD>\n");
	}

	if (out_format & OUT_INVERSE)
	{   printf("<TD ALIGN=CENTER>");
		for(i=1; i<iP[0]; i++) printf("%d, ",iP[i]); /* one line */
		printf("%d",iP[iP[0]]);
		printf("<BR></TD>\n");
	}

	if (out_format & OUT_CYCLE) {
		printf("<TD ALIGN=CENTER>");
		PrintCycle();
		printf("<BR></TD>\n");
	}

	if (out_format & OUT_CHESS) {
		printf("<TD ALIGN=CENTER>");
		PrintBoard(Pi, Pi[0]);
		printf("<BR></TD>\n");
	}

	 if(out_format & OUT_TABLEAU) {
		printf("<TD ALIGN=CENTER>\n");
		printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
		printf("<TR>\n");
		printf("<TD ALIGN=CENTER>\n");
		tableaux(Pi[0]);
		printf("</TD>\n");
		printf("<TD ALIGN=CENTER>\n");
		tableaux2(Pi[0]);
		printf("</TD></TR></TABLE>\n");
		printf("</TD>\n");
	}
	if (out_format & OUT_DIAGRAM) {
		printf("\n");
		gdStampFolding(stamp_zero, Pi[0], 1);
	}
	printf("</TR>\n");

}

void PrintCycle(void) {
	int j,k;
	int N = Pi[0];
	
	int a[N + 2];
	for (k=0; k<=N;k++) a[k] = 1;

	k=1;

	while( k <= N) {
		printf("(");
		j=k;
		do {
			a[j] = 0;
			if (Pi[j] != k) {
			   printf("%d, ",j);
			} else {
			   printf("%d",j);
			}
			j = Pi[j];
		} while (j != k);
		printf(")");
		while ((!a[k]) && (k<=N)) k++;
	}
}



/*
 * PermMatrix
 * ----------
 *
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */
#define odd(x) ((x)&1)
#define even(x) (((x)&1)^1)


void PrintBoard(int Perm[], int n) {
	int row, col;

	printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
	for(row = 1; row <= n; row++) {
		printf("<TR>\n");
		for(col = 1; col <= n; col++) {
			printf("<TD>");
			if (Perm[row] == col) {
			    if(odd(row+col))
			        printf(blackRook);
			    else
			        printf(whiteRook);
			} else {
			    if(odd(row+col))
			        printf(blackSquare);
			    else
			        printf(whiteSquare);
			}
			printf("</TD>\n");
		}
		printf("</TR>\n");
	}
	printf("</TABLE>\n");
}





#define INFINITY 999

int Tab[MAX][MAX];

void init_tab( int n )
{
	int i, j;

	for (i=0; i< n; i++)
		for(j=0; j< n; j++) Tab[i][j] = INFINITY;

	for (i=0; i< n; i++)
		Tab[0][i] = 0;

	for (i=0; i< n; i++)
		Tab[i][0] = 0;
}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
	printf("<TR>");
	for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
	  printf("<TD>%d</TD>",Tab[i][j]);
	printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=N; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
	printf("<TR>");
	for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
	  printf("<TD>%d</TD>",Tab[i][j]);
	printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex)
{
	 int i = 0;
	 int j;
	 int x[MAX];

	 x[1] = ex;

	 for (j = 1; j< MAX; j++)
	   if (Tab[1][j] == INFINITY)
		   break;

 do {
	  i++;
	  while (x[i] < Tab[i][j-1]) j--;
	  x[i+1] = Tab[i][j];
	  Tab[i][j] = x[i];
	} while (x[i+1] != INFINITY);

}


/****************************************/
/* Routines to draw gifs of the foldings */

int num = 0;

void gdStampFolding(struct stamp *stamp_zero, int N, int option)
		{
		gdImagePtr im;
		int black, white, red, notQuiteBlack, color;
		FILE *out;
		char outName[80];
		struct stamp *current;

		int i;
		int width, height;

/*printf("\n entered gdStampFolding\n");*/
		current = stamp_zero->next;
		num++;
		sprintf(outName, "/theory/www/per/tmpImages/test_%d_%d.gif",PID, num);

		width = 30 + (N - 1) * 20;
		height = 80 + N * 5;
		im = gdImageCreate(width, height);
		/* allocate background color stamp_zero */
		white = gdImageColorAllocate(im, 255, 255, 255);
		black = gdImageColorAllocate(im, 0, 0, 0);
		red = gdImageColorAllocate(im, 255, 0, 0);
		notQuiteBlack = gdImageColorAllocate(im, 0, 0, 1);

/*printf("\n Before any loops\n");*/
		/* draw the stamp folding */
		i = 15;
		while (current != stamp_zero)
			    {
/*printf("\n inside stamp_zero loop\n");*/
			    color = black;
			    if ((current->label == 1) && (option == 1)) color = red;
			    gdImageLine(im, i, 20 + N * 2.5, i, 60 + N * 2.5, color);
			    i +=20;
			    current = current->next;
			    }
/*printf("\n outside stamp_zero loop\n");*/
		draw_perfs(im, stamp_zero, N, black, option);    /* draw the
			                                            perforations */

		/* Write to disk and destroy image */
		out = fopen(outName, "wb");
		if (!out)
			    {
			    printf("The file didn't open.<BR></TR></TABLE>\n");
			    exit(1);
			    }
		gdImageGif(im, out);
		fclose(out);
		gdImageDestroy(im);

		printf("<TD>");
		printf("<IMG SRC=\"per/perm/displaygif.pl.cgi?pid=%d&num=%d\">",PID, num);
		printf("</TD>\n");

		return;
		}


void draw_perfs(gdImagePtr im, struct stamp *stamp_zero, int N, int black, int
option)
		{
		struct tree *perf_tree;         /* root of tree built */
		struct tree *current_tree;      /* current piece of tree */
		struct tree *temp_tree;
		struct stamp *current;
		int label = 1;                  /* order of current in permutation */

		if (N == 1) return;

		/* DO UPWARDS(odd) PERFS FIRST */
		/* set up the tree */
		perf_tree = (struct tree *) malloc (sizeof(struct tree));
		perf_tree->left_child = 0;
		current = stamp_zero->next;
		current_tree = perf_tree;
		perf_tree->right_child = 0;
		perf_tree->parent = 0;
		perf_tree->right_order = 0;
		perf_tree->left_order = 0;
		perf_tree->perfor = 0;

/*printf("\n Before odd loop\n");*/
		while (current != stamp_zero)
			    {
/*printf("\n iterate odd loop\n");*/
			    if ((current->label == N) && (N % 2 == 1))
			            {
			            current = current->next;
			            label++;
			            continue;
			            }
			    if (current->label == (current->perf[1])->left_link->label)
			            /* a left parenthesis */
			            {
			            temp_tree = (struct tree*) malloc(sizeof(struct
tree));
			            temp_tree->perfor = current->perf[1];
			            temp_tree->left_order = label;
			            temp_tree->left_child = 0;
			            temp_tree->right_child = 0;
			            current_tree = add_son(current_tree, temp_tree);
			            }
			    else
			            /* a right parenthesis */
			            {
			            current_tree->right_order = label;
			            current_tree = find_parent(current_tree);
			            }


			    current = current->next;
			    label++;
			    }

		/* compute the depth labels for the tree */
		label = compute_depth (perf_tree->left_child);
		/* draw the perforations */
		gif_perf(im, perf_tree->left_child, 1, N, black);

		/* remove the tree from memory */
		remove_tree(perf_tree);


		if (N == 2 ) return;
		/* DO DOWNWARD(even) PERFS SECOND */
		label = 1;
		temp_tree = 0;
		/* set up the tree */
		perf_tree = (struct tree *) malloc (sizeof(struct tree));
		perf_tree->left_child = 0;
		current = stamp_zero->next;
		current_tree = perf_tree;
		perf_tree->right_child = 0;
		perf_tree->parent = 0;
		perf_tree->right_order = 0;
		perf_tree->left_order = 0;
		perf_tree->perfor = 0;

/*printf("\n Before even loop\n"); */
		while (current != stamp_zero)
			    {
/*printf("\n iterating even loop\n");*/
			    if (option != 2)
			      {
			      if ((stamp_restrict != STAMP_CMEANDER) && ((current->label == 1) || ((current->label == N) &&
(N % 2 == 0))))
			            {
			            current = current->next;
			            label++;
			            if (current != 0) continue;
			            else break;
			            }
			      }
			    if (current->label == (current->perf[0])->left_link->label)
			            /* a left parenthesis */
			            {
			            temp_tree = (struct tree*) malloc(sizeof(struct
tree));
			            temp_tree->perfor = current->perf[0];
			            temp_tree->left_order = label;
			            temp_tree->left_child = 0;
			            temp_tree->right_child = 0;
			            current_tree = add_son(current_tree, temp_tree);
			            }
			    else
			            /* a right parenthesis */
			            {
			            current_tree->right_order = label;
			            current_tree = find_parent(current_tree);
			            }


			    current = current->next;
			    label++;
			    }

		/* compute the depth labels for the tree */
		label = compute_depth (perf_tree->left_child);
		/* draw the perforations */
		gif_perf(im, perf_tree->left_child, 2, N, black);

		/* remove the tree from memory */
		remove_tree(perf_tree);
		}


struct tree * add_son(struct tree *current, struct tree *temp)
		{
		if (current->left_child == 0)
			    {
			    temp->parent = current;
			    current->left_child = temp;
			    return temp;
			    }

		/* otherwise, there are other siblings */
		current = current->left_child;
		while (current->right_child != 0)       /* find the last one */
			    {
			    current = current->right_child;
			    }
		/* and attach to the tree here */
		temp->parent = current;
		current->right_child = temp;
		return temp;
		}


struct tree * find_parent(struct tree *current)
		{
		struct tree *proposal;  /* the proposed parent */

		proposal = current->parent;
		while (proposal->left_child != current)
			    {
			    current = proposal;
			    proposal = proposal->parent;
			    }
		current = proposal;

		return current;
		}


int compute_depth (struct tree *node)
		{
		struct tree *child;
		int highest_level;      /* the level of this node */
		int sib_level = 0;      /* needed to pass on to parents
			                       and possibly other siblings */

		if (node->left_child == 0)      /* lowest level */
			    {
			    node->level = 1;
			    highest_level = 1;
			    }
		else                            /* has children */
			    {
			    child = node->left_child;
			    node->level = 1 + compute_depth(child);
			    highest_level = node->level;
			    }

		if (node->right_child != 0)     /* take care of all siblings */
			    {
			    child = node->right_child;
			    sib_level = compute_depth(child);
			    }

		if (sib_level > highest_level)
			    return sib_level;
		else
			    return highest_level;
		}

void gif_perf(gdImagePtr im, struct tree *node, int parity, int N, int
black)
		{
		int hor1, hor2, ver1, ver2;    /* horizontal and vert positions */

		if (node->left_child != 0)
			    gif_perf(im, node->left_child, parity, N, black);
		if (node->right_child != 0)
			    gif_perf(im, node->right_child, parity, N, black);

		if (parity == 1)        /* odd perforations */
			    {
			    hor1 = 15 + (node->left_order - 1) * 20;
			    hor2 = 15 + (node->right_order - 1) * 20;
			    ver1 = 20 + N * 2.5;
			    ver2 = ver1 - node->level * 5;
			    gdImageLine(im, hor1, ver1, hor1, ver2, black);
			    gdImageLine(im, hor1, ver2, hor2, ver2, black);
			    gdImageLine(im, hor2, ver2, hor2, ver1, black);
			    }
		else                    /* even perforations */
			    {
			    hor1 = 15 + (node->left_order - 1) * 20;
			    hor2 = 15 + (node->right_order - 1) * 20;
			    ver1 = 60 + N * 2.5;
			    ver2 = ver1 + node->level * 5;
			    gdImageLine(im, hor1, ver1, hor1, ver2, black);
			    gdImageLine(im, hor1, ver2, hor2, ver2, black);
			    gdImageLine(im, hor2, ver2, hor2, ver1, black);
			    }
		}

void remove_tree(struct tree *node)
		{
		if (node->left_child != 0)
			    remove_tree(node->left_child);
		if (node->right_child != 0)
				remove_tree(node->right_child);

		free(node);
		}

