// Program to generate all k-perms of [n]
// Copyright: Frank Ruskey, 1995-2004.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ProcessInput.c"
#include "commonio.h"

void p(int, int);
void l(int, int);
void swap(int, int);
void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );


int NN, KK;

int main(int argc, char **argv) {
    ProcessInput(argc, argv);
    count = 0;
    NN = N; KK=K; // Kludge until I get everything working properly
    p(NN, KK);
    printf("</TABLE>");
    printf("<b><p> Permutations = %d</p></b>",count);
}

void swap(int i, int j) {
    int temp;
    temp = Pi[i]; Pi[i] = Pi[j]; Pi[j]=temp;
}

void p(int n, int k)
{
    int i;

    if (k <=0) PrintIt();
    else { 
	for(i=n; i>=k;i--) {  // P(n,k) = L(n,k) + L(n-1,k) + ... + L(k, k) 
	    l(i, k);
	}
    }
}

void l(int n, int k) 
{
    int j;

    if(k>=0) {
	Pi[KK-(k-1)] = n;
	p(n-1, k-1); // Initial call, no swap
	for (j=(KK-(k-1)); j >1; j--) {
	    swap(j, j-1);
	    p(n-1, k-1); // L(n,k) = k*P(n-1,k-1);
	}
    }
    // Shuffle for restore
    for (j=1; j<(KK-(k-1)); j++) swap(j, j+1);
}
	


void PrintIt(void) {
    int i;
    
    ++count;
    if(count > LIMIT) exit (-1); // Do this if we exceed max allowable
     
    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<KK; i++) printf("%d, ",Pi[i]); // one line
	    printf("%d",Pi[KK]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=KK; i++) iP[Pi[i]] = i;
	    for(i=1; i<KK; i++) printf("%d, ",iP[i]); // one line
	    printf("%d",iP[KK]);
            printf("<BR></TD>");
	}

    if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(Pi, NN);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(NN);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(NN);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }
    printf("</TR>");

}

void PrintCycle(void) {
    int j,k;
    int a[NN];
    for (k=0; k<=NN;k++) a[k] = 1;

    k=1;

    while( k <= NN) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if(Pi[j] != k) {
	       printf("%d, ",j);
            } else {
               printf("%d",j);
	    }
	    j = Pi[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=NN)) k++;
    }
}
	
    

/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define odd(x)  ((x) % 2)
#define even(x) !odd(x)

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}
	
	    
		


#define INFINITY 999

int Tab[MAX][MAX];


void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;

}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=10 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=NN; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=10 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[MAX];

   x[1] = ex;  

   for (j = 1; j< MAX; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}
