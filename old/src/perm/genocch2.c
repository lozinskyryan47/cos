#include <stdio.h>
#include <stdlib.h>
#include "ProcessInput.c"
#include "commonio.h"

#define TRUE 1
#define FALSE 0
#define odd(x) ( (x)%2==1 ? TRUE : FALSE )
#define LIMIT_ERROR -1
#define MAX_SIZE 200

void PrintIt(void);
void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void taleaux( int );
void tableaux2( int );
int NN;

int count = 0;

int perm[MAX_SIZE];
int taken[MAX_SIZE + 1];	/* which values are used */
int prev[MAX_SIZE + 1];		/* the previous unused value */
int next[MAX_SIZE + 1];		/* the next unused value */
int first;			/* the first in the list */
int last;			/* the last in the list */
int odd_position;		/* the last odd position filled */
int even_position = 2;		/* the last even position filled */


void genocchi2 (int position, int parity);
/* position in array where last value was placed.  parity of last position
filled */

void Remove(int value, int *before, int *after);
void Replace(int value, int before, int after);
void Display(void);

int main(int argc, char *argv[])
	{
	int i;

	ProcessInput(argc, argv);

	NN = N;
	odd_position = N + 1;	/* has to be initialized here */

	/* indicate no numbers have been used */
	taken[0] = 1;
	prev[0] = 0;
	next[0] = 0;
	for (i = 1; i <= N; i++)
		{
		taken[i] = 0;
		prev[i] = i - 1;
		next[i] = i + 1;
		}
	next[N] = 0;
	first = 1;
	last = N;

	genocchi2 (0, 1);

	printf("\n</TABLE><P> Genocchi sequences = %d", count);

	exit(0);
	}

void genocchi2 (int position, int parity)
	{
	int i;		/* loop control */
	int pos;	/* to remember position */
	int before;
	int after;

	if (position == 0)	/* starting case */
		{
		perm[1] = 1;
		taken[1] = 1;
		prev[2] = 0;
		next[1] = 0;
		first = 2;
		genocchi2 (2, 1);
		prev[2] = 1;
		next[1] = 2;
		first = 1;
		taken[1] = 0;
		return;
		}

	if (position == 1)	/* last value placed */
		{
		Display();
		return;
		}

	if (parity == 1)
		{
		odd_position -= 2;
		pos = odd_position;

		i = last;
		if (!taken[i])
			{
			while (i >= odd_position)
				{
				perm[pos - 1] = i;
				taken[i] = 1;
				Remove(i, &before, &after);
				genocchi2 (pos, 0);
				Replace(i, before, after);
				taken[i] = 0;
				i = prev[i];
				}
			}
 		odd_position += 2;
		}
	else
		{
		even_position += 2;
		pos = even_position;
		i = first;
		if (!taken[i])
			{
			while ((!taken[i]) && (i < even_position))
				{
				perm[pos - 1] = i;
				taken[i] = 1;
				Remove(i, &before, &after);
				genocchi2 (pos, 1);
				Replace(i, before, after);
				taken[i] = 0;
				i = next[i];
				}
			}

		even_position -= 2;
		}
	}

void Remove(int value, int *before, int *after)
	{
	*before = prev[value];
	*after = next[value];
	if (value == last)
		{
		if (prev[value] != 0)
			next[prev[value]] = 0;
		last = prev[last];
		if (value == first)	/* the only value */
			{
			first = 0;
			}
		}
	else if (value == first)
		{
		if (next[value] != 0)
			prev[next[value]] = 0;
		first = next[first];
		}
	else
		{
		if (prev[value] != 0)
			next[prev[value]] = next[value];
		if (next[value] != 0)
			prev[next[value]] = prev[value];
		}
	prev[value] = 0;
	next[value] = 0;
	}

void Replace(int value, int before, int after)
	{
	if (first == 0)		/* no entries exist */
		{
		first = value;
		last = value;
		return;
		}
	if (value < first)
		{
		prev[first] = value;
		prev[value] = 0;
		next[value] = first;
		first = value;
		return;
		}
	if (value > last)
		{
		next[last] = value;
		next[value] = 0;
		prev[value] = last;
		last = value;
		return;
		}

	/* if we get here, the value being replaced is not on either end */
	next[value] = after;
	prev[value] = before;
	next[before] = value;
	prev[after] = value;
	return;
	}

void Display(void)
	{
	int i;

	Pi[0] = N + 1;
	iP[0] = N + 1;
	printf("\n");
	for (i = 0; i < N; i++)
		{
		Pi[i + 1] = perm[i];
		iP[perm[i]] = i + 1;
		}

	PrintIt();
	}




/*  from  perm.c
	   Provide routines for I/O of permutations

           Added KoDeZ to add commas if N > 10 
*/


void PrintIt(void) {
    int i;
    
    ++count;
    if(count > LIMIT) exit (-1); /* Do this if we exceed max allowable */

    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<NN; i++) printf("%d, ",Pi[i]); /* one line */
	    printf("%d",Pi[NN]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=NN; i++) iP[Pi[i]] = i;
	    for(i=1; i<NN; i++) printf("%d, ",iP[i]); /* one line */
	    printf("%d",iP[NN]);
	    printf("<BR></TD>");
	}

    if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(Pi, NN);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(NN);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(NN);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }
    printf("</TR>");

}

void PrintCycle(void) {
    int j,k;
    int a[NN];
    for (k=0; k<=NN;k++) a[k] = 1;

    k=1;

    while( k <= NN) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if (Pi[j] != k) {
	       printf("%d, ",j);
	    } else {
               printf("%d",j);
            }
	    j = Pi[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=NN)) k++;
    }
}
	
    

/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define even(x) !odd(x)

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}
	
	    
		


#define INFINITY 999

int Tab[MAX][MAX];


void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;

}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=NN; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[MAX];

   x[1] = ex;  

   for (j = 1; j< MAX; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}

