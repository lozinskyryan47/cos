#!/usr/bin/perl

while (<>) {
  if (/:/ && !/N/) { # contains a `:' but is not `NOTE:'
     print; 
     @s = split( / +/ ); # split on one or more blanks
     $failed = 0;
     for ( $j=2; $j < 36; ++$j ) {
        print "Checking j = $j, s[j] = $s[$j]";
        $k = $j + 1;
        while ( $k <= 36 && $s[$k] != $s[$j]) { ++$k; }
        if (($k-$j)%2 == 1 && $k < 36) { 
           print ", k = $k -- NOT Brunnian\n"; 
           $failed = 1; }   
        else { print "\n"; }
     }
     if ($failed == 0) { print "THIS IS BRUNNIAN!!!\n"; }
  }
}

