#include <stdio.h>

int a[2000];		/* first set - contains 1*/
int b[2000];		/* second set */
int availA[2000];	/* sums made by set 1 */
int availB[2000];	/* sums made by set 2 */
int avail[2000];	/* combined sums */
int N,total;

/*--------------------------------------------------------------*/
void Print( int s, int t) {

	int i;

	total++;
	/* printf("A = { ");
	for (i=1; i<=s; i++) printf("%d ", a[i]);

	printf("},  B = { ");
	for (i=1; i<=t; i++) printf("%d ", b[i]);
    printf("}\n  "); */
}

/*--------------------------------------------------------------*/
/*  Update sums made by adding element val */
/*--------------------------------------------------------------*/
int Add_availA(int val, int min) {

	int i;

	for (i=N-1; i>=1; i--) { 
		if (availA[i] != 0) {
			if (avail[i+val] != 0 && i+val <=N) return(0);
			else {
				availA[i+val] = val; avail[i+val] = 1;
			}
		}
	}
	availA[val] = val;  avail[val] = 1;
}

/*--------------------------------------------------------------*/
/*  Update second set so it includes sums min..val-1 */
/*--------------------------------------------------------------*/
int Add_availB(int val, int min, int *t) {

	int i,j;

	for (i=min; i<val; i++) {
		if (avail[i] == 0) {
			for (j=N-1; j>=1; j--) {
				if (availB[j] != 0) {
					if (avail[j+i] != 0 && j+i <=N) return(0); 
					else {
						availB[j+i] = i; avail[j+i] = 1;
					}
				}
			}
			b[*t] = i; availB[i] = i; avail[i] = 1; *t = *t +1;
		}
	}
	i = val+2;
	while(avail[i] != 0 && i <= N) i++;
	return(i);
}

/*--------------------------------------------------------------*/
/*  Undo all the additions done before recursive call */
/*--------------------------------------------------------------*/
void Remove(int val) {

	int i;
	
	for (i=val; i<=N; i++) { 
		if (availA[i] >= val) {
			availA[i] = 0; avail[i] = 0;
		}
		if (availB[i] >= val) {
			availB[i] = 0; avail[i] = 0;
		}
	}
}

/*--------------------------------------------------------------*/
/*  Recursive back-tracking function  */
/*--------------------------------------------------------------*/
void back(int s, int t, int min_avail) {

	int i,min, temp_t;

	temp_t = t;
	for (i=min_avail; i<=N; i++) {
		if (avail[i] == 0) {
			a[s] = i;
			min = Add_availA(i,min_avail);
			if (min != 0) min = Add_availB(i,min_avail,&t);
			if (min > N) Print(s,t-1);
			else if (min != 0) back(s+1, t, min);
			Remove(min_avail);	
			t = temp_t;
		}
	}
	/* the case where all sums min_avail..N are found in set b */
	min = Add_availB(N+1,min_avail,&t);
	if (min > N) Print(s-1,t-1);
	Remove(min_avail);	 
}

int main() {
int n;
	total = 0;
	printf("Enter N: ");
	scanf("%d", &n);

for (N=1;N<n;++N) {
	a[1] = 1;	availA[1] = 1; avail[1] = 1;
	total = 0;
	back(2,1,2);
	printf("%d : %d\n", N, total);
}
}



