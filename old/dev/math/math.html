<HTML>
<HEAD>

</HEAD>

<BODY>

<CENTER>
<H1>HTML Based Mathematical Presentation<BR>
  on the World-Wide-Web</H1>
</CENTER>

<CENTER>
<I>
Frank Ruskey<BR>
Department of Computer Science<BR>
University of Victoria
</I>
</CENTER>

<CENTER>
Last Update: June 18
</CENTER>

Contents<BR>
&nbsp;&nbsp; <A HREF="#intro">Introduction</A><BR>
&nbsp;&nbsp; <A HREF="#italics">Use Italics</A><BR>
&nbsp;&nbsp; <A HREF="#spaces">Spaces Count</A><BR>
&nbsp;&nbsp; <A HREF="#subscript">Subscripts and Superscripts</A><BR>
&nbsp;&nbsp; <A HREF="#fractions">Fractions</A><BR>
&nbsp;&nbsp; <A HREF="#binomial">Binomial Coefficients</A><BR>
&nbsp;&nbsp; <A HREF="#characters">Special Characters</A><BR>
&nbsp;&nbsp; <A HREF="#final">Final Words of Advice</A><BR>
&nbsp;&nbsp; <A HREF="#refs">References</A><BR>

<A NAME="intro">
<H2>Introduction</H2>

<P>
Currently there is very little support for mathematics presentation
  on the WWW, and authors wishing to present mathematics on the web
  are still best served with existing paper-based tools such as TeX and
  postscript viewers.
Electronic journals, such as the Electronic Journal of Combinatorics,
  are for the most part only using HTML as an access mechanism for
  postscript or dvi files.
Nevertheless, there are instance where authors try to use existing
  capabilities of HTML to present mathematical material.
The author of this article has made some such attempts.

<P>
The purpose of this article is to present some tricks that the author
  has found useful, in the hopes that it will be useful to others.
The reader is expected to be conversant with the basics of HTML,
  including special characters using &amp;, tables, and fonts.

<P>
There are some tools like 
  <A HREF=" ">LaTeX2HTML</A> and 
  <A HREF="http://hutchinson.belmont.ma.us/tth/">T<sub>T</sub>H</A> 
  that can produce a good looking
  page of mathematics but they suffer from a number of well-known
  defects.
The key idea in LaTeX2HTML is the production of gifs for all the 
  mathematics in the original LaTeX document.

We emphasize in this article techniques that do not make use of gifs,
  nor start with a non-HTML document.
All HTML examples in this article are valid HTML 3.2 code.

<P>
In the future, no doubt, there will be more flexible mechanisms for
  presenting mathematical material, but these are years away for the
  average user.
The MathML proposal of the WWW-Consortium is very promising.

<P>
I hope that this article will be obsolete soon!

<A NAME="italics">
<H2>Use italics for variables</H2>

<P>
This is a simple and effective rule, but is often ignored.
Compare:

<blockquote>
<P>
If d|n and d|m, then d|gcd(n,m).

<P>
If <I>d</I> | <I>n</I> and <I>d</I> | <I>m</I>, 
  then <I>d</I> | gcd(<I>n</I>,<I>m</I>).
</blockquote>

<P>
The second example looks much better.
There are a number of tags, <TT>&lt;I&gt;</TT>, 
  <TT>&lt;EM&gt;</TT>, and <TT>&lt;VAR&gt;</TT>
  that produce italics in HTML on most browsers, but I
  recommend using the <TT>&lt;I&gt;</TT> tag.

<P>
When using italics you have to be careful to turn it off around
  symbols such as +, /, =, (, and ).
In italics they look like <I>+ / = ( )</I>.
I find that, generally, the minus sign <FONT COLOR=blue>-</FONT>
  looks better in italics: <I><FONT COLOR=blue>-</FONT></I>.
Often the - is the same italicized or not, but in the popular
  Times font, the italicized version is better.

<A NAME="spaces">
<H2>Spaces count</H2>

<P>
Unlike TeX spaces will make a difference in mathematical expressions.
Compare

<BLOCKQUOTE>
<I>x<SUP>n</SUP></I>+<I>y<sup>n</sup></I>=<I>z<sup>n</sup></I>, 
  &nbsp;&nbsp;&nbsp; and <BR>
<I>x<SUP> n</SUP></I> + <I>y<sup> n</sup></I> = <I>z<sup> n</sup></I>.
</BLOCKQUOTE>

I find the second to be preferable to the first, except that there is
  a little bit too much space before the superscripts.
Ways of reducing that space are discussed in the next paragraph.
Below is the source that produced those two expressions.

<FONT color=green>
<BLOCKQUOTE>
<XMP>
<I>x<SUP>n</SUP></I>+<I>y<sup>n</sup></I>=<I>z<sup>n</sup></I>, and <BR>
<I>x<SUP> n</SUP></I> + <I>y<sup> n</sup><I> = </I>z<sup> n</sup></I>.
</XMP>
</BLOCKQUOTE>
</FONT>

Multiple spaces will appear the same as one space.
If you want to get the effect of multiple spaces then use the
  non-breaking space character <TT>&amp;nbsp;</TT>.

<P>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>
<TD valign=bottom>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TR><TD bgcolor=red><font size=1>&nbsp;</font></TD> 
</TABLE>
<TD valign=bottom>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TD bgcolor=orange><font size=2>&nbsp;</font></TD>
</TABLE>
<TD valign=bottom>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TD bgcolor=red><font size=3>&nbsp;</font></TD>
</TABLE>
<TD valign=bottom>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TD bgcolor=orange><font size=4>&nbsp;</font></TD>
</TABLE>
<TD valign=bottom>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TD bgcolor=red><font size=5>&nbsp;</font></TD>
</TABLE>
<TD valign=bottom>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TD bgcolor=orange><font size=6>&nbsp;</font></TD>
</TABLE>
<TD valign=bottom>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TD bgcolor=red><font size=7>&nbsp;</font></TD>
</TABLE>
<TD>&nbsp;
<TD>
On the left we show the size of a single space in the 
  font sizes of <I>k</I> = 1,2,3,4,5,6,7.
These are sizes (horizontal and vertical) of spaces that you will 
  get in the expression<BR>
  <TT>&lt;FONT SIZE=</TT><I>k</I><TT>&gt;&amp;nbsp;&lt;/FONT&gt;</TT> or<BR> 
  <TT>&lt;FONT SIZE=</TT><I>k</I><TT>&gt; &lt;/FONT&gt;</TT>
</TR>
</TABLE>

<A NAME="subscript">
<H2>Subscripts and superscripts and other goodies</H2>

<P>
When I first started producing HTML pages, there was no 
  subscripts or superscripts and so was forced to use programming
  style array indexing notation.
It was ugly!
E.g., compare 
<blockquote>
  p[1] &lt; p[2] &lt; ... &lt; p[n]   and
  &nbsp;<I>p</I><sub>1</sub> <TT>&lt;</TT> <I>p</I><sub>2</sub> <TT>&lt;</TT> 
  &middot;&middot;&middot; <TT>&lt;</TT> <I>p</I><sub><I>n</I></sub>. 
</blockquote>

<P>
The second example shows also the use of two other tricks:
The special character <TT>&amp;middot;</TT> can
   be used to simulate the <TT>\cdots</TT> or <TT>\cdot</TT> of LaTeX.
Secondly, the less-than symbol <TT>&amp;lt;</TT> looks better
   as a math symbol when set in <TT>&lt;TT&gt;</TT> font.
Here's the source for the second sequence of inequalities:

<FONT color=green>
<XMP>
  <I>p</I><sub>1</sub> <TT>&lt;</TT> <I>p</I><sub>2</sub> <TT>&lt;</TT> 
  &middot;&middot;&middot; <TT>&lt;</TT> <I>p</I><sub><I>n</I></sub>. 
</XMP>
</FONT>


<A NAME="fractions">
<H2>Fractions</H2>

<P>
A few small numeric fractions are available as special characters as
  shown in the table below.
There's no good way to do square roots, but
  expressions like (<I>x</I><sup>2</sup>+<I>y</I><sup>2</sup>)<sup>&frac12;</sup> 
  are reasonable.


<P>
<CENTER>
<TABLE border=1>
<TR>
<TH>result <TH>size=+1 <TH> source <TH> alternate
<TR>
<TD align=center>
&frac12;
<TD align=center>
<FONT size=+1>&frac12;</font>
<TD align=center>
<TT>&amp;frac12;</TT>
<TD align=center>
<TT>&amp;#189;</TT>
<TR>
<TD align=center>
&frac14;
<TD align=center>
<FONT size=+1>&frac14;</font>
<TD align=center>
<TT>&amp;frac14;</TT>
<TD align=center>
<TT>&amp;#188;</TT>
<TR>
<TD align=center>
&frac34;
<TD align=center>
<FONT size=+1>&frac34;</font>
<TD align=center>
<TT>&amp;frac34;</TT>
<TD align=center>
<TT>&amp;#190;</TT>
</TABLE>
</CENTER>

<P>
The basic idea in rendering more general fractions is to use a table with three rows, 
  one each for the numerator, denominator, and horizontal line.
The main problem to be overcome in is that of how to render the horizontal 
  line between the numerator and the denominator.
I recommend one of two solutions: (a) use a non-shaded horizontal rule
  (as in <TT>&lt;HR NOSHADE&gt;</TT>), or (b) use a one-bit black gif
  together with the BGCOLOR=BLACK attribute.
Solution (b) violates our rule about no gifs, but it is is a minor infraction,
  and is the only one we advocate.
Solution (a) has the drawbacks that the amount of horizonal space used by 
  the line is fixed and large, and that it is rendered as a dark gray, 
  and not black, on some browsers.
The infinity is ugly, but I know of no better way to do it.

<P>
<CENTER>
<TABLE border=2>
<TR>
<TD>

<P>
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TR>
  <TD valign=center>
  <TABLE border=0 cellspacing=0 cellpadding=0>
    <TR>
    <TD align=center>
    lim
    </TD>
    <TR>
    <TD valign=top align=center>
    <FONT SIZE=-1><I>h</I></FONT><FONT size=-2><I>--</I>&gt; OO</Font>
    </TD>
  </TABLE>
  </TD>
 
<TD valign=center>
<TABLE border=0 cellspacing=0 cellpadding=0>

<TR>
<TD bgcolor=orange>
&nbsp;<I>f</I><FONT SIZE=1>&nbsp;</FONT>(<I>x</I>)&nbsp;<I>-</I>&nbsp;<I>f</I><FONT SIZE=1>&nbsp;</FONT>(<I>x+h</I>)
</TD>
<TD>&nbsp;&nbsp;&nbsp;</TD>
<TD align=right><I>d</I></TD>
</TR>

<TR>
<TD bgcolor=yellow><HR noshade></TD>
<TD>&nbsp;=&nbsp;</TD> 
<TD><HR noshade></TD>

<TR>
<TD align=center><I>h</I></TD>
<TD>&nbsp;</TD>
<TD><I>dx</I></TD>
</TABLE>
</TD>

<TD valign=center>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>
<TD>&nbsp;<I>f</I>(<I>x</I>)</TD>
</TABLE>

</TD>
 
</TABLE>

<TD>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>
 
<TD valign=center>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>
<TD align=center>
lim
</TD>
<TR>
<TD valign=top align=center bgcolor=yellow>
<FONT SIZE=-1><I>h</I></FONT><FONT size=-2><I>--</I>&gt; OO</Font>
</TD>
</TABLE>
</TD>
 
<TD valign=center>
<TABLE border=0 cellspacing=1 cellpadding=0>

<TR>
<TD>
&nbsp;<I>f</I><FONT SIZE=1>&nbsp;</FONT>(<I>x</I>)&nbsp;<I>-</I>&nbsp;<I>f</I><FONT SIZE=1>&nbsp;</FONT>(<I>x+h</I>)
</TD>
</TR>

<TR>
<TD bgcolor=black><IMG src=bb.gif height=2></TD>
<TR>
<TD align=center><I>h</I></TD>
</TABLE>
</TD>

<TD valign=center>&nbsp;=&nbsp;</TD>

<TD valign=center>
    <TABLE border=0 cellspacing=1 cellpadding=0>
      <TR>
      <TD align=right><I>&nbsp;d</I></TD>
      <TR>
      <TD bgcolor=black><IMG src=bb.gif height=2></TD>
      <TR>
      <TD><I>dx</I></TD>
    </TABLE>
</TD>

<TD valign=center>
    <TABLE border=0 cellspacing=0 cellpadding=0>
      <TR>
      <TD>&nbsp;<I>f</I>(<I>x</I>)</TD>
    </TABLE>
</TD>
 
</TABLE>

<TR>
<TD>
Using &lt;HR&gt; (<A HREF="derivHR.txt">source</A>)
<TD>
Using one bit gif (<A HREF="deriv1bit.txt">source</A>)
</TABLE>
</CENTER>

<P>
Here's a continued fraction expansion, using tables and unshaded horizontal
  rules.
It takes up alot of vertical space, but there's no easy way to avoid it.



<P>
<CENTER>
<TABLE border=0 cellpadding=0 cellspacing=0>
<TR>
  <TD>
  <TD align=center colspan=4>1
<TR>
  <TD><I>a</I><sub>0</sub> + 
  <TD colspan=4><HR noshade>
<TR> 
  <TD><TD>
  <TD align=center colspan=3>1
<TR>
  <TD>
  <TD><I>a</I><sub>1</sub> + 
  <TD colspan=3><HR noshade>
<TR>
  <TD><TD><TD>
  <TD align=center colspan=2>1
<TR>
  <TD><TD>
  <TD><I>a</I><sub>2</sub> + 
  <TD colspan=2><HR noshade>
<TR>
  <TD><TD><TD><TD>
  <TD align=center colspan=1>1
<TR>
  <TD><TD><TD>
  <TD><I>a</I><sub>3</sub> +
  <TD colspan=1><HR noshade>
<TR>
  <TD><TD><TD><TD>
  <TD><I>a</I><sub>4</sub></TD> 
</TABLE>
</CENTER>

<P>
1<BR>
--<BR>
2

<P>
<U>1</U><BR>2

<P>
<U>1<sub>&nbsp;</sub></U><BR>2

<P>
<U>1<sub></sub></U><BR>2

<P>
<I>x+y</I><BR>
<STRIKE>&nbsp;&nbsp;&nbsp;&nbsp;</STRIKE><BR>
<I>x-y</I>

<HR noshade height=5 width=20 color=blue>

<P>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>
<TD bgcolor=yellow>
<I>x+y</I>
<TR><TD bgcolor=black><HR noshade></TD></TR>
<TR>
<TD bgcolor=orange>
<I>x-y</I>
<TR><TD bgcolor=black></TD>
</TABLE>

<P>
&macr;|&macr;

<A NAME="binomial">
<H2>Binomial Coefficients</H2>

<P>
A good looking binomial coefficient can be produced by the 
  judicious use of tables and font changes.
The colored version shows the actual size of the table cells that are used.

<TABLE>
<TR>
<TD valign=center>
<TABLE border=0 CELLPADDING=0 CELLSPACING=0>
<TR>
<TD valign=center>
  <FONT SIZE="+2">(</FONT> </TD>
<TD valign=center align=center>
  <TABLE border=0 cellpadding=0 cellspacing=0>
  <TR><TD align=center><I>n</I> </TD></TR>
  <TR><TD align=center><I>k</I> </TD></TR>
  </TABLE></TD>
<TD valign=center> <FONT size="+2">)</FONT></TD>  
</TABLE></TD>
<TD>
&nbsp;&nbsp;&nbsp;&nbsp;
<TD>
<TABLE border=0 CELLPADDING=0 CELLSPACING=0>
<TR>
<TD valign=center bgcolor=yellow>
  <FONT SIZE="+2">(</FONT> </TD>
<TD valign=center align=center>
  <TABLE border=0 cellpadding=0 cellspacing=0>
  <TR><TD align=center bgcolor=orange><I>n</I> </TD></TR>
  <TR><TD align=center bgcolor=red><I>k</I> </TD></TR>
  </TABLE></TD>
<TD valign=center bgcolor=yellow> <FONT size="+2">)</FONT></TD>  
</TABLE></TD>
</TABLE>

<P>
One disadvantage of using tables is that they can't be used in-line in text
  in any straigtforward manner.
If you want your binomial coefficients in-line then about the best that can be
  done is fake super- and sub-scripting as in the following example.
This binomial coefficient, ( <sup><I>n</I></sup><sub><I>k</I></sub> ), was produced by the
  following HTML: <FONT color=green><TT>
  ( &lt;sup&gt;n&lt;/sup&gt;&lt;sub&gt;&lt;I&gt;k&lt;/I&gt;&lt;/sub&gt; )
  </TT></font>. 

<P>
Below is the recurrence relation for Stirling numbers of the Second
  kind (<A HREF="Stirling.txt">source</A>).

<TABLE CELLPADDING=0 CELLSPACING=0>
<TR>
<TD valign=center>
  <FONT SIZE="+2">{</FONT>
<TD valign=center>
  <I>n</I> <BR> <I>k</I>
<TD valign=center>
  <FONT size="+2">}</FONT>
<TD valign=center>
&nbsp; = &nbsp;
<I>k</I>
<TD valign=center>
  <FONT SIZE="+2">{</FONT>
<TD valign=center>
  <I>n-</I>1 <BR> &nbsp; <I>k</I>
<TD valign=center>
  <FONT size="+2">}</FONT>
<TD valign=center>
+
<TD valign=center>
  <FONT SIZE="+2">{</FONT>
<TD valign=center>   
  <I>n-</I>1 <BR> <I>k-</I>1
<TD valign=center>
  <FONT size="+2">}</FONT>            

</TABLE>

<A NAME="summation">
<H2>Summations</H2>

<P>
A reasonable summation sign may be made by using tables.
Below is the ``Witt formula'' for counting Lyndon words, together
  with the source that produced the summation.

<CENTER>
<TABLE border=0 cellpadding=0 cellspacing=0>
<TR>
<TD valign=center>
<I>L<sub>q</sub></I>(<I>n</I>) &nbsp; = &nbsp;
<TD valign=center>
  <TABLE border=0 cellpadding=0 cellspacing=0>
  <TR><TD><TT>__</TT></TD>    <!-- (**) -->
  <TR><TD>\
  <TR><TD><U>/&nbsp;&nbsp;&nbsp;&nbsp;</U>
  <TR><TD><FONT size=-1><I>d</I> | <I>n</I></font>
  </TABLE>
<TD valign=center>
  <B><I>&micro;</I></B>(<I>n</I>/<I>d</I>) <I>q</I> <sup><I>d</I></sup>
</TABLE>
</CENTER>

<FONT COLOR=green SIZE=-1> 
<BLOCKQUOTE>
<PRE>
&lt;CENTER&gt;
&lt;TABLE border=0 cellpadding=0 cellspacing=0&gt;
&lt;TR&gt;
&lt;TD valign=center&gt;
  &lt;I&gt;L&lt;sub&gt;q&lt;/sub&gt;&lt;/I&gt;(&lt;I&gt;n&lt;/I&gt;) &amp;nbsp; = &amp;nbsp;
&lt;TD valign=center&gt;
  &lt;TABLE border=0 cellpadding=0 cellspacing=0&gt;
  &lt;TR&gt;&lt;TD&gt;&lt;TT&gt;__&lt;/TT&gt;     &lt;!-- (**) --&gt;
  &lt;TR&gt;&lt;TD&gt;\
  &lt;TR&gt;&lt;TD&gt;&lt;U&gt;/&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/U&gt;
  &lt;TR&gt;&lt;TD&gt;&lt;FONT size=-1&gt;&lt;I&gt;d&lt;/I&gt; | &lt;I&gt;n&lt;/I&gt;&lt;/font&gt;
  &lt;/TABLE&gt;
&lt;TD valign=center&gt;
  &lt;B&gt;&lt;I&gt;&amp;micro;&lt;/I&gt;&lt;/B&gt;(&lt;I&gt;n&lt;/I&gt;/&lt;I&gt;d&lt;/I&gt;) &lt;I&gt;q&lt;/I&gt; &lt;sup&gt;&lt;I&gt;d&lt;/I&gt;&lt;/sup&gt;
&lt;/TABLE&gt;
&lt;/CENTER&gt;
</PRE>
</BLOCKQUOTE>
</FONT>

<A NAME="characters">
<H2>Special Characters</H2>
 
<H3>The Greek letter Mu</H3>

Although most Greek letter are unavailable, mu is, using
  either <TT>&amp;micro</TT> or <TT>&amp;#181</TT>. 
Variations may be obtained by specifying particular fonts.

<P>
<CENTER>
<TABLE border=1>
<TR>
  <TH>result
  <TH>source
  <TH colspan=6>size=1..6
<TR>
<TD align=center>
  &micro;
<TD>
  <tt>&amp;micro;</tt>
<TD align=center>
  <FONT SIZE=1>&micro</FONT>
<TD align=center>
  <FONT SIZE=2>&micro</FONT>
<TD align=center>
  <FONT SIZE=3>&micro</FONT>
<TD align=center>
  <FONT SIZE=4>&micro</FONT>
<TD align=center>
  <FONT SIZE=5>&micro</FONT>
<TD align=center>
  <FONT SIZE=6>&micro</FONT>
<TR>
<TD align=center>
  <I>&micro</I>
<TD>
  <tt>&lt;I&gt;&amp;micro;&lt;/I&gt;</tt>
<TD align=center>
  <FONT SIZE=1><I>&micro</I></FONT>
<TD align=center>
  <FONT SIZE=2><I>&micro</I></FONT>
<TD align=center>
  <FONT SIZE=3><I>&micro</I></FONT>
<TD align=center>
  <FONT SIZE=4><I>&micro</I></FONT>
<TR>
<TD align=center>
  <B>&micro</B>
<TD>
  <tt>&lt;B&gt;&amp;micro;&lt;/B&gt;</tt>
<TD align=center>
  <FONT SIZE=1><B>&micro</B></FONT>
<TD align=center>
  <FONT SIZE=2><B>&micro</B></FONT>
<TD align=center>
  <FONT SIZE=3><B>&micro</B></FONT>
<TD align=center>
  <FONT SIZE=4><B>&micro</B></FONT>
<TR>
<TD align=center>
  <B><I>&micro</I></B>
<TD>
  <tt>&lt;B&gt;&lt;I&gt;&amp;micro;&lt;/I&gt;&lt;/B&gt;</tt>
<TD align=center>
  <FONT SIZE=1><B><I>&micro</I></B></FONT>
<TD align=center>
  <FONT SIZE=2><B><I>&micro</I></B></FONT>
<TD align=center>
  <FONT SIZE=3><B><I>&micro</I></B></FONT>
<TD align=center>
  <FONT SIZE=4><B><I>&micro</I></B></FONT>
<TR>
<TD align=center>
  <TT>&micro</TT>
<TD>
  <tt>&lt;TT&gt;&amp;micro;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT>&micro</TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT>&micro</TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT>&micro</TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT>&micro</TT></FONT>
<TR>
<TD align=center>
  <TT><I>&micro</I></TT>
<TD>
  <tt>&lt;TT&gt;&lt;I&gt;&amp;micro;&lt;/I&gt;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT><I>&micro</I></TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT><I>&micro</I></TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT><I>&micro</I></TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT><I>&micro</I></TT></FONT>
<TR>
<TD align=center>
  <TT><B>&micro</B></TT>
<TD>
  <tt>&lt;TT&gt;&lt;B&gt;&amp;micro;&lt;/B&gt;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT><B>&micro</B></TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT><B>&micro</B></TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT><B>&micro</B></TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT><B>&micro</B></TT></FONT>
<TR>
<TD align=center>
  <TT><B><I>&micro</I></B></TT>
<TD>
  <tt>&lt;TT&gt;&lt;B&gt;&lt;I&gt;&amp;micro;&lt;/I&gt;&lt;/B&gt;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT><B><I>&micro</I></B></TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT><B><I>&micro</I></B></TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT><B><I>&micro</I></B></TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT><B><I>&micro</I></B></TT></FONT>
</TABLE>
</CENTER>

<H3>The Greek letter Phi, or the empty set</H3>

An <TT>&amp;Oslash;</TT> (or <TT>&amp;#216;</TT>)
  can be used as a passable phi or empty set.
There is also a lower case version:
  <TT>&amp;oslash;</TT> (or <TT>&amp;#248;</TT>)

<P>
<CENTER>
<TABLE border=1>
<TR>
  <TH>result
  <TH>source
  <TH colspan=4>size=1..4
<TR>
<TD align=center>
  &Oslash;
<TD>
  <tt>&amp;Oslash;</tt>
<TD align=center>
  <FONT SIZE=1>&Oslash</FONT>
<TD align=center>
  <FONT SIZE=2>&Oslash</FONT>
<TD align=center>
  <FONT SIZE=3>&Oslash</FONT>
<TD align=center>
  <FONT SIZE=4>&Oslash</FONT>
<TR>
<TD align=center>
  <I>&Oslash</I>
<TD>
  <tt>&lt;I&gt;&amp;Oslash;&lt;/I&gt;</tt>
<TD align=center>
  <FONT SIZE=1><I>&Oslash</I></FONT>
<TD align=center>
  <FONT SIZE=2><I>&Oslash</I></FONT>
<TD align=center>
  <FONT SIZE=3><I>&Oslash</I></FONT>
<TD align=center>
  <FONT SIZE=4><I>&Oslash</I></FONT>

<TR>
<TD align=center>
  <B>&Oslash</B>
<TD>
  <tt>&lt;B&gt;&amp;Oslash;&lt;/B&gt;</tt>
<TD align=center>
  <FONT SIZE=1><B>&Oslash</B></FONT>
<TD align=center>
  <FONT SIZE=2><B>&Oslash</B></FONT>
<TD align=center>
  <FONT SIZE=3><B>&Oslash</B></FONT>
<TD align=center>
  <FONT SIZE=4><B>&Oslash</B></FONT>

<TR>
<TD align=center>
  <B><I>&Oslash</I></B>
<TD>
  <tt>&lt;B&gt;&lt;I&gt;&amp;Oslash;&lt;/I&gt;&lt;/B&gt;</tt>
<TD align=center>
  <FONT SIZE=1><B><I>&Oslash</I></B></FONT>
<TD align=center>
  <FONT SIZE=2><B><I>&Oslash</I></B></FONT>
<TD align=center>
  <FONT SIZE=3><B><I>&Oslash</I></B></FONT>
<TD align=center>
  <FONT SIZE=4><B><I>&Oslash</I></B></FONT>
<TR>
<TD align=center>
  <TT>&Oslash</TT>
<TD>
  <tt>&lt;TT&gt;&amp;Oslash;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT>&Oslash</TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT>&Oslash</TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT>&Oslash</TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT>&Oslash</TT></FONT>
<TR>
<TD align=center>
  <TT><I>&Oslash</I></TT>
<TD>
  <tt>&lt;TT&gt;&lt;I&gt;&amp;Oslash;&lt;/I&gt;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT><I>&Oslash</I></TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT><I>&Oslash</I></TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT><I>&Oslash</I></TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT><I>&Oslash</I></TT></FONT>
<TR>
<TD align=center>
  <TT><B>&Oslash</B></TT>
<TD>
  <tt>&lt;TT&gt;&lt;B&gt;&amp;Oslash;&lt;/B&gt;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT><B>&Oslash</B></TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT><B>&Oslash</B></TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT><B>&Oslash</B></TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT><B>&Oslash</B></TT></FONT>
<TR>
<TD align=center>
  <TT><B><I>&Oslash</I></B></TT>
<TD>
  <tt>&lt;TT&gt;&lt;B&gt;&lt;I&gt;&amp;Oslash;&lt;/I&gt;&lt;/B&gt;&lt;/TT&gt;</tt>
<TD align=center>
  <FONT SIZE=1><TT><B><I>&Oslash</I></B></TT></FONT>
<TD align=center>
  <FONT SIZE=2><TT><B><I>&Oslash</I></B></TT></FONT>
<TD align=center>
  <FONT SIZE=3><TT><B><I>&Oslash</I></B></TT></FONT>
<TD align=center>
  <FONT SIZE=4><TT><B><I>&Oslash</I></B></TT></FONT>
</TABLE>
</CENTER>

<H3>Other special characters</H3>

A full list of characters may be found at
  <A HREF="http://www.uni-passau.de/%7Eramsch/iso8859-1.html">iso8859-1 table</A>.
Most of these are not of use mathematically.
There are a few exceptions:

<P>
Times <TT>&amp;times;</TT> &times; <I>&times</I> <TT>&times</TT>

<P>
The German sz ligature in italics mode is almost a Greek beta:
  <TT>&amp;szlig;</TT> <I>&szlig;</I>  <TT><I>&szlig;</I></TT>

<P>
"Less than or equal" can be faked with underlining <U>&lt;</U> is
  produced with <TT>&lt;U&gt;&amp;lt;&lt;/U&gt;</TT>.
And <TT>&lt;U&gt;&amp;gt;&lt;/U&gt;</TT> produces
  <U>&gt;</U> &nbsp;.

<P>
Bottom: <U>&nbsp;|&nbsp;</U> 

<H3>The "symbol font"</H3>

If you're willing to cheat and use the "Symbol font" then
  you gain alot of flexibility, but these fonts will generally
  only be available to PC users.
Most likely if you are on a UNXI machine then what's below looks like
  gibberish.

<FONT FACE="Symbol" SIZE=+3>&#229;</FONT>

<FONT FACE="Symbol" SIZE=+1>abcdefghijkl</FONT>

<A NAME="final">
<H2>A few final words of advice</H2>

<UL>
<LI>
  If you are planning to put some mathematics on the web then be
  prepared to spend many  hours getting it to look good.
<LI>
  Test the final result on a variety of browsers, platforms, fonts, and
  font sizes.
<LI>
  Get the latest version of your favorite browser.  Browsers are free,
  but if you insist on using Netscape 1.1, then don't expect good looking
  web pages.
<LI>
  But what about integrals, infinity, and all those other 
  constructs not covered in this article?
  Well, I don't know how to create them.  If you do please let me know,
  and I'll add it to the page.
<LI>
  Don't expect your creations to look perfect; they won't!
</UL>

<A NAME="refs">
<H2>References</H2>

I wish.

oo
OO
00
<P>
<TABLE border=0 cellspacing=0 cellpadding=0 width="50%">
<TR>
<TD bgcolor=red><FONT size=1>aa</FONT></TD>
<TR>
<TD bgcolor=green><FONT size=2>bb</FONT></TD>
<TR>
<TD bgcolor=red><FONT size=3>cc</FONT></TD>
<TR>
<TD bgcolor=green><FONT size=4>dd</FONT></TD>
<TR>
<TD bgcolor=red><FONT size=5>ee</FONT></TD>
<TR>
<TD bgcolor=red><FONT size=1><HR noshade></FONT></TD>
<TR>
<TD bgcolor=green><FONT size=2><HR noshade></FONT></TD>
<TR>
<TD bgcolor=red><FONT size=3><HR noshade></FONT></TD>
<TR>
<TD bgcolor=green><FONT size=4><HR noshade></FONT></TD>
<TR>
<TD bgcolor=red><FONT size=5><HR noshade></FONT></TD>
</TABLE>


<P>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>

<TD valign=center>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>
<TD align=center bgcolor=orange>
lim
</TD>
<TR>
<TD valign=top align=center bgcolor=yellow>
<FONT SIZE=-1><I>h</I></FONT><FONT size=-2><I>--</I>&gt; OO</Font>
</TD>
</TABLE>
</TD>

<TD valign=center>
<TABLE border=0 cellspacing=0 cellpadding=0>
<TR>
<TD>
&nbsp; <I>f</I><FONT SIZE=1> </FONT>(<I>x</I>) <I>-</I> <I>f</I><FONT SIZE=1> </FONT>(<I>x+h</I>)
</TD>
<TR>
<TD>
<HR noshade>
</TD>
<TR>
<TD align=center bgcolor=orange>
<I>h</I>
</TD>
</TABLE>
</TD>

</TABLE>

<P>
"And"  /\ and "Or" \/ are easily formed with slash and backslash.

<P>
<TABLE border=0 cellspacing=0 cellpadding=0>
 
<TR>
<TD>
&nbsp;<I>f</I><FONT SIZE=1>&nbsp;</FONT>(<I>x</I>)&nbsp;<I>-</I>&nbsp;<I>f</I><FONT SIZE=1>&nbsp;</FONT>(<I>x+h</I>)</TD>
<TR>
<TD bgcolor=black><IMG SRC=bb.gif height=2></TD>
<TR>
<TD align=center><I>h</I>

</TR>

</TABLE>

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
<TR>
<TD bgcolor=yellow><U>|&nbsp;</U></TD>
<TD><I>n</I>/2</TD>
<TD bgcolor=orange><U>&nbsp;|</U></TD>
</TABLE>

<P>
a <sup><U>&nbsp;&nbsp;&nbsp;</U></SUP> b
</BODY>
</HTML>

