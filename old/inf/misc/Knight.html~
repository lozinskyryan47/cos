<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<TITLE>Info on Knight's Problem</TITLE>
<BASE HREF="http://theory.cs.uvic.ca/" >
</HEAD>

<BODY>

<A HREF=gen/misc.html>
<IMG ALIGN=left vspace=1 border=0 height=33 width=35 SRC=ico/gen.gif></A>
<!--#include file="inc/Border.include" -->
<HR>
 
<H2>Information on the <I>n</I> Knight's Tour Problem</H2>

The problem is to find a tour of a knight on a <I>n</I> by <I>n</I>
  board.  
Only valid moves of a knight are allowed and every square
  of the board must be visited exactly once.
If it is possible to use a single valid move to get from the last
  visited square to the first, then the tour is said to be
  <I>re-entrant</I>.
Below we show three solutions on a standard 8 by 8 board.

<!--<IMG SRC="ico/Knight.gif" ALT="[GIF OF 3 KNIGHT's TOURS]">-->

The chessboard is encoded as shown below for <I>n</I> = 6.

<PRE>
     0  1  2  3  4  5
  +-------------------
0 |  0  1  2  3  4  5
1 |  6  7  8  9 10 11
2 | 12 13 14 15 16 17
3 | 18 19 20 21 22 23
4 | 24 25 26 27 28 29
5 | 30 31 32 33 34 35
</PRE>

The one-line notation simply gives a list of size <I>n*n</I> of the
  successive square visited.
In matrix notation the ending square of the <I>i</I>th move is 
  labelled with an <I>i</I>.

<P>
A related problem is that of finding the longest "uncrossed"
  knight's tour.
For <I>n</I> = 3,4,5,6,7,8 the longest tours have length 
  2, 5, 10, 17, 24, 35.

This is sequence 
  <A HREF="<!--#include file="inc/IntSeq.include"-->Anum=A003192">
  <B>A003192</B>(M1369)</A> in 
<!--#include file="inc/Sloane.include"-->

<P>
Another related problem is that of finding the minimal number of
  knights needed to attack (or occupy) every square of
  an <I>n</I> by <I>n</I> board.
For <I>n</I> = 1,2,...,12 the minimal numbers are
  1, 4, 4, 4, 5, 8, 10, 12, 14, 16, 21, 24.
This is sequence 
  <A HREF="<!--#include file="inc/IntSeq.include"-->Anum=A006075">
  <B>A006075</B>(M3224)</A> in 
<!--#include file="inc/Sloane.include"-->

<P>
The algorithm used is a well-known backtracking.
Only inputs to <I>n</I> = 7 are allowed.  
If any user knows of a fast algorithm that will give
  lots of solutions for <I>n</I> = 8 (or higher) in a reasonable 
  amount of time, <B>please</B> contact us.

<HR>
Programs available: <IMG SRC=ico/cat.gif height=32 width=43>
<UL>
<LI><A HREF="src/misc/NumericalPartition.c">
  C program </A>
<!--#include file="inc/Red.include" -->
<LI><A HREF="src/misc/NumericalPartition.p">
  Pascal program </A>
<!--#include file="inc/Red.include" -->
</UL>

<HR>
Online Stuff:
<UL>

<!--  These are bad links

<LI><A HREF="http://www.informatik.uni-trier.de/~rossmann/dagstuhl/abstract25.html">Abstract of talk</A> by I. Wegener about paper below.
<LI><A HREF="http://wombat.doc.ic.ac.uk/fp/KnightsTour.hs">
  A Haskell Knight's tour program.</A>

-->
<LI>
Mark Keen has an excellent page 
<A HREF="http://www.escalix.com/freepage/markkeen/knight.htm">The
Knight's Tour</A>, which includes much historical information.
<LI>
Ted Filler has a cool 
<A HREF="http://home.earthlink.net/~tfiller/knight.htm">Knight's
  Tour Game</A>.
<LI>
Yahoo even has a
<A HREF="http://www.yahoo.com/Recreation/Games/Board_Games/Chess/Problems/Knight_s_Tour/">category on Knight's tours</A>.
<LI>A <A HREF="http://www.cs.colostate.edu/~schwicke/java/KnightsTourApplet.html">javaa applet</A> for finding a Knight's tour on a 5 by 5 board.
<LI>A link to a 
  <A HREF="http://homepages.enterprise.net/stevegray/tour.html">shareware 
  program</A> for finding a single solution to
  Knight's tour problems.  It uses Warnsdorff's rule.
<LI><A HREF="http://ls2-www.informatik.uni-dortmund.de/papers/Dagstuhl95/zbdd.ps.gz">Paper on estimating the number of tours</A> </UL>

<HR>
<A HREF=gen/misc.html>
<IMG ALIGN=left vspace=1 border=0 SRC="ico/gen.gif" heigth=33 width=35 ALT=""></A>
<!--#include file="inc/Border.include" -->
<HR>
<FONT SIZE=-1>
<!--#include file="inc/Wizard.include"--><br>
<!-- This page maintained by -->
<!-- <A HREF="mailto:cos@theory.csc.uvic.ca">the wizard of COS</A>.<BR> -->
<!--#exec cgi="inc/cos-counter/counter-nl"--><BR>
It was last updated <!--#echo var="LAST_MODIFIED" -->.<BR>
<!--#include file="inc/Copyright.include"-->
</FONT>
</BODY>
</HTML>

