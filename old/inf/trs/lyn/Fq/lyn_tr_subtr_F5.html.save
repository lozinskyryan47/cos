<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<TITLE>
5-ary Lyndon words with given trace and subtrace
</TITLE>
<BASE HREF="http://theory.cs.uvic.ca/">
</HEAD>

<BODY>
<!--#include file="inc/Border.include"-->
<HR>
                            
<H1>
<center>5-ary Lyndon words with given trace and subtrace.</center>
</H1>

<P>
Here we consider the set <B>L</B>(<I>n</I>;<I>t</I>,<I>s</I>) of length 
<I>n</I>
lyndon words <I>a</I><sub>1</sub><I>a</I><sub>2</sub>...<I>a<sub>n</sub></I>
over the alphabet consisting of the elements of the field 
<i><b>F<sub>5</sub></b></i> that have trace <I>t</I>
and subtrace <I>s</I>.  The <EM>trace</EM> of a 5-ary lyndon word
is the sum of its digits over the field <i><b>F<sub>5</sub></b></i>; 
<I>t</I> = a<sub>1</sub>+a<sub>2</sub>+ ... +a<sub><I>n</I></sub>. 
The <EM>subtrace</EM> is the sum of the products
of all <I>n</I>(<I>n-</I>1)/2 pairs of digits taken over the field 
<i><b>F<sub>5</sub></b></i>; 
<I>s</I> = SUM( <I>a<sub>i</sub>a<sub>j</sub></I> : 
1 <U>&lt;</U> <I>i</I> &lt; <I>j</I> <U>&lt;</U> <I>n</I> ).
<p>


<P>
<CENTER>
<TABLE border=1>
<TR>
  <TD>
  <TD ALIGN="center" COLSPAN=13>(trace,subtrace)</TD>
<TR>
<TD align=center><i>n</i>
  <TD bgcolor=yellow align=center><b>(0,0)</b>
  <TD bgcolor=yellow align=center><b>(0,1)</b><br><b>(0,4)</b>
  <TD bgcolor=yellow align=center><b>(0,2)</b><br><b>(0,3)</b>
  <TD bgcolor=yellow align=center><b>(1,0)</b><br><b>(2,0)</b><br><b>(3,0)</b><br><b>(4,0)</b>
  <TD bgcolor=yellow align=center><b>(1,1)</b><br><b>(2,4)</b><br><b>(3,4)</b><br><b>(4,1)</b>
  <TD bgcolor=yellow align=center><b>(1,2)</b><br><b>(2,3)</b><br><b>(3,3)</b><br><b>(4,2)</b>
  <TD bgcolor=yellow align=center><b>(1,3)</b><br><b>(2,2)</b><br><b>(3,2)</b><br><b>(4,3)</b>
  <TD bgcolor=yellow align=center><b>(1,4)</b><br><b>(2,1)</b><br><b>(3,1)</b><br><b>(4,4)</b>

<TR><TH align=center bgcolor=yellow>1</TH>
  <TD align=right>1  <TD align=right>0  <TD align=right>0
  <TD align=right>1
  <TD align=right>0  <TD align=right>0  <TD align=right>0
  <TD align=right>0  
<TR><TH align=center bgcolor=yellow>2</TH>
  <TD align=right>0  <TD align=right>1  <TD align=right>0
  <TD align=right>1
  <TD align=right>0  <TD align=right>0  <TD align=right>1
  <TD align=right>0  
<TR><TH align=center bgcolor=yellow>3</TH>
  <TD align=right>0  <TD align=right>2  <TD align=right>2
  <TD align=right>2
  <TD align=right>2  <TD align=right>0  <TD align=right>2
  <TD align=right>2 
<TR><TH align=center bgcolor=yellow>4</TH>
  <TD align=right>6  <TD align=right>5  <TD align=right>7
  <TD align=right>5
  <TD align=right>6  <TD align=right>5  <TD align=right>7
  <TD align=right>7 
<TR><TH align=center bgcolor=yellow>5</TH>
  <TD align=right>24  <TD align=right>20  <TD align=right>30
  <TD align=right>25
  <TD align=right>25  <TD align=right>25  <TD align=right>25
  <TD align=right>25 
<TR><TH align=center bgcolor=yellow>6</TH>
  <TD align=right>104  <TD align=right>99  <TD align=right>107
  <TD align=right>104
  <TD align=right>99  <TD align=right>107  <TD align=right>107
  <TD align=right>99  
<TR><TH align=center bgcolor=yellow>7</TH>
  <TD align=right>432  <TD align=right>450  <TD align=right>450
  <TD align=right>450
  <TD align=right>450  <TD align=right>450  <TD align=right>450
  <TD align=right>432 
<TR><TH align=center bgcolor=yellow>8</TH>
  <TD align=right>1950  <TD align=right>1965  <TD align=right>1935
  <TD align=right>1935
  <TD align=right>1965  <TD align=right>1950  <TD align=right>1965
  <TD align=right>1935 
<TR><TH align=center bgcolor=yellow>9</TH>
  <TD align=right>8736  <TD align=right>8666  <TD align=right>8666
  <TD align=right>8666
  <TD align=right>8736  <TD align=right>8666  <TD align=right>8666
  <TD align=right>8666  
<TR><TH align=center bgcolor=yellow>10</TH>
  <TD align=right>39298  <TD align=right>38985  <TD align=right>38990
  <TD align=right>39050
  <TD align=right>39050  <TD align=right>39050  <TD align=right>39050
  <TD align=right>39050 
<TR><TH align=center bgcolor=yellow>11</TH>
  <TD align=right>177784  <TD align=right>177500  <TD align=right>177500
  <TD align=right>177784
  <TD align=right>177500  <TD align=right>177500  <TD align=right>177500
  <TD align=right>177500  
<TR><TH align=center bgcolor=yellow>12</TH>
  <TD align=right>813748  <TD align=right>814006  <TD align=right>813490
  <TD align=right>814006
  <TD align=right>813490  <TD align=right>813490  <TD align=right>814006
  <TD align=right>813748 
<TR><TH align=center bgcolor=yellow>13</TH>
  <TD align=right>3755048  <TD align=right>3756250  <TD align=right>3756250
  <TD align=right>3756250
  <TD align=right>3756250  <TD align=right>3755048  <TD align=right>3756250
  <TD align=right>3756250 
<TR><TH align=center bgcolor=yellow>14</TH>
  <TD align=right>17438400  <TD align=right>17437275  <TD align=right>17439507
  <TD align=right>17437275
  <TD align=right>17438400  <TD align=right>17437275  <TD align=right>17439507
  <TD align=right>17439507  
<TR><TH align=center bgcolor=yellow>15</TH>
  <TD align=right>81380192  <TD align=right>81374990  <TD align=right>81385410
  <TD align=right>81380200
  <TD align=right>81380200  <TD align=right>81380200  <TD align=right>81380200
  <TD align=right>81380200  
</TABLE>
</CENTER>

<H2>Examples:</H2>



<H2>Further Notes:</H2>

<HR>
<!--#include file="inc/Border.include"-->
<HR>
<FONT SIZE=-1>
<!--#include file="inc/Wizard.include"--><br>
It was last updated <!--#echo var="LAST_MODIFIED" -->.<BR>
<!--#exec cgi="inc/cos-counter/counter-nl"--><BR>
<!--#include file="inc/Copyright.include"-->
</FONT>
                                                   
</BODY>
</HTML>

