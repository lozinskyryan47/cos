#!/usr/bin/perl

$Logdir = "LOG";
$Filename = "cos-access_count"; # name of counter file
$Logname = "counter.log";	# name of log file
$Startname = "counter.start";

#----------------------------------------------------------------------------------
-e $Logdir || mkdir $Logdir, 0777;

chop($timestamp = `date`);	# get time and date
$timestamp =~ s/ /_/g;		# replace blanks by underscore
$Filesave = "$Logdir/$Filename" . "." . $timestamp; # compose a name for the old file
rename $Filename,$Filesave;	# rename the oldfile
system "echo '02.000' > $Filename"; # create a new counter file 
# 02.000 is the version number of the counter program

$Logsave = "$Logdir/$Logname" . "." . $timestamp; # name of saved log file
rename $Logname,$Logsave;
system "echo '=== Master log file ===' > $Logname";

chop($Date=`date +"%b %e, %Y"`);
system "echo $Date > $Startname";

chmod 0644, $Filename, $Logname, $Startname; # protect the counter file & log file

print "Content-type: text/html\n\n";
print "Done!";
