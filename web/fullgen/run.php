<?php
  if (isset($_GET["n"]) && isset($_GET["p"]) && isset($_GET["s"]) && isset($_GET["c"]) && isset($_GET["fmt"])) {
    $p = filter_var($_GET["p"], FILTER_VALIDATE_BOOLEAN);
    $n = min(intval($_GET["n"]), ($p ? 120 : 80));  // hard-wired maximum number of vertices
    $ipr = "";
    if ($p) {
      $ipr = " ipr";
    }
    $s = array("C1", "C2", "Ci", "Cs", "C3", "D2", "S4", "C2v", "C2h", "D3", "S6", "C3v", "C3h", "D2h", "D2d", "D5", "D6", "D3h", "D3d", "T", "D5h", "D5d", "D6h", "D6d", "Td", "Th", "I", "Ih");
    $i = intval($_GET["s"]);
    $symm = "";
    if ($i >= 1) {
      $symm = " symm " . $s[$i-1];
    }
    $fmt = intval($_GET["fmt"]);
    // run fullgen
    $cmd = "code/plantri/fullgen"
           . " " . strval($n)
           . $ipr . $symm
           . " code " . intval(strval($_GET["c"])) . " quiet"
           . " L" . strval(limit($fmt))  /* hard-wired maximum number of graphs */
           . " 2>&1"  /* this command redirects stderr to stdout */
           . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    output($result, $fmt, 0, 0, 0, 0, 0, 0, false, false);
  }
?>