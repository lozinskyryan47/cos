<?php
  if (isset($_GET["n"]) && isset($_GET["c"]) && isset($_GET["C"]) && isset($_GET["t"]) && isset($_GET["f"]) && isset($_GET["b"]) && isset($_GET["l"]) && isset($_GET["graph"]) && isset($_GET["fmt"])) {
    $n = min(intval($_GET["n"]), 20);  // hard-wired maximum number of vertices
    $c = filter_var($_GET["c"], FILTER_VALIDATE_BOOLEAN);
    $C = filter_var($_GET["C"], FILTER_VALIDATE_BOOLEAN);
    $t = filter_var($_GET["t"], FILTER_VALIDATE_BOOLEAN);
    $f = filter_var($_GET["f"], FILTER_VALIDATE_BOOLEAN);
    $b = filter_var($_GET["b"], FILTER_VALIDATE_BOOLEAN);
    $l = filter_var($_GET["l"], FILTER_VALIDATE_BOOLEAN);
    $graph = intval($_GET["graph"]);
    $fmt = intval($_GET["fmt"]);
    $erange = "";
    if (isset($_GET["mine"]) || isset($_GET["maxe"])) {
      $from = "0";
      if (isset($_GET["mine"])) {
        $from = strval(intval($_GET["mine"]));
      }
      $to = "0";
      if (isset($_GET["maxe"])) {
        $to = strval(intval($_GET["maxe"]));
      }
      $erange = " " . $from . ":" . $to;
    }
    // run geng
    $cmd = "code/nauty/geng -"
           . ($c ? "c" : "")
           . ($C ? "C" : "")
           . ($t ? "t" : "")
           . ($f ? "f" : "")
           . ($b ? "b" : "")
           . ($l ? "l" : "")
           . "q"  /* quite output */
           . (isset($_GET["mind"]) ? "d" . strval(intval($_GET["mind"])) : "")
           . (isset($_GET["maxd"]) ? "D" . strval(intval($_GET["maxd"])) : "")
           . "G" . strval($graph)
           . "L" . strval(limit($fmt))  /* hard-wired maximum number of graphs */
           . " " . strval($n)
           . $erange
           . " 2>&1"  /* this command redirects stderr to stdout */
           . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    output($result, $fmt, 0, 0, 0, 0, 0, 0, false, false);
  }
?>