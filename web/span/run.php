<?php
  if (isset($_GET["s"]) && isset($_GET["fmt"])) {
    $fmt = intval($_GET["fmt"]);
    // run Python script to convert edge list string to Standford Graph Base file graph.gb
    $cmd = "python code/span/file.py " . escapeshellarg($_GET["s"]) . " 'code/span/graph.gb' 2>&1";  // the last command redirects stderr to stdout
    exec($cmd, $result, $return);
    if ($return == 0) {  // Python script succeeded
      // run Knuth's spanning tree Gray code
      $cmd = "code/span/span code/span/graph.gb "
             . strval(limit($fmt))  /* hard-wired maximum number of spanning trees */
             . " 2>&1"  /* this command redirects stderr to stdout */
             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
      exec($cmd, $result);
      output($result, $fmt, 0, 0, 0, 0, 0, 0, false, false);
    }
  }
?>