A <span class="def">plane graph</span> is a graph embedded in the plane such that no two edges cross, and a graph is called <span class="def">planar</span> if it can be realized as a plane graph.
If a planar graph is 3-connected, then its embedding into the plane is unique, so in this case the distinction between plane and planar graphs is irrelevant.
A <span class="def">triangulation</span> is a plane graph in which every face is a triangle.
Similarly, a <span class="def">quadrangulation</span> is a plane graph in which every face has length 4.
A <span class="def">Eulerian graph</span> is a connected graph where all vertices have even degree.
A <span class="def">triangulation of a disk</span> is a plane graph with a distinguished outer face, which can have any size, and all other faces must be triangles.
The size of the outer face can be specified in the input.
If this size is not specified, then all possible outer face sizes are generated.
An <span class="def">Apollonian network</span> is a plane triangulation that can be formed by starting with the complete graph on 4 vertices, and then repeatedly dividing a face into three by the addition of a new vertex.
The <span class="def">dual</span> of a plane graph is the graph obtained by placing a vertex into every face, and by connecting any two such vertices in faces that share an edge through this edge.
For example, consider the plane triangulation (the octahedron) and its dual quadrangulation (the cube) shown in black and red below, respectively.

<p>
<svg width="362" height="322">
<style> .l { stroke:black; stroke-width:2; fill:none; } </style>
<!--<rect x="0" y="0" width="362" height="322" fill="blue"/>-->
<line class="l" x1="80" y1="80" x2="280" y2="80"/>
<line class="l" x1="80" y1="80" x2="180" y2="252"/>
<line class="l" x1="280" y1="80" x2="180" y2="252"/>
<line class="l" x1="180" y1="80" x2="230" y2="166"/>
<line class="l" x1="180" y1="80" x2="130" y2="166"/>
<line class="l" x1="130" y1="166" x2="230" y2="166"/>
<path class="l" d="M80,80 C80,0 280,0 280,80"/>
<path class="l" d="M80,80 C10,120 110,292 180,252"/>
<path class="l" d="M280,80 C350,120 250,292 180,252"/>
<circle cx="80" cy="80" r="12" stroke="black" fill="white"/>
<circle cx="180" cy="80" r="12" stroke="black" fill="white"/>
<circle cx="280" cy="80" r="12" stroke="black" fill="white"/>
<circle cx="130" cy="166" r="12" stroke="black" fill="white"/>
<circle cx="230" cy="166" r="12" stroke="black" fill="white"/>
<circle cx="180" cy="252" r="12" stroke="black" fill="white"/>
<text x="80" y="81" alignment-baseline="middle" text-anchor="middle">1</text>
<text x="180" y="81" alignment-baseline="middle" text-anchor="middle">2</text>
<text x="280" y="81" alignment-baseline="middle" text-anchor="middle">3</text>
<text x="130" y="167" alignment-baseline="middle" text-anchor="middle">4</text>
<text x="230" y="167" alignment-baseline="middle" text-anchor="middle">5</text>
<text x="180" y="253" alignment-baseline="middle" text-anchor="middle">6</text>
<style> .d { stroke:red; stroke-width:2; fill:none; } </style>
<line class="d" x1="180" y1="46" x2="130" y2="110"/>
<line class="d" x1="180" y1="46" x2="230" y2="110"/>
<line class="d" x1="100" y1="184.5" x2="130" y2="110"/>
<line class="d" x1="100" y1="184.5" x2="180" y2="196"/>
<line class="d" x1="260" y1="184.5" x2="180" y2="196"/>
<line class="d" x1="260" y1="184.5" x2="230" y2="110"/>
<line class="d" x1="180" y1="136" x2="130" y2="110"/>
<line class="d" x1="180" y1="136" x2="230" y2="110"/>
<line class="d" x1="180" y1="136" x2="180" y2="196"/>
<line class="d" x1="180" y1="301" x2="100" y2="184.5"/>
<line class="d" x1="180" y1="301" x2="260" y2="184.5"/>
<path class="d" d="M180,301 C400,200 400,10 180,46"/>
<circle cx="130" cy="109" r="12" stroke="red" fill="lightgray"/>
<circle cx="230" cy="109" r="12" stroke="red" fill="lightgray"/>
<circle cx="180" cy="137" r="12" stroke="red" fill="lightgray"/>
<circle cx="180" cy="195" r="12" stroke="red" fill="lightgray"/>
<circle cx="180" cy="45" r="12" stroke="red" fill="lightgray"/>
<circle cx="100" cy="183.5" r="12" stroke="red" fill="lightgray"/>
<circle cx="260" cy="183.5" r="12" stroke="red" fill="lightgray"/>
<circle cx="180" cy="300" r="12" stroke="red" fill="lightgray"/>
<text x="180" y="46" alignment-baseline="middle" text-anchor="middle">1</text>
<text x="130" y="110" alignment-baseline="middle" text-anchor="middle">2</text>
<text x="230" y="110" alignment-baseline="middle" text-anchor="middle">3</text>
<text x="180" y="138" alignment-baseline="middle" text-anchor="middle">4</text>
<text x="100" y="184.5" alignment-baseline="middle" text-anchor="middle">5</text>
<text x="180" y="196" alignment-baseline="middle" text-anchor="middle">6</text>
<text x="260" y="184.5" alignment-baseline="middle" text-anchor="middle">7</text>
<text x="180" y="301" alignment-baseline="middle" text-anchor="middle">8</text>
</svg>

<p>
A plane graph can be represented uniquely by adjacency lists, where the neighbors of each vertex are listed in clockwise (cw) order around that vertex in the embedding.
The adjacency list representation of the black graph above is 1[2 4 6 3] 2[1 3 5 4] 3[1 6 5 2] 4[1 2 5 6] 5[2 3 6 4] 6[1 4 5 3], and that of its dual in red is 1[2 8 3] 2[1 4 5] 3[1 7 4] 4[2 3 6] 5[2 6 8] 6[4 7 5] 7[3 8 6] 8[1 5 7].

<p>
When deciding isomorphism between plane graphs, one can either allow turning the graph upside down (reversing the orientation of the outer face), or equivalently, reverse the orientation of all adjacency lists, or one can forbid this operation, yielding possibly more non-isomorphic graphs.
As only adjacency lists allow reconstructing the embedding, this option is only available for this output format.
