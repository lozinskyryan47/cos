<?php
  if (isset($_GET["n"]) && isset($_GET["t"]) && isset($_GET["p"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $t = intval($_GET["t"]);
    $p = intval($_GET["p"]);
    $cmd = "code/rect/rect"
                             . " -n" . min(intval($_GET["n"]), 15)  /* hard-wired maximum rectangulation size */
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of rectangulations */
                             . " -t" . $t
                             . (($p != 0) ? " -p" . $p : "")
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    $diag = ($t == 1) ? false : true;
    output($result, $fmt, "string_to_ppairs", 0, "svg_rect", array(false,0,$diag), "animate_rects", $diag, $num, $gfx);
  }
?>
