<?php
  if (isset($_GET["a"]) && isset($_GET["n"]) && isset($_GET["c"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $a = intval($_GET["a"]);
    $n = intval($_GET["n"]);
    $c = intval($_GET["c"]);
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/cperm/cperm"
                             . " " . $a
                             . " " . min($n, 20)  /* hard-wired maximum permutation length */
                             . " " . min($c, 9)  /* hard-wired maximum colors */
                             . " " . strval(limit($fmt))  /* hard-wired maximum number of permutations */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */

    // input validation
    if ($n < 1 || $n > 20) echo "the value n must be between 1 and 20";
    else if ($_GET["a"] == 1 && $c > 2) echo "for max-flip, c must be 1 or 2";
    else if ($c < 1 || $c > 9) echo "the value c must be between 1 and 9";
    else {
      exec($cmd, $result);
      $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
      $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
      output($result, $fmt, "string_to_pairs", 0, "svg_cperm", 0, 0, 0, $num, $gfx);
    }
  }
?>
