/*
 * Copyright (c) 2021 Joe Sawada, Aaron Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>

#define MAX_N 20
#define Max(a,b) (a > b) ? a : b

int MAX=0, MIN=0, NORMAL=0, SIGNED=0,COLOURED=0; 
int GEN=0, RANK=0, UNRANK=0, SUCCESSOR=0;
int n, k, type, a[MAX_N], sign[MAX_N], f[MAX_N], c[MAX_N];
long long int rank, total, powK[MAX_N], factorial[MAX_N],limit=-1;


//----------------------------------------
void Init() {
	int j;
	
	//==============
	// INITIAL PERM	
	//==============
	for (j=1; j<=n; j++) a[j] = j;
	for (j=1; j<=n; j++) sign[j] = 0;
	
	//===========
	// INIT NEXT
	//===========
	for (j=0; j<=n+1; j++) c[j] = 0;
	for (j=0; j<=n+1; j++) f[j] = j;
}

//-------------------------------------------------
// OPERATIONS ON PERMUTATIONS
//-------------------------------------------------
void RotateRight(int t, int j) {
int i, b[MAX_N];

	for (i=1; i<=t-j; i++) b[i+j] = a[i];
	for (i=1; i<=j; i++) b[i] = a[t-j+i];
	for (i=1; i<=t; i++) a[i] = b[i];

	for (i=1; i<=t-j; i++) b[i+j] = sign[i];
	for (i=1; i<=j; i++) b[i] = sign[t-j+i];
	for (i=1; i<=t; i++) sign[i] = b[i];
}
//-------------------------------------------------
void RotateLeft(int t, int j) {
int i, b[MAX_N];

	for (i=j+1; i<=t; i++) b[i-j] = a[i];
	for (i=1; i<=j; i++) b[t-j+i] = a[i];
	for (i=1; i<=t; i++) a[i] = b[i];

	for (i=j+1; i<=t; i++) b[i-j] = sign[i];
	for (i=1; i<=j; i++) b[t-j+i] = sign[i];
	for (i=1; i<=t; i++) sign[i] = b[i];
}
//-------------------------------------------------
void Flip(int t) {
int i, b[MAX_N];

	for (i=1; i<=t; i++) b[i] = a[t-i+1];
	for (i=1; i<=t; i++) a[i] = b[i];

	//============================
	// Flip Signs for colored case
	//============================
	if (k > 1) {
		for (i=1; i<=t; i++) b[i] = sign[t-i+1];
		for (i=1; i<=t; i++) sign[i] = (b[i]+1) % k;
	}
}
//-------------------------------------------------
void FlipSign(int t) {
int i;

	for (i=1; i<=t; i++) sign[i] = (sign[i]+1)%2;
}
//----------------------------------------
// UNRANKING
//----------------------------------------
void UnRankMin(long int rank, int t){
int j,x;

	if (t == 1) { a[1] = 1; return; }

	x =  (rank-1) / factorial[t-1];
	rank = rank - x * factorial[t-1];

	UnRankMin(rank, t-1);
	
	a[t] = t - x;
	for (j=1; j<t; j++)   a[j]  = 1 + (a[j] + a[t] - 1) % t;
}

//----------------------------------------
void UnRankMinColoured(long int rank, int t){
	int j,x;

	if (t == 1) { 
		a[1] = 1; 
		sign[1] = rank - 1;
		return; 
	}
	
	x = (rank-1) / (factorial[t-1] * powK[t-1]);
	rank = rank - x * factorial[t-1] * powK[t-1];
		
	// x ranges from 0 to kn-1
	a[t] = t - (x % t);
	sign[t] = (int) x/t;
		
	UnRankMinColoured(rank, t-1);
	
	for (j=1; j<t; j++)  {
		a[j] =  1 + (a[j] + a[t] - 1) % t;		
		if (a[j] <  a[t]) sign[j] = (sign[j] + sign[t]) % k;
		if (a[j] >  a[t]) sign[j] = (sign[j] + sign[t] + 1) % k;
	}
}
//----------------------------------------
void UnRankMax(long int rank, int t) {
int d;

	//===========
	// BASE CASE	
	//===========
	if (t == 2) {
		if(rank == 1) { a[1] = 1; a[2] = 2;}
		if(rank == 2) { a[1] = 2; a[2] = 1;}
		return;
	}

	a[t] = t;
	if (rank % 2 == 0) {
		UnRankMax( 2*((rank-1)/(2*t)) + 2, t-1);
		d =  2*t - (rank % (2*t));
	}
	else {
		UnRankMax( 2*((rank-1)/(2*t)) + 1, t-1);
		d =  (rank-1) % (2*t);
	}
	RotateLeft(t,d/2);
}
//----------------------------------------
void UnRankMaxSigned(long int rank, int t) {
int d;

	a[t] = t;  sign[t] = 0;
	//===========
	// BASE CASE	
	//===========
	if (t == 1) {
		if (rank == 2) sign[1] = 1;
		return;
	}

	d = (rank-1) % (4*t);
	if (d >= 2*t) {
		d = 4*t - d - 1;
		UnRankMaxSigned( 2*((rank-1)/(4*t)) + 2, t-1);
	}
	else UnRankMaxSigned( 2*((rank-1)/(4*t)) + 1, t-1);
	
	if (d % 2 == 0) {
		FlipSign(d/2);
		RotateLeft(t,d/2);
	}
	else {
		Flip(t);
		RotateRight(t,d/2);
		FlipSign(d/2);
	} 
}
//----------------------------------------
// RANKING
//----------------------------------------
long int RankMin(int t){
int j;

	if (t == 1) return 1;
	for (j=1; j<t; j++)  a[j] = (a[j] - a[t] + t) % t;
	return ((t-a[t]) * factorial[t-1] + RankMin(t-1));
}
//----------------------------------------
long int RankMinColoured(int t){
	int j;
		
	if (t == 1) return sign[1] + 1;
	
	for (j=1; j<t; j++)  {
		if (a[j] <  a[t]) sign[j] = (sign[j] - sign[t] + k) % k;
		if (a[j] >  a[t]) sign[j] = (sign[j] - sign[t] - 1 + k) % k;
		a[j] = (a[j] - a[t] + t) % t;
	}

	return (( (sign[t]+1) * t - a[t]) * factorial[t-1] * powK[t-1] + RankMinColoured(t-1));
}
//----------------------------------------
long int RankMax(int t) {
long int i,d,x,p;

	//===========
	// BASE CASES	
	//===========
	if (t == 2 && a[1] ==1) return 1;
	if (t == 2 && a[1] ==2) return 2;
	
	//=====================================
	// Determine position of largest value 
	//=====================================
	for (i=1; i<=t; i++) if (a[i] == t) p = i;

	//======================================================================
	// Rotate to either the first or last permutation in the bracelet class
	//======================================================================
	d = 2*(t-p);
	RotateRight(t,t-p);
	
	//==================================================================================
	// The parity indicates whether or not the rotate perm is first or last in the class 
	//==================================================================================
	x = RankMax(t-1); 
	if (x % 2 == 0) return (t*x - d);
	else return (t*(x-1) + d+1);
}
//----------------------------------------
long int RankMaxSigned(int t) {
long int i,d,x,p;

	//===========
	// BASE CASE	
	//===========
	if (t == 1 && sign[1] == 0) return 1;
	if (t == 1 && sign[1] == 1) return 2;
	
	//=====================================
	// Determine position of largest value 
	//=====================================
	for (i=1; i<=t; i++) if (a[i] == t) p = i;

	//===========================================================================
	// Rotate/Flip to either the first or last permutation in the bracelet class
	//===========================================================================
	d = 2*(t-p);
	RotateRight(t,t-p);
	FlipSign(t-p);	
	
	if (sign[t] == 1) {
		FlipSign(t);
		d += 2*t;
	}
		
	//==================================================================================
	// The parity indicates whether or not the rotate perm is first or last in the class 
	//==================================================================================
	x = RankMaxSigned(t-1); 
	if (x % 2 == 0) return (2*t*x - d);
	else return (2*t*(x-1) + d+1);
}
//----------------------------------------
// SUCCESSORS
//----------------------------------------
void SuccessorMin() {
int j, incr=0;
	
	for (j=1; j<n; j++) {
		if (a[j] < a[j+1]) incr++;
		if (incr == 2 || (incr == 1 && a[j+1] < a[1])) break;
	}
	Flip(j);
}
//----------------------------------------
void SuccessorMinColoured() {
int j, incr=0;
	
	for (j=1; j<n; j++) {
		if (a[j] < a[j+1]) incr++;
		if (incr == 2 || (incr == 1 && a[j+1] < a[1])) break;
		
		if (k > 1 && a[j] < a[j+1] && (sign[j+1] - sign[j] + k) % k != 1) break;
		if (k > 1 && a[j] > a[j+1] &&  sign[j+1] != sign[j]) break;
	}
	Flip(j);
}
//----------------------------------------
void SuccessorMax() {
	int j, p1, p2, p3, d=0;
	
	for (j=1; j<=n; j++) {
		if (a[j]  == 1) p1 = j; 
		if (a[j]  == 2) p2 = j; 
		if (a[j]  == 3) p3 = j; 
		if (a[j] != j) d = j;
	}
	if ((p1 < p2 && p2 < p3) || (p2 < p3 && p3 < p1) || (p3 < p1 && p1 < p2)) Flip(n);
	else Flip( Max(d-1,2) );
}
//----------------------------------------
void SuccessorMaxSigned() {
int j, p1, p2, s1, s2, d=1;
	
	for (j=1; j<=n; j++) {
		if (a[j]  == 1) { p1 = j; s1 = sign[j]; }
		if (a[j]  == 2) { p2 = j; s2 = sign[j]; }
		if (a[j] != j) d = j+1;
	}
	if ((p1 < p2 && s1 == s2) || (p2 < p1 && s1 != s2)) Flip(n);
	else Flip( Max(d-2,1) );
}
//----------------------------------------
// FLIP SEQUENCE GENERATION 
//----------------------------------------
int NextMin() {
int j;

	j = f[2];
	f[2] = 2;
	c[j]++;
	if (c[j] == j-1) {
		c[j] = 0;
		f[j] = f[j+1];
		f[j+1] = j+1;
	}
	if (j == n+1) return(0);
	return(j);
}
//----------------------------------------
int NextMinColoured() {
int j;

    if (k == 1) { j = f[2];   f[2] = 2;  }
    else        { j = f[1];   f[1] = 1;  }
    
	c[j]++;
	if (c[j] == k*j-1) {
		c[j] = 0;
		f[j] = f[j+1];
		f[j+1] = j+1;
	}
	if (j == n+1) return(0);
	return(j);
}
//----------------------------------------
int NextMax() {
int j;

	j = f[n];
	f[n] = n;
	c[j]++;
	if (c[j] == j || j == n) {
		c[j] = 0;
		f[j] = f[j-1];
		f[j-1] = j-1;
	}
	if (j == 1) return(0);
	return(j);
}
//----------------------------------------
int NextMaxSigned() {
int j;

	j = f[n];
	f[n] = n;
	c[j]++;
	if (c[j] == 2*j+1 || j == n) {
		c[j] = 0;
		f[j] = f[j-1];
		f[j-1] = j-1;
	}
	if (j == 0) return(0);
	return(j);
}
//----------------------------------------
void Print() {
	int i;
	
	for (i=1; i<=n; i++) {
    if (i > 1) printf(" | ");
		if (COLOURED) printf("%d %d", a[i], sign[i]+1);
		else {
			if (sign[i] == 0 || !SIGNED)  printf("%d 1", a[i]);
			else printf("-%d ", a[i]);
		}
	} 
	if (!RANK) printf("\n");  
	total++;
    
    if (limit >=0 && total >= limit) {
        printf("output limit reached\n");
        exit(0);
    }
}
//----------------------------------------
void Gen() {
int j;

	do {
		Print(); 

		if (MIN && NORMAL)   j= NextMin();
		if (MAX && NORMAL)   j= NextMax();
		if (MIN && SIGNED)   j= NextMinColoured();
		if (MAX && COLOURED) j= NextMaxSigned();
		if (MIN && COLOURED) j= NextMinColoured();

		Flip(j);

	} while (j>0);

	
	//printf("Total = %ld\n\n", total);
}
//==========================================================================================
void usage() {
    printf("Usage: cperm [type] [n] [k] \n");
}
//==========================================================================================
int main(int argc, char **argv) {
    int i,j;
    char s[MAX];
    
    if (argc < 3) { usage();  return 1; }
    sscanf(argv[1], "%d", &type);
    sscanf(argv[2], "%d", &n);
    sscanf(argv[3], "%d", &k);
    if (argc > 3) sscanf(argv[4], "%lld", &limit);
		

    factorial[0] = 1;
    for (j=1; j< MAX_N; j++) factorial[j] = factorial[j-1] * j;
    
    if (k == 1) NORMAL = 1;
    //if (k == 2) SIGNED = 1;
    if (k >= 2)  COLOURED = 1;
    
    if (type == 0) MIN = 1;
    if (type == 1) MAX = 1;
    
    powK[0] = 1;
    for (j=1; j< MAX_N; j++) powK[j] = powK[j-1] * k;
    
    Init();
    Gen();
}