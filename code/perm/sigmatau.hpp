/*
 * Copyright (c) 2018 Joe Sawada, Aaron Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMATAU_HPP
#define SIGMATAU_HPP

#include <vector>

class SigmaTau {
public:
  explicit SigmaTau(int n);
  void first();  // initial permutation is pi_[1...n] = (n-1)n(n-2)(n-3)...1
  void print();  // to make the output consistent with the other algorithms, we shift indices to 0...n-1 in the print function
  bool next();

private:
  void sigma();  // shift permutation to the left
  void tau();  // swap first two entries of permutation
  bool special_perm();  // returns true iff pi_[1...n] = n(n-1)...1
  
  int n_;
  unsigned int i_; // index of value n
  std::vector<int> pi_;
  long long perms_gen_;
  long long perms_total_;  // n!
};

#endif
