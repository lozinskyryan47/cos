/*
 * Copyright (c) 2018 Joe Sawada, Aaron Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include "sigmatau.hpp"

SigmaTau::SigmaTau(int n) : n_(n) {
  pi_.resize(n_+1);  // entry 0 is not used
  perms_total_ = 1;
  for (int i=2; i<=n_; i++) {  // compute n_!
    perms_total_ = perms_total_ * i;
  }  
  first();
}

void SigmaTau::first() {
  perms_gen_ = 1;
  if (n_ == 1) {
    pi_[1] = 1;
    i_ = 1;
    return;
  }
  pi_[1] = n_-1;
  pi_[2] = n_;
  i_ = 2;
  for (int i=3; i<=n_; i++) {
    pi_[i] = n_-i+1;
  }
}

void SigmaTau::print() {
  for (int i=1; i<=n_; i++) {
    std::cout << pi_[i] << " ";
  }
}

void SigmaTau::sigma() {
  int tmp = pi_[1];
  for (int i=1; i < n_; i++) {
    pi_[i] = pi_[i+1];
  }
  pi_[n_] = tmp;
}

void SigmaTau::tau() {
  int tmp = pi_[1];
  pi_[1] = pi_[2];
  pi_[2] = tmp;
}

bool SigmaTau::special_perm() {
  for (int i=1; i<=n_; i++) {
    if (pi_[i] != n_-i+1) {
      return false;
    }
  }
  return true;
}

bool SigmaTau::next() {
  if (perms_gen_ == perms_total_) {
    return false;
  }
  int r;
  if (i_ == 1) {
    r = pi_[3];
  } else if (i_ == n_) {
    r = pi_[1];
  } else {
    r = pi_[i_+1];
  }
  if (((r < n_-1 && pi_[2] == r+1) || (r == n_-1 && pi_[2] == 1)) && !special_perm()) {
    tau();
    if (i_ <= 2) i_ = 3 - i_;
  } else {
    sigma();
    if (i_-- == 1) i_ = n_;
  }
  perms_gen_++;
  return true;
}
