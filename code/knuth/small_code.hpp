/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMALL_CODE_HH
#define SMALL_CODE_HH

#include <vector>
#include <unordered_map>
#include <array>

// Class that is used as a wrapper around hardcoded flip sequences for n=1,2,3.
// This replaces usage of the class Periodic_path in those cases.
// In this class all bitstrings are of length 2*n+1 and DO NOT contain the alternating extra first bit.
class Small_code {
// static members first
public:
  static constexpr int MAX_N_PLUS_1 = 4;   // largest n that has hardcoded +1 for the zero indexing
private:
  // For every bitstring we remember its position in the Gray code.
  // This way, we can easily generate code with an arbitrary starting bitstring.
  static const std::array<std::vector<
    std::unordered_map<std::vector<bool>,int>>, MAX_N_PLUS_1> pos_map_;
  // The whole flip sequence for the given Gray code.
  // The bit at position flip_sequence_[n_][s_][i] is flipped in the i-th bitstring
  // in the code to obtain the (i+1)-st bitstring (modulo the length of the code).
  static const std::array<std::vector<std::vector<int>>, MAX_N_PLUS_1>  flip_sequence_;

// instance members
private:
  int n_;
  int s_;
  int pos_;  // position of the current bitstring in the Gray code
public:
  Small_code(int n, int s);
  // Initialize the Gray code with given starting bitstring.
  // If n >= MAX_N_PLUS_1, then the object is in an invalid state and its behavior is undefined.
  void init(const std::vector<bool> &start);

  // advance Gray code to the next bitstring and return the position of the bit flipped in this step
  inline int next() {
    int flipped = flip_sequence_[n_][s_][pos_];
    ++pos_;
    pos_ %= get_length();
    return flipped;
  }

  // return the total number of bitstrings in the Gray code
  inline int get_length() {return flip_sequence_[n_][s_].size();}
};

#endif