/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <functional>
#include <getopt.h>
#include <iostream>
#include <vector>
#include "knuth_gray_code.hpp"
#include "tests.hpp"
#include "utils.hpp"

// display help
void help() {
  std::cout << "./knuth [options]  compute star transposition Gray code à la Knuth from [Merino,Micka,Muetze]" << std::endl;
  std::cout << "-h                 display this help" << std::endl;
  std::cout << "-n{1,2,...}        list bitstrings of length 2n+2 with n+1 many 0s and 1s" << std::endl;
  std::cout << "-s{1,...,2n}       shift (must be coprime to 2n+1; default s=1)" << std::endl;
  std::cout << "-l{-1,0,1,2,...}   number of bitstrings to list; -1 for all" << std::endl;
  std::cout << "-v{0,1}^{2n+2}     initial bitstring (length 2n+2, with n+1 many 0s and 1s)" << std::endl;
  std::cout << "-p                 print the flip positions {1,...,2n+1} instead of bitstrings" << std::endl;
  std::cout << "-i                 ignore first bit (position 0) in output" << std::endl;
  std::cout << "-q                 quiet output" << std::endl;
  std::cout << "examples:  ./knuth -n2" << std::endl;
  std::cout << "           ./knuth -n2 -p" << std::endl;
  std::cout << "           ./knuth -n2 -v010101" << std::endl;
  std::cout << "           ./knuth -n2 -v010101 -i" << std::endl;
  std::cout << "           ./knuth -n10 -l50" << std::endl;
  std::cout << "           ./knuth -n12 -q" << std::endl;
}

void opt_n_missing() {
  std::cerr << "option -n is mandatory and must come before -v" << std::endl;
}

void opt_v_error() {
  std::cerr << "option -v must be followed by a bitstring of length 2n+2 with n+1 many 0s and 1s" << std::endl;
}

void opt_s_error() {
  std::cerr << "option -s must be followed by an integer from {1,...,2n} coprime to 2n+1" << std::endl;
}

void print_flips(int p, const std::vector<bool> &x){
  std::cout << p << std::endl;
}

void print_bits(int p, const std::vector<bool> &x, int start){
  for (int i = start; i < x.size(); ++i){
    std::cout << x[i];
  }
  std::cout << std::endl;
}

void print_quiet(int p, const std::vector<bool> &x){
  static int step = 0;
  if (step % 100000 == 0){
    std::cout << '.' << std::flush;
  }
  step += 1;
}

int main(int argc, char* argv[]) {
  int n;
  int s;
  bool n_set = false;  // flag whether option -n is present
  bool s_set = false;  // flag whether option -s is present
  long long steps = -1;  // compute all vertices by default
  std::vector<bool> start;  // starting vertex
  bool v_set = false;  // flag whether option -v is present
  bool print_flip_pos = false;  // print bitstrings by default
  bool ignore_first_bit = false;  // print first bit by default
  bool quiet_output = false;  // print output by default

  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hn:s:l:v:piq")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'n':
        n = atoi(optarg);
        if (n < 1) {
          std::cerr << "option -n must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        if (s_set && (s > 2*n)) {  // s may be set later than n
          opt_s_error();
          return 1;
        }
        start.resize(2*n+2,0);
        n_set = true;
        break;
      case 's':
        s = atoi(optarg);
        if ((s < 1) || (n_set && (s > 2*n))) {  // n may be set later than s
          opt_s_error();
          return 1;
        }
        s_set = true;
        break;
      case 'l':
        steps = atoi(optarg);
        if (steps < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 'v':
        {
        if (!n_set) {
          opt_n_missing();
          help();
          return 1;
        }
        char *p = optarg;
        int length = 0;
        int num_ones = 0;
        // parse bitstring
        while (*p != 0) {
          if ((*p == '0') || (*p == '1')) {
            start[length] = ((int)(*p)) - 48;  // convert ASCII character to bit
            length++;
            num_ones += ((*p) - 48);
            if (length > 2*n+2) {
              opt_v_error();
              return 1;
            }
          } else {
            opt_v_error();
            return 1;
          }
          p++;
        }
        // check length and weight of bitstring
        if ((length != 2*n+2) || (num_ones < n+1) || (num_ones > n+1)) {
          opt_v_error();
          return 1;
        }
        v_set = true;
        break;
        }
      case 'p':
        {
        print_flip_pos = true;
        break;
        }
      case 'i':
        {
        ignore_first_bit = true;
        break;
        }
      case 'q':
        {
        quiet_output = true;
        break;
        }
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!n_set) {
    opt_n_missing();
    help();
    return 1;
  }

  // default shift
  if (!s_set) {
    s = 1;
  }

  // stop the program if s and 2n+1 are not coprime
  if (gcd(2*n+1,s) != 1) {
    opt_s_error();
    return 1;
  }

  // define default starting vertex if not chosen by the user
  if (!v_set) {
    for (int i = 0; i <= n; ++i) {
      start[i] = 1;
      start[n+i] = 0;
    }
  }

  // set appropriate visit function
  std::function<void (int, const std::vector<bool> &)> visit;
  if (quiet_output){
    visit = print_quiet;
  }
  else if (print_flip_pos){
    visit = print_flips;
  }
  else if (ignore_first_bit){
    visit = [](int p,const std::vector<bool> &x){
      print_bits(p, x, 1);
    };
  }
  else{
    visit = [](int p,const std::vector<bool> &x){
      print_bits(p, x, 0);
    };
  }

  if (DEBUG) run_tests();

  auto code = Knuth_gray_code(n, s, start, steps, visit);

  return 0;
}
