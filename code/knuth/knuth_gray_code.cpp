/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <functional>
#include <iostream>
#include <tuple>
#include <vector>
#include "knuth_gray_code.hpp"
#include "switch.hpp"
#include "utils.hpp"

Knuth_gray_code::Knuth_gray_code(
  int n, int s, const std::vector<bool> &start, long long steps, std::function<void (int, const std::vector<bool> &)> visit
  ) : n_(n), s_(s), x_(start), start_(start), steps_(steps), current_step_(0), visit_(visit), path_(n_, s_), small_(n_, s_) {

  assert(start.size() == 2*n+2 && "initial bitstring must have length 2n+2");
  assert((gcd(s_, 2*n_ + 1) == 1) && "shift s must be coprime to 2n+1");

  // steps = 0 => nothing to do
  if (steps_ == 0) {
    return;
  }

  // initialize algorithms
  use_hardcoded_ = (n_ < Small_code::MAX_N_PLUS_1);
  if (!use_hardcoded_) {
    // initialize scaling factors and switches
    init_switches(catalan(n_));  // default shift without any switches
  }
  else {
    // no scaling and no switches
    s_scaled_ = s_;
    fac_ = 1;
    fac_inv_ = 1;
  }

  // compute scaled starting bitstring
  scale(fac_);
  // remove initial alternating bit
  start_.erase(start_.begin());
  std::copy(x_scaled_.begin()+1, x_scaled_.end(), back_inserter(start_scaled_));

  if (!use_hardcoded_) {
    path_.init(start_scaled_);
  }
  else {
    small_.init(start_scaled_);
  }

  // bit flipped by applying next()
  int pos, pos_scaled;

  do {
    // check whether we are to use a switch
    std::pair<bool, Switch> use_switch = get_usable_switch();  // looks at path_, so must come before path_.next()

    // apply next() function
    if (!use_hardcoded_) {
      pos_scaled = path_.next();
    }
    else {
      pos_scaled = small_.next();
    }

    // use switch, if we are supposed to
    if (use_switch.first) {
      if (DEBUG) std::cout << "switch" << std::endl;

      Switch swi = use_switch.second;
      int shift_val = swi.get_eff_shift();
      path_.rotate_right(shift_val);

      // also modify the flip position
      // (switching requires us to compare strings of length 2n+1 anyway)
      std::vector<bool> new_x_scaled = path_.get_x();
      for (int i = 0; i < 2*n+1; ++i) {
        if (x_scaled_[i+1] != new_x_scaled[i]) {
          pos_scaled = i;
          break;
        }
      }
    }

    pos_scaled += 1; // account for initial alternating bit
    // apply unscaling
    pos = (pos_scaled * fac_inv_) % (2*n+1);
    if (pos == 0) {
      pos = 2*n_+1;
    }

    if (DEBUG) {
      std::cout << current_step_ << ": ";
      print_bitstring(x_);
      std::cout << " " << pos << " ";
      print_bitstring(x_scaled_);
      std::cout << " " << pos_scaled << std::endl;
    }

    // call visit function
    visit_(pos, x_);

    // update x_ and x_scaled_ (includes toggling the first bit)
    flip(x_,0);
    flip(x_,pos);
    flip(x_scaled_,0);
    flip(x_scaled_,pos_scaled);

    current_step_ += 1;

  } while (!reached_end());
}

void Knuth_gray_code::scale(int fac) {
  assert(x_.size() == 2*n_+2 && "input bitstring must have length 2*n+2");
  x_scaled_.resize(2*n_+2,0);
  for (int i=0; i<2*n_+1; ++i) {
    x_scaled_[(fac*i % (2*n_+1))] = x_[i];
  }
  x_scaled_[2*n_+1] = x_[2*n_+1];
}

void Knuth_gray_code::init_switches(int shift) {
  s_scaled_ = shift;  // shift without switches
  if (gcd(s_scaled_, 2*n_+1) != 1) {  // only use switches if shift is not coprime to 2n+1
    if (n_>=4 && n_<=10) {
      if (gcd(s_scaled_ + 1, 2*n_+1) == 1) {
        s_scaled_ += 1;
        switches_.push_back(Switch(n_,1));
      } else {
        // our lemmas guarantee that either shift+1 or shift+2 are coprime
        s_scaled_ += 2;
        switches_.push_back(Switch(n_,2));
      }
    } else {
      assert(n_>=11 && "n>=11 is needed");
      if (is_prime_power(2*n_+1)) {
        assert(((s_scaled_ == 0) || is_prime_power(s_scaled_)) && "shift must also be a prime power");
        s_scaled_ += 1;
        switches_.push_back(Switch(n_,1));
      }
      else {
        // 2n+1 has at least two distinct prime factors
        std::vector<int> factors, mult;
        factor(2*n_+1, factors, mult);
        assert(factors.size() >= 2 && "2*n+1 must not be a prime power");
        int first_d = -1;
        int d;
        do {  // need either one or two switches
          d = 1;
          for (int i=0; i<factors.size(); ++i) {
            if ((s_scaled_ % factors[i]) == 0) continue;  // skip factors common with shift
            d *= factors[i];
          }
          if (d == 1) {
            assert((first_d == -1) && "after first modification we must have d>1");
            int p = factors[0];  // take an arbitrary factor (w.l.o.g. the smallest one)
            s_scaled_ = mod(s_scaled_-p, 2*n_+1);
            switches_.push_back(Switch(n_,p));
            first_d = p;
          }
        } while (d == 1);
        assert(d >= 3 && "d cannot be too small");
        assert(((2*n_+1) % d) == 0 && "d should divide 2n+1");
        assert(((first_d == -1) || (d != first_d)) && "two d's should be distinct");
        s_scaled_ = mod(s_scaled_-d, 2*n_+1);
        switches_.push_back(Switch(n_,d));
      }
    }
  }
  assert(gcd(s_scaled_, 2*n_+1) == 1 && "shift must now be coprime to 2n+1");
  // compute scaling factors
  int s_inv = inverse(s_, 2*n_+1);
  fac_     = (s_inv * s_scaled_) % (2*n_+1);
  fac_inv_ = inverse(fac_, 2*n_+1);
}

bool Knuth_gray_code::are_on_switch(Switch &swi) {
  bool is_x, is_y;
  int dummy;
  is_x = path_.is_rotation_of(swi.get_x(), dummy);
  // y' is a rotation of y so we cannot distinguish these two
  is_y = path_.is_rotation_of(swi.get_x_underlined(), dummy);

  if (!is_x && !is_y) return false;

  if (!path_.on_reversed_path()) {
    // forward path where f is applied
    // we are at x, so we need f(x) \in {y, y'}
    if (is_x) return (swi.get_f() == swi.get_overlined()) || (swi.get_f() == swi.get_underlined());
    // we are at y or y', so we need f(y/y') = x
    if (is_y) return (swi.get_finv() == swi.get_underlined()) || (swi.get_finv() == swi.get_overlined());
  }
  else {
    // reversed path where f^{-1} is applied
    // we are at x, so we need f^{-1}(x) \in {y, y'}
    if (is_x) return (swi.get_finv() == swi.get_overlined()) || (swi.get_finv() == swi.get_underlined());
    // we are at y or y', so we need f^{-1}(y/y') = x
    if (is_y) return (swi.get_f() == swi.get_underlined()) || (swi.get_f() == swi.get_overlined());
  }

  return false;
}

std::pair<bool, Switch> Knuth_gray_code::get_usable_switch() {
  for (Switch swi : switches_) {
    if (are_on_switch(swi)) {
      return std::make_pair(true, swi);
    }
  }
  return std::make_pair(false, Switch());
}

bool Knuth_gray_code::reached_end() {
  // check whether upper bound is reached
  if ((steps_ != -1) && (current_step_ >= steps_)) {
    std::cout << "output limit reached" << std::endl;
    return true;
  }

  if (use_hardcoded_) {
    // for the small cases, we have explicit length information
    return current_step_ >= small_.get_length();
  }
  else {
    // otherwise compare bitstrings
    return path_.get_x() == start_scaled_;
  }
}