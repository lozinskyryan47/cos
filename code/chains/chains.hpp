/*
 * Copyright (c) 2019 Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHAINS_HH
#define CHAINS_HH

#include <vector>

// A class representing a Gray code that contains the Greene-Kleitman
// symmetric chain decomposition as described in the [Gregor, Micka, Muetze]
// paper. This is an implementation of Algorithms C and C' from that
// paper which provide a loopless implementation of this Gray code.

// Type of user-defined visit function.
typedef void (*visit_f_t)(const std::vector<int> &c);

class Chains {
public:
  // If the constructor is called with full_cycle == false, then we only
  // compute the Gray code of chains, encoded over the alphabet {0,1,2}, where *==2.
  // If the constructor is called with full_cycle == true, then we compute
  // the entire Gray code of all 2^n bitstrings of length n.
  explicit Chains(int n, bool full_cycle, visit_f_t visit_f, int limit);

private:
  int n_;  // the dimension of the cube
  int m_;  // n==2*m or n==2*m+1
  bool full_cycle_;  // whether to compute the full Hamilton cycle, or only the chains
  visit_f_t visit_f_;  // the visit function
  int count_;  // number of objects (chains/bitstrings) generated
  int limit_;  // bound on the number of objects to be generated (-1 for all)
  

  // We mostly use 1-indexed arrays like in the paper, so we make all arrays 1 entry longer, not using the entry 0.
  std::vector<int> c_;  // the current chain, encoded by 0, 1, *==2
  std::vector<int> x_;  // the current bitstring
  std::vector<int> p_;  // if c_[i]==0/1, p_[i] gives position of matched 1/0 in c
  std::vector<int> s_, t_;  // if c_[i]==*, s_[i]/t_[i] gives position of closest * to the right/left of position i
  std::vector<int> l_;  // l_[i] is the length of the chain in dimension 2i (middle 2i bits of c_)
  std::vector<int> b_;  // b_[i] indicates whether we are in the first (+1) or second case (-1) of the \lambda recursion defined in the paper
  std::vector<int> o_;  // o_[i] indicates whether we are in the first (+1) or second case (-1) of the \rho definition defined in the paper
  std::vector<char> q_;  // next operation on the chain in dimension 2i, encoded by 'f', 'l', f^{-1}=='f'-1=='e', l^{-1}=='l'-1=='k', f^{-1}*l=='f'-1*'l' (only for odd n_), l^{-1}*l=='l'-1*'f' (only for odd n_)
  std::vector<int> d_;  // array to determine dimension in each step
  bool up_;  // direction in which the current chain is traversed

  // Algorithm C and Algorithm C' from the paper
  void chains_even();
  void chains_odd();

  // either visits the current chain, or all bitstrings on that chain (depending on the value of full_cycle)
  bool visit();  // return true if output limit is reached
  // auxiliary functions the perform the operations f, f^{-1}, l, l^{-1}
  void first(int i);
  void first_inv(int i);
  void last(int i);
  void last_inv(int i);
};

#endif
