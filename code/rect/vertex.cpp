/*
 * Copyright (c) 2021 Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vertex.hpp"

Vertex::Vertex()
{
  type_ = Type::none;
}

void Vertex::init(int north, int east, int south, int west)
{
  north_ = north;
  east_ = east;
  south_ = south;
  west_ = west;
  int zeros = int(north_ == 0)+int(south_ == 0)+int(west_ == 0)+int(east_ == 0);
  if (zeros >= 3 or zeros <= 0)
    type_ = Type::none;
  else if (zeros == 2)
    type_ = Type::corner;
  else if (south_ == 0)
    type_ = Type::top;
  else if (north_ == 0)
    type_ = Type::bottom;
  else if (east_ == 0)
    type_ = Type::left;
  else if (west_ == 0)
    type_ = Type::right;
}