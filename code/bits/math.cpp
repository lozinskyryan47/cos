/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include "math.hpp"

long long binom(int n, int k) {
  long long r = 1;
  if (k > n/2) {
    k = n - k;  // binom(n,k)=binom(n,n-k)
  }
  for (int i = n; i > n - k; --i) {
    r = (r * i) / (n + 1 - i);
  }
  return r;
}

long long num_vertices_Qnkl(int n, int k, int l) {
  assert(k <= l);
  long long b = binom(n, k);
  long long r = b;
  // add up level sizes
  for (int i = k + 1; i <= l; ++i) {
    b = b * (n - i + 1) / i;
    r += b;
  }
  return r;
}

long long delta_Qnkl(int n, int k, int l) {
  long long r;
  // see the explicit formulas in the paper
  if ((l - k) % 2 == 0) {
    r = (k > 0 ? binom(n - 1, k - 1) : 0) + (l < n ? binom(n - 1, l) : 0);
  } else {  // l - k % 2 == 1
    r = (k > 0 ? binom(n - 1, k - 1) : 0) - (l < n ? binom(n - 1, l) : 0);
    if (r < 0) {
      r = -r;  // take the absolute value
    }
  }
  return r;
}
