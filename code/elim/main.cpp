/*
 * Copyright (c) 2021 Jean Cardinal, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <cctype>
#include <getopt.h>
#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include "tests_chordal.hpp"
#include "tests_tree.hpp"
#include "tree.hpp"
#include "chordal.hpp"

// display help
void help() {
  std::cout << "./elim [options]     compute elimination forests of a chordal graph in rotation Gray code order" << std::endl;
  std::cout << "-h                   display this help" << std::endl;
  std::cout << "-e'u1,v1;u2,v2;...'  edge list of the graph: vertices and edges separated by comma and semicolon, resp." << std::endl;
  std::cout << "-l{-1,0,1,2,...}     number of elimination forests to generate; -1 for all" << std::endl;
  std::cout << "-q                   quiet output" << std::endl;
  std::cout << "-c                   output number of elimination forests" << std::endl;
  std::cout << "examples:  ./elim -e'1,2;2,3;3,4;4,5' -c" << std::endl;
  std::cout << "           ./elim -e'1,2;1,3;1,4;1,5' -c" << std::endl;
  std::cout << "           ./elim -e'1,2;1,3;1,4;2,3;2,4;3,4' -c" << std::endl;
  std::cout << "           ./elim -e'1,2;2,3;3,4;4,5;5,6;6,7;7,8;8,9;9,10' -l100" << std::endl;
  std::cout << "           ./elim -e'1,2;2,3;3,4;4,5;5,6;6,7;7,8;8,9;9,10' -q -c" << std::endl;
}

// read in the edge list from the command line and translate vertex names to integers 1,...,n
// whitespace is ignored
bool read_edge_list(int &n, char* string, std::vector<std::string> &vertex_strings, std::vector<std::pair<int,int>> &edge_list) {
  std::unordered_map<std::string,int> str_vtx_map;
  int i = 0;
  n = 0;
  int from, to;
  bool second_vertex = false;
  vertex_strings.push_back("0");
  do {
    std::string vtx_str;
    while ((string[i] != ',') && (string[i] != ';') && (string[i] != 0)) {
      // skip whitespace and quotation marks
      if (!isspace(string[i])) {
        vtx_str.push_back(string[i]);
      }
      i++;
    }
    if (vtx_str.length() == 0) {
      if ((string[i] == 0) && !second_vertex) {
        break;
      } else {
        std::cerr << "vertex names cannot be empty strings" << std::endl;
        return false;
      }
    }
    auto found = str_vtx_map.find(vtx_str);
    int vtx_id;
    if (found == str_vtx_map.end()) {
      n++;
      str_vtx_map.emplace(vtx_str,n);
      vertex_strings.push_back(vtx_str);
      vtx_id = n;
    } else {
      vtx_id = found->second;
    }
    if (((string[i] == ',') && second_vertex) || (((string[i] == ';') || (string[i] == 0) || (string[i+1] == 0)) && !second_vertex)) {
      std::cerr << "exactly two vertices must be specified per edge" << std::endl;
      return false;
    }
    if (string[i] == ',') {
      from = vtx_id;
      second_vertex = true;
    } else {
      assert((string[i] == ';') || (string[i] == 0));
      to = vtx_id;
      edge_list.push_back(std::pair<int,int>(from, to));
      second_vertex = false;
    }
    if (string[i] != 0) {
      i++;
    }
  } while (string[i] != 0);
  return true;
}

int main(int argc, char* argv[]) {
  int n;
  bool e_set = false;  // flag whether option -e is present
  long long steps = -1;  // compute all elimination forests by default
  bool quiet = false;  // print output by default
  int c;
  bool output_counts = false; // omit counts by default
  int quiet_dot = 10000000;  // print one dot every 10^7 elimination forests in quiet output mode

  std::vector<std::string> vertex_strings;
  std::vector<std::pair<int,int>> edge_list;
  while ((c = getopt (argc, argv, "he:l:qc")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'e':
        {
        bool success = read_edge_list(n, optarg, vertex_strings, edge_list);
        if (!success) {
          return 1;
        }
        e_set = true;
        break;
        }
      case 'l':
        steps = atoi(optarg);
        if (steps < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 'q':
        quiet = true;
        break;
      case 'c':
        output_counts = true;
        break;
    }
  }
  if (!e_set) {
    std::cerr << "option -e is mandatory" << std::endl;
    help();
    return 1;
  }

  if (DEBUG) {
    run_tests_tree();
    run_tests_chordal();
  }

  int num_forests = 0;
  bool is_tree, is_chordal;
  Elimination_tree tree(n, vertex_strings, edge_list, is_tree);
  Elimination_forest forest(n, vertex_strings, edge_list, is_chordal);
  if (!is_chordal) {
    std::cerr << "graph is not chordal" << std::endl;
    return 1;
  }
  if (is_tree) {
    tree.print_peo();
  } else {
    forest.print_peo();
  }

  if (steps == 0) {
    std::cout << "output limit reached" << std::endl;
    return 0;
  }

  bool next;
  do {
    num_forests++;
    if (!quiet) {
      if (is_tree) {
        tree.print();
      } else {
        forest.print();
      }
    } else if (num_forests % quiet_dot == 0) {
      std::cout << "." << std::flush;
    }

    if (is_tree) {
      next = tree.next();
    } else {
      next = forest.next();
    }
    if (next && (steps >= 0) && (num_forests >= steps)) {
      std::cout << "output limit reached" << std::endl;
      break;
    }
  } while (next);
  if (output_counts)
  {
    if (quiet && num_forests >= quiet_dot)
      std::cout << std::endl;
    std::cout << "number of elimination forests: " << num_forests << std::endl;
  }

  return 0;
}