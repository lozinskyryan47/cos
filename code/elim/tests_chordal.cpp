/*
 * Copyright (c) 2021 Jean Cardinal, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <vector>
#include <tuple>
#include <utility>
#include <iostream>
#include "tests_chordal.hpp"
#include "chordal.hpp"

struct Tests{
  static inline void expect(bool condition, const std::string &message) {
    if (!condition) {
      std::cout << "FAILED TEST" << std::endl;
      std::cout << message << std::endl;
      exit(-1);
    }
  }
    
  static void check_chordal(int n, std::vector<std::pair<int,int>> edge_list, std::vector<int> answer_peo, std::vector<int> answer_oep, bool answer_success) {
    std::vector<std::string> strings = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};
    bool success;
    Elimination_forest f = Elimination_forest(n, strings, edge_list, success);
    /*for (int i=1; i <= f.n_; ++i) {
       std::cout << f.peo_[i]; 
    }*/
    for (int i=1; i <= f.n_; ++i) {
      expect((answer_peo[i] == f.peo_[i]) && (answer_oep[i] == f.oep_[i]), "wrong PEO ordering"); 
    }
    expect(answer_success == success, "wrong chordality checking");
  }
  
  static void test_chordal() {
    std::vector<std::tuple<int, std::vector<std::pair<int,int>>, std::vector<int>, std::vector<int>, bool>> tests = {
      {5,{{1,2},{1,3},{1,4},{1,5}},{0,1,2,3,4,5},{0,1,2,3,4,5},true},
      {7,{{2,1},{4,1},{3,2},{6,5},{5,2},{4,7}},{0,1,2,4,3,5,7,6},{0,1,2,4,3,5,7,6},true},
      {7,{{6,5},{3,5},{1,5},{5,2},{2,4},{2,7}},{0,1,5,3,6,2,4,7},{0,1,5,3,6,2,4,7},true},
      {8,{{2,3},{2,1},{3,1},{1,8},{1,6},{1,7},{7,8},{7,6},{6,8},{4,6},{4,5}},{0,1,2,3,8,7,6,4,5},{0,1,2,3,7,8,6,5,4},true},
      {8,{{2,3},{2,1},{3,1},{1,8},{1,6},{1,7},{7,6},{6,8},{4,6},{4,5}},{0,1,2,3,8,6,7,4,5},{0,1,2,3,7,8,5,6,4},true},
      {8,{{2,3},{2,1},{3,1},{1,8},{1,7},{7,6},{6,8},{4,6},{4,5}},{0,1,2,3,8,7,6,4,5},{0,1,2,3,7,8,6,5,4},false},
      {6,{{1,2},{2,3},{3,4},{4,5},{5,6},{1,6}},{0,1,2,6,3,5,4},{0,1,2,4,6,5,3},false},
      {11,{{1,2},{1,7},{1,9},{2,9},{3,10},{3,8},{3,4},{8,10},{8,4},{4,10},{4,6},{6,10},{11,6},{11,5},{11,4},{5,6},{5,4}},{0,1,2,9,7,5,6,4,11,10,3,8},{0,1,2,10,7,5,6,4,11,3,9,8},true},
      {8,{{1,2},{1,7},{1,8},{2,7},{2,8},{7,8},{3,4},{3,6},{4,5},{5,6}},{0,1,2,8,7,5,6,4,3},{0,1,2,8,7,5,6,4,3},false},
      {5,{{3,4}},{0,1,2,3,4,5},{0,1,2,3,4,5},true},
      {5,{{1,2},{4,5}},{0,1,2,3,4,5},{0,1,2,3,4,5},true},
    };
    std::cout << "testing lex BFS and chordal graph checking" << std::endl;
    for (auto test_case : tests) {
      check_chordal(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case), std::get<4>(test_case));
    }
    std::cout << "OK" << std::endl;
  }
  
  static void check_elim(int n, std::vector<std::pair<int,int>> edge_list, std::vector<int> answer_parent, std::vector<int> answer_first_child, std::vector<int> answer_last_child, std::vector<int> answer_left_sibling, std::vector<int> answer_right_sibling) {
    std::vector<std::string> strings = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};
    bool success;
    Elimination_forest f = Elimination_forest(n, strings, edge_list, success);
    for (int i = 1; i <= f.n_; ++i) {
      expect(
        (answer_parent[i] == f.parent_[i]) && (answer_first_child[i] == f.first_child_[i]) && (answer_last_child[i] == f.last_child_[i])
        && (answer_left_sibling[i] == f.left_sibling_[i]) && (answer_right_sibling[i] == f.right_sibling_[i]),
        "wrong elimination tree"
      );
    }
  }

  static void test_elim() {
    std::vector<std::tuple<int, std::vector<std::pair<int,int>>, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int>>> tests = {
      {5,{{1,2},{1,3},{1,4},{1,5}},{0,0,1,1,1,1},{0,2,0,0,0,0},{0,5,0,0,0,0},{0,0,0,2,3,4},{0,0,3,4,5,0}},
      {7,{{2,1},{4,1},{3,2},{6,5},{5,2},{4,7}},{0,0,1,1,2,2,3,5},{0,2,4,6,0,7,0,0},{0,3,5,6,0,7,0,0},{0,0,0,2,0,4,0,0},{0,0,3,0,5,0,0,0}},
      {7,{{6,5},{3,5},{1,5},{5,2},{2,4},{2,7}},{0,0,1,2,2,2,5,5},{0,2,3,0,0,6,0,0},{0,2,5,0,0,7,0,0},{0,0,0,0,3,4,0,6},{0,0,0,4,5,0,7,0}},
      {8,{{2,3},{2,1},{3,1},{1,8},{1,6},{1,7},{7,8},{7,6},{6,8},{4,6},{4,5}},{0,0,1,2,1,4,5,6,7},{0,2,3,0,5,6,7,8,0},{0,4,3,0,5,6,7,8,0},{0,0,0,0,2,0,0,0,0},{0,0,4,0,0,0,0,0,0}},
      {5,{{3,4}},{0,0,0,0,3,0},{0,0,0,4,0,0},{0,0,0,4,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0}},
      {5,{{1,2},{4,5}},{0,0,1,0,0,4},{0,2,0,0,5,0},{0,2,0,0,5,0},{0,0,0,0,0,0},{0,0,0,0,0,0}},
    };
    std::cout << "testing elimination forest initialization" << std::endl;
    for (auto test_case : tests) {
      check_elim(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case), std::get<4>(test_case), std::get<5>(test_case), std::get<6>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

};

void run_tests_chordal() {
  std::cout << "running chordal graph algorithm tests" << std::endl;
  Tests::test_chordal();
  Tests::test_elim();
  std::cout << "testing finished OK" << std::endl;
}