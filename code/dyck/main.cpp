/*
 * Copyright (c) 2016 Torsten Muetze, Christoph Standke, Veit Wiechert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <unistd.h>
#include <vector>
#include "dyck.hpp"

// display help
void help() {
  std::cout << "./dyck [options]  compute min-change bijection f from [Muetze,Standke,Wiechert]" << std::endl;
  std::cout << "-h                display this help" << std::endl;
  std::cout << "-k{1,2,...}       apply f to all Dyck paths of length 2k with zero flaws" << std::endl;
  std::cout << "-l{-1,0,1,2,...}  apply f this many times (-1 until maximum flaw value)" << std::endl;
  std::cout << "-x{0,1}^{2k}      apply f only to the given Dyck path (1=upstep, 0=downstep)" << std::endl;
  std::cout << "-p{0,1}           print flip positions instead of Dyck paths (0=no, 1=yes)" << std::endl;
  std::cout << "examples:  ./dyck -k3" << std::endl;
  std::cout << "           ./dyck -k3 -p1" << std::endl;
  std::cout << "           ./dyck -k4 -x11110000" << std::endl;
  std::cout << "           ./dyck -k4 -x00111100 -l1" << std::endl;
}

void opt_k_missing() {
  std::cerr << "option -k is mandatory" << std::endl;
}

void opt_x_error() {
  std::cerr << "option -x must be followed by a Dyck path of length 2k (1=upstep, 0=downstep; same number of 0- and 1-bits)" << std::endl;
}

// print a vector of integers without separation characters
inline std::ostream& operator<<(std::ostream& os, const std::vector<int>& x) {
  for (int i = 0; i < x.size(); ++i) {
    os << x[i];
  }
  return os;
}

// generate step vectors (1=upstep, 0=downstep) of all Dyck paths
// with zero flaws of the given length (must be even)
void dyck_paths(int length, std::vector<std::vector<int> > &paths) {
  assert(length % 2 == 0);

  std::deque<std::vector<std::vector<int> > > all_paths;
  // one Dyck path of length 0 ending at height 0
  all_paths.push_back(std::vector<std::vector<int> > (1, std::vector<int> (0)));
  // no Dyck paths of length 0 ending at height 2
  all_paths.push_back(std::vector<std::vector<int> > (0));
  // We include this artificial empty set to avoid special treatment
  // of the boundary cases in the inductive construction.

  // construct all Dyck paths with length ell in this round
  for (int ell = 1; ell <= length; ++ell) {
    // construct all Dyck paths with length ell ending at height 2*i + (ell % 2)
    for (int i = 0; i < std::min((int)(all_paths.size() - 1), length - ell + 1); ++i) {
      std::vector<std::vector<int> > new_paths;
      // append upstep to all these shorter paths
      for (int j = 0; j < all_paths[i].size(); ++j) {
        std::vector<int> x = all_paths[i][j];
        x.push_back(1);
        new_paths.push_back(x);
      }
      // append downstep to all these shorter paths
      for (int j = 0; j < all_paths[i+1].size(); ++j) {
        std::vector<int> x = all_paths[i+1][j];
        x.push_back(0);
        new_paths.push_back(x);
      }
      all_paths[i] = new_paths;  // this entry is not needed anymore, so overwrite it
    }
    if (ell % 2 == 1) {
      // no Dyck paths of length ell ending at height -1
      all_paths.push_front(std::vector<std::vector<int> > (0));
      // We include this artificial empty set to avoid special treatment
      // of the boundary cases in the inductive construction.
    }
  }

  paths = all_paths[0];
}

int main(int argc, char *argv[]) {
  int k;
  bool k_set = false;  // flag whether option -k is present
  int limit = -1;        // compute until maximum flaw value by default
  std::vector<int> x;  // user-specified Dyck path
  bool x_set = false;  // flag whether option -x is present
  bool print_flip_pos = false;

  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hk:l:x:p:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'k':
        k = atoi(optarg);
        if (k < 1) {
          std::cerr << "option -k must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        k_set = true;
        break;
      case 'l':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 'x':
        {
        if (!k_set) {
          opt_k_missing();
          help();
          return 1;
        }
        x.resize(2*k, 0);
        char *p = optarg;
        int length = 0;
        int num_usteps = 0;
        // parse Dyck path
        while (*p != 0) {
          if ((*p == '0') || (*p == '1')) {
            if (length >= 2*k) {
              opt_x_error();
              return 1;
            }
            x[length] = ((int)(*p)) - 48;  // convert ASCII character to bit
            length++;
            num_usteps += ((*p) - 48);
          } else {
            opt_x_error();
            return 1;
          }
          p++;
        }
        // check length and whether Dyck path has the same number of upsteps and downsteps
        if ((length != 2*k) || (num_usteps != k)) {
          opt_x_error();
          return 1;
        }
        x_set = true;
        break;
        }
      case 'p':
        {
        int arg = atoi(optarg);
        if ((arg < 0) || (arg > 1)) {
          std::cerr << "option -p must be followed by 0 or 1" << std::endl;
          return 1;
        }
        print_flip_pos = (bool) arg;
        break;
        }
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!k_set) {
    opt_k_missing();
    help();
    return 1;
  }

  // the step vectors of all Dyck paths that f will be applied to
  std::vector<std::vector<int> > paths;
  if (x_set) {
    // apply f only to the single specified Dyck path
    paths.push_back(x);
  } else {
    // apply f to all Dyck paths of length 2k with zero flaws
    dyck_paths(2*k, paths);
  }

  for (int i = 0; i < paths.size(); ++i) {
    // generate Dyck path object
    DyckPath x(paths[i]);
    int flaws = x.get_num_flaws();
    std::cout << flaws << " | " << x.get_steps() << std::endl;  // output original Dyck path
    if (print_flip_pos) {
      // option -l is ignored in this case
      const std::vector<int>& xb = x.get_flip_pos();
      for (int j = 0; j < xb.size(); j+=2) {
        int f;
        if (j/2 < flaws) {
          f = j/2;
        } else {
          f = j/2+1;
        }
        std::cout << f << " | " << xb[j] << "," << xb[j+1] << std::endl;
      }
    } else {
      int emax = paths[i].size()/2;  // set maximum flaw value
      if (limit != -1) {
        // stop applying f already earlier if specified by the user
        emax = std::min(emax, x.get_num_flaws() + limit);
      }
      for (int e = x.get_num_flaws(); e < emax; e++) {
        assert(e == x.get_num_flaws());
        x.apply_f();
        std::cout << x.get_num_flaws() << " | " << x.get_steps() << std::endl;  // output Dyck path after applying f
      }
    }
  }

  return 0;
}
